import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { PipesModule } from '../pipes/pipes.module';
import { ErrorConnectionComponent } from '../components/error-connection/error-connection.component';

@NgModule({
    declarations: [ErrorConnectionComponent],
    imports: [
		PipesModule,
		IonicModule
	],
    exports: [ErrorConnectionComponent]
})

export class SharedModule {
}
