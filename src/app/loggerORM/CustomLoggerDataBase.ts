import { Logger, QueryRunner } from "typeorm";
import { LogService } from "../services/log.service";
import { UserProvider } from "../services/user.service";

export class CustomLoggerDataBase implements Logger {

    constructor(private logService: LogService, private userProvider: UserProvider) { }

    logQueryError(error: string | Error, query: string, parameters?: any[], queryRunner?: QueryRunner): void {
        this.logService.logDevelopment(JSON.stringify(error) + "Query: " + query, this.userProvider.getUser());
    }

    logQuerySlow(time: number, query: string, parameters?: any[], queryRunner?: QueryRunner): void {
        if(!query.startsWith('INSERT INTO "logEntry"')){
            console.log(time + " "+query);
            this.logService.logSqlTime("Query: " + query, this.userProvider.getUser(),time);
        }
    }

    logQuery(query: string, parameters?: any[], queryRunner?: QueryRunner): void { }

    logSchemaBuild(message: string, queryRunner?: QueryRunner): void { }

    logMigration(message: string, queryRunner?: QueryRunner): void { }

    log(level: 'log' | 'info' | 'warn', message: any, queryRunner?: QueryRunner): void { }
}