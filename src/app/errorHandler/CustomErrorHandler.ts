import { ErrorHandler, Injectable } from "@angular/core";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { DbProvider } from "../services/db.service";
import { LogService } from "../services/log.service";
import { UserProvider } from "../services/user.service";

@Injectable()
export class CustomErrorHandler implements ErrorHandler {

    lsFileErrorMsg = ["NOT_FOUND_ERR", 
    "SECURITY_ERR", 
    "ABORT_ERR", 
    "NOT_READABLE_ERR", 
    "ENCODING_ERR", 
    "NO_MODIFICATION_ALLOWED_ERR", 
    "INVALID_STATE_ERR",
    "SYNTAX_ERR",
    "INVALID_MODIFICATION_ERR",
    "QUOTA_EXCEEDED_ERR",
    "TYPE_MISMATCH_ERR",
    "PATH_EXISTS_ERR"
    ]; 

    constructor(private dbProvider: DbProvider,
        private userProvider: UserProvider,
        private logService: LogService,
        private splashScreen: SplashScreen) { }

    public handleError(error): void {
        let user = this.userProvider.getUser();
        let checkMsg =  (error instanceof Error) ? error.message : JSON.stringify(error);
        if (this.splashScreen.show){
            this.splashScreen.hide();
        }
        if(this.dbProvider.getConnection() != null && user != null){
            if(this.checkFileErrorMsg(checkMsg)){
                this.logService.logDevelopment(error, user)
            }else{
                this.logService.logCritic(error, user).then( response => {
                    console.log(response);
                    this.logService.createLogFile(user);
                    this.logService.checkDBForErrors(user)
                });
            }
        }
    }

    /**
     * Checks if it is a file plugin error.
     * @param message 
     * @returns 
     */
    private checkFileErrorMsg(message: string): boolean {
        for(let error of this.lsFileErrorMsg){
            if(message.includes(error)){
                return true;
            };
        }
        return false;
    }
}