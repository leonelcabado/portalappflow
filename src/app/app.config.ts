// Application static configuration

export const APP = {
    DB_NAME: "deyel_r10",
    PATH_DB_FILE: "file:///data/data/com.optaris.deyel/databases/",
    PATH_SEND: "DeyelChat/send/",
    PATH_RECEIVED: "DeyelChat/received/",
    PATH_FORM_DOWNLOAD: "DeyelChat/form_download/",
    API_BASE_NAMESPACE: "/rest/tedis/V1",
    RTM_BASE_NAMESPACE: "/rtm/tedis/V1",
    DEFAULT_GROUP_IMAGE: "assets/imgs/users/default_group.png",
    DEFAULT_LOAD_ITEMS_COUNT: 20,
    STATUSBAR_COLOR: "#FFFFFF",
    STATUSBAR_COLOR_RED: "#b11b21",
    STATUSBAR_COLOR_GREEN: "#7ca32c",
    MAX_USED_EMOJIS: 30
};
