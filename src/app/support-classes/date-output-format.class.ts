export class DateOutputFormat {

    static isSameDay(ts_1: number, ts_2: number): boolean {
        let d_1 = new Date(ts_1);
        let d_2 = new Date(ts_2);
        return (d_1.getDate() === d_2.getDate() && d_1.getMonth() === d_2.getMonth() && d_1.getFullYear() === d_2.getFullYear());
    }

    static getRelativeDtCreatedOutputFormat(ts: number, param: string): string {
        let sameDayPattern = "\'TODAY\'";
        if (param && param == "same-day-exact") {
            sameDayPattern = "H:mm";
        }
        let oneDay = 24 * 60 * 60 * 1000; // in microseconds
        let d = new Date(ts);
        let today = new Date();
        if (d.getFullYear() === today.getFullYear()) {
            if (this.isSameDay(today.getTime(), d.getTime())) {
                return sameDayPattern;
            }

            let yesterday = new Date(today.getTime() - oneDay);
            if (this.isSameDay(yesterday.getTime(), d.getTime())) {
                return "\'YESTERDAY\'";
            }

            let lastSeventhDay = new Date(today.getTime() - oneDay * 7);
            if (d.getTime() > lastSeventhDay.getTime()) {
                return "EEEE";
            }

            return "d/M";

        } else {
            return "d/M/yy";
        }
    }
}
