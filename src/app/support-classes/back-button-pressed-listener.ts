export interface BackButtonPressedListener {
    backButtonPressed(): void;
}