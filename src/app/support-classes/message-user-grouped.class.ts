import {Message} from '../model/message.entity';

export class MessageUserGouped {
    user: string;
    messages?: Message[] = [];

    constructor(u: string) {
        this.user = u;
        this.messages = [];
    }

    addMessage(m: Message) {
        this.messages.push(m);
    }
}
