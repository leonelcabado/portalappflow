const stateDescriptions: string[] = ["DISCONNECTED", "CONNECTED", "ABSENT", "NOT_AVAILABLE"];
const stateClasses: string[] = ["not-active", "active", "away", "busy"];

export class StateOutput {
    static getStateClass(n: number): string {
        return stateClasses[n];
    };

    static getStateDescription(n: number) {
        return stateDescriptions[n];
    };
}
