import {MessageUserGouped} from './message-user-grouped.class';
import {Message} from '../model/message.entity';
import {Conversation} from '../model/conversation.entity';
import {DateOutputFormat} from './date-output-format.class';

export class MessageDateGrouped {
    date: Date;
    userGroups?: MessageUserGouped[] = [];

    constructor(d: Date) {
        this.date = d;
        this.userGroups = [];
    }

    static build(c: Conversation): MessageDateGrouped[] {
        if (c.lsMessages.length === 0) return [];

        let xResult: MessageDateGrouped[] = [];
        let messageDateGrouped_1 = new MessageDateGrouped(new Date(c.lsMessages[0].dtLastUpdate));
        xResult.push(messageDateGrouped_1);
        let index = 0;
        for (let m of c.lsMessages) {
            let m_date: Date = new Date(m.dtLastUpdate);
            if (DateOutputFormat.isSameDay(m.dtLastUpdate, xResult[index].date.getTime())) {
                xResult[index].addMessage(m);
            } else {
                let aux = new MessageDateGrouped(m_date);
                aux.addMessage(m);
                xResult.push(aux);
                index++;
            }
        }
        return xResult;
    }

    addMessage(m: Message): void {
        if (this.userGroups.length > 0) {
            let last = this.userGroups[this.userGroups.length - 1];
            if (last.user === m.cdParticipant) {
                last.addMessage(m);
            } else {
                this.newMessageUserGrouped(m);
            }
        } else {
            this.newMessageUserGrouped(m);
        }
    }

    private newMessageUserGrouped(m: Message) {
        let u = new MessageUserGouped(m.cdParticipant);
        u.addMessage(m);
        this.userGroups.push(u);
    }
}
