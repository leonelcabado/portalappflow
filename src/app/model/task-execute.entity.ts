import { Serializable } from "./serializable.entity";

export class TaskExecute extends Serializable{
    href: string;
    rel: string

    constructor(href: string, rel: string){
        super();
        this.href = href;
        this.rel = rel;
    }
    
    /**
     * It sets a request url.
     * @param href 
     */
    public setHref(href: string): void {
        this.href = href;
    }

    /**
     * It returns the request url.
     * @returns 
     */
    public getHref(): string {
        return this.href;
    }

    /**
     * It sets a request method.
     * @param rel 
     */
    public setRel(rel: string): void {
        this.rel = rel;
    }

    /**
     * It returns the request method.
     * @returns 
     */
    public getRel(): string {
        return this.rel;
    }
}