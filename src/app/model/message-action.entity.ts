import {Entity, Column, PrimaryGeneratedColumn, ManyToOne} from 'typeorm';

import {Message} from './message.entity';
import {MessageInput} from './message-input.entity';


@Entity("message_action")
export class MessageAction {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        length: '255'
    })
    name: string;

    @Column("text")
    value: string;

    @Column({
        length: '100'
    })
    type: string;

    @Column("text")
    text: string;

    @ManyToOne(type => Message, message => message.lsActions)
    message: Message;
}

export class MessageActionRequest {
    lsInputs: MessageInput[] = [];
    cdAction: string;
    cdConversationElement: number;
    cdConversation: number;
    cdUser: string
}

export class MessageActionResponse {
    status: string;
    error?: string;
}
