import { HeaderConfiguration } from './header-configuration.entity';
import { ButtonConfiguration } from './button-configuration.entity';
import { InputConfiguration } from './input-configuration.entity';
import { StatusBarConfiguration } from './status-bar-configuration.entity';

export class PageConfiguration {

    private initialPage: boolean;
    private headerConfiguration: HeaderConfiguration;
    private buttonsConfigurations: Array<ButtonConfiguration>;
    private inputsConfigurations: Array<InputConfiguration>;
	private statusBarConfiguration: StatusBarConfiguration;
    private topBackgroundColor: string;
    private bottomBackgroundColor: string;
    private backgroundImage: boolean;

    constructor(headerConfiguration?: HeaderConfiguration, initialPage?: boolean, buttonsConfigurations?: Array<ButtonConfiguration>, inputsConfigurations?: Array<InputConfiguration>, statusBarConfiguration?: StatusBarConfiguration, topBackgroundColor?: string, bottomBackgroundColor?: string, backgroundImage? : boolean) {
        this.setHeaderConfiguration(headerConfiguration);
        this.setInitialPage(initialPage);
        this.setButtonsConfigurations(buttonsConfigurations);
        this.setInputsConfigurations(inputsConfigurations);
        this.setStatusBarConfiguration(statusBarConfiguration);
        this.setTopBackgroundColor(topBackgroundColor);
        this.setBottomBackgroundColor(bottomBackgroundColor);
        this.setBackgroundImage(backgroundImage);
    }

    /**
     * It sets if the page is initial.
     * @param {boolean} initialPage
     */
    public setInitialPage(initialPage: boolean): void {
        this.initialPage = initialPage;
    }

    /**
     * It returns if the page is initial.
     * @return {boolean}
     */
    public getInitialPage(): boolean {
        return this.initialPage;
    }

    /**
     * It sets the header configuration.
     * @param {HeaderConfiguration} headerConfiguration
     */
    public setHeaderConfiguration(headerConfiguration: HeaderConfiguration): void {
        this.headerConfiguration = headerConfiguration;
    }

    /**
     * It returns the header configuration.
     * @return {HeaderConfiguration}
     */
    public getHeaderConfiguration(): HeaderConfiguration {
        return this.headerConfiguration;
    }

    /**
     * It sets the buttons configurations.
     * @param {Array<ButtonConfiguration>} buttonsConfigurations
     */
    public setButtonsConfigurations(buttonsConfigurations: Array<ButtonConfiguration>): void {
        this.buttonsConfigurations = buttonsConfigurations;
    }

    /**
     * It returns the buttons configurations.
     * @return {Array<ButtonConfiguration>}
     */
    public getButtonsConfigurations(): Array<ButtonConfiguration> {
        return this.buttonsConfigurations;
    }

    /**
     * It sets the inputs configurations.
     * @param {Array<InputConfiguration>} inputsConfigurations
     */
    public setInputsConfigurations(inputsConfigurations: Array<InputConfiguration>): void {
        this.inputsConfigurations = inputsConfigurations;
    }

    /**
     * It returns the inputs configurations.
     * @return {Array<InputConfiguration>}
     */
    public getInputsConfigurations(): Array<InputConfiguration> {
        return this.inputsConfigurations;
    }

    /**
     * It sets the status bar configuration.
     * @param {StatusBarConfiguration} statusBarConfiguration
     */
    public setStatusBarConfiguration(statusBarConfiguration: StatusBarConfiguration): void {
        this.statusBarConfiguration = statusBarConfiguration;
    }

    /**
     * It returns the status bar configuration.
     * @return {StatusBarConfiguration}
     */
    public getStatusBarConfiguration(): StatusBarConfiguration {
        return this.statusBarConfiguration;
    }

    /**
     * It finds one button configuration by name.
     * @param  {string}              buttonName
     * @return {ButtonConfiguration}
     */
    public getButtonConfigurationByName(buttonName: string): ButtonConfiguration {
        if (this.buttonsConfigurations) {
            return this.buttonsConfigurations.find((buttonConfiguration: ButtonConfiguration) => {
                return buttonConfiguration.getName() === buttonName;
            });
        }
    }

    /**
     * It finds one input configuration by name.
     * @param  {string}              inputName
     * @return {InputConfiguration}
     */
    public getInputConfigurationByName(inputName: string): InputConfiguration {
        if (this.inputsConfigurations) {
            return this.inputsConfigurations.find((inputConfiguration: InputConfiguration) => {
                return inputConfiguration.getName() === inputName;
            });
        }
    }

    /**
     * It returns the url identifier.
     * @return {string}
     */
    public getUrlId(): string {
    	return '';
    }

    /**
     * It sets the top background color of the page.
     * @param {string} topBackgroundColor
     */
     public setTopBackgroundColor(topBackgroundColor: string): void {
        this.topBackgroundColor = topBackgroundColor;
    }

    /**
     * It returns the top background color of the page.
     * @return {string}
     */
    public getTopBackgroundColor(): string {
        return this.topBackgroundColor;
    }

    /**
     * It sets the bottom background color of the page.
     * @param {string} bottomBackgroundColor
     */
     public setBottomBackgroundColor(bottomBackgroundColor: string): void {
        this.bottomBackgroundColor = bottomBackgroundColor;
    }

    /**
     * It returns the bottom background color of the page.
     * @return {string}
     */
    public getBottomBackgroundColor(): string {
        return this.bottomBackgroundColor;
    }

    /**
     * It sets if the background page exists.
     * @param {boolean} backgroundImage
     */
     public setBackgroundImage(backgroundImage: boolean): void {
        this.backgroundImage = backgroundImage;
    }

    /**
     * It returns if the background page exists.
     * @return {boolean}
     */
    public getBackgroundImage(): boolean {
        return this.backgroundImage;
    }

}
