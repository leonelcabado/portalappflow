export class CaseButtons {
    dsName: string; //Label del botón, ej: “Desarrollar”.
    isDefault: Boolean; //Indica si el botón es la acción default.
    dsURL: string;//URL que debe cargarse en el iframe al ser apretado.
    dsColor: string; // Clase que debe tener el botón, que le da el color. Valores posibles: buttonSuccess: Verde buttonDefault: Azul buttonError: Rojo

    constructor() {
    }
}
