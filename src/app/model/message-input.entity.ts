import {Entity, Column, PrimaryGeneratedColumn, OneToMany, ManyToOne} from 'typeorm';

import {Message} from './message.entity';
import {SelectValue} from './select-value.entity';

@Entity("message_input")
export class MessageInput {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        length: '255'
    })
    name: string;

    @Column({
        length: '100'
    })
    type: string;

    @Column({
        length: '255'
    })
    label: string;

    @Column("text")
    value?: string;

    @Column()
    readOnly: boolean = false;

    @ManyToOne(type => Message, message => message.lsInputs)
    message: Message;

    @OneToMany(type => SelectValue, selectValue => selectValue.messageInput)
    possibleValues: SelectValue[];
}
