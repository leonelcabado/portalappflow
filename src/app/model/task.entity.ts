import {Serializable} from './serializable.entity';
import {CaseButtons} from './case-buttons.entity';
import { TaskExecute } from './task-execute.entity';

const PRIORITY = {
    URGENT: {code: 1, class: "urgent-priority", name: "arrow-up-outline"},
    HIGH: {code: 2, class: "high-priority", name: "arrow-up-outline"},
    MEDIUM: {code: 3, class: "medium-priority", name: "arrow-forward-outline"},
    LOW: {code: 4, class: "low-priority", name: "arrow-down-outline"}
};

const ALERT = {
    NOT_DEFINED: {code: 0, class: "not-defined-alert"},
    LONG_TERM: {code: 1, class: "long-term-alert"},
    SHORT_TERM: {code: 2, class: "short-term-alert"},
    EXPIRED: {code: 3, class: "expired-alert"}
}

export class Task extends Serializable {
    id: string; // Identificador de la tarea. Ej: “00CASO000362218000”
    dsCase: string; //Descripción de la tarea. Ej: “Comunicación de Ausencia de: Juan Perez para el día: 14/08/2018”
    cdProcess: string; //Identificador del proceso al que pertenece la tarea.
    dsProcess: string; //Descripción del proceso, es el texto que ve el usuario. Ej: “Autoatención – Solicitud de Vacaciones”.
    dtInit: number; //Fecha de inicio, en forma numérica.
    cdAlert: number; //Define el estado del icono de reloj, que indica el estado de la tarea. Si la tarea no define tiempo máximo de duración, tiene el valor 0 (No definida). 0: No definida (reloj en color gris) 1: A vencer en largo plazo (reloj en color verde) 2: A vencer en corto plazo (reloj en color naranja) 3: Vencida (reloj en color rojo)
    cdPriority: number; //Las tareas definen la prioridad con el que deben hacerse. Esta prioridad está dada por un número, del 1 al 4. 1: Urgente (Flecha roja) 2: Prioridad Alta (Flecha naranja) 3: Media (Flecha verde) 4: Prioridad baja (Flecha celeste)
    lsButtons: CaseButtons[] = [];
    dsActivity: string;
    execute: TaskExecute;

    constructor() {
        super();
    }

    get priorityColor(): string {
        return this.cdPriority == PRIORITY.URGENT.code ? PRIORITY.URGENT.class : (this.cdPriority == PRIORITY.HIGH.code ? PRIORITY.HIGH.class : (this.cdPriority == PRIORITY.MEDIUM.code ? PRIORITY.MEDIUM.class : PRIORITY.LOW.class));
    }

    get alertColor(): string {
        return this.cdAlert == ALERT.NOT_DEFINED.code ? ALERT.NOT_DEFINED.class : (this.cdAlert == ALERT.LONG_TERM.code ? ALERT.LONG_TERM.class : (this.cdAlert == ALERT.SHORT_TERM.code ? ALERT.SHORT_TERM.class : ALERT.EXPIRED.class));
    }

    get iconName(): string {
        return this.cdPriority == PRIORITY.URGENT.code ? PRIORITY.URGENT.name : (this.cdPriority == PRIORITY.HIGH.code ? PRIORITY.HIGH.name : (this.cdPriority == PRIORITY.MEDIUM.code ? PRIORITY.MEDIUM.name : PRIORITY.LOW.name));
    }

    get defaultButtonName(): string {
        let buttonName: string = "";
        for (let button of this.lsButtons) {
            if (button.isDefault) buttonName = button.dsName;
        }
        return buttonName;
    }

    get defaultButtonUrl(): string {
        let buttonUrl: string = "";
        for (let button of this.lsButtons) {
            if (button.isDefault) buttonUrl = button.dsURL;
        }
        return buttonUrl;
    }

    get defaultButtonColor(): string {
        let buttonColor: string = "";
        for (let button of this.lsButtons) {
            if (button.isDefault) buttonColor = button.dsColor;
        }
        console.log(buttonColor);
        return buttonColor;
    }

    /**
     * Checks if the button name is the default button.
     * @param  {string}  buttonName
     * @return {boolean}
     */
    public isDefaultButton(buttonName: string): boolean {
        if (this.lsButtons && this.lsButtons.length > 0) {
            return this.defaultButtonName === buttonName;
        }
        return false;
    }

    /**
     * Assign the description of the activity.
     * @param {string} dsActivity
     */
    public setDsActivity(dsActivity: string): void {
        this.dsActivity = dsActivity;
    }

    /**
     * Returns the description of the activity.
     * @return {string}
     */
    public getDsActivity(): string {
        return this.dsActivity;
    }

    /*get defaultButtonTag(): string {
      let htmlDefaultButton = "";
      for (let button of this.lsButtons) {
         if (button.isDefault){
           htmlDefaultButton = '<button ion-button small color="'+button.dsColor+'">'+button.dsName+'</button>';
         }
      }
      return htmlDefaultButton;
    }*/

}
