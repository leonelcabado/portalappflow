export class Serializable {

    public fromJSON(jsonObj: JSON) {
        for (var propName in jsonObj) {
            this[propName] = jsonObj[propName];
        }
        return this;
    }

}
