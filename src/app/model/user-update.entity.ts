import {Entity, Column, PrimaryGeneratedColumn} from 'typeorm';

@Entity("user_update")
export class UserUpdate {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    userId: number;

    @Column()
    dtLastUpdate: number

}