export const EVENT_TYPE = {
    NEW_MESSAGE: 0,
    USER_LOGGED_IN: 1,
    USER_STATE_CHANGED: 2,
    MESSAGES_STATE_CHANGED: 3,
    LEAVE_CONVERSATION: 4,
    NEW_CONV_PARTICIPANT: 5,
    CONV_TITLE_CHANGED: 6,
    USER_TYPING: 7,
    USER_STOP_TYPING: 8,
    MESSAGE_EXECUTED: 9,
    MESSAGE_DELETED: 10,
    NEW_USER: 11,
    CHANGE_GROUP_OWNER: 13,
    USER_TASKS_CHANGED: 14,
    NEW_TASK: 15,
    THEME_CHANGED: 17
}

export class RTMEvent {
    cdType: number;
    dsData: any;

    constructor(cdType: number, dsData: any) {
        this.cdType = cdType;
        this.dsData = dsData;
    }
}
