import {Participant} from './participant.entity';
import {Entity, Column, PrimaryGeneratedColumn, ManyToOne} from 'typeorm';

@Entity("command")
export class Command {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    cdCommand: string;

    @Column()
    dsCommand: string;

    @ManyToOne(type => Participant, participant => participant.lsCommands)
    participant: Participant;
}
