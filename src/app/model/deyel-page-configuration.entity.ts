import { PageConfiguration } from './page-configuration.entity';
import { HeaderConfiguration } from './header-configuration.entity';
import { ButtonConfiguration } from './button-configuration.entity';
import { InputConfiguration } from './input-configuration.entity';
import { StatusBarConfiguration } from './status-bar-configuration.entity';

export class DeyelPageConfiguration extends PageConfiguration {

    idPage: string;

    constructor(idPage?: string, headerConfiguration?: HeaderConfiguration, initialPage?: boolean, buttonsConfigurations?: Array<ButtonConfiguration>, inputsConfigurations?: Array<InputConfiguration>, statusBarConfiguration?: StatusBarConfiguration, topBackgroundColor?: string, bottomBackgroundColor?: string, backgroundImage? : boolean) {
        super(headerConfiguration, initialPage, buttonsConfigurations, inputsConfigurations, statusBarConfiguration, topBackgroundColor, bottomBackgroundColor, backgroundImage);
        this.setIdPage(idPage);
    }

    /**
     * It sets a id page.
     * @param {string} cdDocumentName
     */
    public setIdPage(idPage: string): void {
    	this.idPage = idPage;
    }

    /**
     * It returns the id page.
     * @return {string}
     */
    public getIdPage(): string {
    	return this.idPage;
    }

    /**
     * It returns the url identifier.
     * @return {string}
     */
    public getUrlId(): string {
        return this.getIdPage();
    }

}
