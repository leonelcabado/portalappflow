import {Entity, Column, PrimaryGeneratedColumn} from 'typeorm';

@Entity("conversation_unread")
export class ConversationUnread {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    cdConversation: number;

    @Column()
    dtLastMsg: number; // last message dtStore

    @Column()
    userId: number;

    @Column()
    createdAt: number;

    @Column()
    synchronized: boolean = false;

}
