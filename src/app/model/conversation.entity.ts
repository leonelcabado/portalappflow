import {Entity, Column, PrimaryGeneratedColumn, ManyToMany, ManyToOne, JoinTable, OneToMany} from 'typeorm';

import {Participant} from './participant.entity';
import {Message} from './message.entity';
import {MessageDateGrouped} from '../support-classes/message-date-grouped.class';
import {StateOutput} from '../support-classes/state-output.class';
import {Serializable} from './serializable.entity';

export const CONVERSATION_TYPE = {
    PRIVATE: 0,
    GROUP: 1
};

@Entity("conversation")
export class Conversation extends Serializable {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    userId: number;

    @Column({nullable: true})
    cdConversation: number;

    @Column({
        length: '100', nullable: true
    })
    dsTitle?: string;

    @Column({nullable: true})
    cdType: number; //0: private, 1: group

    @Column("text", {nullable: true})
    dsImage?: string;

    @ManyToMany(type => Participant, participant => participant.conversations, {
        cascade: ['insert', 'update']
    })
    @JoinTable({
        name: "conversation_participant"
    })
    lsParticipants: Participant[];

    @Column({nullable: true})
    nuUnreaded: number = 0;

    @OneToMany(type => Message, message => message.conversationId)
    lsMessages: Message[];

    @Column({
        length: '200', nullable: true
    })
    cdUserCreator ?: string;

    @Column({nullable: true})
    dtLastUpdate ?: number;

    @Column({nullable: true})
    cdObject ?: string;

    @Column({nullable: true})
    dsTypeObject ?: string;

    @Column({nullable: true})
    dsObjectUrl ?: string;

    //Not related by ORM
    lsUsersTyping ?: Participant[] = [];

    @ManyToOne(type => Participant)
    receptor: Participant;

    @Column()
    synchronized: boolean = false;

    @Column()
    dtLastChange: number;

    @Column({nullable: true})
    dtStore ?: number;

    @Column()
    blocked: boolean = false;

    maskedUsers?: any[] = [];

    lsParticipantsHistory: Participant[] = [];

    constructor() {
        super();
    }

    /**
     * Retorna el codigo de la conversacion
     * @return {number}
     */
    getCdConversation(): number {
        return this.cdConversation;
    }

    /**
     * Asigna un arreglo de participantes a la conversacion.
     * @param  {Array<Participant>}
     * @return {Conversation}
     */
    setParticipants(participants: Array<Participant>): Conversation {
        this.lsParticipants = participants;
        return this;
    }

    /**
     * Asigna un arreglo de mensajes a la conversacion.
     * @param  {Array<Message>}
     * @return {Conversation}
     */
    setMessages(messages: Array<Message>): Conversation {
        if(messages){
            this.lsMessages = messages;
        }else{
            this.lsMessages = [];
        }
        return this;
    }

    /**
     * Asigna la fecha de ultima actualizacion en la conversacion.
     * @param  {number}
     * @return {Conversation}
     */
    setDtLastUpdate(date: number): Conversation {
        this.dtLastUpdate = date;
        return this;
    }

    /**
     * Asigna una descripcion de un titulo a la conversacion.
     * @param  {string}
     * @return {Conversation}
     */
    setDsTitle(title: string): Conversation {
        this.dsTitle = title;
        return this;
    }

    /**
     * Retorna el ultimo mensaje de la conversacion.
     * @return {Message}
     */
    public getLastMessage(): Message {
        if (this.lsMessages && this.lsMessages.length > 0) {
            return this.lsMessages[this.lsMessages.length - 1];
        }
        return null;
    }

    /**
     * Retorna true si la conversacion es privada, caso contrario false.
     * @return {boolean}
     */
    public isPrivateConversation(): boolean {
        return this.cdType === 0;
    }

    /**
     * Chequea si existe el participante en la conversacion.
     * @param  {string}
     * @return {boolean}
     */
    public hasParticipant(cdParticipant: string): boolean {
        for (let p of this.lsParticipants) {
            if (p.cdParticipant.toUpperCase() === cdParticipant.toUpperCase()) {
                return true;
            }
        }
        console.error("NO encuentro al participant");
        return false;
    }

    /**
     * Elimina el participante de la conversación, si existe.
     * @param  {string}
     * @return {boolean}
     */
    public removeParticipant(cdParticipant: string): boolean {
        for (let p of this.lsParticipants) {
            if (p.cdParticipant === cdParticipant) {
                this.lsParticipants.splice(this.lsParticipants.indexOf(p), 1);
                return true;
            }
        }
        return false;
    }

    /**
     * Retorna el estado del participante.
     * @param  {string}
     * @return {string}
     */
    public getParticipantStateClass(cdCurrentParticipant: string): string {
        for (let p of this.lsParticipants) {
            if (p.cdParticipant != cdCurrentParticipant) {
                return StateOutput.getStateClass(p.cdState);
            }
        }
        return "undefined";
    }

    /**
     * Retorna la dsecripcion del participante.
     * @param  {string}
     * @return {string}
     */
    public getParticipantStateDescription(cdCurrentParticipant: string): string {
        for (let p of this.lsParticipants) {
            if (p.cdParticipant != cdCurrentParticipant) {
                return StateOutput.getStateDescription(p.cdState);
            }
        }
        return "undefined";
    }

    /**
     * Retorna un mensaje buscado por id.
     * @param  {number}
     * @return {Message}
     */
    public getMessageById(id: number): Message {
        if(this.lsMessages){
            for (let m of this.lsMessages) {
                if (m.cdConversationElement == id) {
                    return m;
                }
            }
        }
        return null;
    }

    /**
     * Retorna los mensajes del grupp.
     * @return {MessageDateGrouped[]}
     */
    public getGroupedMessages(): MessageDateGrouped[] {
        return MessageDateGrouped.build(this);
    };

    /**
     * Copia la conversacion a una conversacion destino.
     * @param {Conversation}
     */
    public copy(destino: Conversation) {
        destino.nuUnreaded = this.nuUnreaded;
        destino.lsMessages = this.lsMessages;
        destino.dtLastUpdate = this.dtLastUpdate;
        destino.dsTitle = this.dsTitle;
        destino.dsImage = this.dsImage;
        destino.cdType = this.cdType;
        destino.cdUserCreator = this.cdUserCreator;
        destino.cdObject = this.cdObject;
        destino.dsObjectUrl = this.dsObjectUrl;
        destino.lsUsersTyping = this.lsUsersTyping;
    }


    /**
     * Actualiza los estados de los mensajes.
     */
    public updateMessageStates(lsMessageStates) {
        for (let s of lsMessageStates) {
            let m = this.getMessageById(s.cdConversationElement);
            if (m != null) {
                m.setMsgState(s.msgState);
            }
        }
    }

    public setLsMessages(messages: Message[]): Conversation {
        this.lsMessages = messages;
        return this;
    }

    public getLsMessages(): Message[] {
        return this.lsMessages;
    }

    public setLsParticipants(participants: Participant[]): Conversation {
        this.lsParticipants = participants;
        return this;
    }

    public getLsParticipants(): Participant[] {
        return this.lsParticipants;
    }

    public getParticipantsExceptMe(p: Participant): Participant[] {
        let participants = this.getLsParticipants();
        let value = [];
        for (let i of participants) {
            if (i.cdParticipant != p.cdParticipant) {
                value.push(i);
            }
        }
        return value;
    }

    public getParticipantsExceptMeByCd(p: string): Participant[] {
        let value = [];
        for (let i of this.getLsParticipants()) {
            if (i.cdParticipant != p.toUpperCase()) {
                value.push(i);
            }
        }
        return value;
    }

    public getParticipantByCd(cdParticipant: string): Participant {
        if (cdParticipant) {
            let participants = this.getLsParticipants();
            for (let i of participants) {
                if (i.cdParticipant == cdParticipant.toUpperCase()) {
                    return i;
                }
            }
        }
        return null;
    }

    public getCdUserCreator() {
        return this.cdUserCreator;
    }

    public setReceptor(participant: Participant) {
        this.receptor = participant;
        return this;
    }

    public getReceptor() {
        return this.receptor;
    }

    get isGroup(): boolean {
        return this.cdType === CONVERSATION_TYPE.GROUP;
    }

    get image(): string {
        let image = "";
        if (this.isGroup) {
            image = this.dsImage;
        }
        else {
            image = this.receptor ? this.receptor.flImage : this.dsImage;
        }
        return image;
    }

    public setDsImage(dsImage: string): void {
        this.dsImage = dsImage;
    }

    get groupParticipants(): string {
        let dsParticipants = [];
        for (let participant of this.lsParticipants) {
            dsParticipants.push(participant.dsParticipant.replace(",", ""));
        }
        return dsParticipants.join(", ");
    }

    fromSync(syncConversation, userId) {
        let lsParticipants = syncConversation.lsParticipants;
        this.fromJSON(syncConversation);
        this.userId = userId;
        this.lsParticipants = lsParticipants;
        this.nuUnreaded = 0;
        this.synchronized = true;
        this.dtLastChange = this.dtStore;
        return this;
    }

    getPlainObject(){
        const plainObject = Object.assign({}, this);
        delete plainObject['lsMessages'];
        delete plainObject['lsUsersTyping'];
        delete plainObject['lsParticipantsHistory'];
        delete plainObject['lsParticipants'];
        delete plainObject['cdExternalConversation'];
        delete plainObject['cdExternalLicence'];
        delete plainObject['dsExternalUrl'];
        delete plainObject['maskedUsers'];
        return plainObject;
    }
}
