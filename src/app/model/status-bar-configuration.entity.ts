export class StatusBarConfiguration {

    private backgroundColor: string;
	private lightText: boolean;

    constructor(backgroundColor: string, lightText: boolean) {
        this.setBackgroundColor(backgroundColor);
		this.setLightText(lightText);
    }

    /**
     * It sets the background color.
     * @param {string} backgroundColor
     */
    public setBackgroundColor(backgroundColor: string): void {
        this.backgroundColor = backgroundColor;
    }

    /**
     * It returns the background color.
     * @return {string}
     */
    public getBackgroundColor(): string {
        return this.backgroundColor;
    }

    /**
     * It sets the status bar contains light text.
     * @param {boolean} lightText
     */
    public setLightText(lightText: boolean): void {
        this.lightText = lightText;
    }

    /**
     * It returns the status bar contains light text.
     * @return {boolean}
     */
    public getLightText(): boolean {
        return this.lightText;
    }

}
