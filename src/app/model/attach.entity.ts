import {Entity, Column, PrimaryGeneratedColumn, ManyToOne} from 'typeorm';
import {Serializable} from './serializable.entity';
import {Message} from './message.entity';

@Entity("attach")
export class Attach extends Serializable {

    @PrimaryGeneratedColumn()
    id: number

    @Column({
        length: '255',
        nullable: true
    })
    private _cdFile?: string;

    @Column({
        length: '255',
        nullable: true
    })
    private _cdTempPath?: string;

    @Column({
        length: '255',
        nullable: true
    })
    private _cdLocalPath?: string;

    @Column({
        length: '255',
        nullable: true
    })
    private _dsName: string;

    @Column({nullable: true})
    private _cdState: number; // 0 = temp still not created / 1 = temp created / 2 = error creating temp

    @Column({
        length: '100',
        nullable: true
    })
    private _cdExtension: string;

    @Column("text", {nullable: true})
    private _imagePreview?: string;

    @Column({nullable: true})
    dtLastUpdate: number;

    @Column({nullable: true})
    dtStore?: number;

    @ManyToOne(type => Message, message => message.lsAnnexs)
    message: Message;

    downloading: boolean;
    realPath: string;


    get cdFile(): string {
        return this._cdFile;
    }

    set cdFile(value: string) {
        this._cdFile = value;
    }

    get cdTempPath(): string {
        return this._cdTempPath;
    }

    set cdTempPath(value: string) {
        this._cdTempPath = value;
    }

    get cdLocalPath(): string {
        return this._cdLocalPath;
    }

    set cdLocalPath(value: string) {
        this._cdLocalPath = value;
    }

    get dsName(): string {
        return this._dsName;
    }

    set dsName(value: string) {
        this._dsName = value;
    }

    get cdState(): number {
        return this._cdState;
    }

    set cdState(value: number) {
        this._cdState = value;
    }

    get cdExtension(): string {
        return this._cdExtension;
    }

    set cdExtension(value: string) {
        this._cdExtension = value;
    }

    get imagePreview(): string {
        return this._imagePreview;
    }

    set imagePreview(value: string) {
        this._imagePreview = value;
    }

     /**
     * Assigns the date of the last update.
     * @param  {number}
     * @return {Attach}
     */
    public setDtLastUpdate(date: number): Attach {
        this.dtLastUpdate = date;
        return this;
    }

    /**
     * Returns the date of the last update.
     * @return {Date}
     */
    public getDtLastUpdate(): Date {
        return new Date(this.dtLastUpdate);
    }

    /**
     * Assigns the date of the last storage.
     * @param  {number}
     * @return {Attach}
     */
    public setDtStore(date: number): Attach {
        this.dtStore = date;
        return this;
    }

    /**
     * Returns the date of the last storage.
     * @return {Date}
     */
    public getDtStore(): Date {
        return new Date(this.dtStore);
    }

    constructor() {
        super();
    }

    setInitAttributes(dsName: string, localPath: string) {
        this.cdFile = "";
        this.cdTempPath = "";
        this.cdLocalPath = localPath;
        this.dsName = dsName;
        this.cdExtension = dsName.substring(dsName.lastIndexOf(".") + 1, dsName.length).toLowerCase();
        this.cdState = 0;
        return this;
    }

    getImageRoute(extensionMap: Map<string, string>): string {
        if (extensionMap.has(this.cdExtension)) {
            return "file-icon-" + extensionMap.get(this.cdExtension);
        } else {
            return "file-icon-default";
        }
    }

    getAttachIcon(pAttachName: string) {
        const xAttachExt = pAttachName.substring(pAttachName.lastIndexOf(".") + 1, pAttachName.length).toLowerCase();
        switch (xAttachExt) {
            case "xls":
            case "xlsx": {
                return "document"
            }
            case "png":
            case "jpg":
            case "jpeg":
            case "gif": {
                return "camera"
            }
            case "mp3":
            case "wav":
            case "wma":
            case "ogg": {
                return "musical-notes"
            }
            default: {
                return "document";
            }
        }
    }

    getAttachExtension(pAttachName: string) {
        return pAttachName.substring(pAttachName.lastIndexOf(".") + 1, pAttachName.length).toLowerCase();
    }

    getPlainObject(){
        const plainObject = Object.assign({}, this);
        delete plainObject['downloading'];
        delete plainObject['cdMessage'];
        delete plainObject['filePath'];
        delete plainObject['realPath'];
        return plainObject;
    }

}
