import {Serializable} from './serializable.entity';

export class TedisConfiguration extends Serializable {

    userConfiguration: UserConfiguration; // Still not implemented!
    systemConfiguration: SystemConfiguration;

    constructor() {
        super();
    }

    isGuest(): boolean {
        return this.userConfiguration.isGuest();
    }

    public fromJSON(jsonObj: JSON) {
        super.fromJSON(jsonObj);
        this.userConfiguration = new UserConfiguration().fromJSON(jsonObj["userConfiguration"]);
        this.systemConfiguration = new SystemConfiguration().fromJSON(jsonObj["systemConfiguration"]);
        return this;
    }

}


class SystemConfiguration extends Serializable {
    maxUploadAttachSize: number;
    theme: string;

    constructor() {
        super();
    }

}

class UserConfiguration extends Serializable {
    cdUser: string;
    guest: boolean;

    constructor() {
        super();
    }

    isGuest(): boolean {
        return this.guest;
    }

}
