import { PageConfiguration } from './page-configuration.entity';

export class FormBrowseConfiguration extends PageConfiguration {

    cdDocumentName: string;

    constructor(cdDocumentName: string) {
        super();
        this.setCdDocumentName(cdDocumentName);
    }

    /**
     * It sets a cdDocumentName.
     * @param {string} cdDocumentName
     */
    public setCdDocumentName(cdDocumentName: string): void {
    	this.cdDocumentName = cdDocumentName;
    }

    /**
     * It returns the cdDocumentName.
     * @return {string}
     */
    public getCdDocumentName(): string {
    	return this.cdDocumentName;
    }

    /**
     * It returns the url identifier.
     * @return {string}
     */
    public getUrlId(): string {
        return this.getCdDocumentName();
    }

}
