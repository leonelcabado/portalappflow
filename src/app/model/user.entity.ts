import {Entity, Column, PrimaryGeneratedColumn, ManyToOne} from 'typeorm';
import {Context} from './context.entity';

@Entity('user')
export class User {

    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type => Context)
    context: Context;

    @Column({
        length: '100'
    })
    username: string;

    @Column({
        length: '100'
    })
    password: string;

    @Column()
    createdAt: number;

    @Column()
    loggedIn: boolean

    @Column('text')
    token: string;

    @Column('text')
    refreshToken: string;

    @Column('text', {nullable: true})
    translations?: string;

    @Column('text', {nullable: true})
    lastUsedEmojis?: string;

    @Column("text", { nullable: true, array: true })
    lsSecurityFunctions: String[];

    getPlainObject(){
        const plainObject = Object.assign({}, this);
        return plainObject;
    }

    /**
     * Set the translations list.
     * @param {string} translations
     */
    public setTranslations(translations: string): void {
        this.translations = translations;
    }

    /**
     * Returns the user translations list
     * @return {string}
     */
    public getTranslations(): string {
        return this.translations;
    }

    /**
     * It sets a refresh token.
     * @param {string} refreshToken
     */
    public setRefreshToken(refreshToken: string): void {
        this.refreshToken = refreshToken;
    }

    /**
     * It returns the user refresh token.
     * @return {string}
     */
    public getRefreshToken(): string {
        return this.refreshToken;
    }

    /**
     * It returns the username.
     * @return {string}
     */
    public getUsername(): string {
        return this.username;
    }

    /**
     * It sets the username.
     * @param {string} username
     */
    public setUsername(username: string): void {
        this.username = username;
    }

    /**
     * It sets the token.
     * @param {string} token
     */
    public setToken(token: string): void {
        this.token = token;
    }

    /**
     * It returns the token.
     * @return {string}
     */
    public getToken(): string {
        return this.token;
    }

    /**
     * It sets the password.
     * @param {string} password
     */
    public setPassword(password: string): void {
        this.password = password;
    }

    /**
     * It returns the password.
     * @return {string}
     */
    public getPassword(): string {
        return this.password;
    }

    /**
     * It sets the context.
     * @param {Context} context
     */
    public setContext(context: Context): void {
        this.context = context;
    }

    /**
     * It returns the context.
     * @return {Context}
     */
    public getContext(): Context {
        return this.context;
    }

    /**
     * It sets if the user is logged.
     * @param {boolean} loggedIn
     */
    public setLoggedIn(loggedIn: boolean): void {
        this.loggedIn = loggedIn;
    }

    /**
     * It returns if the user is logged.
     * @return {boolean}
     */
    public getLoggedIn(): boolean {
        return this.loggedIn;
    }

    /**
     * It sets the instant of creation.
     * @param {number}
     */
    public setCreatedAt(createdAt: number): void {
        this.createdAt = createdAt;
    }

    /**
     * It returns the instant of creation.
     * @return {number}
     */
    public getCreatedAt(): number {
        return this.createdAt;
    }

    /**
     * It sets the user security functions.
     * @param {String[]} lsSecurityFunctions
     */
     public setLsSecurityFunctions(lsSecurityFunctions: String[]): void {
        this.lsSecurityFunctions = lsSecurityFunctions;
    }

    /**
     * It returns the user security functions.
     * @return {String[]}
     */
    public getLsSecurityFunctions(): String[] {
        return this.lsSecurityFunctions;
    }

}
