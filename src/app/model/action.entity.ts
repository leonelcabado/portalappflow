export class Action {

    name: string;
    code: string;
    type: string;
    url: string;
    icon: string;

    constructor() {
    }

    /**
     * Set the name.
     * @param name
     */
    public setName(name: string): void {
        this.name = name;
    }

    /**
     * Returns the name,
     */
    public getName(): string {
        return this.name;
    }

    /**
     * Set the code.
     * @param code
     */
    public setCode(code: string): void {
        this.code = code;
    }

    /**
     * Returns the code,
     */
    public getCode(): string {
        return this.code;
    }

    /**
     * Set the type.
     * @param type
     */
    public setType(type: string): void {
        this.type = type;
    }

    /**
     * Returns the type,
     */
    public getType(): string {
        return this.type;
    }

    /**
     * Set the url.
     * @param url
     */
    public setUrl(url: string): void {
        this.url = url;
    }

    /**
     * Returns the url,
     */
    public getUrl(): string {
        return this.url;
    }

    /**
     * Set the icon.
     * @param icon
     */
    public setIcon(icon: string): void {
        this.icon = icon;
    }

    /**
     * Returns the icon,
     */
    public getIcon(): string {
        return this.icon;
    }

    /**
     * Returns if the action is a form.
     * @return {boolean}
     */
    public isForm(): boolean {
        return this.getType() === 'formulario';
    }

}
