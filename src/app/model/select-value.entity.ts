import {Entity, Column, PrimaryGeneratedColumn, ManyToOne} from 'typeorm';

import {MessageInput} from './message-input.entity';

@Entity("select_value")
export class SelectValue {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        length: '255'
    })
    key: string;

    @Column("text")
    description: string;

    @ManyToOne(type => MessageInput, messageInput => messageInput.possibleValues)
    messageInput: MessageInput;
}