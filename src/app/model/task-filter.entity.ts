export class TaskFilter {
    lsAlert?: number[]; // Lista de codigos de alertas.
    lsProirity?: number[]; // Lista de codigos de prioridad.
    lsFilters: TaskFilterParameter[]; // Lista de filtros para api rest.

    constructor(){}
    
    public setLsAlert(lsAlert: number[]){
        this.lsAlert = lsAlert;
    }
    public getLsAlert(){
        this.lsAlert;
    }
    public setLsPriority(lsPriority: number[]){
        this.lsProirity = lsPriority;
    }
    public getLsPriority(){
        this.lsProirity;
    }

    public setLsFilters(lsFilters: TaskFilterParameter[]){
        this.lsFilters = lsFilters;
    }
}

export class TaskFilterParameter {
    cdKey: string;
    dsParameter: string;
    dsOperation: string;
    dsValue: string;

    constructor(cdKey?: string, dsParameter?: string, dsOperation?: string, dsValue?: string){
        this.setCdKey(cdKey);
        this.setDsParameter(dsParameter);
        this.setDsOperation(dsOperation);
        this.setDsValue(dsValue);
    }

    public setCdKey(cdKey: string){
        this.cdKey = cdKey;
    }
    public getCdKey(){
        this.cdKey;
    }

    public setDsParameter(dsParameter: string){
        this.dsParameter = dsParameter;
    }
    public getDsParameter(){
        this.dsParameter;
    }

    public setDsOperation(dsOperation: string){
        this.dsOperation = dsOperation;
    }
    public getDsOperation(){
        this.dsOperation;
    }

    public setDsValue(dsValue: string){
        this.dsValue = dsValue;
    }
    public getDsValue(){
        this.dsValue;
    }
}