export class HeaderConfiguration {

	private title: string;
	private style: string;

    constructor(title?: string, style?: string) {
		this.setTitle(title);
    	this.setStyle(style);
    }

    /**
     * It sets the button title.
     * @param {string} title
     */
    public setTitle(title: string): void {
    	this.title = title;
    }

    /**
     * It returns the button title.
     * @return {string}
     */
    public getTitle(): string {
    	return this.title;
    }

    /**
     * It sets the button style.
     * @param {string} style
     */
    public setStyle(style: string): void {
    	this.style = style;
    }

    /**
     * It returns the button style.
     * @return {string}
     */
    public getStyle(): string {
    	return this.style;
    }

    /**
     * It returns the url identifier.
     * @return {string}
     */
    public getUrlId(): string {
    	return '';
    }

}
