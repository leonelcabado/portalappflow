import {Entity, Column, PrimaryGeneratedColumn, ManyToOne} from 'typeorm';

import {Message} from './message.entity';
import {Serializable} from './serializable.entity';

@Entity("message_button")
export class MessageButton extends Serializable {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        length: '255'
    })
    dsLabel: string;

    @Column({
        length: '255'
    })
    dsAction: string;

    @ManyToOne(type => Message, message => message.lsButtons)
    message: Message;

    generateUrl() {
        this.dsAction.includes("?") ? this.dsAction += "&" : this.dsAction += "?";
    }
}
