import {Entity, Column, PrimaryGeneratedColumn, OneToMany, Index} from 'typeorm';
import {Command} from './command.entity';
import {Conversation} from './conversation.entity';
import {Serializable} from './serializable.entity';


const STATE = {
    OFFLINE: {code: 0, class: "offline"},
    ONLINE: {code: 1, class: "online"},
    ABSENT: {code: 2, class: "absent"},
    NOT_AVAILABLE: {code: 3, class: "not-available"}
};

@Entity("participant")
@Index(["cdParticipant", "tpParticipant", "userId"])
export class Participant extends Serializable {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    userId: number;

    @Column()
    cdParticipant: string;

    @Column()
    tpParticipant: string;

    @Column("text", {nullable: true})
    flImage?: string;

    @Column({nullable: true})
    cdState?: number; //0: Desconectado, 1: Conectado, 2: Ausente, 3: No disponible

    @Column({nullable: true})
    dsParticipant?: string;

    @OneToMany(type => Command, command => command.participant)
    lsCommands?: Command[];

    @Column({nullable: true})
    readed: boolean = false;

    @Column({nullable: true})
    isVisible: boolean = true;

    @Column({nullable: true})
    pinned: boolean = false;

    //Not related by ORM
    conversations: Conversation[] = [];

    //Not persisted by ORM, only for use in select functionality
    selected: boolean = false;

    constructor(pCdParticipant?: string, pTpParticipant?: string, pDsParticipant?: string, pState?: number, pflImage?: string) {
        super();
        this.cdParticipant = (pCdParticipant ? pCdParticipant.toUpperCase() : "");
        this.tpParticipant = (pTpParticipant ? pTpParticipant : "");
        this.dsParticipant = (pDsParticipant ? pDsParticipant : "");
        this.cdState = (pState ? pState : 0);
        this.flImage = (pflImage ? pflImage : "");
        this.lsCommands;
        this.readed = false;
    }

    get stateClass(): string {
        return this.cdState == STATE.OFFLINE.code ? STATE.OFFLINE.class : (this.cdState == STATE.ONLINE.code ? STATE.ONLINE.class : (this.cdState == STATE.ABSENT.code ? STATE.ABSENT.class : STATE.NOT_AVAILABLE.class));
    }

    get hasCommands(): boolean {
        return this.lsCommands && this.lsCommands.length > 0;
    }

    updateFrom(participant: Participant) {
        this.isVisible = participant.isVisible;
        this.dsParticipant = participant.dsParticipant;
        this.flImage = participant.flImage;
        this.readed = participant.readed;
        this.cdState = participant.cdState;
        this.pinned = participant.pinned;
    }

    getPlainObject(){
        const plainObject = Object.assign({}, this);
        delete plainObject['conversations'];
        delete plainObject['selected'];
        delete plainObject['lsCommands'];
        return plainObject;
    }

    static getSysUser(): Participant {
        return new Participant("SYS_USER", "USER", "Usuario del sistema", 1, "");
    }
}
