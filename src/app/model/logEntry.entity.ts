import {Entity, Column, PrimaryGeneratedColumn, Index} from 'typeorm';

@Entity("logEntry")
@Index(["environmentId", "type"])
export class LogEntry {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    type: number;

    @Column({nullable: true})
    environmentId?: number;

    @Column({nullable: true})
    userId?: number;

    @Column()
    message: string;

    @Column({nullable: true})
    duration?: number;

    @Column({nullable: true})
    appVersion: string;

    @Column({nullable: true})
    deyelVersion: string;

    @Column({nullable: true})
    device: string;

    @Column({nullable: true})
    platform: string;

    @Index()
    @Column()
    createdAt: number;

}