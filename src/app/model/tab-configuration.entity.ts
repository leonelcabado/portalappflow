import { PageConfiguration } from './page-configuration.entity';

export class TabConfiguration {

    type: string;
    title: string;
    iconName: string | Array<string>;
    iconColor: string;
    url: string;
    isActived: boolean;
    isDefault: boolean;
    configuration: PageConfiguration;

    constructor(type: string, title: string, iconName: string | Array<string>, iconColor: string, url: string, isActived: boolean, isDefault:boolean, configuration: PageConfiguration) {
        this.setType(type);
        this.setTitle(title);
        this.setIconName(iconName);
        this.setIconColor(iconColor);
        this.setUrl(url);
        this.setIsActived(isActived);
        this.setIsDefault(isDefault);
        this.setConfiguration(configuration);
    }

    /**
     * It sets a type.
     * @param {string} type
     */
    public setType(type: string): void {
    	this.type = type;
    }

    /**
     * It returns the type.
     * @return {string}
     */
    public getType(): string {
    	return this.type;
    }

    /**
     * It sets a title.
     * @param {string} title
     */
    public setTitle(title: string): void {
    	this.title = title;
    }

    /**
     * It returns the title.
     * @return {string}
     */
    public getTitle(): string {
    	return this.title;
    }

    /**
     * It sets the icon name.
     * @param {string | Array<string>} iconName
     */
    public setIconName(iconName: string | Array<string>): void {
    	this.iconName = iconName;
    }

    /**
     * It returns the icon name.
     * @return {string | Array<string>}
     */
    public getIconName(): string | Array<string> {
    	return this.iconName;
    }

    /**
     * It sets a icon color.
     * @param {string} iconColor
     */
    public setIconColor(iconColor: string): void {
        this.iconColor = iconColor;
    }

    /**
     * It returns the icon color.
     * @return {string}
     */
    public getIconColor(): string {
        return this.iconColor;
    }

    /**
     * It sets the tab url.
     * @param {string} url
     */
    public setUrl(url: string): void {
    	this.url = url;
    }

    /**
     * It returns the tab url.
     * @return {string}
     */
    public getUrl(): string {
    	return this.url;
    }

    /**
     * It sets a flag for the tab actived.
     * @param {boolean} isActived
     */
     public setIsActived(isActived: boolean): void {
    	this.isActived = isActived;
    }

    /**
     * It returns flag for the tab active.
     * @return {boolean}
     */
    public getIsActived(): boolean {
    	return this.isActived;
    }

    /**
     * It sets a flag for the tab default.
     * @param {boolean} isDefault
     */
     public setIsDefault(isDefault: boolean): void {
    	this.isDefault = isDefault;
    }

    /**
     * It returns flag for the tab default.
     * @return {boolean}
     */
    public getIsDefault(): boolean {
    	return this.isDefault;
    }

    /**
     * It sets a configuration.
     * @param {PageConfiguration} configuration
     */
    public setConfiguration(configuration: PageConfiguration): void {
    	this.configuration = configuration;
    }

    /**
     * It returns a configuration.
     * @return {PageConfiguration}
     */
    public getConfiguration(): PageConfiguration {
    	return this.configuration;
    }

    /**
     * It returns the url identifier.
     * @return {string}
     */
    public getUrlId(): string {
        return this.getConfiguration().getUrlId();
    }

}
