export class ButtonConfiguration {

    private name: string;
	private label: string;
	private style: string;

    constructor(name: string, label: string, style: string) {
        this.setName(name);
		this.setLabel(label);
    	this.setStyle(style);
    }

    /**
     * It sets the button name.
     * @param {string} name
     */
    public setName(name: string): void {
        this.name = name;
    }

    /**
     * It returns the button name.
     * @return {string}
     */
    public getName(): string {
        return this.name;
    }

    /**
     * It sets the button label.
     * @param {string} label
     */
    public setLabel(label: string): void {
    	this.label = label;
    }

    /**
     * It returns the button label.
     * @return {string}
     */
    public getLabel(): string {
    	return this.label;
    }

    /**
     * It sets the button style.
     * @param {string} style
     */
    public setStyle(style: string): void {
    	this.style = style;
    }

    /**
     * It returns the button style.
     * @return {string}
     */
    public getStyle(): string {
    	return this.style;
    }

    /**
     * It returns the url identifier.
     * @return {string}
     */
    public getUrlId(): string {
    	return '';
    }

}
