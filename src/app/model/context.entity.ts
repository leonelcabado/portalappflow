import {Entity, Column, PrimaryGeneratedColumn} from 'typeorm';

//app configuration
import {APP} from '../app.config';

@Entity('context')
export class Context {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        length: '30'
    })
    name: string;

    @Column({
        length: '255'
    })
    url: string;

    @Column()
    createdAt: number;

    apiEndpoint() {
        return this.url + APP.API_BASE_NAMESPACE;
    }

    wssEndpoint(protocol: string = "wss") {
        return this.url.replace('https', protocol).replace('http', protocol) + APP.RTM_BASE_NAMESPACE;
    }

    getPlainObject(){
        const plainObject = Object.assign({}, this);
        return plainObject;
    }

    /**
     * Sets the context name.
     * @param {string} name
     * @return {undefined}
     */
    public setName(name: string): void {
        this.name = name;
    }

    /**
     * Return the context name.
     * @return {string}
     */
    public getName(): string {
        return this.name;
    }

    /**
     * Sets the context url.
     * @param {string} url
     * @return {undefined}
     */
    public setUrl(url: string): void {
        this.url = url;
    }

    /**
     * It returns the context url.
     * @return {string}
     */
    public getUrl(): string {
        return this.url;
    }

    /**
     * It returns the context id.
     * @return {number}
     */
    public getId(): number {
        return this.id;
    }

}
