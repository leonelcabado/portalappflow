import {Entity, Column, PrimaryGeneratedColumn, OneToMany, ManyToOne, JoinColumn} from 'typeorm';


import {Participant} from './participant.entity';
import {Attach} from './attach.entity';
import {Serializable} from './serializable.entity';
import {MessageAction} from './message-action.entity';
import {MessageInput} from './message-input.entity';
import {MessageButton} from './message-button.entity';
import {Conversation} from "./conversation.entity";

@Entity("message")
export class Message extends Serializable {

    @PrimaryGeneratedColumn()
    id: number

    @Column({nullable: true})
    cdConversationElement?: number;

    @Column("text")
    dsContent: string;

    @Column()
    dtLastUpdate: number;

    @Column({
        length: '200',
        nullable: true
    })
    cdParticipant?: string;

    @Column({
        length: '10',
        nullable: true
    })
    tpParticipant?: string;

    @OneToMany(type => Attach, attach => attach.message, {})
    lsAnnexs?: Attach[];

    @Column({nullable: true})
    cdConversation ?: number;

    @Column({
        length: '200',
        nullable: true
    })
    cdExecutedCommand ?: string;

    @Column("text", {nullable: true})
    dsExecutionPath ?: string;

    @OneToMany(type => MessageAction, messageAction => messageAction.message, {
        cascade: ['insert']
    })
    lsActions?: MessageAction[];

    @OneToMany(type => MessageInput, messageInput => messageInput.message, {
        cascade: ['insert']
    })
    lsInputs?: MessageInput[];

    @OneToMany(type => MessageButton, messageButton => messageButton.message, {
        cascade: ['insert', 'update']
    })
    lsButtons?: MessageButton[];

    @Column("text", {nullable: true})
    dsValidation?: string;

    @ManyToOne(type => Conversation, conv => conv.lsMessages)
    @JoinColumn({ name: "conversationId" })
    conversationId: number;

    @Column({nullable: true})
    dtStore?: number;

    @Column("text", {nullable: true})
    msgState?: string;

    @Column("text", {nullable: true})
    dsExternalSenderName?: string;

    @Column("text", {nullable: true})
    externalSenderImg?: string;

    constructor() {
        super();
    }

    /**
     * Asigna un contenido al mensaje.
     * @param  {string}
     * @return {Message}
     */
    public setDsContent(content: string): Message {
        this.dsContent = content;
        return this;
    }

    /**
     * Asigna un tipo de participante  al mensaje.
     * @param  {string}
     * @return {Message}
     */
    public setTpParticipant(tpParticipant: string): Message {
        this.tpParticipant = tpParticipant;
        return this;
    }

    /**
     * Asigna un codigo de conversacion al mensaje.
     * @param  {number}
     * @return {Message}
     */
    public setCdConversation(cdConversation: number): Message {
        this.cdConversation = cdConversation;
        return this;
    }

    /**
     * Asigna una lista de anexos al mensaje.
     * @param  {Array<Attach>}
     * @return {Message}
     */
    public setLsAnnexs(annexs: Array<Attach>): Message {
        this.lsAnnexs = annexs;
        return this;
    }

    /**
     * Asigna el codigo del participante en el mensaje.
     * @param  {string}
     * @return {Message}
     */
    public setCdParticipant(cdParticipant: string): Message {
        this.cdParticipant = cdParticipant;
        return this;
    }

    /**
     * Asigna un codigo de comando en el mensaje.
     * @param  {string}
     * @return {Message}
     */
    public setCdExecutedCommand(cdExecutedCommand: string): Message {
        this.cdExecutedCommand = cdExecutedCommand;
        return this;
    }

    /**
     * Asigna la fecha de la ultima actualizacion al mensaje.
     * @param  {number}
     * @return {Message}
     */
    public setDtLastUpdate(date: number): Message {
        this.dtLastUpdate = date;
        return this;
    }

    /**
     * Retorna la fecha del mensaje.
     * @return {Date}
     */
    public getDate(): Date {
        return new Date(this.dtLastUpdate);
    }

    /**
     * Retorna el path donde debe enviarse el request con la acción del mensaje.
     * @return {string}
     */
    public getDsExecutionPath(): string {
        return this.dsExecutionPath;
    }

    /**
     * Setea el path donde debe enviarse el request con la acción del mensaje.
     * @param {string}
     */
    public setDsExecutionPath(v: string): void {
        this.dsExecutionPath = v;
    }

    /**
     * Retorna las acciones del mensaje accionable.
     * @return {MessageAction[]}
     */
    public getLsActions(): MessageAction[] {
        return this.lsActions;
    }

    /**
     * Setea las acciones del mensaje accionable
     * @param {MessageAction[]}
     */
    public setLsActions(v: MessageAction[]): void {
        this.lsActions = v;
    }

    /**
     * Retorna los campos del mensaje accionable.
     * @return {MessageInput[]}
     */
    public getLsInputs(): MessageInput[] {
        return this.lsInputs;
    }

    /**
     * Setea los campos del mensaje accionable
     * @param {MessageInput[]}
     */
    public setLsInputs(v: MessageInput[]): void {
        this.lsInputs = v;
    }

    /**
     * Setea el resultado de la validación de la acción del mensaje accionable.
     * @return {string}
     */
    public getDsValidation(): string {
        return this.dsValidation;
    }

    /**
     * Setea el resultado de la validación de la acción del mensaje accionable.
     * @param {string}
     */
    public setDsValidation(s: string): void {
        this.dsValidation = s;
    }

    /**
    * Retorna el nombre del participante externo.
    * @return {string}
    */
    public getDsExternalSenderName(): string {
        return this.dsExternalSenderName;
    }

    /**
    * Setea el nombre del participante externo.
    * @param {string}
    */
    public setDsExternalSenderName(dsExternalSenderName: string): void {
        this.dsExternalSenderName = dsExternalSenderName;
    }

    /**
    * Retorna el path de la imagen del participante externo.
    * @return {string}
    */
    public getExternalSenderImg(): string {
        return this.externalSenderImg;
    }

    /**
    * Setea el path de la imagen del participante externo.
    * @param {string}
    */
    public setExternalSenderImg(externalSenderImg: string): void {
        this.externalSenderImg = externalSenderImg;
    }

    public getLsButtons(): MessageButton[] {
        return this.lsButtons;
    }

    public setLsButtons(v: MessageButton[]): Message {
        this.lsButtons = v;
        return this;
    }

    /**
     * Setea el estado del mensaje.
     * SENT, SEEN, SEEN_BY_ALL y DELETED
     * @param {string}
     */
    public setMsgState(s: string): void {
        this.msgState = s;
    }

    public getMsgState(): String {
        return this.msgState;
    }

    get stateClass(): string {
        switch (this.msgState) {
            case 'SEEN':
                return "checkmark-outline";
            case 'SEEN_BY_ALL':
                return "checkmark-done-outline";
            default:
                return "time-outline";
        }
    }

    isDeleteLogic() {
        return this.msgState == 'DELETED';
    }


    /**
     * Formatea los inputs al formato adecuado exigido por el servicio de backend.
     * @return {MessageInput[]}
     */
    public formatInputsForRequest(): MessageInput[] {
        return this.lsInputs.map(input => {
            if (input.type === 'datetime-local') {
                input.value = new Date(input.value).toLocaleString();
            }
            return input;
        });
    }

    getPlainObject(){
        const plainObject = Object.assign({}, this);
        delete plainObject['cdExternalConversation'];
        delete plainObject['lsAnnexs'];
        delete plainObject['lsActions'];
        delete plainObject['lsButtons'];
        delete plainObject['lsInputs'];
        return plainObject;
    }
}
