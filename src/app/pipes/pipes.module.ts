import {NgModule} from '@angular/core';
import {RelativeDatePipe} from './relative-date.pipe';
import {EmoticonPipe} from './emoticon.pipe';
import {TranslatePipe} from './translate.pipe';
import {LinkPipe} from './link.pipe';
import {FilterActionPipe} from './filter-action/filter-action.pipe';

@NgModule({
    declarations: [RelativeDatePipe, EmoticonPipe, TranslatePipe, LinkPipe, FilterActionPipe],
    imports: [],
    exports: [RelativeDatePipe, EmoticonPipe, TranslatePipe, LinkPipe, FilterActionPipe]
})

export class PipesModule {
}
