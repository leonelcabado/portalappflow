/**
 * Created by Juan on 30/08/2017.
 */
import {Pipe, PipeTransform} from '@angular/core';

const URL_PATTERN = /\b(?:(https?|ftp|file):\/\/|www\.)[-A-Z0-9+()&@$#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|]/gi;
const PROTOCOL_PATTERN = /^[a-z]+:\/\//i;

@Pipe({name: 'link'})
export class LinkPipe implements PipeTransform {

    transform(value: string): string {
        var getHttpProtocol = this.getHttpProtocol;
        return value.replace(URL_PATTERN, function (text) {
            let url = text;
            if (!PROTOCOL_PATTERN.test(text)) {
                url = getHttpProtocol() + '//' + text;
            }
            return '<a class="ts-link-text">' + text + '</a>';
        });
    }

    getHttpProtocol(): string {
        return window.location.protocol.match(/https/) ? 'https:' : 'http:';
    }
}
