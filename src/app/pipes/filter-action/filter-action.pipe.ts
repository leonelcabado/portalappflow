import {Pipe, PipeTransform} from '@angular/core';
import {Action} from '../../model/action.entity';

/**
 * Pipe used to filter the actions list by name.
 */
@Pipe({
    name: 'filterAction',
})
export class FilterActionPipe implements PipeTransform {

    /**
     * Filter one text by name in the actions list.
     * Returns the actions list if the text is empty.
     * @param actions
     * @param text
     */
    transform(actions: Array<Action>, text: string): Array<Action> {
        if (!text || text.length === 0) {
            return actions;
        }
        text = text.toLocaleLowerCase();
        return this.filterAction(actions, text);
    }

    /**
     * Filter one text by name in the actions list.
     * @param actions
     * @param text
     */
    private filterAction(actions: Array<Action>, text: string): Array<Action> {
        if (actions && actions.length > 0) {
            return actions.filter((action: Action) => {
                return action.getName().toLocaleLowerCase().includes(text);
            });
        }
        return new Array();
    }
}
