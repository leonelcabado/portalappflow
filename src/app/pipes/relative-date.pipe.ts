import {DatePipe} from '@angular/common';
import {Pipe, PipeTransform} from '@angular/core';
import {TranslateService} from '../services/translate.service';
import {DateOutputFormat} from '../support-classes/date-output-format.class';

@Pipe({
    name: 'relativeDate',
    pure: false
})
export class RelativeDatePipe implements PipeTransform {

    constructor(private translateService: TranslateService) {
    }

    /**
     * Obtiene el lenguaje configurado y setea el formato del date.
     * @param  {any}    value
     * @param  {string} param
     * @return {any}
     */
    transform(value: any, param?: string): any {
        const datePipe: DatePipe = new DatePipe(this.translateService.getLanguaje());
        let formatPipe = DateOutputFormat.getRelativeDtCreatedOutputFormat(value, param);
        formatPipe = formatPipe.replace('OF', this.translateService.instant('OF'));
        return datePipe.transform(value, formatPipe);
    }

}