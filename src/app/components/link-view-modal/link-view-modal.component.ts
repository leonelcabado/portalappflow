import { Component, Input, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-link-view-modal',
  templateUrl: './link-view-modal.component.html',
  styleUrls: ['./link-view-modal.component.scss'],
})
export class LinkViewModalComponent implements OnInit {

  @Input() url: string;

  constructor(public sanitizer: DomSanitizer, private modalCtrl: ModalController) { }

  ngOnInit() {}

  closeModal(){
    this.modalCtrl.dismiss();
  }

  /**
   * Validates if the url has protocol.
   * @param url 
   * @returns 
   */
  validateProtocol(url: string): string{
    return (this.url.startsWith("https://") || this.url.startsWith("http://")) ? url : "https://"+ this.url;
  }

}
