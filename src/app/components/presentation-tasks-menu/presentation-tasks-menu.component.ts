import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-presentation-tasks-menu',
  templateUrl: './presentation-tasks-menu.component.html',
  styleUrls: ['./presentation-tasks-menu.component.scss'],
})
export class PresentationTasksMenuComponent implements OnInit {

  presentationTasks: any[];
  lastItem: boolean = false;

  constructor(private toastController: ToastController) { }

  ngOnInit() {}

  /**
   * Detects if it is the last item that is selected in the checkbox list.
   * @returns 
   */
  processEntry(event?: any){
    if(this.isLastChecked()){
      event.target.checked = true;
      this.presentToast();
    }
  }

  /**
   * Evaluate quantity of selected elements.
   * @returns 
   */
  isLastChecked(): boolean{
    let cont = 0;
    for (var i = 0 ; i < this.presentationTasks.length ; i++){
        if(this.presentationTasks[i].isChecked){
            cont += 1;
        } 
    }
    return cont == 0;
  }

  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Se debe seleccionar al menos un atributo',
      duration: 2000,
      color: 'dark',
    });
    toast.present();
  }

}
