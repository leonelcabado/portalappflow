import { Component, ViewChild } from '@angular/core';
import { LoadingController, PopoverController, ToastController, AlertController, IonInfiniteScroll, ModalController } from '@ionic/angular';
import { Observable, Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { BackButtonProvider } from '../../services/back-button.service';
import { UserProvider } from '../../services/user.service';
import { TaskService } from '../../services/task.service';
import { ConversationService } from '../../services/conversation.service';
import { RTMService } from '../../services/rtm.service';
import { TranslateService } from '../../services/translate.service';
import { NetworkService } from '../../services/network.service';
import { LogService } from '../../services/log.service';
import { PopoverService } from '../../services/popover.service';
import { AppConfigurationService } from '../../services/app-configuration.service';
import { Task} from '../../model/task.entity';
import { TaskMoreActionsPage } from '../task-more-actions/task-more-actions';
import { TaskFilterMenuPage } from '../task-filter-menu/task-filter-menu';
import { PresentationTasksMenuComponent } from 'src/app/components/presentation-tasks-menu/presentation-tasks-menu.component';
import { TaskFilter } from 'src/app/model/task-filter.entity';

@Component({
    selector: 'page-task-list',
    templateUrl: 'task-list.html',
    styleUrls: ['task-list.scss']
})
export class TaskListPage {
    @ViewChild( IonInfiniteScroll ) infiniteScroll: IonInfiniteScroll;
    tasks : Task[] = [];
    paging: any;
    onTaskChangeSubscription: Subscription;
    private scrollTop: number;
    private connectionSubscription: Subscription;
    public connectionOn: boolean;
    public showButtonPlus: boolean;
    pageResultTask: number;
    presentationTasks = [
        { val: 'Caso', isChecked: true },
        { val: 'Proceso', isChecked: false },
        { val: 'Actividad', isChecked: false },
        { val: 'Inicio', isChecked: true },
        { val: 'Alerta', isChecked: true },
        { val: 'Prioridad', isChecked: true },
    ];
    isFilteredPresentationTask = false;
    tasksFilters = new TaskFilter();
    isTaskFilters = false;
    public showButtonFilter: boolean;

    constructor(private taskService: TaskService,
                private conversationService: ConversationService,
                private rtmService: RTMService,
                private loadingCtrl: LoadingController,
                private popoverCtrl: PopoverController,
                private backButtonProvider: BackButtonProvider,
                public userProvider: UserProvider,
                private toastCtrl: ToastController,
                private translateService: TranslateService,
                private alertCtrl: AlertController,
                private networkService: NetworkService,
                protected logService: LogService,
                protected popoverService: PopoverService,
                private appConfigurationService: AppConfigurationService,
                private activatedRoute: ActivatedRoute,
                private router: Router,
                public modalController: ModalController,
                private popoverController: PopoverController) {

        this.onTaskChangeSubscription = this.rtmService.onTaskChange$.subscribe(_ => {
                this.resetTasks();
            }
        );

        this.activatedRoute.queryParams.subscribe((params: any) => {
            let navigation = this.router.getCurrentNavigation();
            if(navigation != null){
                if(navigation.extras.state && navigation.extras.state.filters){
                    //this.filtersTasks = navigation.extras.state.filters;
                    //this.isFiltered = true;
                }
            }
        });
    }

    ngOnInit(): void {
        this.checkConnection();
        this.showButtonPlus = this.showPlusButton();
        this.showButtonFilter = this.showFilterButton();
        this.scrollTop = 0;
    }

    private getTaskById(id: string): Task {
        for(let t of this.tasks){
            if(id == t.id){
                return t;
            }
        }
        return null;
    }

    private concatTasks(tasksList: Task[], newTasks: Task[]){
        if (tasksList && tasksList.length === 0 && newTasks && newTasks.length > 0) {
            return newTasks;
        }
        for(let t of newTasks){
            if(this.getTaskById(t.id) == null) {
                tasksList.push(t);
            }
        }
        return tasksList;
    }

    /**
     * Get the task list from server.
     */
    public getTasks(event?: any) {
        if(this.tasks.length == 0) this.userProvider.inLoadTasks = true;
        //this.userProvider.inLoadTasks = true;
        if(this.isTaskFilters)
            this.getTasksFilter(event);
        else
            this.getTasksWithoutFilter(event);
    }

    /**
     * Get the task list from server with parameters for filter.
     * @param event 
     */
    private getTasksFilter(event?: any){
        this.taskService.getTasks(this.pageResultTask, this.tasksFilters.lsFilters).subscribe((data: any) => {
            this.processGetTasks(data, event);
        },
            (err) => {
                this.userProvider.inLoadTasks = false;
                console.log(JSON.stringify(err));
            });
    }

    /**
     * Get the task list from server without parameters for filter.
     * @param event 
     */
    private getTasksWithoutFilter(event?: any){
        this.taskService.getTasks(this.pageResultTask).subscribe((data: any) => {
            this.processGetTasks(data, event);
        },
            (err) => {
                this.userProvider.inLoadTasks = false;
                console.log(JSON.stringify(err));
            });
    }

    processGetTasks(data: any, event?: any){
        this.tasks = this.concatTasks(this.tasks, data.tasks);
        this.paging = data.paging;
        this.userProvider.inLoadTasks = false;
        this.userProvider.nuTasks = this.tasks.length;
        if (event && event.target) {
            event.target.complete();
        }
        if (this.paging.pageNumber == this.paging.pages) {
            if (this.infiniteScroll) {
                this.infiniteScroll.disabled = true;
            }
        } else {
            this.pageResultTask += 1;
        }
    }

    ionViewDidEnter() {
        if(this.tasks.length == 0){
            this.pageResultTask = 1;
            this.getTasks();
        } 
        this.taskService.unreadedNotification = '';
    }

    showButtons(task) {
        if (task.buttonsShown)
            task.buttonsShown = false;
        else
            task.buttonsShown = true;
    }

    /**
     * Shows the modal to select more actions.
     * @param {Task} task
     * @param {any}  event
     */
    public moreActions(task: Task, event:any): void {
        event.stopPropagation();
        if (this.networkService.getConnected()) {
            this.popoverCtrl.create({
                component: TaskMoreActionsPage,
                componentProps: {actions: task.lsButtons},
                cssClass: 'actions-modal'
            }).then(tasksModal => {
                tasksModal.present();
                this.backButtonProvider.registerDismissable(tasksModal);
                tasksModal.onDidDismiss().then(executeAction => {
                    if (executeAction.data && executeAction.data.dsName && executeAction.data.dsName != 'BACKBUTTON') this.executeAction(task, executeAction.data.dsName)
                });
            });
        }
        else {
            this.errorNotConnected();
        }
    }

    /**
    * It returns the fab button style.
    * @return {string}
    */
    public getFabButtonStyle(): string {
        return this.appConfigurationService.getFabButtonStyle();
    }

    async openFilterMenu(){
        const modal = await this.modalController.create({
            component: TaskFilterMenuPage,
            componentProps: {tasksFilters: this.tasksFilters, isTaskFilters: this.isTaskFilters}
        });
        modal.onDidDismiss().then((resultFilters) => {
            if (resultFilters && resultFilters.data) {
                this.tasksFilters = resultFilters.data.tasksFilters ? resultFilters.data.tasksFilters : this.tasksFilters;
                this.isTaskFilters = resultFilters.data.isTaskFilters;
                this.resetTasks();
            }
        });
        return await modal.present();
    }

    openRelatedChat(objectId) {
        this.conversationService.getObjectChat(objectId, this.getObjectUrl(objectId), 'TSObjectConvCase', 1, 'CASE', true)
        .then( c => this.router.navigate(['/chat/' + c.cdConversation], {state: {conversation: c}}))
        .catch( error => this.logService.logError(error, this.userProvider.getUser()));
    }

    consult(task: Task) {
        if (this.networkService.getConnected()) {
            this.userProvider.setConsultedTask(task.id);
            this.router.navigate(['/iframe-view'], { state: this.taskService.createParamsFor(task, this.tasks, true), skipLocationChange: true });
        }
        else {
            this.errorNotConnected();
        }
    }

    executeAction(task: Task, action) {
        if (this.networkService.getConnected()) {
            this.loadingCtrl.create({
                spinner: 'dots',
                message: this.translateService.instant('EXECUTING_ACTION'),
            }).then(executing => {
                executing.present();
                this.taskService.executeAction(task, action)
                    .then(
                        (msj: string) => {
                            this.showToast(msj, "toastOk");
                            this.resetTasks();
                            executing.dismiss();
                        },
                        (err) => {
                            this.showToast(err, "toastError");
                            executing.dismiss();
                        }
                    );
            });
        }
        else {
            this.errorNotConnected();
        }
    }

    showToast($message: string, $class: string) {
        this.toastCtrl.create({
            message: $message,
            duration: 2000,
            position: 'bottom',
            cssClass: $class,
        }).then(toast => {
            toast.present();
        });
    }

    errorNotConnected() {
        this.alertCtrl.create({
            header: this.translateService.instant('NOT_CONNECTION_ERROR'),
            message: this.translateService.instant('NOT_CONNECTION_ERROR_MESSAGE'),
            buttons: [this.translateService.instant('ACCEPT')]
        }).then(alert => {
            this.backButtonProvider.registerDismissable(alert);
            alert.present();
        });
    }

    ngOnDestroy() {
        this.onTaskChangeSubscription.unsubscribe();
        this.connectionSubscription.unsubscribe();
    }

    /**
     * Open the page of the actions lists.
     */
    public openActionsList(): void {
        this.router.navigate(['/action-list'], {state: { previousPageData: this.taskService.getPageDataOfTaskList(this.tasks) }});
    }

    /**
     * Returns the current context name.
     * @return {string}
     */
    public getContextName(): string {
        return this.userProvider.getContextName();
    }

    /**
     * Download the log file.
     */
    public downloadLog(): void {
        this.logService.createLogFile(this.userProvider.getUser());
    }

    /**
     * Open the main menu page.
     * @param {any} event
     */
    public openMainMenu(event: any): void {
        this.popoverService.openMainMenu(event);
    }

    /**
     * Determines if it shows the fab buttons according to the scroll.
     * @param {any} event
     */
    public processShowButton(event: any): void {
        event.target.getScrollElement().then((scrollElement: any) => {
            if (scrollElement.scrollTop > this.scrollTop) {
                this.scrollTop = scrollElement.scrollTop;
                this.showButtonPlus = false;
                this.showButtonFilter = false;
                return;
            }
            this.scrollTop = scrollElement.scrollTop;
            this.showButtonPlus = this.showPlusButton();
            this.showButtonFilter = this.showFilterButton();
        });
    }

    /**
     * It checks the connection with the RTM service.
     */
    private checkConnection(): void {
        this.connectionOn = true;
        this.connectionSubscription = this.rtmService.onConnect$.subscribe((connected: boolean) => {
            this.connectionOn = connected;
        });
    }

    async openOptionsPresentation(ev: any) {
        const popover = await this.popoverController.create({
          component: PresentationTasksMenuComponent,
          translucent: true,
          event: ev,
          componentProps: {presentationTasks: this.presentationTasks}
        });
        popover.onDidDismiss().then(() => this.isFilteredPresentationTask = this.validDefaultPresentation());
        return await popover.present();
    }

    /**
     * Compares the default presentation object of the columns for the tasks with the current object.
     * @returns 
     */
    validDefaultPresentation(): boolean{
        let presentationDefaultTasks = [
            { val: 'Caso', isChecked: true },
            { val: 'Proceso', isChecked: false },
            { val: 'Actividad', isChecked: false },
            { val: 'Inicio', isChecked: true },
            { val: 'Alerta', isChecked: true },
            { val: 'Prioridad', isChecked: true },
        ];
        for (var i = 0 ; i < this.presentationTasks.length ; i++){
            if(this.presentationTasks[i].isChecked != presentationDefaultTasks[i].isChecked){
                return true;
            } 
        }
        return false;
    }

    /**
     * Determines column visibility for the task list.
     * @param taskValue 
     * @returns 
     */
    showTaskValue(taskValue: string): boolean {
        for(let value of this.presentationTasks){
            if(value.val == taskValue)
                return value.isChecked;
        }
        return false;
    }

    /**
     * Validates if the case, process and activity checkbox was not selected
     * @returns
     */
    validatePositionColumns(): boolean{
        return (this.presentationTasks 
            && !this.presentationTasks[0].isChecked 
            && !this.presentationTasks[1].isChecked 
            && !this.presentationTasks[2].isChecked);
    }

    /**
     * Reset paging and task list.
     */
    private resetTasks(): void {
        this.tasks = [];
        this.pageResultTask = 1;
        this.getTasks();
    }

    /**
     * It returns from app configuration if the header should be displayed.
     * @returns boolean
     */
    public showHeader(): boolean {
        return this.appConfigurationService.getShowHeader();
    }

    /**
     * It returns the plus button should be displayed from configuration.
     * @return {boolean}
     */
    public showPlusButton(): boolean {
        return this.appConfigurationService.showFabButton();
    }

    /**
     * Creates the object url to object conversation.
     * @param {string} objectId
     * @return {string}
     */
    private getObjectUrl(objectId: string): string {
        return 'SLCase%3F%3D' + objectId + '%26cdActionHidden%3DCONSULTAR_EXP';
    }

    /**
     * It returns the filter button should be displayed from configuration.
     * @return {boolean}
     */
    public showFilterButton(): boolean {
        return !this.appConfigurationService.getShowHeader();
    }

}
