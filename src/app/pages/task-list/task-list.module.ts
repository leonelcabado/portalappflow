import { NgModule } from '@angular/core';
import { TaskListPage } from './task-list';
import { PipesModule } from '../../pipes/pipes.module';
import { RouterModule, Routes } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { PresentationTasksMenuComponent } from 'src/app/components/presentation-tasks-menu/presentation-tasks-menu.component';

const routes: Routes = [
    {
        path: 'list',
        component: TaskListPage,
    }
];

@NgModule({
    declarations: [
        TaskListPage,
        PresentationTasksMenuComponent
    ],
    imports: [
        RouterModule.forChild(routes),
        PipesModule,
        IonicModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        SharedModule
    ],
})
export class TaskListPageModule {
}
