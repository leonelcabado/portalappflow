import {NgModule} from '@angular/core';
import {ContextListPage} from './context-list';
import {RouterModule, Routes} from '@angular/router';
import {CommonModule} from '@angular/common';
import {IonicModule} from '@ionic/angular';

const routes: Routes = [
    {
        path: 'context-list',
        component: ContextListPage,
    },
];

@NgModule({
    declarations: [
        ContextListPage,
    ],
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        IonicModule
    ],
})
export class ContextListPageModule {
}
