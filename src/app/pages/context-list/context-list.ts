import { Component } from '@angular/core';
import { AlertController, LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { ContextsProvider } from '../../services/contexts.service';
import { BackButtonProvider } from '../../services/back-button.service';
import { TranslateService } from '../../services/translate.service';
import { Context } from '../../model/context.entity';
import { LogService } from 'src/app/services/log.service';
import { AppConfigurationService } from '../../services/app-configuration.service';

@Component({
    selector: 'page-context-list',
    templateUrl: 'context-list.html',
    styleUrls: ['context-list.scss']
})
export class ContextListPage {
    contexts: Context[] = [];

    constructor(public contextsProvider: ContextsProvider,
                private alertCtrl: AlertController,
                private backButtonProvider: BackButtonProvider,
                private loadingCtrl: LoadingController,
                private translateService: TranslateService,
                private appConfigurationService: AppConfigurationService,
                private router: Router,
                private location: Location,
                private logService: LogService) {}

    ionViewDidEnter() {
        this.loadContexts();
    }

    public addContext() {
        this.router.navigate(['/context-form']);
    }

    public updateContext(contextId) {
        for (let c of this.contexts) {
            if (c.id == contextId) this.router.navigate(['/context-form'], {state: {context: c}});

        }
    }

    public removeContext(context) {
        this.alertCtrl.create({
            header: 'Confirm',
            message: 'Are you sure you want to remove this context?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel'
                },
                {
                    text: 'Accept',
                    handler: () => {
                        this.loadingCtrl.create({
                            spinner: 'dots',
                            message: 'Removing context',
                        }).then(loading => {
                            loading.present();
                            let index = this.contexts.indexOf(context);
                            this.contextsProvider.removeContext(context)
                                .then(() => {
                                    loading.dismiss();
                                    this.contexts.splice(index, 1);
                                })
                                .catch(error => {
                                    //this.logService.logError(error, null);
                                    loading.dismiss();
                                });
                        });
                    }
                }
            ]
        }).then(alert => {
            this.backButtonProvider.registerDismissable(alert);
            alert.present();
        });
    }

    public goToLogin() {
        this.location.back();
    }

    /**
     * It loads the contexts list.
     */
    private loadContexts(): void {
        this.contextsProvider.getContexts().then((contexts: Array<Context>) => {
            this.contexts = contexts;
        });
    }

    /**
    * It returns the fab button style.
    * @return {string}
    */
    public getFabButtonStyle(): string {
        return this.appConfigurationService.getFabButtonStyle();
    }

}
