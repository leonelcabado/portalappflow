import { Component, EventEmitter, Output } from '@angular/core';
import { Platform, AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { AppMinimize } from '@ionic-native/app-minimize/ngx';
import { Location } from '@angular/common';
import { UserProvider } from '../../services/user.service';
import { NotificationProvider } from '../../services/notification.service';
import { BackButtonProvider } from '../../services/back-button.service';
import { RTMService } from '../../services/rtm.service';
import { TaskService } from '../../services/task.service';
import { ConversationService } from '../../services/conversation.service';
import { NetworkService } from '../../services/network.service';
import { APP } from '../../app.config';
import { ConfigurationService } from '../../services/configuration.service';
import { TranslateService } from '../../services/translate.service';
import { AppConfigurationService } from '../../services/app-configuration.service';
import { Task } from '../../model/task.entity';
import { MainMenuPage } from "../main-menu/main-menu";
import { DbProvider } from '../../services/db.service';

@Component({
    selector: 'page-main',
    templateUrl: 'main.html',
    styleUrls: ['main.scss']
})
export class MainPage {

    constructor(private platform: Platform,
                private location: Location,
                private backButtonProvider: BackButtonProvider,
                private userProvider: UserProvider,
                private dbProvider: DbProvider,
                private rtm: RTMService,
                private taskService: TaskService,
                private conversationService: ConversationService,
                private configurationService: ConfigurationService,
                private notificationProvider: NotificationProvider,
                private appMinimize: AppMinimize,
                private networkService: NetworkService,
                private translateService: TranslateService,
                private appConfigurationService: AppConfigurationService,
                private alertCtrl: AlertController,
                private router: Router) {
        this.subscribeHardwareBackButton();
        this.platform.resume.subscribe(() => {
            this.checkRedirectAction();
        });
    }

    ngOnInit() {
        if(!this.userProvider.getContext() && !this.dbProvider.getConnection()) {
            window.location.assign('/');
        }else{
            if (this.userProvider.getUser()) {
                this.notificationProvider.platformState = 'active';
                if (this.networkService.getConnected()) this.taskService.showNuTasks = true;
                this.configurationService.init();
                this.checkRedirectAction();
            }
        }  
    }

    checkRedirectAction() {
        if (this.userProvider.redirectAction == this.userProvider.OPEN_CONVERSATION_ACTION) {
            let conversation = this.conversationService.getLocalConversationById(parseInt(this.userProvider.redirectTargetId));
            if (conversation) {
                this.router.navigate(['/chat/' + conversation.cdConversation ], {state: {conversation: conversation}});
            }
            this.userProvider.redirectAction = null;
            this.userProvider.redirectTargetId = null;
        }
        else if (this.userProvider.redirectAction == this.userProvider.OPEN_TASK_ACTION && this.userProvider.redirectTargetId && this.userProvider.redirectDsCase) {
            let task = new Task();
            task.id = this.userProvider.redirectTargetId;
            task.dsCase = this.userProvider.redirectDsCase;
            task.setDsActivity(this.userProvider.redirecDsActivity);
            if (this.networkService.getConnected()) {
                this.userProvider.setConsultedTask(task.id);
                this.router.navigate(['/iframe-view'], {state: this.taskService.createParamsFor(task, null, false) });
            }
            else {
                this.errorNotConnected();
            }
            this.userProvider.redirectAction = null;
            this.userProvider.redirectTargetId = null;
            this.userProvider.redirectDsCase = null;
        }
    }

    errorNotConnected() {
        this.alertCtrl.create({
            header: this.translateService.instant('NOT_CONNECTION_ERROR'),
            message: this.translateService.instant('NOT_CONNECTION_ERROR_MESSAGE'),
            buttons: [this.translateService.instant('ACCEPT')]
        }).then(alert => {
            this.backButtonProvider.registerDismissable(alert);
            alert.present();
        });
    }

    /**
     * Subscribe to hardware back button in devices.
     * @return {undefined}
     */
    private subscribeHardwareBackButton(): void {
        this.platform.backButton.subscribeWithPriority(1, (processNextHandler) => {
            if (!this.backButtonProvider.back()) {
                if (this.userProvider.getUser() && this.userProvider.getUser().createdAt == 0) {
                    return;
                }
                if (this.checkLocationBack()) {
                    this.location.back();
                    return;
                }
                if (this.checkAppMinimize()) {
                    if(this.userProvider.socketId) {
                        this.rtm.setState(0);
                    }
                    this.backButtonProvider.resetViewMap();
                    this.appMinimize.minimize();
                    return;
                }
                processNextHandler();
            }
        });
    }

    /**
     * It checks if it should do a location back.
     * @return {boolean}
     */
    private checkLocationBack(): boolean {
        return this.router.url === '/main/dashboard/list' && !this.backButtonProvider.checkLastViewName('login') && !this.backButtonProvider.checkLastViewName('action-list');
    }

    /**
     * It checks if it should minimize the app.
     * @return {boolean}
     */
    private checkAppMinimize(): boolean {
        return this.router.url === '/' || this.router.url === '/login' || this.router.url.indexOf('main') !== -1;
    }

}
