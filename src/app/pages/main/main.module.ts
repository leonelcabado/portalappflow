import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {MainPage} from './main';
import {DirectivesModule} from '../../directives/directives.module';
import {PipesModule} from '../../pipes/pipes.module';
import {RouterModule, Routes} from '@angular/router';
import {MainTabsPageModule} from '../main-tabs/main-tabs.module';
import {IonicModule} from '@ionic/angular';
import {CommonModule} from '@angular/common';

const routes: Routes = [
    {
        path: 'main',
        component: MainPage,
        children: [
            {
                path: 'zurich',
                loadChildren: '../zurich-card-list/zurich-card-list.module#ZurichCardListPageModule',
            },
            {
                path: 'dashboard',
                loadChildren: '../dashboard-list/dashboard-list.module#DashboardListPageModule',
            },
            {
                path: 'chat',
                loadChildren: '../chat-list/chat-list.module#ChatListPageModule',
            },
            {
                path: 'task',
                loadChildren: '../task-list/task-list.module#TaskListPageModule',
            },
            {
                path: 'forms',
                loadChildren: '../form-list/form-list.module#FormListPageModule',
            },
            {
                path: 'form',
                loadChildren: '../browse-form/browse-form.module#BrowseFormPageModule',
            },
            {
                path: 'page',
                loadChildren: '../page-iframe/page-iframe.module#PageIframePageModule',
            }
        ]
    },
];

@NgModule({
    declarations: [
        MainPage
    ],
    imports: [
        RouterModule.forChild(routes),
        PipesModule,
        IonicModule,
        MainTabsPageModule,
        CommonModule
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MainPageModule {
}
