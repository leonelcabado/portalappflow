import {NgModule} from '@angular/core';
import {BotListPage} from './bot-list';
import {RouterModule, Routes} from '@angular/router';
import {IonicModule} from '@ionic/angular';
import {CommonModule} from '@angular/common';

const routes: Routes = [
    {
        path: 'bot-list',
        component: BotListPage,
    },
];

@NgModule({
    declarations: [
        BotListPage,
    ],
    imports: [
        RouterModule.forChild(routes),
        IonicModule,
        CommonModule
    ],
})
export class BotListPageModule {
}
