import {Component} from '@angular/core';
import {PopoverController, AlertController} from '@ionic/angular';
import {DomSanitizer} from '@angular/platform-browser';


//pages
import {BotCommandsPage} from '../bot-commands/bot-commands';

//providers
import {BackButtonProvider} from '../../services/back-button.service';
import {UserProvider} from '../../services/user.service';


//services
import {ParticipantService} from '../../services/participant.service'
import {ConversationService} from '../../services/conversation.service'
import {TranslateService} from '../../services/translate.service'
import {NetworkService} from '../../services/network.service'


//models
import {Participant} from '../../model/participant.entity';
import {Command} from '../../model/command.entity';
import {Router} from "@angular/router";
import { LogService } from 'src/app/services/log.service';


@Component({
    selector: 'page-bot-list',
    templateUrl: 'bot-list.html',
    styleUrls: ['bot-list.scss']
})
export class BotListPage {
    bots: Participant[] = [];
    opening: boolean = false;

    constructor(private alertCtrl: AlertController,
                public sanitizer: DomSanitizer,
                private popoverCtrl: PopoverController,
                private backButtonProvider: BackButtonProvider,
                public userProvider: UserProvider,
                private participantService: ParticipantService,
                private conversationService: ConversationService,
                private translateService: TranslateService,
                private networkService: NetworkService,
                private router: Router,
                private logService: LogService) {
    }

    ngOnInit() {
        this.bots = this.participantService.getBots();
    }

    openCommands(bot) {
        //if(this.networkService.getConnected()) {
        this.conversationService.getCommands(bot)
            .then((xResult: Command[]) => {
                if (xResult.length > 0) {
                    bot.lsCommands = xResult;
                    this.popoverCtrl.create({
                        component: BotCommandsPage,
                        componentProps: {bot: bot}
                    }).then((commandsModal) => {
                        commandsModal.present();
                        this.backButtonProvider.registerDismissable(commandsModal);
                        commandsModal.onDidDismiss().then(command => {
                            //if(this.networkService.getConnected()){
                            if (command && command.data) {
                                this.openChat(bot, command.data);
                            }
                        });
                    });
                } else {
                    this.alertCtrl.create({
                        header: this.translateService.instant('NOT_COMMAND_LIST'),
                        message: this.translateService.instant('NOT_COMMAND_LIST_MESSAGE'),
                        buttons: [this.translateService.instant('ACCEPT')]
                    }).then(alert => {
                        this.backButtonProvider.registerDismissable(alert);
                        alert.present();
                    });
                }
            })
            .catch(error => {
                this.logService.logError(error, this.userProvider.getUser());
                this.alertCtrl.create({
                    header: this.translateService.instant('NOT_COMMAND_LIST_ERROR'),
                    message: this.translateService.instant('NOT_COMMAND_LIST_ERROR_MESSAGE'),
                    buttons: [this.translateService.instant('ACCEPT')]
                }).then(alert => {
                    this.backButtonProvider.registerDismissable(alert);
                    alert.present();
                });
            });
        //}else{
        //  this.errorNotConnected();
        //}

    }

    openChat(bot: Participant, command?: Command) {
        if (!this.opening) {
            this.opening = true;
            this.createConversation(bot, command);
        }
    }

    createConversation(bot: Participant, command?: Command) {
        this.conversationService.getConversationByParticipant(bot, true)
            .then(conversation => {
                this.router.navigate(['/chat/' + conversation.cdConversation ], {state: {conversation: conversation, command: command}});
                this.opening = false;
            })
            .catch(error => {
                this.logService.logError(error, this.userProvider.getUser());
            });
    }

    errorNotConnected() {
        this.alertCtrl.create({
            header: this.translateService.instant('NOT_CONNECTION_ERROR'),
            message: this.translateService.instant('NOT_CONNECTION_ERROR_MESSAGE'),
            buttons: [this.translateService.instant('ACCEPT')]
        }).then(alert => {
            this.backButtonProvider.registerDismissable(alert);
            alert.present();
        });
    }
}
