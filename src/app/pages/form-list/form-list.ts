import { Component, ViewChild, ElementRef } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Conversation } from '../../model/conversation.entity';
import { UserProvider } from '../../services/user.service';
import { AuthProvider } from '../../services/auth.service';
import { ConversationService } from '../../services/conversation.service';
import { LogService } from '../../services/log.service';
import { PopoverService } from '../../services/popover.service';
import { TranslateService } from '../../services/translate.service';
import { TaskService } from '../../services/task.service';
import { AppConfigurationService } from '../../services/app-configuration.service';
import { RTMService } from '../../services/rtm.service';

@Component({
    selector: 'page-form-list',
    templateUrl: 'form-list.html',
    styleUrls: ['form-list.scss']
})
export class FormListPage {

    url: SafeResourceUrl;
    @ViewChild('frame') frame: ElementRef;
    processMessageFn: any;
    public searchClicked: boolean;
    private iframeScroll: number;
    private connectionSubscription: Subscription;
    public connectionOn: boolean;

    constructor(private userProvider: UserProvider,
                private domSanitizer: DomSanitizer,
                protected authProvider: AuthProvider,
                private router: Router,
                protected logService: LogService,
                protected popoverService: PopoverService,
                protected translateService: TranslateService,
                protected taskService: TaskService,
                private appConfigurationService: AppConfigurationService,
                private rtmService: RTMService,
                private conversationService: ConversationService) {
    }

    ngOnInit(): void {
        this.checkConnection();
        this.iframeScroll = 0;
        this.searchClicked = false;
        this.url = this.createSecurityUrl();
    }

    ngOnDestroy() {
        this.connectionSubscription.unsubscribe();
    }

    ionViewDidEnter() {
        this.processMessageFn = (event) => this.processMessage(event);
        window.addEventListener('message', this.processMessageFn, false);
    }

    ionViewWillLeave() {
        window.removeEventListener('message', this.processMessageFn, false);
    }

    processMessage(event){
        switch (event.data.operation) {
            case 'updateForm':
                this.router.navigate(['/iframe-view'], this.getNavigationConfigurationToIframe('SLGenericDocumentUpdate&' + this.taskService.getParametersToFormsURL(event.data), this.translateService.instant('UPDATE')));
                break;
            case 'showForm':
                this.router.navigate(['/iframe-view'], this.getNavigationConfigurationToIframe('SLGenericDocumentShow&' + this.taskService.getParametersToFormsURL(event.data), this.translateService.instant('CONSULT')));
                break;
            case 'deleteForm':
                this.router.navigate(['/iframe-view'], this.getNavigationConfigurationToIframe('SLGenericDocumentDelete&' + this.taskService.getParametersToFormsURL(event.data), this.translateService.instant('DELETE')));
                break;
            case 'browseForm':
                const cdDocumentName = event.data.cdDocumentNameHidden;
                this.router.navigate(['/browse/' + cdDocumentName], { state: { cdDocumentName: cdDocumentName, previousPageData: this.getPageData() }});
                break;
            case 'myTasks':
                this.router.navigate(['/main/task/list'], {
                    state: {
                        filters: {
                            lsFiltersAlert: [],
                            lsFiltersPriority: [],
                            filterProcess: event.data.process
                        }
                    }
                });
                break;
            case 'createForm':
                this.router.navigate(['/iframe-view'], this.getNavigationConfigurationToIframe('SLGenericDocumentCreate&cdDocumentNameHidden=' + event.data.cdDocumentNameHidden + '&mobile=true&isEmbed=true', this.translateService.instant('NEW')));
                break;
            case 'startProcess':
                this.router.navigate(['/iframe-view'], this.getNavigationConfigurationToIframe('SLProcessDispatcher&cdAsunto=' + event.data.cdAsunto + '&cdProcess=' + event.data.cdProcess + '&buttonHidden=new&mobile=true&isEmbed=true', this.translateService.instant('NEW')));
                break;
            case 'openTask':
                this.router.navigate(['/iframe-view'], this.getNavigationConfigurationToIframe('SLExpedienteConsulta&nuExpedienteHidden=' + event.data.nuExpedienteHidden + '&nuExpediente=' + event.data.nuExpediente + '&nuExp=' + event.data.nuExp + '&cdActionHidden=CONSULTAR_EXP&mobile=true&isEmbed=true', this.translateService.instant('CONSULT')));
                break;
            case 'openTaskChat':
                this.openRelatedChat(event.data.idTask);
                break;
            case 'scrollChange':
                this.processScrollEvent(event);
                break;
        }
    }

    /**
     * Returns the navigation configuration to load iframe.
     * @param  {string} url
     * @param  {string} title
     * @return {any}
     */
    private getNavigationConfigurationToIframe(url: string, title: string): any {
        return {
            state: {
                url: url,
                title: title,
                previousPageData: this.getPageData()
            },
            skipLocationChange: true
        };
    }

    /**
     * Returns the page data for navigation.
     * @return {any}
     */
    private getPageData(): any {
        return {
            route: '/main/forms/list',
            data: undefined
        };
    }

    /**
    * It returns the fab button style.
    * @return {string}
    */
    public getFabButtonStyle(): string {
        return this.appConfigurationService.getFabButtonStyle();
    }

    /**
     * Open related chat to the object.
     * @param {string} objectId
     */
    public openRelatedChat(objectId: string): void {
        this.conversationService.getObjectChat(objectId, this.getObjectUrl(objectId), 'TSObjectConvCase', 1, 'CASE', true).then((conversation: Conversation) => {
            this.router.navigate(['/chat/' + conversation.cdConversation], { state: { conversation: conversation } });
        });
    }

    /**
     * Creates the object url to object conversation.
     * @param {string} objectId
     * @return {string}
     */
    private getObjectUrl(objectId: string): string {
        return 'SLCase%3F%3D' + objectId + '%26cdActionHidden%3DCONSULTAR_EXP';
    }

    /**
     * Open the page of the actions lists.
     */
    public openActionsList(): void {
        this.router.navigate(['/action-list'], { state: { previousPageData: this.getPageData() }});
    }

    /**
     * Download the log file.
     */
    public downloadLog(): void {
        this.logService.createLogFile(this.userProvider.getUser());
    }

    /**
     * Returns the current context name.
     * @return {string}
     */
    public getContextName(): string {
        return this.userProvider.getContextName();
    }

    /**
     * Open the main menu page.
     * @param {any} event
     */
    public openMainMenu(event: any): void {
        this.popoverService.openMainMenu(event);
    }

    /**
     * Show or hide the search bar.
     */
    public toggleSearchBar(): void {
        this.searchClicked = !this.searchClicked;
    }

    /**
     * Filter to forms and tasks.
     * @param {any} event
     */
    public filterFormsAndTasks(event: any): void {
        if (event && event.detail) {
            this.sentEventToIframe('filter-forms-and-tasks', event.detail.value);
        }
    }

    /**
     * Create the safe path for the iframe.
     * @return {SafeResourceUrl}
     */
    private createSecurityUrl(): SafeResourceUrl {
        return this.domSanitizer.bypassSecurityTrustResourceUrl(this.userProvider.getContextUrl() + '/SLResourceShorcut?access_token=' + this.authProvider.getToken() +'&_idCreateSession=true&_idExecuteInMainFrame=false&_idResource=SLExecutionsUI&mobile=true&refresh_token=' + this.userProvider.getRefreshToken());
    }

    /**
     * Process a scroll event and set scroll in iframe.
     * @param {any} iframeEvent
     */
    private processScrollEvent(iframeEvent: any): void {
        if (iframeEvent.data.iframeScroll === 0 && this.iframeScroll > 0) {
            this.sentEventToIframe('set-scroll', this.iframeScroll);
            return;
        }
        this.iframeScroll = iframeEvent.data.iframeScroll;
    }

    /**
     * Send a event to embedded iframe.
     * @param {string} operation
     * @param {any}    value
     */
    private sentEventToIframe(operation: string, value: any): void {
        if (this.frame && this.frame.nativeElement && this.frame.nativeElement.contentWindow) {
            this.frame.nativeElement.contentWindow.postMessage({operation: operation, value: value}, '*');
        }
    }

    /**
     * Reload the embedded iframe.
     */
    public reloadIframe(): void {
        this.url = this.createSecurityUrl();
    }

    /**
     * It checks the connection with the RTM service.
     */
    private checkConnection(): void {
        this.connectionOn = true;
        this.connectionSubscription = this.rtmService.onConnect$.subscribe((connected: boolean) => {
            this.connectionOn = connected;
        });
    }

    /**
     * It returns from app configuration if the header should be displayed.
     * @returns boolean
     */
    public showHeader(): boolean {
        return this.appConfigurationService.getShowHeader();
    }

    /**
     * It returns the fab button should be displayed from configuration.
     * @return {boolean}
     */
    public showFabButton(): boolean {
        return this.appConfigurationService.showFabButton();
    }

}
