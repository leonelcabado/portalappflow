import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { PageConfiguration } from '../../model/page-configuration.entity';
import { Action } from '../../model/action.entity';
import { AppConfigurationService } from '../../services/app-configuration.service';
import { LogService } from '../../services/log.service';
import { PopoverService } from '../../services/popover.service';
import { UserProvider } from '../../services/user.service';
import { ActionService } from '../../services/action.service';
import { CaseService } from '../../services/case.service';

@Component({
  selector: 'app-zurich-card-list-page',
  templateUrl: './zurich-card-list.html',
  styleUrls: ['./zurich-card-list.scss'],
})
export class ZurichCardListPage implements OnInit {

	private cdProcess: string;
	public pageConfiguration: PageConfiguration;
	public sinisters: Array<any>;
	public actions: Array<any>;

	constructor(private router: Router,
				private platform: Platform,
				private activatedRoute: ActivatedRoute,
				private logService: LogService,
				private popoverService: PopoverService,
				private actionService: ActionService,
				private caseService: CaseService,
				private userProvider: UserProvider,
  				private appConfigurationService: AppConfigurationService) {}

	ngOnInit() {
		this.cdProcess = 'd1612f5dd0564bf2b2cf5f71ef4bac01';
		this.setPageConfiguration();
        this.activatedRoute.queryParams.subscribe((params: any) => {
        	this.getSinisters();
    	});
	}

	ionViewDidEnter() {
		this.appConfigurationService.setStatusBar(this.pageConfiguration);
	}

	/**
	 * It sets the page configuration.
	 */
	private setPageConfiguration(): void {
	    const configuration = this.router.getCurrentNavigation().extras.state.configuration;
	    if (configuration) {
	        this.pageConfiguration = configuration;
	    }
	}

    /**
     * Download the log file.
     */
    public downloadLog(): void {
        this.logService.createLogFile(this.userProvider.getUser());
    }

    /**
     * Open the main menu page.
     * @param {any} event
     */
    public openMainMenu(event: any): void {
        this.popoverService.openMainMenu(event);
    }

	/**
	* It returns the fab button style.
	* @return {string}
	*/
	public getFabButtonStyle(): string {
		return this.appConfigurationService.getFabButtonStyle();
	}

	/**
	 * It gets the all user sinisters.
	 */
	public getSinisters(): void {
    	this.caseService.getCases(1, `cdProcess=EQ:${this.cdProcess},cdUser=EQ:${this.userProvider.getUsername()}`).then((response: any) => {
    		this.formatSinisters(response.cases);
    	});
	}

	/**
	 * It formats the sinisters list.
	 * @param {Array<any>} cases
	 */
	private formatSinisters(cases: Array<any>): void {
		if (cases && cases.length > 0) {
			this.sinisters = cases.map((caseObject: any, index: number) => {
				return this.formatSinister(caseObject, index);
			}).filter((formattedSinister: any) => {
				return formattedSinister !== undefined;
			});
			return;
		}
		this.sinisters = new Array<any>();
	}

	/**
	 * It returns the object to view of the one sinister.
	 * @param  {any} caseObject
	 * @param  {number} index
	 * @return {any}
	 */
	private formatSinister(caseObject: any, index: number): any {
		if (caseObject.forms && caseObject.forms.REGI1626807100) {
			const sinisterForm = caseObject.forms.REGI1626807100;
			if (sinisterForm) {
				const formFields = sinisterForm.fields;
				const sinisterType = formFields.tpSiniestro;
				const nuCase = caseObject.documentKeyMain;
				return {
					type: sinisterType,
					typeTitle: this.getTypeTitle(sinisterType),
					iconType: this.getIconType(sinisterType),
					nuCase: nuCase,
					nuFolio: this.formatNuCase(nuCase),
					report: formFields.dtStore,
					policy: this.formatPolicy(formFields),
					product: formFields.dsProducto,
					stage: formFields.estadoDelSiniestro,
					dsNameCurrentActivity: caseObject.dsNameCurrentActivity,
					dsNameProcess: caseObject.dsNameProcess,
					opened: index === 0 ? true : false
				}
			}
		}
	}

	/**
	 * It replaces the beginning zeros. 
	 * @param  {string} nuCase
	 * @return {string}
	 */
	private formatNuCase(nuCase: string): string {
		if (nuCase) {
			return nuCase.replace(/\b0+/g, '');
		}
	}

	/**
	 * It formats the policy
	 * @param  {any}
	 * @return {string}
	 */
	private formatPolicy(formFields: any): string {
		if (formFields && formFields.antiguedad) {
			return `${formFields.nroPza} - ${formFields.dsRamo} - ${formFields.nuPoliza} - ${formFields.antiguedad}`;
		}
		return `${formFields.nroPza} - ${formFields.dsRamo} - ${formFields.nuPoliza}`;
	}

    /**
     * It opens to create new sinister.
     */
    public openAddSinister(): void {
    	if (this.actionService.getActionsList()) {
            if (this.actionService.getActionsList().mostUsed && this.actionService.getActionsList().rest) {
            	this.actions = this.actionService.getActionsList().mostUsed.concat(this.actionService.getActionsList().rest);
            }
            const addSinisterAction = this.actions.find((action: Action) => {
            	return action.getCode() === `${this.cdProcess}@|@1`;
            });
            if (addSinisterAction) {
    			this.router.navigate(['/iframe-view'], {state: {url: addSinisterAction.getUrl(), title: addSinisterAction.getName(), previousPageData: this.getPageData()}, skipLocationChange: true });
            }
        }
    }

    /**
     * It shows one sinister.
     * @param {any} sinister
     */
    showSinister(sinister: any): void {
    	this.userProvider.setConsultedTask(sinister.nuCase);
		this.router.navigate(['/iframe-view'], { state: {url: `SLExpedienteConsulta&nuExpedienteHidden=${sinister.nuCase}&cdActionHidden=CONSULTAR_EXP&mobile=true&isEmbed=true`, title: sinister.dsNameProcess, previousPageData: this.getPageData()}, skipLocationChange: true });
    }

    /**
     * It returns the icon type.
     * @param  {number} sinisterType
     * @return {string}
     */
    public getIconType(sinisterType: number): string {
		switch (sinisterType) {
			case 1:
				return 'shield';
			case 4:
				return 'medical_services';
			case 10: 
				return 'volunteer_activism';
			case 13: 
				return 'paid';
			default: 
				return '';
		}
    }

    /**
     * It returns the type title.
     * @param  {number} sinisterType
     * @return {string}
     */
    public getTypeTitle(sinisterType: number): string {
		switch (sinisterType) {
			case 1:
				return 'Fraude';
			case 4:
				return 'Salud';
			case 10: 
				return 'Eventos de Vida';
			case 13: 
				return 'Desempleo';
			default: 
				return '';
		}
    }

    /**
     * Returns the page data for navigation.
     * @return {any}
     */
    private getPageData(): any {
        return {
            route: '/main/zurich/card-list',
            data: undefined
        };
    }

    /**
     * It opens the closed card.
     * @param {any} sinister
     */
    public openCard(sinister: any): void {
    	this.closeAllCards();
    	sinister.opened = true;
    }

    /**
     * It closes the all cards.
     */
    private closeAllCards(): void {
    	if (this.sinisters && this.sinisters.length > 0) {
    		this.sinisters.forEach((sinister: any) => {
    			sinister.opened = false;
    		});
    	}
    }

    /**
     * It checks in what stage is the sinister.
     * @param  {any}     sinister
     * @param  {number}  stage
     * @return {boolean}
     */
    public checkStage(sinister: any, stage: number): boolean {
    	if (sinister) {
    		return sinister.stage === stage;
    	}
    	return false;
    }

    /**
     * It returns the progress class according to sinister stage.
     * @param  {any}    sinister
     * @return {string}
     */
    public getProgressClass(sinister: any): string {
    	switch (sinister.stage) {
			case 1:
				return 'stage-progress-reported';
			case 2:
				return 'stage-progress-reception';
			case 3: 
				return 'stage-progress-resolution';
			case 4: 
				return 'stage-progress-closed';
			default: 
				return '';
		}
    }

}
