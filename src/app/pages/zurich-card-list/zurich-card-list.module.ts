import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { ZurichCardListPage } from './zurich-card-list';

const routes: Routes = [
    {
        path: 'card-list',
        component: ZurichCardListPage,
    },
];

@NgModule({
    declarations: [
        ZurichCardListPage,
    ],
    imports: [
        RouterModule.forChild(routes),
        FormsModule,
        ReactiveFormsModule,
        IonicModule,
        CommonModule
    ],
})
export class ZurichCardListPageModule {}
