import {NgModule} from '@angular/core';
import {GroupParticipantMenuPage} from './group-participant-menu';
import {PipesModule} from '../../pipes/pipes.module';
import {RouterModule, Routes} from '@angular/router';
import {IonicModule} from '@ionic/angular';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

const routes: Routes = [
    {
        path: 'group-participant-menu',
        component: GroupParticipantMenuPage,
    },
];

@NgModule({
    declarations: [
        GroupParticipantMenuPage,
    ],
    imports: [
        RouterModule.forChild(routes),
        PipesModule,
        IonicModule,
        CommonModule,
        FormsModule
    ],
})
export class GroupParticipantMenuPageModule {
}
