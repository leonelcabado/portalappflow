import {Component} from '@angular/core';
import {AlertController, ToastController, LoadingController, PopoverController} from '@ionic/angular';
import {Router} from '@angular/router';

//models
import {Participant} from '../../model/participant.entity';
import {Conversation} from '../../model/conversation.entity';

//providers
import {BackButtonProvider} from '../../services/back-button.service';
import {DbProvider} from '../../services/db.service';
import {UserProvider} from '../../services/user.service';

//services
import {ConversationService} from '../../services/conversation.service';
import {NetworkService} from '../../services/network.service';
import {TranslateService} from '../../services/translate.service';
import { LogService } from 'src/app/services/log.service';


@Component({
    selector: 'page-group-participant-menu',
    templateUrl: 'group-participant-menu.html',
    styleUrls: ['group-participant-menu.scss']
})
export class GroupParticipantMenuPage {

    participant: Participant;
    conversation: Conversation;
    canEdit: boolean = false;

    constructor(private userProvider: UserProvider,
                private alertCtrl: AlertController,
                private backButtonProvider: BackButtonProvider,
                private viewCtrl: PopoverController,
                private toastCtrl: ToastController,
                private loadingCtrl: LoadingController,
                private conversationService: ConversationService,
                private networkService: NetworkService,
                private dbProvider: DbProvider,
                private translateService: TranslateService,
                private router: Router,
                private logService: LogService) {
    }

    ngOnInit() {
    }

    run() {
        this.alertCtrl.create({
            header: 'Functionality in development.',
            message: 'This functionality is being developed.',
            buttons: ['OK']
        }).then(alert => {
            this.close().then(() => {
                this.backButtonProvider.registerDismissable(alert);
                alert.present();
            });
        });
    }

    sendMessage(participant: Participant) {
        this.conversationService.getConversationByParticipant(participant, true)
            .then(conversation => {
                this.router.navigate(['/chat/' + conversation.cdConversation], {state: {conversation: conversation}});
                this.close();
            })
            .catch(error => {
                this.logService.logError(error, this.userProvider.getUser());
                console.error("Ocurrió un error al agregar conversación.");
            });
    }

    close() {
        return this.viewCtrl.dismiss();
    }

    remove() {
        if (this.userProvider.inSync) {
            this.errorInSync();
            return;
        }
        if (this.networkService.getConnected()) {
            this.close();
            this.loadingCtrl.create({
                spinner: 'dots',
                message: this.translateService.instant('DELETING_PARTICIPANT')
            }).then(loading => {
                loading.present();
                this.conversationService.removeConversationParticipants(this.conversation, [this.participant])
                    .then(() => {
                        let dsRemoveParticipant = this.participant.dsParticipant;
                        // let conversationRepository = this.dbProvider.getRepository(Conversation) as Repository<Conversation>;
                        // this.conversation.removeParticipant(this.participant.cdParticipant);
                        // this.conversation.dtLastChange = new Date().getTime();
                        // conversationRepository.save(this.conversation)
                        //   .then(() => {
                        loading.dismiss();
                        this.toastCtrl.create({
                            message: dsRemoveParticipant + " " + this.translateService.instant('PARTICIPANT_REMOVE_SUCCESS_MESSAGE'),
                            duration: 2500,
                            position: 'bottom',
                            cssClass: 'toast-ok'
                        }).then(toast => {
                            toast.present();
                        });
                        // })
                        // .catch((err) => {
                        //   loading.dismiss();
                        // });
                    })
                    .catch((error) => {
                        this.logService.logError(error, this.userProvider.getUser());
                        loading.dismiss();
                        this.errorNotConnected();
                    });
            });
        }
        else {
            this.errorNotConnected();
        }
    }

    errorInSync() {
        this.alertCtrl.create({
            header: this.translateService.instant('NOT_SERVER_WORKING'),
            message: this.translateService.instant('NOT_SERVER_WORKING_MESSAGE'),
            buttons: [this.translateService.instant('ACCEPT')]
        }).then(alert => {
            this.backButtonProvider.registerDismissable(alert);
            alert.present();
        });
    }

    errorNotConnected() {
        this.alertCtrl.create({
            header: this.translateService.instant('NOT_CONNECTION_ERROR'),
            message: this.translateService.instant('NOT_CONNECTION_ERROR_MESSAGE'),
            buttons: [this.translateService.instant('ACCEPT')]
        }).then(alert => {
            this.backButtonProvider.registerDismissable(alert);
            alert.present();
        });
    }

}
