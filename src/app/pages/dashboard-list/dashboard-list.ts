import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { UserProvider } from '../../services/user.service';
import { AuthProvider } from '../../services/auth.service';
import { LogService } from '../../services/log.service';
import { PopoverService } from '../../services/popover.service';
import { TranslateService } from '../../services/translate.service';
import { TaskService } from '../../services/task.service';
import { AppConfigurationService } from '../../services/app-configuration.service';
import { RTMService } from '../../services/rtm.service';

/**
 * This page show the dashboards list of the user.
 */
@Component({
    selector: 'page-dashboard-list',
    templateUrl: 'dashboard-list.html',
    styleUrls: ['dashboard-list.scss']
})
export class DashboardListPage {

    public url: SafeResourceUrl;
    private processMessageFn: any;
    private connectionSubscription: Subscription;
    public connectionOn: boolean;

    constructor(protected domSanitizer: DomSanitizer,
                protected userProvider: UserProvider,
                protected authProvider: AuthProvider,
                protected logService: LogService,
                protected popoverService: PopoverService,
                protected translateService: TranslateService,
                protected taskService: TaskService,
                private appConfigurationService: AppConfigurationService,
                private rtmService: RTMService,
                private router: Router) {}

    ngOnInit(): void {
        this.checkConnection();
        this.loadUrl();
    }

    ngOnDestroy() {
        this.connectionSubscription.unsubscribe();
    }

    public ionViewDidEnter(): void {
        this.processMessageFn = (event) => this.processMessage(event);
        window.addEventListener('message', this.processMessageFn, false);
    }

    public ionViewWillLeave(): void {
        window.removeEventListener('message', this.processMessageFn, false);
    }

    /**
     * Returns the current context name.
     * @return {string}
     */
    public getContextName(): string {
        return this.userProvider.getContextName();
    }

    /**
     * Open the page of the actions lists.
     */
    public openActionsList(): void {
        this.router.navigate(['/action-list'], { state: { previousPageData: this.getPageData() }});
    }

    /**
     * Download the log file.
     */
    public downloadLog(): void {
        this.logService.createLogFile(this.userProvider.getUser());
    }

    /**
     * Open the main menu page.
     * @param {any} event
     */
    public openMainMenu(event: any): void {
        this.popoverService.openMainMenu(event);
    }

    /**
    * It returns the fab button style.
    * @return {string}
    */
    public getFabButtonStyle(): string {
        return this.appConfigurationService.getFabButtonStyle();
    }

    /**
     * Returns the page data for navigation.
     * @return {any}
     */
    private getPageData(): any {
        return {
            route: '/main/dashboard/list',
            data: undefined
        };
    }

    /**
     * Process the message event that comes from the Deyel Iframe.
     * @param {any} event
     */
    private processMessage(event: any): void {
        if (event.data.operation === 'updateForm') {
            this.navigateIframeView('SLGenericDocumentUpdate&' + this.taskService.getParametersToFormsURL(event.data), this.translateService.instant('UPDATE'));
        }
        if (event.data.operation === 'showForm') {
            this.navigateIframeView('SLGenericDocumentShow&' + this.taskService.getParametersToFormsURL(event.data), this.translateService.instant('CONSULT'));
        }
        if (event.data.operation === 'deleteForm') {
            this.navigateIframeView('SLGenericDocumentDelete&' + this.taskService.getParametersToFormsURL(event.data), this.translateService.instant('DELETE'));
        }
    }

    /**
     * Returns the navigation configuration to load iframe.
     * @param  {string} url
     * @param  {string} title
     * @return {any}
     */
    private getNavigationConfigurationToIframe(url: string, title: string): any {
        return {
            state: {
                url: url,
                title: title,
                previousPageData: this.getPageData()
            },
            skipLocationChange: true
        };
    }

    /**
     * Navigate to iframe view according to url.
     * @param {string} destineUrl
     * @param {string} title
     */
    private navigateIframeView(destineUrl: string, title: string): void {
        this.router.navigate(['/iframe-view'], this.getNavigationConfigurationToIframe(destineUrl, title));
        this.loadUrl();
    }

    /**
     * Load the iframe url to show.
     */
    public loadUrl(): void {
        this.url = this.domSanitizer.bypassSecurityTrustResourceUrl(this.userProvider.getContextUrl() + '/SLResourceShorcut?access_token=' + this.authProvider.getToken() +'&_idCreateSession=true&_idExecuteInMainFrame=false&_idResource=SLDashboardUI&mobile=true&refresh_token=' + this.userProvider.getRefreshToken());
    }

    /**
     * It checks the connection with the RTM service.
     */
    private checkConnection(): void {
        this.connectionOn = true;
        this.connectionSubscription = this.rtmService.onConnect$.subscribe((connected: boolean) => {
            this.connectionOn = connected;
        });
    }

    /**
     * It returns from app configuration if the header should be displayed.
     * @returns boolean
     */
    public showHeader(): boolean {
        return this.appConfigurationService.getShowHeader();
    }

    /**
     * It returns the fab button should be displayed from configuration.
     * @return {boolean}
     */
    public showFabButton(): boolean {
        return this.appConfigurationService.showFabButton();
    }

}
