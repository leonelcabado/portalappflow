import { NgModule } from '@angular/core';
import { DashboardListPage } from './dashboard-list';
import { RouterModule, Routes } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { PipesModule } from '../../pipes/pipes.module';
import { ErrorConnectionComponent } from '../../components/error-connection/error-connection.component';
import { SharedModule } from '../../shared/shared.module';

const routes: Routes = [
    {
        path: 'list',
        component: DashboardListPage,
    }
];

@NgModule({
    declarations: [
        DashboardListPage
    ],
    imports: [
        RouterModule.forChild(routes),
        IonicModule,
        CommonModule,
        PipesModule,
        SharedModule
    ],
})
export class DashboardListPageModule {
}
