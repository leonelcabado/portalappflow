import { Component, ViewChild } from '@angular/core';
import { AlertController, IonInfiniteScroll, LoadingController } from '@ionic/angular';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { ParticipantService } from '../../services/participant.service'
import { ConversationService } from '../../services/conversation.service'
import { TranslateService } from '../../services/translate.service';
import { Participant } from '../../model/participant.entity';
import { Conversation } from '../../model/conversation.entity';
import { BackButtonProvider } from '../../services/back-button.service';
import { UserProvider } from '../../services/user.service';
import { APP } from '../../app.config';
import { LogService } from 'src/app/services/log.service';
import { AppConfigurationService } from '../../services/app-configuration.service';

@Component({
    selector: 'page-group-participants',
    templateUrl: 'group-participants.html',
    styleUrls: ['group-participants.scss']
})
export class GroupParticipantsPage {
    @ViewChild( IonInfiniteScroll ) infiniteScroll: IonInfiniteScroll;
    searchClicked = false;
    isSearching: boolean = false;
    conversation: Conversation;
    participants: Participant[] = [];
    selectedParticipants: Participant[] = [];
    saving: boolean = false;
    cdUser: string = "";
    allUsersLoaded = false;


    constructor(private participantService: ParticipantService,
                private conversationService: ConversationService,
                public sanitizer: DomSanitizer,
                private alertCtrl: AlertController,
                private loadingCtrl: LoadingController,
                private backButtonProvider: BackButtonProvider,
                private userProvider: UserProvider,
                private translateService: TranslateService,
                private router: Router,
                private location: Location,
                private appConfigurationService: AppConfigurationService,
                private logService: LogService) {
        if (this.router.getCurrentNavigation().extras.state && this.router.getCurrentNavigation().extras.state.conversation) {
            this.conversation = this.router.getCurrentNavigation().extras.state.conversation;
        }

        this.cdUser = this.userProvider.getUser().username.toUpperCase();
        this.participantService.setParticipants(this.participants);
    }

    ngOnInit() {
        this.loadParticipants();
    }

    showsSearchBar() {
        this.searchClicked = !this.searchClicked;
    }

    loadParticipants(event?:any) {
        let participantsExcept: Participant[] = this.conversation.lsParticipants;
        participantsExcept = participantsExcept.concat(this.participantService.getParticipants());
        this.participantService.getUsers(null, participantsExcept, APP.DEFAULT_LOAD_ITEMS_COUNT, true)
            .then(
                (participants: Participant[]) => {
                    this.participants = this.participants.concat(participants);
                    /*if (participants.length == 0 || participants.length < APP.DEFAULT_LOAD_ITEMS_COUNT) {
                        this.allUsersLoaded = true;
                    }*/
                    this.allUsersLoaded = (participants.length == 0)
                    if(event && event.target){
                        event.target.complete();
                        if(this.allUsersLoaded){
                            event.target.complete();
                            this.infiniteScroll.disabled = true;
                        }
                    } 
                }
            );
    }

    select(participant: Participant) {
        if (this.isSelected(participant)) {
            let index = this.selectedParticipants.indexOf(participant);
            if (index > -1) {
                this.selectedParticipants.splice(index, 1);
            }
        }
        else {
            this.selectedParticipants.push(participant);
        }
    }

    isSelected(participant) {
        return this.selectedParticipants.indexOf(participant) > -1;
    }

    save() {
        if (!this.saving) {
            this.saving = true;
            if (this.selectedParticipants.length > 0) {
                this.loadingCtrl.create({
                    spinner: 'dots',
                    message: this.translateService.instant('ADDING_PARTICIPANTS'),
                }).then(loading => {
                    loading.present();
                    this.conversationService.addConversationParticipants(this.conversation, this.selectedParticipants)
                        .then((conversation) => {
                            this.saving = false;
                            this.router.navigate(['/chat/' + conversation.cdConversation], {state: {conversation: conversation}});
                            loading.dismiss();
                        })
                        .catch((error) => {
                            this.logService.logError(error, this.userProvider.getUser());
                            this.saving = false;
                            this.alertCtrl.create({
                                header: this.translateService.instant('ADDING_PARTICIPANTS_ERROR'),
                                subHeader: this.translateService.instant('ADDING_PARTICIPANTS_ERROR_MESSAGE'),
                                buttons: [this.translateService.instant('ACCEPT')]
                            }).then(alert => {
                                this.backButtonProvider.registerDismissable(alert);
                                alert.present();
                                loading.dismiss();
                            });
                        });
                });
            }
            else {
                this.alertCtrl.create({
                    header: this.translateService.instant('NO_PARTICIPANT_SELECTED_ERROR'),
                    subHeader: this.translateService.instant('NO_PARTICIPANT_SELECTED_ERROR_MESSAGE'),
                    buttons: [this.translateService.instant('ACCEPT')]
                }).then(alert => {
                    this.backButtonProvider.registerDismissable(alert);
                    alert.present();
                });
                this.saving = false;
            }
        }
    }

    back() {
        this.location.back();
    }

    searchUsers(event: any) {
        this.allUsersLoaded = true;
        this.participants = [];
        if (event && event.detail && event.detail.value && event.detail.value != "") {
            this.isSearching = true;
            this.participantService.searchParticipants(event.detail.value.toLowerCase())
                .then(
                    (participants: Participant[]) => {
                        this.participants = participants;
                        this.isSearching = false;
                    });
        } else {
            this.loadParticipants();
        }
    }

    exitSearch() {
        this.searchClicked = false;
        this.searchUsers(null);
    }

    /**
    * It returns the fab button style.
    * @return {string}
    */
    public getFabButtonStyle(): string {
        return this.appConfigurationService.getFabButtonStyle();
    }

}
