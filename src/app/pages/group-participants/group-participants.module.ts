import {NgModule} from '@angular/core';
import {GroupParticipantsPage} from './group-participants';
import {PipesModule} from '../../pipes/pipes.module';
import {RouterModule, Routes} from '@angular/router';
import {IonicModule} from '@ionic/angular';
import {CommonModule} from '@angular/common';

const routes: Routes = [
    {
        path: 'group-participants/:id',
        component: GroupParticipantsPage,
    },
];

@NgModule({
    declarations: [
        GroupParticipantsPage,
    ],
    imports: [
        RouterModule.forChild(routes),
        PipesModule,
        IonicModule,
        CommonModule
    ],
})
export class GroupParticipantsPageModule {
}
