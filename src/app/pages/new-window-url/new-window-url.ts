import { Component, OnInit, ViewChild, ElementRef, HostBinding} from '@angular/core';
import { IonRouterOutlet } from '@ionic/angular';
import { Location } from '@angular/common';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { LogService } from 'src/app/services/log.service';
import { AuthProvider } from 'src/app/services/auth.service';
import { UserProvider } from 'src/app/services/user.service';
import { AppConfigurationService } from '../../services/app-configuration.service';
import { EventService } from 'src/app/services/event.service';
import { AttachService } from '../../services/attach.service';

@Component({
  selector: 'app-new-window-url',
  templateUrl: './new-window-url.html',
  styleUrls: ['./new-window-url.scss'],
})
export class NewWindowUrlPage implements OnInit {

  @HostBinding('class.safe-area-top-header') safeAreaTopHeader: boolean;
  @ViewChild('iframe') iframe: ElementRef;
  public url: SafeResourceUrl;
  private id: string;
  private iframeUrl: string;
  private type: string;
  private processEventFn: any;
  private previousPageData: any;
  private token: string;

  constructor(private location: Location,
    private router: Router,
    private routerOutlet: IonRouterOutlet,
    protected domSanitizer: DomSanitizer,
    protected toastController: ToastController,
    protected authProvider: AuthProvider,
    protected userProvider: UserProvider,
    protected appConfigurationService: AppConfigurationService,
    protected attachService: AttachService,
    protected eventService: EventService,
    protected logService: LogService) {}

  public ngOnInit(): void {
    this.setParametersFromRoute();
    this.setSafeAreaTopHeader();
    this.loadUrl();
    this.subscribeAppEvents();
  }

  public ionViewDidEnter(): void {
    this.subscribeWindowEvents();
  }

  public ionViewWillLeave(): void {
    window.removeEventListener('message', this.processEventFn, false);
  }

  /**
   * Susbscribes the component to window events.
   * @return {undefined}
   */
  public subscribeWindowEvents(): void {
    this.processEventFn = (event) => this.processEvent(event);
    window.addEventListener('message', this.processEventFn, false);
  }

  /**
    * Set the url passed by the route.
    * @return {undefined}
  */
  private setIframeUrlFromRoute(): void {
    if (this.router.getCurrentNavigation().extras.state) {
      this.setNavigationData();
      if (this.router.getCurrentNavigation().extras.state.url) {
        this.iframeUrl = this.router.getCurrentNavigation().extras.state.url;
      }
    }
    this.token = this.authProvider.getToken();
  }

  /**
  * Set the parameters passed by the route.
  */
  private setParametersFromRoute(): void {
    if (this.router.getCurrentNavigation().extras.state && this.router.getCurrentNavigation().extras.state.previousPageData) {
      this.previousPageData = this.router.getCurrentNavigation().extras.state.previousPageData;
    }
    this.setIframeUrlFromRoute();
  }

  /**
   * It sets the navigation data.
   */
  private setNavigationData(): void {
    const id = this.router.getCurrentNavigation().extras.state.id;
    if (id) {
      this.id = id;
    }
    const type = this.router.getCurrentNavigation().extras.state.type;
    if (type) {
      this.type = type;
    }
  }

  /**
   * Process the iframe event.
   * @param {any} event
   * @return {undefined}
   */
  private async processEvent(event: any): Promise<any> {
    if (this.checkSameIframeInEvent(event)) {
      switch (event.data.operation) {
        case 'redirect_to_page':
          this.router.navigate(['/new-window-url', Math.random() * 100], {state: {url: event.data.url, type: event.data.type, previousPageData: this.getPageData()}, skipLocationChange: true });
          break;
        case 'reload_current_page':
          this.loadUrl();
          break;
        case 'close_page':
          this.goToBack();
          break;
        case 'download_file_from_url':
          this.attachService.downloadFileFromUrl(event.data.url, event.data.fileName, event.data.fileExtension);
          break;
        case 'show_toast':
          this.processButtonsInToast(event.data.toastOptions, event.source);
          const toast = await this.toastController.create(event.data.toastOptions);
          toast.present();
          break;
        case 'attach':
          this.attachService.attachEventFromIframe(event.data.type, event.data.elementId, event.data.containerId, event.data.iterativeIndex, this.iframe);
          break;
      }
    }
  }

   /**
   * It checks if the event source is for iframe window.
   * @param  {any}     event
   * @return {boolean}
   */
  private checkSameIframeInEvent(event: any): boolean {
    const iframeContentWindow = this.iframe.nativeElement.contentWindow;
    return (event.source === iframeContentWindow) || (iframeContentWindow.frames && iframeContentWindow.frames.length > 0 && event.source === iframeContentWindow.frames[0]);
  }

  /**
   * It loads the iframe url to show.
   */
  public loadUrl(): void {
    this.url = undefined;
    this.url = this.domSanitizer.bypassSecurityTrustResourceUrl(this.getDeyelPageUrl());
  }

  /**
   * Returns the url to go to the form browse.
   * @return {string}
   */
  private getDeyelPageUrl(): string {
    (this.iframeUrl && this.iframeUrl.includes('?')) ? this.iframeUrl += '&' : this.iframeUrl += '?';
    return this.iframeUrl + 'access_token=' + this.authProvider.getToken() + '&refresh_token=' + this.userProvider.getRefreshToken() + '&mobile=true&isEmbed=true' + '&onLoginPage=' + this.appConfigurationService.getLoginPage();
  }

  /**
   * It returns the route to navigate when backs it to the page.
   * @return {string}
   */
  private getRouteToBackPage(): string {
    return '/new-window-url/' + this.id;
  }

   /**
   * Returns the page data for navigation.
   * @return {any}
   */
  private getPageData(): any {
    return {
      route: this.getRouteToBackPage(),
      data: {
        state: {
          url: this.iframeUrl,
          type: this.type
        }
      }
    };
  }

  /**
  * Go back to the previous page.
  * @return {undefined}
  */
  public goToBack(): void {
    this.routerOutlet.pop();
  }

  /**
  * It set from app configuration if the header is safe area.
  * @return {undefined}
  */
  public setSafeAreaTopHeader(): void {
    this.safeAreaTopHeader = !this.appConfigurationService.getShowHeader();
  }

  /**
   * Subscribes the app events.
   * @return {undefined}
   */
  public subscribeAppEvents(): void {
    this.eventService.subscribeEvents().subscribe((event: any) => {
      if (event.type === 'change_token' && this.token !== this.authProvider.getToken()) {
        this.token = this.authProvider.getToken();
        this.loadUrl();
      }
    });
  }

  /**
   * Sets the handler function of the buttons in toast configuration.
   * @param {any} toastOptions
   * @param {any} source
   */
  private processButtonsInToast(toastOptions: any, source: any): void {
    if (toastOptions.buttons && toastOptions.buttons.length > 0) {
      toastOptions.buttons.forEach((button: any) => {
        button.handler = () => {
          source.window.postMessage({operation: 'button_executed_in_toast', role: button.role}, '*');
        }
      });
    }
  }

}
