import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { NewWindowUrlPageRoutingModule } from './new-window-url-routing.module';
import { NewWindowUrlPage } from './new-window-url';
import { PipesModule } from 'src/app/pipes/pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PipesModule,
    NewWindowUrlPageRoutingModule
  ],
  declarations: [NewWindowUrlPage]
})
export class NewWindowUrlPageModule {}
