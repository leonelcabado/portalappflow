import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewWindowUrlPage } from './new-window-url';

const routes: Routes = [
  {
    path: 'new-window-url/:id',
    component: NewWindowUrlPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NewWindowUrlPageRoutingModule {}
