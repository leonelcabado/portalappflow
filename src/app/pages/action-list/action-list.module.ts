import {NgModule} from '@angular/core';
import {ActionListPage} from './action-list';
import {PipesModule} from '../../pipes/pipes.module';
import {RouterModule, Routes} from '@angular/router';
import {IonicModule} from '@ionic/angular';
import {CommonModule} from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';

const routes: Routes = [
    {
        path: 'action-list',
        component: ActionListPage,
    },
];

@NgModule({
    declarations: [
        ActionListPage,
    ],
    imports: [
        RouterModule.forChild(routes),
        PipesModule,
        IonicModule,
        CommonModule,
        SharedModule
    ],
})
export class ActionListPageModule {
}
