import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { Action } from '../../model/action.entity';
import { ActionService } from '../../services/action.service';
import { TranslateService } from '../../services/translate.service';
import { BackButtonProvider } from '../../services/back-button.service';
import { LogService } from 'src/app/services/log.service';
import { UserProvider } from 'src/app/services/user.service';

/**
 * This page show the actions list that the user can execute.
 */
@Component({
    selector: 'page-action-list',
    templateUrl: 'action-list.html',
    styleUrls: ['action-list.scss']
})
export class ActionListPage {

    public mostUsedActions: Array<Action>;
    public restActions: Array<Action>;
    public filteredText = '';
    private previousPageData: any;
    public isLoading: boolean = true;
    public isServerError: boolean = false;

    constructor(protected actionService: ActionService,
                protected backButtonProvider: BackButtonProvider,
                private location: Location,
                private translateService: TranslateService,
                private router: Router,
                private logService: LogService,
                private userProvider: UserProvider) {
        this.filteredText = '';
        this.mostUsedActions = new Array();
        this.restActions = new Array();
    }

    public ngOnInit() {
        this.setValueActionsList();
        this.actionService.getActions()
            .then((actionsObject: any) => {
                if(actionsObject){
                    if(JSON.stringify(actionsObject.mostUsed) != JSON.stringify(this.mostUsedActions)){
                        this.mostUsedActions = actionsObject.mostUsed;
                    }
                    if(JSON.stringify(actionsObject.rest) != JSON.stringify(this.restActions)){
                        this.restActions = actionsObject.rest;
                    }
                    this.isServerError = false;
                }else{
                    this.isServerError = true;
                }
                this.isLoading = false;
            })
            .catch(error => {
                this.logService.logError(error, this.userProvider.getUser());
                this.isLoading = false;
                return undefined;
            });
        this.setParametersFromRoute();
        this.backButtonProvider.registerView('action-list', '/action-list');
    }

    /**
     * Setting values ​​of the action list.
     */
     private setValueActionsList(): void {
        if (this.actionService.getActionsList()) {
            this.mostUsedActions = this.actionService.getActionsList().mostUsed;
            this.restActions = this.actionService.getActionsList().rest;
            this.isLoading = false;
        }
    }

    /**
     * Set the parameters passed by the route.
     */
    private setParametersFromRoute(): void {
        if (this.router.getCurrentNavigation().extras.state && this.router.getCurrentNavigation().extras.state.previousPageData) {
            this.previousPageData = this.router.getCurrentNavigation().extras.state.previousPageData;
        }
    }

    /**
     * Go back to the previous page.
     */
    public goToBack(): void {
        if (this.previousPageData) {
            this.router.navigate([this.previousPageData.route], this.previousPageData.data);
            return;
        }
        this.location.back();
    }

    /**
     * Open the action selected.
     * @param action
     */
    public openAction(action: Action): void {
        if (action.isForm()) {
            this.router.navigate(['/iframe-view'], {state: {url: action.getUrl(), title: this.translateService.instant('NEW'), previousPageData: this.getPageData()}, skipLocationChange: true });
            return;
        }
        this.router.navigate(['/iframe-view'], {state: {url: action.getUrl(), title: action.getName(), previousPageData: this.getPageData()}, skipLocationChange: true });
    }

    /**
     * Find the action entered in the filter.
     * @param event
     */
    public searchAction(event: any): void {
        const text = (event && event.detail) ? event.detail.value : '';
        this.filteredText = text;
    }

    /**
     * Returns the page data for navigation.
     * @return {any}
     */
    private getPageData(): any {
        return {
            route: '/action-list',
            data: undefined
        };
    }

    /**
     * Load the action list to show.
     */
     public loadActionList(): void {
        this.actionService.getActions();
        this.setValueActionsList();
    }

}
