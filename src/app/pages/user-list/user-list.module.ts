import {NgModule} from '@angular/core';
import {UserListPage} from './user-list';
import {DirectivesModule} from '../../directives/directives.module';
import {PipesModule} from '../../pipes/pipes.module';
import {RouterModule, Routes} from '@angular/router';
import {IonicModule} from '@ionic/angular';
import {CommonModule} from '@angular/common';
import {CanDeactivateGuard} from '../../auth/can-deactivate-guard';

const routes: Routes = [
    {
        path: 'user-list',
        component: UserListPage,
        canDeactivate: [CanDeactivateGuard]
    },
];

@NgModule({
    declarations: [
        UserListPage,
    ],
    imports: [
        RouterModule.forChild(routes),
        DirectivesModule,
        PipesModule,
        IonicModule,
        CommonModule
    ],
})
export class UserListPageModule {
}
