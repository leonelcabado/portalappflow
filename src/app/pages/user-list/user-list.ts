import { Component, ViewChild } from '@angular/core';
import { IonInfiniteScroll, Platform } from '@ionic/angular';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { Location, SlicePipe } from '@angular/common';
import { UserProvider } from '../../services/user.service';
import { BackButtonProvider } from '../../services/back-button.service';
import { DbProvider } from '../../services/db.service';
import { ParticipantService } from '../../services/participant.service'
import { ConversationService } from '../../services/conversation.service'
import { RTMService } from '../../services/rtm.service';
import { Participant } from '../../model/participant.entity';
import { Conversation } from '../../model/conversation.entity';
import { APP } from '../../app.config';
import { CanComponentDeactivate } from '../../auth/can-deactivate-guard';
import { first } from 'rxjs/internal/operators';
import { LogService } from 'src/app/services/log.service';
import { AppConfigurationService } from '../../services/app-configuration.service';

@Component({
    selector: 'page-user-list',
    templateUrl: 'user-list.html',
    styleUrls: ['user-list.scss']
})
export class UserListPage implements CanComponentDeactivate {
    @ViewChild( IonInfiniteScroll ) infiniteScroll: IonInfiniteScroll;
    searchClicked = false;
    conversation: Conversation;
    participants: Participant[] = [];
    fullParticipants: any;
    selectedParticipants: Participant[] = [];
    opening = false;
    last = 0;
    allUsersLoaded = true;
    isSearching: boolean = false;

    constructor(private participantService: ParticipantService,
                private conversationService: ConversationService,
                public sanitizer: DomSanitizer,
                private userProvider: UserProvider,
                private backButtonProvider: BackButtonProvider,
                private dbProvider: DbProvider,
                private rtm: RTMService,
                private platform: Platform,
                private router: Router,
                private location: Location,
                private appConfigurationService: AppConfigurationService,
                private logService: LogService) {
        this.participantService.setParticipants(this.participants);
    }

    ngOnInit() {
        this.loadParticipants();
    }

    showsSearchBar() {
        this.searchClicked = !this.searchClicked;
    }

    loadParticipants(lastParticipant?: string, event?:any) {
        this.participantService.getUsers(lastParticipant, [], APP.DEFAULT_LOAD_ITEMS_COUNT, true)
            .then(
                (participants: Participant[]) => {
                    this.participants = this.participants.concat(participants);
                    this.allUsersLoaded = (participants.length == 0);
                    if(this.allUsersLoaded) {
                        this.infiniteScroll.disabled = true;
                    }
                    if(event && event.target) {
                        event.target.complete();
                    }
                }
            );   
    }

    onTap(participant: Participant) {
        if (this.isSelected(participant)) {
            this.deselect(participant);
        }
        else {
            this.selectedParticipants.push(participant);
            participant.selected = true;
        }
    }

    createConversation(participant: Participant) {
        this.conversationService.getConversationByParticipant(participant, true)
            .then(conversation => {
                this.router.navigate(['/chat/' + conversation.cdConversation], {state: {conversation: conversation}});
                this.opening = false;
            })
            .catch(error => {
                this.logService.logError(error, this.userProvider.getUser());
            });
    }

    openChat() {
        if (!this.opening) {
            this.opening = true;
            if (this.selectedParticipants.length > 1) {
                this.router.navigate(['/group-new'], {state: {participants: this.selectedParticipants}});
                this.opening = false;
            }
            else if (this.selectedParticipants.length == 1) {
                this.createConversation(this.selectedParticipants[0]);
            }
        }
    }

    public goToMain() {
        this.location.back();
    }

    deselect(participant) {
        if (this.isSelected(participant)) {
            let index = this.selectedParticipants.indexOf(participant);
            if (index > -1) {
                this.selectedParticipants.splice(index, 1);
                participant.selected = false;

            }
        }
    }

    isSelected(participant) {
        return participant.selected;
    }

    get isGroup(): boolean {
        return this.selectedParticipants.length > 0;
    }

    ionViewWillEnter() {}

    searchUsers(event: any) {
        this.allUsersLoaded = true;
        this.participants = [];
        if (event && event.detail && event.detail.value && event.detail.value != "") {
            this.isSearching = true;
            this.participantService.searchParticipants(event.detail.value.toLowerCase())
                .then(
                    (participants: Participant[]) => {
                        this.participants = participants;
                        this.isSearching = false;
                    });
        } else {
            this.loadParticipants();
        }
    }

    exitSearch() {
        this.searchClicked = false;
        this.searchUsers(null);
    }

    public canDeactivate(): boolean {
        let canLeave = !this.searchClicked;
        if (this.searchClicked) {
            this.exitSearch();
        }
        return canLeave;
    }

    /**
    * It returns the fab button style.
    * @return {string}
    */
    public getFabButtonStyle(): string {
        return this.appConfigurationService.getFabButtonStyle();
    }

}
