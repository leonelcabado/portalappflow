import {NgModule} from '@angular/core';
import {TaskMoreActionsPage} from './task-more-actions';
import {PipesModule} from '../../pipes/pipes.module';
import {RouterModule, Routes} from '@angular/router';
import {IonicModule} from '@ionic/angular';
import {CommonModule} from '@angular/common';

const routes: Routes = [
    {
        path: 'task-more-actions',
        component: TaskMoreActionsPage,
    },
];

@NgModule({
    declarations: [
        TaskMoreActionsPage,
    ],
    imports: [
        RouterModule.forChild(routes),
        PipesModule,
        IonicModule,
        CommonModule
    ],
})
export class TaskMoreActionsPageModule {
}
