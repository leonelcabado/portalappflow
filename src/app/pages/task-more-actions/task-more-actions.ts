import {Component} from '@angular/core';
import {PopoverController} from '@ionic/angular';

//models
import {CaseButtons} from '../../model/case-buttons.entity';

@Component({
    selector: 'page-task-more-actions',
    templateUrl: 'task-more-actions.html',
    styleUrls: ['task-more-actions.scss']
})
export class TaskMoreActionsPage {

    actions: CaseButtons[] = [];

    constructor(public viewCtrl: PopoverController) {
    }

    executeAction(action) {
        this.viewCtrl.dismiss(action);
    }

}
