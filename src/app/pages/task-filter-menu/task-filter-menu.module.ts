import {NgModule} from '@angular/core';
import {TaskFilterMenuPage} from './task-filter-menu';
import {PipesModule} from '../../pipes/pipes.module';
import {RouterModule, Routes} from '@angular/router';
import {IonicModule} from '@ionic/angular';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

const routes: Routes = [
    {
        path: 'task-filter-menu',
        component: TaskFilterMenuPage,
    },
];

@NgModule({
    declarations: [
        TaskFilterMenuPage,
    ],
    imports: [
        RouterModule.forChild(routes),
        PipesModule,
        IonicModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
    ],
})
export class TaskFilterMenuPageModule {
}
