import { Component } from '@angular/core';
import { AlertController, ModalController, PopoverController } from '@ionic/angular';
import { TaskFilter, TaskFilterParameter } from 'src/app/model/task-filter.entity';

@Component({
    selector: 'page-task-filter-menu',
    templateUrl: 'task-filter-menu.html',
    styleUrls: ['task-filter-menu.scss']
})
export class TaskFilterMenuPage {

    /**
     * Fixed chip attributes for either start or expiration date.
     */
    SELECTED_DATE_OPERATION = {
        "TODAY": {op:"eq", value: new Date().getTime()},
        "LAST_WEEK": {op:"gte", value: new Date(Date.now() - 7 * 24 * 60 * 60 * 1000).getTime()},
        "CURRENT_MONTH": {op:"gte", value: new Date(new Date().getFullYear(), new Date().getMonth(), 1).getTime()},
        "CURRENT_YEAR": {op:"gte", value: new Date(new Date().getFullYear(), 0, 1).getTime()},
        "LAST_MONTH": {op:"gte,lte", value: new Date(new Date().getFullYear(), new Date().getMonth() - 1, 1).getTime()+","+new Date(new Date().getFullYear(), new Date().getMonth(), 1).getTime()},
        "LAST_YEAR": {op:"gte,lte", value: new Date(new Date().getFullYear() - 1, 1).getTime()+","+new Date(new Date().getFullYear(), 0, 1).getTime()},
        "FROM_DATE": {op:"gte"},
        "TO_DATE": {op:"lte"},
        "EQUAL_DATE": {op:"eq"}
    };
    /**
     * Priority values ​​for ApiRest.
     */
    PRIORITY_FILTER = {
        URGENT: {code: 0, value: "1%7C"},
        HIGH: {code: 1, value: "2%7C"},
        MEDIUM: {code: 2, value: "3%7C"},
        LOW: {code: 3, value: "4%7C"}
    };
    /**
     * Alert values ​​for ApiRest.
     */
    ALERT_FILTER = {
        NOT_DEFINED: {code: 0, value: "NONE%7C"},
        LONG_TERM: {code: 1, value: "TO_EXPIREL%7C"},
        SHORT_TERM: {code: 2, value: "TO_EXPIREH%7C"},
        EXPIRED: {code: 3, value: "EXPIRED%7C"}
    }

    tasksFilters: TaskFilter;
    expirationDate = {from: "", to: "", equal: ""}
    executionDate = {from: "", to: "", equal: ""}
    lsAlertFilters = [];
    lsPriorityFilters = [];
    lsFilters: TaskFilterParameter[] = [];
    lsDateFilterIds = ['TODAY', 'LAST_WEEK','CURRENT_MONTH', 'CURRENT_YEAR', 'LAST_MONTH', 'LAST_YEAR'];
    lsDateFilterName = ['Hoy', 'Últimos 7 días ','Mes Actual', 'Año Actual', 'Mes Anterior', 'Año Anterior'];
    isTaskFilters = false;

    constructor(public viewCtrl: PopoverController,
                private alertController: AlertController,
                private modalController: ModalController) {
    }

    ngOnInit(){
        if( this.tasksFilters.lsFilters){
            for (let f of this.tasksFilters.lsFilters) {
                this.lsFilters.push(f);
            }
            this.setInitialFilters();
        }
        if( this.tasksFilters.lsProirity){
            for (let f of this.tasksFilters.lsProirity) {
                this.lsPriorityFilters.push(f);
            }
        }
        if( this.tasksFilters.lsAlert){
            for (let f of this.tasksFilters.lsAlert) {
                this.lsAlertFilters.push(f);
            }
        }
        this.clearFiltersByKey("PRIORITY");
        this.clearFiltersByKey("ALERT");
    }

    /**
     * Set the values ​​for the initial ion-datetime.
     */
    setInitialFilters(): void{
        for(let obj of this.tasksFilters.lsFilters){
            if(obj.cdKey == "FROM_DATE" || obj.cdKey == "TO_DATE" || obj.cdKey == "EQUAL_DATE"){
                let dtValue = new Date(parseInt(obj.dsValue)).toISOString();
                if(obj.cdKey == "FROM_DATE" && obj.dsParameter == "executionDate") this.executionDate.from = dtValue;
                if(obj.cdKey == "TO_DATE" && obj.dsParameter == "executionDate") this.executionDate.to = dtValue;
                if(obj.cdKey == "EQUAL_DATE" && obj.dsParameter == "executionDate") this.executionDate.equal = dtValue;
                if(obj.cdKey == "FROM_DATE" && obj.dsParameter == "expiredDate") this.expirationDate.from = dtValue;
                if(obj.cdKey == "TO_DATE" && obj.dsParameter == "expiredDate") this.expirationDate.to = dtValue;
                if(obj.cdKey == "EQUAL_DATE" && obj.dsParameter == "expiredDate") this.expirationDate.equal = dtValue;
            }
        }
    }

    /**
     * Detect changes in the priority filter.
     * @param numberPriority
     */
    changeFiltersPriority(numberPriority): void{
        let i = this.lsPriorityFilters.indexOf(numberPriority);
        if (i < 0) {
            this.lsPriorityFilters.push(numberPriority);
        } else {
            this.lsPriorityFilters.splice(i, 1);
        }
    }

    /**
     * Detect changes in the Alerts filter.
     * @param numberAlert 
     */
    changeFiltersAlert(numberAlert): void{
        let i = this.lsAlertFilters.indexOf(numberAlert);
        if (i < 0) {
            this.lsAlertFilters.push(numberAlert);
        } else {
            this.lsAlertFilters.splice(i, 1);
        }
    }

    /**
     * Detects changes to the default chips defined in the lsDateFilterIds list.
     * @param pKey 
     * @param pParameter 
     */
    changeFiltersDate(pKey: string, pParameter: string): void{
        if(!this.checkInFilters(pKey, pParameter, null, this.SELECTED_DATE_OPERATION[pKey].op)){
            let filterParameter = new TaskFilterParameter(pKey, pParameter, this.SELECTED_DATE_OPERATION[pKey].op, this.SELECTED_DATE_OPERATION[pKey].value+"");
            this.lsFilters.push(filterParameter);
        }
        this.resetDateObject(pParameter);
    }

    /**
     * Checks if it exists within the filter list.
     * @param pKey 
     * @param pParameter 
     * @param pValue 
     * @param pOperation 
     * @returns 
     */
    checkInFilters(pKey: string, pParameter: string, pValue?: string, pOperation?: string): boolean{
        for(let obj of this.lsFilters){
            if(obj.dsParameter == pParameter){
                if(obj.cdKey == pKey){
                    let index = this.lsFilters.indexOf(obj);
                    this.lsFilters.splice(index, 1);
                }else {
                    if(pValue){
                        obj.setDsValue(pValue);
                    } else {
                        obj.setDsValue(this.SELECTED_DATE_OPERATION[pKey].value+"");
                    }
                    obj.setCdKey(pKey);
                    obj.setDsOperation(pOperation);
                }
                return true;
            }
        }
        return false;
    }

    /**
     * Applies the selected filters and returns the model and a Boolean flag to the task list of the pages.
     */
    applyFilters(): void{
        this.tasksFilters.setLsAlert(this.lsAlertFilters);
        this.tasksFilters.setLsPriority(this.lsPriorityFilters);
        this.setAlertFilters();
        this.setPriorityFilters();
        if(this.lsFilters.length > 0) this.isTaskFilters = true;
        this.tasksFilters.setLsFilters(this.lsFilters)
        let result = {
            tasksFilters: this.tasksFilters,
            isTaskFilters: this.isTaskFilters
        }
        this.modalController.dismiss(result)
    }

    /**
     * Generates, if not existing, new filters for the dates that come from the ion-datetime.
     * @param pValue 
     * @param pParameter
     * @param pKey 
     */
    setFilterDateTime(pValue: any, pParameter: string, pKey: string): void{
        if(pValue && !this.checkInFilters(pKey, pParameter, this.formatDateTimeValue(pValue), this.SELECTED_DATE_OPERATION[pKey].op)){
            let filterParameter = new TaskFilterParameter(pKey, pParameter, this.SELECTED_DATE_OPERATION[pKey].op, this.formatDateTimeValue(pValue));
            this.lsFilters.push(filterParameter);
            this.validateDateEntry(pKey,pParameter);
        }
        if(pValue) this.validateDateEntry(pKey,pParameter);
    }

    formatDateTimeValue(dtValue: string): string{
        return new Date(dtValue.split('T')[0].split("-").join("/")).getTime().toString();
    }

    /**
     * Generates filter object corresponding to the selected Alerts.
     */
    setAlertFilters(): void{
        let lsAlertValue = "";
        for(let cdAlert of this.lsAlertFilters){
            let alertValue = this.ALERT_FILTER.EXPIRED.code == cdAlert ? this.ALERT_FILTER.EXPIRED.value : (this.ALERT_FILTER.LONG_TERM.code == cdAlert ? this.ALERT_FILTER.LONG_TERM.value : (this.ALERT_FILTER.SHORT_TERM.code == cdAlert ? this.ALERT_FILTER.SHORT_TERM.value : this.ALERT_FILTER.NOT_DEFINED.value));
            lsAlertValue += alertValue;
        }
        if(lsAlertValue != "") 
            this.lsFilters.push(new TaskFilterParameter("ALERT", "alertType", "in", lsAlertValue));
    }

    /**
     * Generates filter object corresponding to the selected priorities.
     */
    setPriorityFilters(): void{
        let lsPriorityValue = "";
        for(let cdPriority of this.lsPriorityFilters){
            let priorityValue = this.PRIORITY_FILTER.URGENT.code == cdPriority ? this.PRIORITY_FILTER.URGENT.value : (this.PRIORITY_FILTER.HIGH.code == cdPriority ? this.PRIORITY_FILTER.HIGH.value : (this.PRIORITY_FILTER.MEDIUM.code == cdPriority ? this.PRIORITY_FILTER.MEDIUM.value : this.PRIORITY_FILTER.LOW.value));
            lsPriorityValue += priorityValue;
        }
        if(lsPriorityValue != "") 
            this.lsFilters.push(new TaskFilterParameter("PRIORITY", "priority", "in", lsPriorityValue));
    }

    closeFilters(): void{
        this.modalController.dismiss(null);
    }

    /**
     * Reset all filters of the model.
     */
    clearFilters(): void{
        this.tasksFilters = new TaskFilter();
        this.lsAlertFilters = [];
        this.lsPriorityFilters = [];
        this.lsFilters = [];
        this.expirationDate = {from: "", to: "", equal: ""}
        this.executionDate = {from: "", to: "", equal: ""}
        var list = document.getElementsByTagName("ion-chip");
        for (let i = 0; i < list.length; i++) {
            list[i].classList.remove("selectedFilter");
        }
        this.isTaskFilters = false;
    }

    /**
     * Creates an alert for filter input values ​​and adds filter objects to the model list.
     * @param pKey 
     * @param pValue 
     * @param pSubHeader 
     */
    async presentAlertFilterValue(pKey: string, pOperation?: string, pSubHeader?: string) {
        let higher = 'CASE_HIGHER';
        let less_or_Equal = 'CASE_LESS_OR_EQUAL';
        const alert = await this.alertController.create({
          cssClass: 'alert-filter-menu',
          subHeader: pSubHeader ? pSubHeader : 'Contiene',
          inputs: [
            {
              name: 'containsValue',
              type: (pKey == higher || pKey == less_or_Equal) ? 'number' : 'text',
              value: this.getTaskFilterObject(pKey)?.dsValue
            }
          ],
          buttons: [
            {
              text: 'Filtrar',
              handler: data => {
                    if(data.containsValue){
                        let operationValue = (pOperation) ? pOperation : 'like';
                        let taskFilter = new TaskFilterParameter(pKey, pKey.toLowerCase().split("_")[0], operationValue, data.containsValue)
                        if(!this.checkInFilters(taskFilter.cdKey, taskFilter.dsParameter, taskFilter.dsValue, taskFilter.dsOperation)){
                            this.lsFilters.push(taskFilter);
                        }
                    }
                }
            }
          ]
        });
        await alert.present();
    }

    /**
     * Removes the object from the filter list using the CD key.
     * @param pKey 
     */
    clearFiltersByKey(pKey: string): void{
        for(let obj of this.lsFilters){
            if(obj.cdKey == pKey){
                let index = this.lsFilters.indexOf(obj);
                this.lsFilters.splice(index, 1);
            }
        }
    }

    /**
     * Search for the existence of an object in the filter list using cdKey.
     * @param pKey 
     * @returns 
     */
    isExistInFilter(pKey: string): boolean{
        for(let obj of this.lsFilters){
            if(obj.cdKey == pKey){
                return true;
            }
        }
        return false;
    }

    /**
     * Gets an object from the filter list.
     * @param pKey 
     * @param pParameter 
     */
    getTaskFilterObject(pKey: string, pParameter?: string): TaskFilterParameter{
        for(let obj of this.lsFilters){
            if(pParameter){
                if(obj.cdKey == pKey && obj.dsParameter == pParameter){
                    return obj;
                }
            }else if(obj.cdKey == pKey){
                return obj;
            }
        }
        return null;
    }

    /**
     * Sets default value for data-time component.
     * @param pParameter 
     */
    resetDateObject(pParameter: string): void{
        if(pParameter == "executionDate")
            this.executionDate = {from: "", to: "", equal: ""}
        else
            this.expirationDate = {from: "", to: "", equal: ""}
    }

    /**
     * Validates selected date by data-time componente.
     * @param pKey 
     * @param pParameter 
     */
    validateDateEntry(pKey: string, pParameter: string){
        if(pKey == "EQUAL_DATE"){
            if (pParameter == "executionDate"){
                this.executionDate.from = "";
                this.executionDate.to = "";
            }else{
                this.expirationDate.from = "";
                this.expirationDate.to = "";
            }
        }else{
            if (pParameter == "executionDate")
                this.executionDate.equal = "";
            else
                this.expirationDate.equal = "";
        }
    }
}
