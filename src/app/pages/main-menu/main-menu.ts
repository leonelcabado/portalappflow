import {Component} from '@angular/core';
import { PopoverController, AlertController, LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { UserProvider } from '../../services/user.service';
import { BackButtonProvider } from '../../services/back-button.service';
import { RTMService } from '../../services/rtm.service';
import { ConversationService } from '../../services/conversation.service';
import { ParticipantService } from '../../services/participant.service';
import { TranslateService } from '../../services/translate.service';
import { SynchronizationService } from '../../services/synchronization.service';
import { EVENT_TYPE, RTMEvent } from '../../model/rtm-event.entity';
import { ConfigurationService } from '../../services/configuration.service';
import { ActionService } from 'src/app/services/action.service';
import { AppConfigurationService } from '../../services/app-configuration.service';
import { RuleService } from 'src/app/services/rule.service';
import { EventService } from 'src/app/services/event.service';

@Component({
    selector: 'page-main-menu',
    templateUrl: 'main-menu.html',
    styleUrls: ['main-menu.scss']
})
export class MainMenuPage {

    constructor(private viewCtrl: PopoverController,
                private userProvider: UserProvider,
                private alertCtrl: AlertController,
                private loadingCtrl: LoadingController,
                private backButtonProvider: BackButtonProvider,
                private rmtService: RTMService,
                private conversationService: ConversationService,
                private configurationService: ConfigurationService,
                private participantService: ParticipantService,
                private translateService: TranslateService,
                private synchronizationService: SynchronizationService,
                private appConfigurationService: AppConfigurationService,
                private router: Router,
                private actionService: ActionService,
                private eventService: EventService,
                private ruleService: RuleService) {
    }

    goToSettings() {
        this.alertCtrl.create({
            header: 'Functionality in development.',
            message: 'This functionality is being developed.',
            buttons: ['Ok']
        }).then(alert => {
            this.backButtonProvider.registerDismissable(alert);
            alert.present();
        });
    }

    signOut() {
        this.alertCtrl.create({
            header: this.translateService.instant('CONFIRM'),
            message: this.translateService.instant('LOGOUT_CONFIRM_MESSAGE'),
            buttons: [
                {
                    text: this.translateService.instant('CANCEL'),
                    role: 'cancel',
                    handler: () => {
                        this.viewCtrl.dismiss();
                    }
                },
                {
                    text: this.translateService.instant('ACCEPT'),
                    handler: () => {
                        this.synchronizationService.closeErrorSyncToast();
                        this.viewCtrl.dismiss();
                        this.eventService.emitEvent({type: 'logout'});
                    }
                }
            ]
        }).then(alert => {
            this.backButtonProvider.registerDismissable(alert);
            alert.present();
        });
    }

    deleteChats() {
        this.conversationService.db.removeConversationsByUser(this.userProvider.getUser().id)
            .then(_ => {
                this.viewCtrl.dismiss();
            });
    }

    errorAlert() {
        this.alertCtrl.create({
            header: 'Error',
            subHeader: 'There was an error while the session was closing. Please try again.',
            buttons: ['Ok']
        }).then(alert => {
            this.backButtonProvider.registerDismissable(alert);
            alert.present();
        });
    }

}
