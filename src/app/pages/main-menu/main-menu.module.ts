import {NgModule} from '@angular/core';
import {MainMenuPage} from './main-menu';
import {PipesModule} from '../../pipes/pipes.module';
import {RouterModule, Routes} from '@angular/router';
import {IonicModule} from '@ionic/angular';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

const routes: Routes = [
    {
        path: 'main-menu',
        component: MainMenuPage,
    },
];

@NgModule({
    declarations: [
        MainMenuPage,
    ],
    imports: [
        RouterModule.forChild(routes),
        PipesModule,
        IonicModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
    ]
})
export class MainMenuPageModule {
}
