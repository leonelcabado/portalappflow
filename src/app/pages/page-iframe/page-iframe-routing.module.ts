import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PageIframePage } from './page-iframe';

const routes: Routes = [
  {
    path: 'iframe/:idPage',
    component: PageIframePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PageIframePageRoutingModule {}
