import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { PageIframePageRoutingModule } from './page-iframe-routing.module';
import { PageIframePage } from './page-iframe';
import { PipesModule } from 'src/app/pipes/pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PipesModule,
    PageIframePageRoutingModule
  ],
  declarations: [PageIframePage]
})
export class PageIframePageModule {}
