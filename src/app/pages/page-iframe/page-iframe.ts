import { Location } from '@angular/common';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { Router, RouterEvent } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { Action } from 'src/app/model/action.entity';
import { DeyelPageConfiguration } from 'src/app/model/deyel-page-configuration.entity';
import { ActionService } from 'src/app/services/action.service';
import { AppConfigurationService } from 'src/app/services/app-configuration.service';
import { AuthProvider } from 'src/app/services/auth.service';
import { LogService } from 'src/app/services/log.service';
import { PopoverService } from 'src/app/services/popover.service';
import { TaskService } from 'src/app/services/task.service';
import { TranslateService } from 'src/app/services/translate.service';
import { UserProvider } from 'src/app/services/user.service';
import { EventService } from 'src/app/services/event.service';
import { AttachService } from '../../services/attach.service';

@Component({
  selector: 'app-page-iframe',
  templateUrl: './page-iframe.html',
  styleUrls: ['./page-iframe.scss'],
})
export class PageIframePage implements OnInit {

  @ViewChild('iframe') iframe: ElementRef;
  public url: SafeResourceUrl;
  public actions: Array<Action>;
  public showInTab: boolean;
  private previousPageData: any;
  private deyelPageConfiguration: DeyelPageConfiguration;
  private processEventFn: any;
  private token: string;

  constructor(private location: Location,
    private router: Router,
    protected domSanitizer: DomSanitizer,
    protected toastController: ToastController,
    protected popoverService: PopoverService,
    protected logService: LogService,
    protected userProvider: UserProvider,
    protected authProvider: AuthProvider,
    protected actionService: ActionService,
    protected translateService: TranslateService,
    protected taskService: TaskService,
    protected eventService: EventService,
    private attachService: AttachService,
    private appConfigurationService: AppConfigurationService) {}

  public ngOnInit(): void {
    this.setParametersFromRoute();
    this.loadUrl();
    this.subscribeAppEvents();
  }

  public ionViewDidEnter(): void {
    this.subscribeWindowEvents();
  }

  public ionViewWillLeave(): void {
    window.removeEventListener('message', this.processEventFn, false);
  }

  /**
   * Set the parameters passed by the route.
   * @return {undefined}
   */
  private setParametersFromRoute(): void {
    if (this.router.getCurrentNavigation().extras.state) {
      this.setDeyelPageConfiguration();
      if (this.router.getCurrentNavigation().extras.state.previousPageData) {
        this.previousPageData = this.router.getCurrentNavigation().extras.state.previousPageData;
      }
    }
    this.token = this.authProvider.getToken();
  }

  /**
   * Susbscribes the component to window events.
   * @return {undefined}
   */
  public subscribeWindowEvents(): void {
    this.processEventFn = (event) => this.processEvent(event);
    window.addEventListener('message', this.processEventFn, false);
  }

  /**
   * Process the iframe event.
   * @param {any} event
   * @return {undefined}
   */
  private async processEvent(event: any): Promise<any> {
    if (this.checkSameIframeInEvent(event)) {
      switch (event.data.operation) {
        case 'redirect_to_page':
          this.router.navigate(['/new-window-url', Math.random() * 100], {state: {url: event.data.url, type: event.data.type, previousPageData: this.getPageData()}, skipLocationChange: true });
          break;
        case 'reload_current_page':
          this.loadUrl();
          break;
        case 'attach':
          this.attachService.attachEventFromIframe(event.data.type, event.data.elementId, event.data.containerId, event.data.iterativeIndex, this.iframe);
          break;
        case 'download_file_from_url':
          this.attachService.downloadFileFromUrl(event.data.url, event.data.fileName, event.data.fileExtension);
          break;
        case 'show_toast':
          this.processButtonsInToast(event.data.toastOptions, event.source);
          const toast = await this.toastController.create(event.data.toastOptions);
          toast.present();
          break;
      }
    }
  }

  /**
   * It checks if the event source is for iframe window.
   * @param  {any}     event
   * @return {boolean}
   */
  private checkSameIframeInEvent(event: any): boolean {
    const iframeContentWindow = this.iframe.nativeElement.contentWindow;
    return (event.source === iframeContentWindow) || (iframeContentWindow && iframeContentWindow.frames && iframeContentWindow.frames.length > 0 && event.source === iframeContentWindow.frames[0]);
  }

  /**
   * It loads the iframe url to show.
   */
  public loadUrl(): void {
    this.url = undefined;
    this.url = this.domSanitizer.bypassSecurityTrustResourceUrl(this.appConfigurationService.getInitialContextUrl() + '/SLPage?idPage=' + this.getDeyelPageUrl());
  }

  /**
   * It opens the main menu page.
   * @param {any} event
   */
  public openMainMenu(event: any): void {
    this.popoverService.openMainMenu(event);
  }

  /**
   * Download the log file.
   */
  public downloadLog(): void {
    this.logService.createLogFile(this.userProvider.getUser());
  }

  /**
   * Open the page of the actions lists.
   */
  public openActionsList(): void {
    this.router.navigate(['/action-list'], { state: { previousPageData: this.getPageData() } });
  }

  /**
   * Go back to the previous page.
   */
  public goToBack(): void {
    if (this.previousPageData) {
      this.router.navigate([this.previousPageData.route], this.previousPageData.data);
      return;
    }
    this.location.back();
  }

  /**
   * It returns the fab button style.
   * @return {string}
   */
  public getFabButtonStyle(): string {
    return this.appConfigurationService.getFabButtonStyle();
  }

  /**
   * Returns the url to go to the form browse.
   * @return {string}
   */
  private getDeyelPageUrl(): string {
    return this.deyelPageConfiguration.getIdPage() + '&mobile=true&isEmbed=true&access_token=' + this.authProvider.getToken() + '&refresh_token=' + this.userProvider.getRefreshToken() + '&onLoginPage=' + this.appConfigurationService.getLoginPage();
  }

  /**
   * It Returns the navigation configuration to load iframe.
   * @param  {string} url
   * @param  {string} title
   * @return {any}
   */
  private getNavigationConfigurationToIframe(url: string, title: string): any {
    return {
      state: {
        url: url,
        title: title,
        previousPageData: this.getPageData()
      },
      skipLocationChange: true
    };
  }

  /**
   * Open the action selected.
   * @param {Action} action
   */
  public openAction(action: Action): void {
    this.router.navigate(['/iframe-view'], this.getNavigationConfigurationToIframe(action.getUrl(), action.getName()));
  }

  /**
   * Returns the page data for navigation.
   * @return {any}
   */
  private getPageData(): any {
    return {
      route: this.getRouteToBackPage(),
      data: {
        state: {
          idPage: this.deyelPageConfiguration.getIdPage(),
          configuration: this.deyelPageConfiguration
        }
      }
    };
  }

  /**
   * It returns the route to navigate when backs it to the page.
   * @return {string}
   */
  private getRouteToBackPage(): string {
    return '/main/page/iframe/' + this.deyelPageConfiguration.getIdPage();
  }

  /**
   * Returns the current context name.
   * @return {string}
   */
  public getContextName(): string {
    return this.userProvider.getContextName();
  }

  /**
   * It sets the deyel page configuration.
   */
  private setDeyelPageConfiguration(): void {
    const configuration = this.router.getCurrentNavigation().extras.state.configuration;
    if (configuration) {
      this.deyelPageConfiguration = configuration;
      this.showInTab = true;
      return;
    }
    const idPage = this.router.getCurrentNavigation().extras.state.idPage;
    if (idPage) {
      this.deyelPageConfiguration = new DeyelPageConfiguration(idPage);
    }
  }

  /**
  * It returns from app configuration if the header should be displayed.
  * @returns boolean
  */
  public showHeader(): boolean {
    return this.appConfigurationService.getShowHeader();
  }

  /**
   * It returns the fab button should be displayed from configuration.
   * @return {boolean}
   */
  public showFabButton(): boolean {
    return this.appConfigurationService.showFabButton();
  }

  /**
   * Subscribes the app events.
   * @return {undefined}
   */
  public subscribeAppEvents(): void {
    this.eventService.subscribeEvents().subscribe((event: any) => {
      if (event.type === 'change_token' && this.token !== this.authProvider.getToken()) {
        this.token = this.authProvider.getToken();
        this.loadUrl();
      }
    });
  }

  /**
   * Sets the handler function of the buttons in toast configuration.
   * @param {any} toastOptions
   * @param {any} source
   */
  private processButtonsInToast(toastOptions: any, source: any): void {
    if (toastOptions.buttons && toastOptions.buttons.length > 0) {
      toastOptions.buttons.forEach((button: any) => {
        button.handler = () => {
          source.window.postMessage({operation: 'button_executed_in_toast', role: button.role}, '*');
        }
      });
    }
  }

}
