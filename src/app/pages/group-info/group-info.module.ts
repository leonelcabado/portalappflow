import {NgModule} from '@angular/core';
import {GroupInfoPage} from './group-info';
import {PipesModule} from '../../pipes/pipes.module';
import {RouterModule, Routes} from '@angular/router';
import {IonicModule} from '@ionic/angular';
import {CommonModule} from '@angular/common';

const routes: Routes = [
    {
        path: 'group-info/:id',
        component: GroupInfoPage,
    },
];

@NgModule({
    declarations: [
        GroupInfoPage,
    ],
    imports: [
        RouterModule.forChild(routes),
        PipesModule,
        IonicModule,
        CommonModule
    ],
})
export class GroupInfoPageModule {
}
