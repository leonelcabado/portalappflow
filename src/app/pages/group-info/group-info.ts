import {Component} from '@angular/core';
import {AlertController, PopoverController, ToastController, LoadingController} from '@ionic/angular';
import {DomSanitizer} from '@angular/platform-browser';
import {Router} from '@angular/router';
import {Location} from '@angular/common';

//models
import {Participant} from '../../model/participant.entity';
import {Conversation} from '../../model/conversation.entity';

//providers
import {UserProvider} from '../../services/user.service';
import {BackButtonProvider} from '../../services/back-button.service';

//services
import {ParticipantService} from '../../services/participant.service';
import {ConversationService} from '../../services/conversation.service';
import {NetworkService} from '../../services/network.service';
import {TranslateService} from '../../services/translate.service';

//pages
import {GroupParticipantMenuPage} from '../group-participant-menu/group-participant-menu';

//config
import {APP} from '../../app.config';
import { LogService } from 'src/app/services/log.service';


@Component({
    selector: 'page-group-info',
    templateUrl: 'group-info.html',
    styleUrls: ['group-info.scss']
})
export class GroupInfoPage {

    canEdit: boolean = false;
    canLeave: boolean = false;
    canDelete: boolean = false;
    dsCreator: string;
    conversation: Conversation;
    APP = APP;

    constructor(private userProvider: UserProvider,
                private popoverCtrl: PopoverController,
                private backButtonProvider: BackButtonProvider,
                private alertCtrl: AlertController,
                private toastCtrl: ToastController,
                private loadingCtrl: LoadingController,
                public sanitizer: DomSanitizer,
                private participantService: ParticipantService,
                private conversationService: ConversationService,
                private networkService: NetworkService,
                private translateService: TranslateService,
                private router: Router,
                private location: Location,
                private logService: LogService) {
        if (this.router.getCurrentNavigation().extras.state && this.router.getCurrentNavigation().extras.state.conversation) {
            this.conversation = this.router.getCurrentNavigation().extras.state.conversation;
        }
    }

    setDsCreator() {
        this.dsCreator = "-";
        if (this.conversation.cdUserCreator == this.userProvider.getUsername().toUpperCase()) {
            this.dsCreator = this.userProvider.dsUser;
        }
        else {
            let participant = this.conversation.getParticipantByCd(this.conversation.cdUserCreator);
            if (participant) {
                this.dsCreator = participant.dsParticipant;
            }
            else {
                this.participantService.getOne(this.conversation.cdUserCreator, "USER").then((part) => {
                    if (part) {
                        this.dsCreator = part.dsParticipant;
                    }
                });
            }
        }
    }

    ngOnInit() {
        this.setDsCreator();
        this.canEdit = this.conversation.cdUserCreator == this.userProvider.getUsername().toUpperCase() && !this.conversation.blocked;
        this.canLeave = !this.conversation.blocked;
        this.canDelete = this.conversation.blocked;
    }

    edit() {
        if (this.userProvider.inSync) {
            this.errorInSync();
            return;
        }
        if (this.networkService.getConnected()) {
            this.alertCtrl.create({
                header: this.translateService.instant('NEW_GROUP_NAME'),
                inputs: [
                    {
                        name: 'name',
                        placeholder: this.translateService.instant('GROUP_NAME'),
                        value: this.conversation.dsTitle,
                        id: 'conversation-name'
                    }
                ],
                buttons: [
                    {
                        text: this.translateService.instant('CANCEL'),
                        role: 'cancel',
                        handler: data => {
                            console.warn('Cancel clicked');
                        }
                    },
                    {
                        text: this.translateService.instant('CHANGE'),
                        handler: data => {
                            if (this.networkService.getConnected()) {
                                if (data.name.trim() != "") {
                                    this.updateGroupName(data.name);
                                }
                                else {
                                    this.alertCtrl.create({
                                        header: this.translateService.instant('CHANGE_GROUP_NAME_ERROR'),
                                        message: this.translateService.instant('CHANGE_GROUP_NAME_ERROR_MESSAGE'),
                                        buttons: [
                                            {
                                                text: this.translateService.instant('ACCEPT'),
                                                handler: () => {
                                                    this.edit();
                                                }
                                            }
                                        ]
                                    }).then(errorAlert => {
                                        errorAlert.present();
                                    });
                                }
                            }
                            else {
                                this.errorNotConnected();
                            }
                        }
                    }
                ]
            }).then(alert => {
                this.backButtonProvider.registerDismissable(alert);
                alert.present();
            });
        }
        else {
            this.errorNotConnected();
        }
    }

    errorNotConnected() {
        this.alertCtrl.create({
            header: this.translateService.instant('NOT_CONNECTION_ERROR'),
            message: this.translateService.instant('NOT_CONNECTION_ERROR_MESSAGE'),
            buttons: [this.translateService.instant('ACCEPT')]
        }).then(alert => {
            this.backButtonProvider.registerDismissable(alert);
            alert.present();
        });
    }

    errorInSync() {
        this.alertCtrl.create({
            header: this.translateService.instant('NOT_SERVER_WORKING'),
            message: this.translateService.instant('NOT_SERVER_WORKING_MESSAGE'),
            buttons: [this.translateService.instant('ACCEPT')]
        }).then(alert => {
            this.backButtonProvider.registerDismissable(alert);
            alert.present();
        });
    }

    updateGroupName(name) {
        this.loadingCtrl.create({
            spinner: 'dots',
            message: this.translateService.instant('UPDATING_GROUP_NAME')
        }).then(loading => {
            loading.present();
            this.conversationService.updateTitle(this.conversation, name).then(() => {
                loading.dismiss();
                this.toastCtrl.create({
                    message: this.translateService.instant('GROUP_NAME_CHANGE_SUCCESS'),
                    duration: 2500,
                    position: 'bottom',
                    cssClass: 'toast-ok'
                }).then(toast => toast.present());
            })
                .catch((error) => {
                    this.logService.logError(error, this.userProvider.getUser());
                    console.error("Error al cambiar el nombre del grupo: ", JSON.stringify(error));
                    loading.dismiss();
                    this.errorNotConnected();
                });
        });
    }

    isActualParticipant(participant: Participant) {
        return this.userProvider.getUsername().toUpperCase() == participant.cdParticipant;
    }

    selectedParticipant(participant: Participant) {
        if (!this.isActualParticipant(participant)) {
            this.popoverCtrl.create({
                component: GroupParticipantMenuPage,
                componentProps: {
                    canEdit: this.canEdit,
                    participant: participant,
                    conversation: this.conversation
                }
            }).then(participantModal => {
                participantModal.present();
                this.backButtonProvider.registerDismissable(participantModal);
            });
        }
    }

    back() {
        this.location.back();
    }

    addParticipants() {
        if (this.userProvider.inSync) {
            this.errorInSync();
            return;
        }
        if (this.networkService.getConnected()) {
            this.router.navigate(['/group-participants/' + this.conversation.cdConversation], {state: {conversation: this.conversation}});
        }
        else {
            this.errorNotConnected();
        }
    }

    delete() {
        if (this.userProvider.inSync) {
            this.errorInSync();
            return;
        }
        if (this.networkService.getConnected()) {
            this.alertCtrl.create({
                header: this.translateService.instant('CONFIRM'),
                message: this.translateService.instant('DELETE_GROUP_CONFIRM_MESSAGE'),
                buttons: [
                    {
                        text: this.translateService.instant('CANCEL'),
                        role: 'cancel',
                        handler: () => {
                        }
                    },
                    {
                        text: this.translateService.instant('ACCEPT'),
                        handler: () => {
                            if (this.networkService.getConnected()) {
                                this.loadingCtrl.create({
                                    spinner: 'dots',
                                    message: this.translateService.instant('DELETING_GROUP')
                                }).then(loading => {
                                    loading.present();
                                    this.conversationService.deleteConversationHistory(this.conversation).then(
                                        _ => {
                                            loading.dismiss();
                                            this.toastCtrl.create({
                                                message: this.translateService.instant('GROUP_REMOVE_SUCCESS'),
                                                duration: 2500,
                                                position: 'bottom',
                                                cssClass: 'toast-ok'
                                            }).then(toast => {
                                                this.router.navigate(['/main/chat/list']);
                                                toast.present();
                                            });
                                        })
                                        .catch((error) => {
                                            this.logService.logError(error, this.userProvider.getUser());
                                            loading.dismiss();
                                            console.error("Error al eliminar el grupo: ", JSON.stringify(error));
                                        });
                                });
                            }
                            else {
                                this.errorNotConnected();
                            }
                        }
                    }
                ]
            }).then(alert => {
                this.backButtonProvider.registerDismissable(alert);
                alert.present();
            });
        }
        else {
            this.errorNotConnected();
        }
    }

    leave() {
        if (this.userProvider.inSync) {
            this.errorInSync();
            return;
        }
        if (this.networkService.getConnected()) {
            this.alertCtrl.create({
                header: this.translateService.instant('CONFIRM'),
                message: this.translateService.instant('LEAVE_GROUP_CONFIRM_MESSAGE'),
                buttons: [
                    {
                        text: this.translateService.instant('CANCEL'),
                        role: 'cancel',
                        handler: () => {
                        }
                    },
                    {
                        text: this.translateService.instant('ACCEPT'),
                        handler: () => {
                            if (this.networkService.getConnected()) {
                                this.loadingCtrl.create({
                                    spinner: 'dots',
                                    message: this.translateService.instant('LEAVING_GROUP')
                                }).then(loading => {
                                    loading.present();
                                    let p = this.conversation.getParticipantByCd(this.userProvider.getUser().username.toUpperCase());
                                    this.conversationService.removeConversationParticipants(this.conversation, [p])
                                        .then(() => {
                                            // let dsRemoveParticipant = p.dsParticipant;
                                            // let conversationRepository = this.dbProvider.getRepository(Conversation) as Repository<Conversation>;
                                            // this.conversation.removeParticipant(p.cdParticipant);
                                            this.conversation.blocked = true;
                                            // this.conversation.dtLastChange = new Date().getTime();
                                            // conversationRepository.save(this.conversation)
                                            //   .then(() => {
                                            loading.dismiss();
                                            this.toastCtrl.create({
                                                message: this.translateService.instant('LEAVE_GROUP_SUCCESS'),
                                                duration: 2500,
                                                position: 'bottom',
                                                cssClass: 'toast-ok'
                                            }).then(toast => {
                                                toast.present();
                                            });
                                            this.canLeave = false;
                                            this.canDelete = true;

                                            // })
                                            // .catch((err) => {
                                            //   loading.dismiss();
                                            // });
                                        })
                                        .catch((error) => {
                                            this.logService.logError(error, this.userProvider.getUser());
                                            loading.dismiss();
                                            this.errorNotConnected();
                                        });
                                });
                            }
                            else {
                                this.errorNotConnected();
                            }
                        }
                    }
                ]
            }).then(alert => {
                this.backButtonProvider.registerDismissable(alert);
                alert.present();
            });
        }
        else {
            this.errorNotConnected();
        }
    }

    showFullGroupName() {
        this.alertCtrl.create({
            header: this.translateService.instant('FULL_GROUP_NAME'),
            message: this.conversation.dsTitle,
            buttons: [this.translateService.instant('ACCEPT')]
        }).then(alert => {
            this.backButtonProvider.registerDismissable(alert);
            alert.present();
        });
    }
}
