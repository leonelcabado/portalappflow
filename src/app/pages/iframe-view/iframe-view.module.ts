import {NgModule} from '@angular/core';
import {IframeViewPage} from './iframe-view';
import {RouterModule, Routes} from '@angular/router';
import {IonicModule} from '@ionic/angular';
import {CommonModule} from '@angular/common';

const routes: Routes = [
    {
        path: 'iframe-view',
        component: IframeViewPage,
    },
];

@NgModule({
    declarations: [
        IframeViewPage,
    ],
    imports: [
        RouterModule.forChild(routes),
        IonicModule,
        CommonModule
    ],
})
export class IframeViewPageModule {
}
