import { Component, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { Platform, AlertController, PopoverController, ActionSheetController } from '@ionic/angular';
import { AppMinimize } from '@ionic-native/app-minimize/ngx';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { Location } from '@angular/common';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { UserProvider } from '../../services/user.service';
import { AuthProvider } from '../../services/auth.service';
import { BackButtonProvider } from '../../services/back-button.service';
import { TranslateService } from '../../services/translate.service';
import { NetworkService } from '../../services/network.service';
import { TaskService } from '../../services/task.service';
import { AttachService } from '../../services/attach.service';
import { NotificationProvider } from '../../services/notification.service';
import { AppConfigurationService } from '../../services/app-configuration.service';
import { TaskMoreActionsPage } from '../task-more-actions/task-more-actions';
import { Task } from 'src/app/model/task.entity';
import { LogService } from 'src/app/services/log.service';

@Component({
    selector: 'page-iframe-view',
    templateUrl: 'iframe-view.html',
    styleUrls: ['iframe-view.scss']
})
export class IframeViewPage {

    url: SafeResourceUrl;
    title: string;
    paramURL: string;
    @ViewChild('frame') frame: ElementRef;
    loadsiFrame: number = 0;
    actions: any = "";
    iframe: any = "";
    menuActionSheet: any;
    processMessageFn: any;
    private previousPageData: any;

    constructor(private location: Location,
                private platform: Platform,
                private appMinimize: AppMinimize,
                public userProvider: UserProvider,
                private authProvider: AuthProvider,
                private notificationProvider: NotificationProvider,
                private taskService: TaskService,
                private sanitizer: DomSanitizer,
                private backButtonProvider: BackButtonProvider,
                private alertCtrl: AlertController,
                private translateService: TranslateService,
                private networkService: NetworkService,
                private appConfigurationService: AppConfigurationService,
                private popoverCtrl: PopoverController,
                private attachService: AttachService,
                private changeDetectorRef: ChangeDetectorRef,
                private actionSheetController: ActionSheetController,
                private router: Router,
                public alertController: AlertController,
                private logService: LogService) { }

    ngOnInit() {
        this.setParametersFromRoute();
        this.notificationProvider.clearNotification(this.userProvider.getConsultedTask());
    }

    ionViewDidEnter(){
        this.processMessageFn = (event) => this.processMessage(event);
        window.addEventListener('message', this.processMessageFn, false);
        this.url = this.sanitizer.bypassSecurityTrustResourceUrl(this.userProvider.getContextUrl() + '/SLResourceShorcut?access_token=' + this.authProvider.getToken() + '&_idCreateSession=true&_idExecuteInMainFrame=false&_idResource=' + this.paramURL);
    }

    ionViewWillLeave(){
        window.removeEventListener('message', this.processMessageFn, false);
    }

    /**
     * Subscribe to the back button event.
     */
    private subscribeBackButton(): void {
        this.platform.backButton.subscribe(() => {
            this.goToBack();
        });
    }

    /**
     * Set the parameters passed by the route.
     */
    private setParametersFromRoute(): void {
        if (this.router.getCurrentNavigation().extras.state) {
            if (this.router.getCurrentNavigation().extras.state.url) {
                this.paramURL = this.router.getCurrentNavigation().extras.state.url;
            }
            if (this.router.getCurrentNavigation().extras.state.title) {
                this.title = this.router.getCurrentNavigation().extras.state.title;
            }
            if (this.router.getCurrentNavigation().extras.state.previousPageData) {
                this.previousPageData = this.router.getCurrentNavigation().extras.state.previousPageData;
            }
        }
    }

    processMessage(event){
        switch (event.data.operation){
            case 'mobile_actions':
                this.actions = [];
                this.processActionsLength(event.data.actions);
                break;
            case 'attach':
                this.attachService.attachEventFromIframe(event.data.type, event.data.elementId, event.data.containerId, event.data.iterativeIndex, this.frame);
                break;
            case 'download':
                this.attachService.downloadFileWithStoreableString(event.data.elementValue);
                break;
            case 'open-url':
                this.attachService.showBase64(event.data.url, 'application/pdf', 'Zurich Santander');
                break;
            case 'running_case':
                this.userProvider.setConsultedTask(event.data.nuCase);
                break;
        }
    }

    /**
     * Return to the previous page.
     */
    public goToBack(): void {
        if (this.userProvider.getConsultedTask()) {
            this.userProvider.setConsultedTask(null);
        }
        if (this.previousPageData) {
            this.router.navigate([this.previousPageData.route], this.previousPageData.data);
            return;
        }
        this.appMinimize.minimize();
    }

    errorNotConnected() {
        this.alertCtrl.create({
            header: this.translateService.instant('NOT_CONNECTION_ERROR'),
            message: this.translateService.instant('NOT_CONNECTION_ERROR_MESSAGE'),
            buttons: [this.translateService.instant('ACCEPT')]
        }).then(alert => {
            this.backButtonProvider.registerDismissable(alert);
            alert.present();
        });
    }

    /**
     * Creates the action sheets view.
     * @param openActionSheet
     */
    public createActionSheets(openActionSheet: boolean): void {
        this.actionSheetController.create(
            {
                buttons: this.createButtonsSheets()
            }
        ).then(actionSheet => {
            this.menuActionSheet = actionSheet;
            if (openActionSheet) {
                this.openActionSheets();
            }
        });
    }

    /**
    * It returns the fab button style.
    * @return {string}
    */
    public getFabButtonStyle(): string {
        return this.appConfigurationService.getFabButtonStyle();
    }

    /**
    * It returns the fab list button style.
    * @return {string}
    */
    public getFabListButtonStyle(): string {
        return this.appConfigurationService.getFabListButtonStyle();
    }

    /**
     * Creates the action buttons to the action sheets view.
     */
    private createButtonsSheets(): Array<any> {
        return this.actions.map((action: any) => {
            return {
                text: action.dsName,
                handler: () => {
                    this.executeAction('execute_action', action);
                }
            };
        });
    }

    /**
     * Run the first action.
     */
    public executeFirstAction(): void {
        this.executeAction('execute_action', this.actions[0]);
    }

    public executeAction(operation: string, action: any): void {
        if (this.frame.nativeElement && this.frame.nativeElement.contentWindow) {
            this.frame.nativeElement.contentWindow.postMessage({operation: operation, action: action}, '*');
        }
    }

    /**
     * Open the action sheets and registers it in back button provider.
     */
    public openActionSheets(): void {
        if (this.menuActionSheet) {
            this.backButtonProvider.registerDismissable(this.menuActionSheet);
            this.menuActionSheet.present();
        }
    }

    /**
     * Process the actions length.
     */
    private processActionsLength(actions: Array<any>): void {
        this.actions = actions;
        if (this.actions && this.actions.length > 0) {
            if (this.actions.length === 1) {
                this.changeDetectorRef.detectChanges();
                return;
            }
        }
        this.changeDetectorRef.detectChanges();
    }

    /**
     * It determinates if show the save button and check button.
     * @return {boolean}
     */
    public checkShowSaveButton(): boolean {
        if (this.actions && this.actions.length === 2) {
            return this.actions[0].id === 'botonAceptar' && this.actions[1].id === 'saveButton';
        }
        return false;
    }

    public presentAlertActions(): void {
        this.popoverCtrl.create({
            component: TaskMoreActionsPage,
            componentProps: { actions: this.actions },
            cssClass: 'actions-modal'
        }).then(tasksModal => {
            tasksModal.present();
            this.backButtonProvider.registerDismissable(tasksModal);
            tasksModal.onDidDismiss().then(executeAction => {
                if (executeAction.data && executeAction.data != 'BACKBUTTON') this.executeAction("execute_action", executeAction.data)
            });
        });
    }

}
