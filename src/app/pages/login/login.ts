import { Component, ChangeDetectorRef } from '@angular/core';
import { Platform, AlertController, LoadingController, IonRouterOutlet } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { Router } from '@angular/router';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { AppMinimize } from '@ionic-native/app-minimize/ngx';
import { UserProvider } from '../../services/user.service';
import { LoaderProvider } from '../../services/loader.service';
import { DbProvider } from '../../services/db.service';
import { BackButtonProvider } from '../../services/back-button.service';
import { VersionUpdateService } from '../../services/version-update.service';
import { NetworkService } from '../../services/network.service';
import { Context } from '../../model/context.entity';
import { DeyelPageConfiguration } from '../../model/deyel-page-configuration.entity';
import { ContextsProvider } from '../../services/contexts.service';
import { ActionService } from '../../services/action.service';
import { LogService } from 'src/app/services/log.service';
import { AppConfigurationService } from '../../services/app-configuration.service';
import { SecurityConfigurationService } from 'src/app/services/security-configuration.service';
import { EventService } from 'src/app/services/event.service';

@Component({
    selector: 'page-login',
    templateUrl: 'login.html',
    styleUrls: ['login.scss']
})
export class LoginPage {

    public contexts = [];
    public loginForm: FormGroup;
    public selectedContext: Context;
    public pageConfiguration: DeyelPageConfiguration;
    public openedKeyboard: boolean;
    loader: any;
    loginIn: boolean = false;
    public showPassword = false;
    public showPage: boolean;
    public pageUrl: SafeResourceUrl;
    public allowBack: boolean;
    private processEventFn: any;

    constructor(public contextsProvider: ContextsProvider,
                public alertCtrl: AlertController,
                public formBuilder: FormBuilder,
                public loadingCtrl: LoadingController,
                public keyboard: Keyboard,
                private splashScreen: SplashScreen,
                private changeDetectorRef:ChangeDetectorRef,
                private userProvider: UserProvider,
                private loaderProvider: LoaderProvider,
                private dbProvider: DbProvider,
                private networkService: NetworkService,
                private backButtonProvider: BackButtonProvider,
                private versionUpdateService: VersionUpdateService,
                private router: Router,
                private actionService: ActionService,
                private appConfigurationService: AppConfigurationService,
                private logService: LogService,
                private routerOutlet: IonRouterOutlet,
                private securityConfigurationService: SecurityConfigurationService,
                private eventService: EventService,
                private platform: Platform,
                private appMinimize: AppMinimize,
                protected domSanitizer: DomSanitizer) {}

    ngOnInit() {
        this.openedKeyboard = false;
        this.showPage = false;
        this.backButtonProvider.registerView('login', '/login');
        this.initializeForm();
        this.setPageConfiguration();
        this.subscribeKeyboardEvents();
        this.subscribeHardwareBackButton();
    }

    ionViewDidEnter() {
        this.appConfigurationService.setStatusBar(this.pageConfiguration);
        this.initializeContexts();
        this.processConfiguration();
        this.subscribeWindowEvents();
    }

    public ionViewWillLeave(): void {
        window.removeEventListener('message', this.processEventFn, false);
    }

    /**
    * Susbscribes the component to window events.
    * @return {undefined}
    */
    public subscribeWindowEvents(): void {
        this.processEventFn = (event) => this.processEvent(event);
        window.addEventListener('message', this.processEventFn, false);
    }

    public goToContexts() {
        this.router.navigate(['/context-list'], {state: {contexts: this.contexts}});
    }

    presentLoading() {
        return this.loadingCtrl.create({
            spinner: null,
            cssClass: "firstload",
            message: '<ion-img src="../../../../resources/android/icon/drawable-xxhdpi-icon.png" alt="loading..."></ion-img>' +
                '<ion-text>Loading user data...</ion-text>' +
                '<div class="progress-bar">' +
                    '<div class="progress-bar-value"></div>' +
                '</div>'
        }).then(loader => {
            this.loader = loader;
            this.loader.present();
        });
    }

    dismissLoading() {
        if (this.loader) {
            this.loader.dismiss();
        }
    }

    public signIn() {
        if (!this.networkService.getConnected()) {
            this.alertCtrl.create({
                header: 'Network disconnected',
                subHeader: 'Internet connection is not working',
                buttons: ['OK']
            }).then(alert => {
                this.backButtonProvider.registerDismissable(alert);
                alert.present();
            });
            return;
        }

        if (!this.selectedContext) {
            this.alertCtrl.create({
                header: 'Login Error',
                subHeader: 'Please select context.',
                buttons: ['OK']
            }).then(alert => {
                this.backButtonProvider.registerDismissable(alert);
                alert.present();
            });
            return;
        }


        if (!this.loginForm.valid || !this.selectedContext) {
            this.alertCtrl.create({
                header: 'Login Error',
                subHeader: 'Authentication failed: please verify your username and password.',
                buttons: ['OK']
            }).then(alert => {
                this.backButtonProvider.registerDismissable(alert);
                alert.present();
            });
            return;
        }

        this.loginIn = true;
        this.userProvider.login(this.loginForm.get('username').value, this.loginForm.get('password').value, this.selectedContext)
            .then(
                (userFirstTime: boolean) => {
                    if (this.allowBack) {
                        this.routerOutlet.pop();
                        return;
                    }
                    this.splashScreen.show();
                    this.loaderProvider.init(userFirstTime)
                        .then(async () => {
                                this.loginIn = false;
                                this.versionUpdateService.checkAppUpdate();
                                this.actionService.getActions();
                                await this.securityConfigurationService.getSecurityFunctions();
                                const firstTabUrl = this.appConfigurationService.getFirstTabUrl(this.securityConfigurationService);
                                this.router.navigate([firstTabUrl], { state: { configuration: this.appConfigurationService.getConfigurationByUrl(firstTabUrl) }});
                                this.splashScreen.hide();
                            },
                            (error) => {
                                //this.logService.logError(error, this.userProvider.getUser());
                                this.loginIn = false;
                                this.onError(error);
                                this.splashScreen.hide();
                            });
                },
                (error) => {
                    //this.logService.logError(error, this.userProvider.getUser());
                    this.loginIn = false;
                    this.onError(error);
                }
            );
    }

    onError(error) {
        let errorMsg;
        if ((error) == '401')
            errorMsg = 'Authentication failed: please verify your username and password.';
        else
            errorMsg = 'Ups! Something went wrong...';

        this.loginForm.get('password').setValue("");
        this.alertCtrl.create({
            header: 'Login Error',
            subHeader: errorMsg,
            buttons: ['OK']
        }).then(alert => {
            alert.present();
        });
    }

    toggleShowPassword(){
        this.showPassword = !this.showPassword;
    }

    /**
     * It initializes the login form.
     */
    private initializeForm(): void {
        this.loginForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        });
    }

    /**
     * It changes the selected context.
     * @param {any} selectedValue
     */
    public onSelectChange(selectedValue: any): void {
        if (this.contexts && this.contexts.length > 1) {
            this.selectedContext = this.contexts.filter((context: Context) => {
                return context.id == selectedValue.detail.value;
            })[0];
        }
    }

    /**
     * It checks if exist a initial context in the app configuration.
     * @return {boolean}
     */
    public checkInitialContextInConfiguration(): boolean {
        return this.appConfigurationService.checkInitialContext();
    }

    /**
     * It initializes the initial context or the contexts list.
     * @return {undefined}
     */
    private initializeContexts(): void {
        if (this.checkInitialContextInConfiguration()) {
            this.contextsProvider.processInitialContext().then((context: Context) => {
                this.selectedContext = context;
                this.changeDetectorRef.detectChanges();
            })
            .catch((error) => {
                this.logService.logError(error, null);
            });
        }
        this.processContexts();
    }

    /**
     * It sets and process the contexts.
     * @return {undefined}
     */
    private processContexts(): void {
        this.contextsProvider.processContexts().then((contexts: Array<Context>) => {
            if (contexts && contexts.length > 0) {
                this.contexts = contexts;
                this.selectedContext = contexts[0];
                this.changeDetectorRef.detectChanges();
            }
        })
        .catch((error) => {
            this.logService.logError(error, null);
        });
    }

    /**
     * It sets the page configuration.
     * @return {undefined}
     */
    private setPageConfiguration(): void {
        const configuration = this.router.getCurrentNavigation().extras.state.configuration;
        if (configuration) {
            this.pageConfiguration = configuration;
        }
        const allowBack = this.router.getCurrentNavigation().extras.state.allowBack;
        if (allowBack) {
            this.allowBack = allowBack;
        }
    }

    /**
     * It navigates to the page url.
     * @return {undefined}
     */
    public goToTitle(): void {
        this.router.navigate(['/title'], { state: { configuration: this.appConfigurationService.getConfigurationByUrl('/title') }});
    }

    /**
     * It returns if the form is valid.
     * @return {boolean}
     */
    public isValidForm(): boolean {
        return this.loginForm.valid;
    }

    /**
     * It subscribes to keyboard events.
     */
    public subscribeKeyboardEvents(): void {
        this.keyboard.onKeyboardShow().subscribe(() => {
            this.openedKeyboard = true;
            this.changeDetectorRef.detectChanges();
        });
        this.keyboard.onKeyboardHide().subscribe(() => {
            this.openedKeyboard = false;
            this.changeDetectorRef.detectChanges();
        });
    }

    /**
     * Validates value for background image.
     */
     public getBackgroundImage(): string {
        if(this.pageConfiguration && !this.pageConfiguration.getBackgroundImage()){
            let top = this.pageConfiguration.getTopBackgroundColor() ? this.pageConfiguration.getTopBackgroundColor() : "#FFFFFF";
            if(this.pageConfiguration.getStatusBarConfiguration() && this.pageConfiguration.getStatusBarConfiguration().getBackgroundColor()){
                top = this.pageConfiguration.getStatusBarConfiguration().getBackgroundColor();
            }
            let bottom = this.pageConfiguration.getBottomBackgroundColor() ? this.pageConfiguration.getBottomBackgroundColor() : "#E0F6FD";
            let backgroundValue = `--background: transparent linear-gradient(180deg, ${top} 0%, ${bottom} 100%) 0% 0% no-repeat padding-box`;
            return backgroundValue;
        }
        return '';
    }

    /**
     * It returns if the form is valid.
     * @return {undefined}
     */
    public processConfiguration(): void {
        if (this.appConfigurationService.showLoginPage()) {
            this.showPage = true;
            if (this.pageConfiguration.getIdPage()) {
                this.loadUrl();
            }
        }
    }

    /**
    * It loads the page url to show.
    */
    public loadUrl(): void {
        this.pageUrl = undefined;
        this.pageUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(this.appConfigurationService.getInitialContextUrl() + '/SLPage?idPage=' + this.pageConfiguration.getIdPage() + '&mobile=true&isEmbed=true');
    }

    /**
     * Subscribe to hardware back button in devices.
     */
    private subscribeHardwareBackButton(): void {
        this.platform.backButton.subscribeWithPriority(1, (processNextHandler) => {
            if (this.allowBack) {
                this.routerOutlet.pop();
                return;
            }
            this.appMinimize.minimize();
        });
    }

    /**
     * Determinate if it should go back.
     * @return {undefined}
     */
    private processBackLocation(): void {
        if (this.allowBack) {
            this.routerOutlet.pop();
        }
    }

    /**
    * Process the iframe event.
    * @param {any} event
    * @return {undefined}
    */
    private processEvent(event: any): void {
        switch (event.data.operation) {
            case 'logged_user':
                this.processLoginEvent(event.data);
                break;
            case 'redirect_to_page':
                this.router.navigate(['/new-window-url', Math.random() * 100], {state: {url: event.data.url, type: event.data.type, previousPageData: this.appConfigurationService.getConfigurationByUrl('/login')}, skipLocationChange: true });
                break;
        }
    }

    /**
    * Process the login event.
    * @param {any} data
    * @return {undefined}
    */
    private processLoginEvent(data: any): void {
        this.userProvider.loginFromEvent(data.user, data.password, data.access_token, data.refresh_token, this.selectedContext)
            .then((userFirstTime: boolean) => {
                this.splashScreen.show();
                this.loaderProvider.init(userFirstTime)
                    .then(() => {
                        this.navigateHomeAfterLoginEvent();
                        this.splashScreen.hide();
                    })
                    .catch((error) => {
                        this.splashScreen.hide();
                        this.logService.logError(error, this.userProvider.getUser());
                    });
            },
            (error) => {
                this.logService.logError(error, this.userProvider.getUser());
                this.onError(error);
            });
    }

    /**
    * Operations after login event and navigate home.
    * @return {undefined}
    */
    private async navigateHomeAfterLoginEvent() {
        this.actionService.getActions();
        await this.securityConfigurationService.getSecurityFunctions();
        const firstTabUrl = this.appConfigurationService.getFirstTabUrl(this.securityConfigurationService);
        this.router.navigate([firstTabUrl], { state: { configuration: this.appConfigurationService.getConfigurationByUrl(firstTabUrl) }});
        this.eventService.emitEvent({type: 'change_token'});
    }

}
