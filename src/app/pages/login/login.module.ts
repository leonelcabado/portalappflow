import {NgModule} from '@angular/core';
import {LoginPage} from './login';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';
import {CommonModule} from '@angular/common';

const routes: Routes = [
    {
        path: 'login',
        component: LoginPage,
    },
];

@NgModule({
    declarations: [
        LoginPage,
    ],
    imports: [
        RouterModule.forChild(routes),
        FormsModule,
        ReactiveFormsModule,
        IonicModule,
        CommonModule
    ],
})
export class LoginPageModule {
}
