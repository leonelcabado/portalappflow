import {NgModule} from '@angular/core';
import {ChatMenuPage} from './chat-menu';
import {PipesModule} from '../../pipes/pipes.module';
import {RouterModule, Routes} from '@angular/router';
import {IonicModule} from '@ionic/angular';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

const routes: Routes = [
    {
        path: 'chat-menu',
        component: ChatMenuPage,
    },
];

@NgModule({
    declarations: [
        ChatMenuPage,
    ],
    imports: [
        RouterModule.forChild(routes),
        PipesModule,
        IonicModule,
        CommonModule,
        FormsModule
    ],
})
export class ChatMenuPageModule {
}