import {Component} from '@angular/core';
import {AlertController, ToastController, LoadingController, PopoverController} from '@ionic/angular';
import {Router} from '@angular/router';

//models
import {Conversation} from '../../model/conversation.entity';

//service
import {NetworkService} from '../../services/network.service';
import {ConversationService} from '../../services/conversation.service';
import {TranslateService} from '../../services/translate.service';

//providers
import {BackButtonProvider} from '../../services/back-button.service';
import {UserProvider} from '../../services/user.service';
import { LogService } from 'src/app/services/log.service';


@Component({
    selector: 'page-chat-menu',
    templateUrl: 'chat-menu.html',
    styleUrls: ['chat-menu.scss']
})
export class ChatMenuPage {

    conversation: Conversation;

    constructor(private loadingCtrl: LoadingController,
                private alertCtrl: AlertController,
                private toastCtrl: ToastController,
                private userProvider: UserProvider,
                private backButtonProvider: BackButtonProvider,
                private viewCtrl: PopoverController,
                private conversationService: ConversationService,
                private translateService: TranslateService,
                private networkService: NetworkService,
                private router: Router,
                private logService: LogService) {
    }

    ngOnInit() {
    }

    info() {
        this.close();
        this.router.navigate(['/group-info/' + this.conversation.cdConversation], {state: {conversation: this.conversation}});
    }

    removeHistory() {
        if (this.userProvider.inSync) {
            this.errorInSync();
            return;
        }
        this.close();
        if (this.networkService.getConnected()) {
            this.alertCtrl.create({
                header: this.translateService.instant('CONFIRM'),
                message: this.translateService.instant('REMOVE_HISTORY_CONFIRM_MESSAGE'),
                buttons: [
                    {
                        text: this.translateService.instant('CANCEL'),
                        role: 'cancel',
                        handler: () => {
                        }
                    },
                    {
                        text: this.translateService.instant('ACCEPT'),
                        handler: () => {
                            if (this.networkService.getConnected()) {
                                this.loadingCtrl.create({
                                    spinner: 'dots',
                                    message: this.translateService.instant('REMOVING_HISTORY')
                                }).then(loading => {
                                    loading.present();
                                    this.conversationService.deleteConversationHistory(this.conversation).then(
                                        _ => {
                                            loading.dismiss();
                                            this.toastCtrl.create({
                                                message: this.translateService.instant('HISTORY_REMOVE_SUCCESS'),
                                                duration: 2500,
                                                position: 'bottom',
                                                cssClass: 'toast-ok'
                                            }).then(toast => {
                                                this.router.navigate(['/main/chat/list']);
                                                toast.present();
                                            });
                                        })
                                        .catch((error) => {
                                            this.logService.logError(error, this.userProvider.getUser());
                                            // TODO
                                            loading.dismiss();
                                            console.error("Error al eliminar el chat: ", JSON.stringify(error));
                                        });
                                });
                            }
                            else {
                                this.errorNotConnected();
                            }
                        }
                    }
                ]
            }).then(alert => {
                this.backButtonProvider.registerDismissable(alert);
                alert.present();
            });
        }
        else {
            this.errorNotConnected();
        }

    }

    errorNotConnected() {
        this.alertCtrl.create({
            header: this.translateService.instant('NOT_CONNECTION_ERROR'),
            message: this.translateService.instant('NOT_CONNECTION_ERROR_MESSAGE'),
            buttons: [this.translateService.instant('ACCEPT')]
        }).then(alert => {
            this.backButtonProvider.registerDismissable(alert);
            alert.present();
        });
    }

    errorInSync() {
        this.alertCtrl.create({
            header: this.translateService.instant('NOT_SERVER_WORKING'),
            message: this.translateService.instant('NOT_SERVER_WORKING_MESSAGE'),
            buttons: [this.translateService.instant('ACCEPT')]
        }).then(alert => {
            this.backButtonProvider.registerDismissable(alert);
            alert.present();
        });
    }

    close() {
        return this.viewCtrl.dismiss();
    }

}
