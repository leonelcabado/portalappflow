import { NgModule } from '@angular/core';
import { MainTabsPage } from './main-tabs';
import { PipesModule } from '../../pipes/pipes.module';
import { IonicModule } from '@ionic/angular';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

const routes: Routes = [];

@NgModule({
    declarations: [
        MainTabsPage,
    ],
    imports: [
        RouterModule.forChild(routes),
        PipesModule,
        IonicModule,
        CommonModule,
        FontAwesomeModule
    ],
    exports: [
        MainTabsPage
    ]
})
export class MainTabsPageModule {
}
