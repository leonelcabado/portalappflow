import { Component } from '@angular/core';
import {LoadingController, ToastController} from '@ionic/angular';
import { Router, RouterEvent } from '@angular/router';
import { SecurityConfigurationService } from 'src/app/services/security-configuration.service';
import { TabConfiguration } from '../../model/tab-configuration.entity';
import { AppConfigurationService } from '../../services/app-configuration.service';
import { UserProvider } from '../../services/user.service';
import { LoaderProvider } from '../../services/loader.service';
import { LogService } from 'src/app/services/log.service';
import { EventService } from 'src/app/services/event.service';
import { ContextsProvider } from 'src/app/services/contexts.service';
import { ConversationService } from '../../services/conversation.service'
import { ParticipantService } from '../../services/participant.service'
import { TranslateService } from '../../services/translate.service';
import { RTMService } from '../../services/rtm.service';
import { RuleService } from 'src/app/services/rule.service';
import { ActionService } from 'src/app/services/action.service';
import { ConfigurationService } from '../../services/configuration.service';
import { Context } from '../../model/context.entity';
import { Conversation } from '../../model/conversation.entity';
import { Participant } from '../../model/participant.entity';
import { APP } from '../../app.config';
import {SplashScreen} from "@ionic-native/splash-screen/ngx";

@Component({
    selector: 'page-main-tabs',
    templateUrl: 'main-tabs.html',
    styleUrls: ['main-tabs.scss']
})
export class MainTabsPage {

	public activeTab: string;
    public tabsConfigurations: Array<TabConfiguration> = [];
    private processEventFn: any;

    constructor(private router: Router,
                private splashScreen: SplashScreen,
                private loadingController: LoadingController,
                private appConfigurationService: AppConfigurationService,
                private userProvider: UserProvider,
                private loaderProvider: LoaderProvider,
                private contextProvider: ContextsProvider,
                private logService: LogService,
                private eventService: EventService,
                private conversationService: ConversationService,
                private participantService: ParticipantService,
                private translateService: TranslateService,
                private rtmService: RTMService,
                private ruleService: RuleService,
                private actionService: ActionService,
                private configurationService: ConfigurationService,
                private toastCtrl: ToastController,
                private securityConfiguration: SecurityConfigurationService) {}

    public ngOnInit(): void {
        this.subscribeRouteChanges();
        this.initializeTabsConfigurations();
        this.subscribeWindowEvents();
        this.subscribeAppEvents();
    }

    public ionViewWillLeave(): void {
        window.removeEventListener('message', this.processEventFn, false);
    }

    /**
    * Susbscribes the component to window events.
    * @return {undefined}
    */
    public subscribeWindowEvents(): void {
        this.processEventFn = (event) => this.processEvent(event);
        window.addEventListener('message', this.processEventFn, false);
    }

    /**
    * Process the iframe event.
    * @param {any} event
    * @return {undefined}
    */
    private processEvent(event: any): void {
        switch (event.data.operation) {
            case 'redirect_login':
                this.router.navigate(['/login'], { state: { configuration: this.appConfigurationService.getConfigurationByUrl('/login'), allowBack: true}});
                break;
            case 'refresh_token':
                //its obligatory to receive a username
                if(event.data.user && event.data.user != ''){
                    event.data.user = event.data.user.toLowerCase();
                    this.updateNewToken(event.data);
                }else{
                    this.sendToast(this.translateService.instant('USERNAME_NOT_DECLARED'));
                }
                break;
            case 'logged_user':
                //its obligatory to receive a username
                if(event.data.user && event.data.user != ''){
                    event.data.user = event.data.user.toLowerCase();
                    this.processLoginEvent(event.data);
                } else {
                    this.sendToast(this.translateService.instant('USERNAME_NOT_DECLARED'));
                }
                break;
            case 'logout':
                this.logOut();
                break;
            case 'openObjectChat':
                this.openObjectChat(event.data);
                break;
            case 'openUserChat':
                this.openUserChat(event.data);
                break;
            case 'openGroupChat':
                this.openGroupChat(event.data);
                break;
        }
    }

    private sendToast($message: string): void {
        this.toastCtrl.create({
            message: $message,
            duration: 2000,
            position: 'bottom'
        }).then(toast => {
            toast.present();
        });
    }

    /**
     * Subscribe to the route changes.
     */
    public subscribeRouteChanges(): void {
		this.router.events.subscribe((event: RouterEvent) => {
            if(event && event.url) {
                this.activeTab = event.url;
            }
		});
    }

    /**
     * Check if the tab is active.
     * @param  {string}  tabName
     * @return {boolean}
     */
    public isActiveTab(tabName: string): boolean {
        if (this.activeTab) {
            return this.activeTab.includes(tabName);
        }
        return false;
    }

    /**
     * It initializes the tabs configurations.
     */
    private initializeTabsConfigurations(): void {
        this.tabsConfigurations = this.appConfigurationService.getTabsConfigurations();
    }

    /**
     * It opens one tab and sends the configuration.
     * @param {TabConfiguration} tabConfiguration
     */
    public openTab(tabConfiguration: TabConfiguration): void {
        this.router.navigate([tabConfiguration.getUrl()], { state: { configuration: tabConfiguration.getConfiguration() }});
    }

    /**
     * Check if the tab has the appropriate security settings for visibility.
     * @param tab 
     * @returns boolean
     */
    public checkSecurity(tab: any): boolean {
        return this.securityConfiguration.validateSecurity(this.appConfigurationService.getTabVisibilitySecurityCode(tab));
    }

    /**
     * It returns from app configuration if the header should be displayed.
     * @returns boolean
     */
    public showHeader(): boolean {
        return this.appConfigurationService.getShowHeader();
    }

    /**
    * Process the login event.
    * @param {any} data
    * @return {undefined}
    */
    private processLoginEvent(data: any): void {
        this.contextProvider.processInitialContext()
            .then((context: Context) => {
                this.userProvider.loginFromEvent(data.user, data.password, data.access_token, data.refresh_token, context)
                    .then((userFirstTime: boolean) => {
                    this.splashScreen.show();
                        this.loaderProvider.init(userFirstTime)
                            .then(() => {
                                this.splashScreen.hide();
                                this.eventService.emitEvent({ type: 'change_token' });
                            })
                            .catch((error) => {
                                this.splashScreen.hide();
                                this.logService.logError(error, this.userProvider.getUser());
                            });
                    })
                    .catch((error) => {
                        this.logService.logError(error, this.userProvider.getUser());
                    });
            })
            .catch((error) => {
                this.logService.logError(error, this.userProvider.getUser());
            });
    }

    /**
     * It returns from app configuration if the header should be displayed.
     * @param {any} data
     * @returns {undefined}
     */
    public updateNewToken(data: any): void {
        if (this.userProvider.getUser()) {
            this.userProvider.refreshTokenInLoggedUser(data.user, this.userProvider.getUser().getPassword() , this.userProvider.getUser().getContext(), data.access_token, data.refresh_token);
        }
    }

    /**
    * Subscribes the app events.
    * @return {undefined}
    */
    public subscribeAppEvents(): void {
        this.eventService.subscribeEvents().subscribe((event: any) => {
            if (event.type === 'logout') {
                this.logOut();
            }
        });
    }

    /**
    * Closes the user session.
    * @return {undefined}
    */
    private logOut(): void {
        this.loadingController.create({
            spinner: 'dots',
            message: this.translateService.instant('LOGGING_OUT'),
        }).then(loading => {
            loading.present();
            this.userProvider.signOut()
                .then(() => {
                    loading.dismiss();
                    this.successLogOut();
                },
                (err) => {
                    loading.dismiss();
                    this.goToLogin();
                });
        });
    }

    /**
    * Operations performed on a successful logout.
    * @return {undefined}
    */
    private successLogOut(): void {
        this.rtmService.processLogOut();
        this.ruleService.clearTokenDeyel();
        this.conversationService.resetConversations();
        this.participantService.resetParticipants();
        this.actionService.setActionsList(null);
        this.translateService.setTranslations(undefined);
        window.removeEventListener('message', this.processEventFn, false);
        this.goToLogin();
    }

    /**
    * Redirect to login.
    * @return {undefined}
    */
    private goToLogin(): void {
        this.removeTheme();
        this.router.navigate(['/login'], { replaceUrl: true, state: { configuration: this.appConfigurationService.getConfigurationByUrl('/login') } });
    }

    /**
    * Reset the user theme.
    * @return {undefined}
    */
    public removeTheme(): void {
        this.configurationService.notifyThemeChanged('Azul');
    }

    /**
     * Open chat associated with object.
     * @param {any} data
     */
    public openObjectChat(data: any): void {
        this.conversationService.getObjectChat(data.objectId, data.objectURL, data.relatedRuleName, data.relatedRuleVersion, data.objectType, data.create).then((conversation: Conversation) => {
            this.router.navigate(['/chat/' + conversation.getCdConversation()], { state: { conversation: conversation, backWithPop: true } });
        });
    }

    /**
     * Open a private chat with participant.
     * @param {any} data
     */
    public openUserChat(data: any): void {
        this.participantService.getUsers(undefined, [], null, true)
            .then((participants: Participant[]) => {
                this.navigateToConversation(data);
            }).catch(error => {
                this.logService.logError(error, this.userProvider.getUser());
            });
    }

    /**
     * Opens the chat with navigate.
     * @param {any} data
     */
    private navigateToConversation(data: any): void {
        this.conversationService.getConversationByParticipant(this.participantService.getParticipant(data.cdParticipant, data.tpParticipant), true)
            .then((conversation: Conversation) => {
                this.router.navigate(['/chat/' + conversation.getCdConversation()], { state: { conversation: conversation, backWithPop: true } });
            })
            .catch(error => {
                this.logService.logError(error, this.userProvider.getUser());
            });
    }

    /**
     * Open a group chat with conversation code.
     * @param {any} data
     * @return {undefined}
     */
    public openGroupChat(data: any): void {
        this.conversationService.getOne(data.cdConversation)
            .then((conversation: Conversation) => {
                this.router.navigate(['/chat/' + conversation.getCdConversation()], { state: { conversation: conversation, backWithPop: true } });
            })
            .catch(error => {
                this.logService.logError(error, this.userProvider.getUser());
            });
    }

}
