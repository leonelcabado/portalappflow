import {NgModule} from '@angular/core';
import {GroupNewPage} from './group-new';
import {PipesModule} from '../../pipes/pipes.module';
import {RouterModule, Routes} from '@angular/router';
import {IonicModule} from '@ionic/angular';
import {CommonModule} from '@angular/common';
import { FormsModule } from '@angular/forms';

const routes: Routes = [
    {
        path: 'group-new',
        component: GroupNewPage,
    },
];

@NgModule({
    declarations: [
        GroupNewPage,
    ],
    imports: [
        RouterModule.forChild(routes),
        PipesModule,
        IonicModule,
        CommonModule,
        FormsModule
    ],
})
export class GroupNewPageModule {
}
