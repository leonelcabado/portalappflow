import { Component, ViewChild } from '@angular/core';
import { AlertController, LoadingController } from '@ionic/angular';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { ConversationService } from '../../services/conversation.service'
import { TranslateService } from '../../services/translate.service';
import { Participant } from '../../model/participant.entity';
import { BackButtonProvider } from '../../services/back-button.service';
import { LogService } from 'src/app/services/log.service';
import { UserProvider } from 'src/app/services/user.service';
import { AppConfigurationService } from '../../services/app-configuration.service';

@Component({
    selector: 'page-group-new',
    templateUrl: 'group-new.html',
    styleUrls: ['group-new.scss']
})
export class GroupNewPage {

    selectedParticipants: Participant[] = [];
    groupTitle: string = "";
    @ViewChild('groupName') groupNameInput;
    saving: boolean = false;

    constructor(public sanitizer: DomSanitizer,
                private alertCtrl: AlertController,
                private conversationService: ConversationService,
                public loadingCtrl: LoadingController,
                private backButtonProvider: BackButtonProvider,
                private translateService: TranslateService,
                private router: Router,
                private location: Location,
                private userProvider: UserProvider,
                private appConfigurationService: AppConfigurationService,
                private logService: LogService) {
        if (this.router.getCurrentNavigation().extras.state && this.router.getCurrentNavigation().extras.state.participants) {
            this.selectedParticipants = this.router.getCurrentNavigation().extras.state.participants;
        }
    }

    ngOnInit() {
        this.focusGroupNameInput();
    }

    focusGroupNameInput(timeout = 1000) {
        let $this = this;
        setTimeout(() => {
            $this.groupNameInput.setFocus();
        }, timeout)
    }

    back() {
        this.location.back();
    }

    create() {
        if (!this.saving) {
            this.saving = true;
            this.loadingCtrl.create({
                spinner: 'dots',
                message: this.translateService.instant('CREATING_GROUP'),
            }).then(loading => {
                loading.present();
                if (this.groupTitle.trim() != "") {
                    this.conversationService.insertConversationInDb(this.selectedParticipants, this.groupTitle.trim())
                        .then(conv => {
                            this.conversationService.addConversation(conv);
                            this.saving = false;
                            this.router.navigate(['/chat/' + conv.cdConversation], {state: {conversation: conv}});
                            loading.dismiss();
                        })
                        .catch(error => {
                            this.logService.logError(error, this.userProvider.getUser());
                            loading.dismiss();
                            this.saving = false;
                            console.log(error.message);
                        });
                }
                else {
                    loading.dismiss();
                    this.alertCtrl.create({
                        header: this.translateService.instant('GROUP_NAME_REQUIRED_ERROR'),
                        subHeader: this.translateService.instant('GROUP_NAME_REQUIRED_ERROR_MESSAGE'),
                        buttons: [{
                            text: this.translateService.instant('ACCEPT'),
                            role: 'cancel',
                            handler: () => {
                                this.focusGroupNameInput(600);
                            }
                        }]
                    }).then(alert => {
                        this.backButtonProvider.registerDismissable(alert);
                        alert.present();
                    });
                    this.saving = false;
                }
            });
        }
    }

    /**
    * It returns the fab button style.
    * @return {string}
    */
    public getFabButtonStyle(): string {
        return this.appConfigurationService.getFabButtonStyle();
    }

}
