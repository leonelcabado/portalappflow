import {Component, Renderer2, ElementRef, ViewChild} from '@angular/core';
import {AlertController, PopoverController} from '@ionic/angular';

//providers
import {TranslateService} from '../../services/translate.service';


@Component({
    selector: 'page-bot-commands',
    templateUrl: 'bot-commands.html',
    styleUrls: ['bot-commands.scss']
})
export class BotCommandsPage {
    bot: any;

    constructor(public renderer: Renderer2,
                public viewCtrl: PopoverController,
                private alertCtrl: AlertController,
                public translateService: TranslateService) {
        // this.renderer.setElementClass(viewCtrl.pageRef().nativeElement, 'bot-commands', true);
    }

    ngOnInit() {
    }

    runCommand(command) {
        // TODO
        this.viewCtrl.dismiss(command);
    }

}
