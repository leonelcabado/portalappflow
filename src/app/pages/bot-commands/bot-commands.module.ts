import {NgModule} from '@angular/core';
import {BotCommandsPage} from './bot-commands';
import {RouterModule, Routes} from '@angular/router';
import {IonicModule} from '@ionic/angular';
import {CommonModule} from '@angular/common';

const routes: Routes = [
    {
        path: 'bot-commands',
        component: BotCommandsPage,
    },
];

@NgModule({
    declarations: [
        BotCommandsPage,
    ],
    imports: [
        RouterModule.forChild(routes),
        IonicModule,
        CommonModule
    ],
})
export class BotCommandsPageModule {
}
