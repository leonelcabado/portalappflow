import { Component } from '@angular/core';
import { AlertController, Platform, ToastController } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { ContextsProvider } from '../../services/contexts.service';
import { BackButtonProvider } from '../../services/back-button.service';
import { Context } from '../../model/context.entity';
import { LogService } from 'src/app/services/log.service';

@Component({
    selector: 'page-context-form',
    templateUrl: 'context-form.html',
    styleUrls: ['context-form.scss']
})
export class ContextFormPage {

    context: Context = new Context;
    inScanQR: boolean = false;
    contextForm: FormGroup
    scanSub: any;
    validUrl = new RegExp(/^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/i);


    constructor(public contextsProvider: ContextsProvider,
                public formBuilder: FormBuilder,
                private alertCtrl: AlertController,
                private toastController: ToastController,
                private platform: Platform,
                private backButtonProvider: BackButtonProvider,
                private location: Location,
                private router: Router,
                private logService: LogService) {
        this.initForm();
        if (this.router.getCurrentNavigation().extras.state && this.router.getCurrentNavigation().extras.state.context) {
            this.context = this.router.getCurrentNavigation().extras.state.context;
            this.initForm();
        }
    }

    private initForm() {
        this.contextForm = this.formBuilder.group({
            name: [this.context.name, Validators.required],
            url: [this.context.url, Validators.required]
        });
    }

    /**
    * It shows a invalid URL message.
    */
    private showInvalidURLMessage(): void {
        this.alertCtrl.create({
            header: 'Error',
            message: 'Invalid format URL. Please check that the URL is in the following format \"https://example.deyel.com\"',
            buttons: ['Ok']
        }).then(alert => {
            this.backButtonProvider.registerDismissable(alert);
            alert.present();
        });
        return;
    }

    /**
    * Valids the http url format.
    * @param {string} urlContext
    * @return {boolean}
    */
    private validHttpUrl(urlContext: string): boolean {
        try {
            let url = new URL(urlContext);
            return (url.protocol === 'http:' || url.protocol === 'https:') && (url && url.host && url.host.substr(url.host.length - 10) === '.deyel.com');
        } catch(error) {
            return false;
        }
        return false;
    }

    public save() {
        if (this.contextForm.valid) {
            const urlContext = this.contextForm.controls["url"].value;
            if (!this.validUrl.test(urlContext) || !this.validHttpUrl(urlContext)) {
                this.showInvalidURLMessage();
                return;
            }
            this.context.name = this.contextForm.controls["name"].value;
            this.context.url = (urlContext.slice(-1) != "/") ? urlContext : urlContext.substring(0, urlContext.length - 1);
            if (!this.context.id) {
                this.contextsProvider.addContext(this.context.getName(), this.context.getUrl()).then((cont) => {
                    this.showMessageToast('Context added succesfully');
                    this.location.back();
                });
            }
            else {
                this.contextsProvider.updateContext(this.context).then((cont) => {
                    this.showMessageToast('Context updated succesfully');
                    this.location.back();
                });
            }
        }
        else {
            this.alertCtrl.create({
                header: 'Error',
                message: 'Please complete both fields.',
                buttons: ['Ok']
            }).then(alert => {
                this.backButtonProvider.registerDismissable(alert);
                alert.present();
            });
        }
    }

    showCamera() {
        this.inScanQR = true;
        (window.document.querySelector('ion-app') as HTMLElement).classList.add('cameraView');
    }

    hideCamera() {
        this.inScanQR = false;
        (window.document.querySelector('ion-app') as HTMLElement).classList.remove('cameraView');
    }

    public backToContextList() {
        this.location.back();
    }

    /**
     * It Shows a toast with a message.
     * @param {string} message
     */
    private showMessageToast(message: string): void {
        this.toastController.create({
            message: message,
            duration: 2500,
            position: 'bottom',
            cssClass: 'toast-ok'
        }).then(toast => {
            toast.present();
        });
    }

}
