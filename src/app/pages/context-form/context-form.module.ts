import {NgModule} from '@angular/core';
import {ContextFormPage} from './context-form';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';
import {CommonModule} from '@angular/common';

const routes: Routes = [
    {
        path: 'context-form',
        component: ContextFormPage,
    },
];

@NgModule({
    declarations: [
        ContextFormPage,
    ],
    imports: [
        RouterModule.forChild(routes),
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        IonicModule
    ],
})
export class ContextFormPageModule {
}
