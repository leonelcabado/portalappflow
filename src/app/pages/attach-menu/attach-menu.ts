import {Component} from '@angular/core';
import {PopoverController} from '@ionic/angular';

@Component({
    selector: 'page-attach-menu',
    templateUrl: 'attach-menu.html',
    styleUrls: ['attach-menu.scss']
})
export class AttachMenuPage {
    newAttach: any;

    constructor(private viewCtrl: PopoverController) {
    }

    close(origin) {
        this.newAttach.next(origin);
        this.viewCtrl.dismiss();
    }

}
