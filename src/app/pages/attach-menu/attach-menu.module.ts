import {NgModule} from '@angular/core';
import {AttachMenuPage} from './attach-menu';
import {RouterModule, Routes} from '@angular/router';
import {IonicModule} from '@ionic/angular';
import {CommonModule} from '@angular/common';

const routes: Routes = [
    {
        path: 'attach-menu',
        component: AttachMenuPage,
    },
];

@NgModule({
    declarations: [
        AttachMenuPage,
    ],
    imports: [
        RouterModule.forChild(routes),
        IonicModule,
        CommonModule
    ],
})
export class AttachMenuPageModule {
}
