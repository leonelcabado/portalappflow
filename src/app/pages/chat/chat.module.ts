import {NgModule} from '@angular/core';
import {ChatPage} from './chat';
import {PipesModule} from '../../pipes/pipes.module';
import {RouterModule, Routes} from '@angular/router';
import {IonicModule} from '@ionic/angular';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CanDeactivateGuard} from '../../auth/can-deactivate-guard';
import { LinkViewModalComponent } from 'src/app/components/link-view-modal/link-view-modal.component';

const routes: Routes = [
    {
        path: 'chat/:id',
        component: ChatPage,
        canDeactivate: [CanDeactivateGuard],
        pathMatch: 'full'
    },
];

@NgModule({
    declarations: [
        ChatPage,
        LinkViewModalComponent
    ],
    imports: [
        RouterModule.forChild(routes),
        PipesModule,
        FormsModule,
        ReactiveFormsModule,
        IonicModule,
        CommonModule
    ],
    providers: [CanDeactivateGuard],
})
export class ChatPageModule {
}
