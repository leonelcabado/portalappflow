import {Component, ViewChild, OnInit, OnDestroy, ElementRef, NgZone, Renderer2, HostListener} from '@angular/core';
import { IonContent, PopoverController, LoadingController, AlertController, MenuController, IonSlides, IonDatetime, Platform, ModalController, IonRouterOutlet } from '@ionic/angular';
import {DomSanitizer} from '@angular/platform-browser';
import {Subscription, Subject, Observable, fromEvent} from 'rxjs';
import {debounceTime} from 'rxjs/internal/operators';
import {Keyboard} from '@ionic-native/keyboard/ngx';
import {ScreenOrientation} from '@ionic-native/screen-orientation/ngx';
import {File} from '@ionic-native/file/ngx';
import {Camera, CameraOptions} from '@ionic-native/camera/ngx';
import {Location} from '@angular/common';
import {Router} from '@angular/router';

//pages
import {AttachMenuPage} from "../attach-menu/attach-menu";
import {ChatMenuPage} from "../chat-menu/chat-menu";

//services
import {ConversationService} from '../../services/conversation.service';
import {NetworkService} from '../../services/network.service';
import {AttachService} from '../../services/attach.service';
import {SynchronizationService} from '../../services/synchronization.service';
import {TranslateService} from '../../services/translate.service';

//models
import {Participant} from '../../model/participant.entity';
import {Conversation, CONVERSATION_TYPE} from '../../model/conversation.entity';
import {Message} from '../../model/message.entity';
import {Attach} from '../../model/attach.entity';
import {Command} from '../../model/command.entity';
import {MessageAction, MessageActionRequest} from '../../model/message-action.entity';

//providers
import {UserProvider} from '../../services/user.service';
import {NotificationProvider} from '../../services/notification.service';
import {BackButtonProvider} from '../../services/back-button.service';
import {DbProvider} from '../../services/db.service';

//helpers
import {DateOutputFormat} from '../../support-classes/date-output-format.class';
import {BackButtonPressedListener} from '../../support-classes/back-button-pressed-listener';
import {EMOJIS} from '../../support-classes/emoji.data';
import {ApiItemInterface} from '../../support-classes/api-item-interface'

//pipes
import {NG_EMBED_EMOJI_LIST, EmoticonPipe} from '../../pipes/emoticon.pipe';

//config
import {APP} from '../../app.config';

import {CanComponentDeactivate} from "../../auth/can-deactivate-guard";
import { LogService } from 'src/app/services/log.service';
import { AppConfigurationService } from '../../services/app-configuration.service';
import { LinkViewModalComponent } from 'src/app/components/link-view-modal/link-view-modal.component';

@Component({
    selector: 'page-chat',
    templateUrl: 'chat.html',
    styleUrls: ['chat.scss']
})
export class ChatPage implements OnInit, OnDestroy, BackButtonPressedListener, CanComponentDeactivate {

    private subscription: Subscription;
    private onKeyboardShowSubscription: Subscription;
    private onKeyboardHideSubscription: Subscription;
    private onScreenOrientationChangeSubscription: Subscription;
    private onScrollSubscription: Subscription;
    private onFinishSyncSubscription: Subscription;
    private onConversationsUpdatedSubscription: Subscription;
    private nuMessages = 0;
    private nuUsersTyping = 0;
    private shouldScrollDown = true;
    private backWithPop;
    public showBackToBottomButton: boolean;
    private subscriptionAttach: Subscription;
    private newAttach = new Subject<string>();
    newAttach$ = this.newAttach.asObservable();

    // Emojis
    NG_EMBED_EMOJI_LIST = NG_EMBED_EMOJI_LIST;
    EMOJIS = EMOJIS;
    @ViewChild(IonSlides) emojisSlides: IonSlides;
    currentEmojisSlide = 0;
    initialEmojisSlide = {
        initialSlide: 0,
        speed: 400
    }
    userEmojis = [];

    @ViewChild(IonContent) content: IonContent;
    @ViewChild('scrollContent') scrollContent: ElementRef;
    @ViewChild("message") message: ElementRef;
    @ViewChild("dateTime") dateTime: IonDatetime;
    DateOutputFormat = DateOutputFormat;

    attachedFiles: Attach[] = [];
    hasAttachedFiles: boolean = false;
    private maxLengthAttahs: number = 6;
    uploading: boolean = false;
    messages: Message[] = [];
    conversation: Conversation;
    allMessagesLoaded: boolean = false;
    isLoading: boolean = false;
    processAction: boolean = false;
    focusInput: boolean = false;
    hideMessagesClass: string = "hide";
    maxLengthClass: string = "";

    baseUrl: string = "";
    firstLoad = true;

    NUM_MESSAGES_PER_REQUEST = 20;

    isTyping = false;

    cdUser: string = "";

    colors: string[] = [
        "#0080FF", "#86B404", "#8904B1", "#04B486", "#FA58AC", "#DF013A", "#FF8000", "#FFFF00", "#2EFE2E", "#81F7BE", "#A9A9F5", "#0000FF", "#D358F7", "#F5A9D0", "#61210B", "#424242", "#04B404", "#A9F5BC", "#F5DA81", "#F5A9A9", "#F4FA58", "#F5A9F2", "#D0A9F5", "#81F79F", "#D0FA58", "#F5DA81", "#F5BCA9", "#F7819F", "#8A0829", "#6A0888", "#0B3861", "#0B6138", "#01DF3A"
    ];

    participantsColorsMap: Map<string, string> = new Map<string, string>();

    openEmojiPicker = false;
    openDatePicker = false;
    keyboardPresent = false;

    leaving = true;

    command: Command = null;

    @HostListener('click', ['$event.target']) onClickLink(clickedElement) {
        if(clickedElement.classList.contains('ts-link-text')){
            this.openLinkModal(clickedElement.innerHTML);
        }
    }

    constructor(public popoverCtrl: PopoverController,
                private routerOutlet: IonRouterOutlet,
                private conversationService: ConversationService,
                private networkService: NetworkService,
                private synchronizationService: SynchronizationService,
                private loadingCtrl: LoadingController,
                public attachService: AttachService,
                public userProvider: UserProvider,
                public sanitizer: DomSanitizer,
                private alertCtrl: AlertController,
                public menuCtrl: MenuController,
                private zone: NgZone,
                private renderer: Renderer2,
                private notificationProvider: NotificationProvider,
                private backButtonProvider: BackButtonProvider,
                private emoticonPipe: EmoticonPipe,
                private translateService: TranslateService,
                private keyboard: Keyboard,
                private dbProvider: DbProvider,
                private screenOrientation: ScreenOrientation,
                private camera: Camera,
                private file: File,
                private location: Location,
                private router: Router,
                private platform: Platform,
                private appConfigurationService: AppConfigurationService,
                private logService: LogService,
                private modalCtrl: ModalController) {
        if (this.router.getCurrentNavigation().extras.state && this.router.getCurrentNavigation().extras.state.conversation) {
            this.conversation = this.router.getCurrentNavigation().extras.state.conversation;
        }
        if (this.router.getCurrentNavigation().extras.state && this.router.getCurrentNavigation().extras.state.command) {
            this.command = this.router.getCurrentNavigation().extras.state.command;
        }

        this.baseUrl = this.userProvider.getContextUrl() + "/";
        this.cdUser = this.userProvider.getUser().username.toUpperCase();
        this.userEmojis = this.userProvider.getUsedEmojis();
        if (this.userEmojis.length == 0) {
            this.initialEmojisSlide.initialSlide = 1;
            this.currentEmojisSlide = 1;
        }
        this.onFinishSyncSubscription = this.synchronizationService.onFinishSync$.subscribe(_ => this.actualizarMensajes());
        if (this.conversation && this.conversation.cdType == CONVERSATION_TYPE.PRIVATE) {
            this.conversationService.initCommandsForUser(this.conversation.receptor);
        }
        this.onConversationsUpdatedSubscription = this.conversationService.conversationUpdated$.subscribe(() => {
            this.conversation = this.conversationService.getLocalConversationById(this.conversation.cdConversation);
            this.actualizarMensajes();
        });
    }


    loadConversation() {
        if (this.conversation.isGroup) this.loadParticipantsHistory();
        this.loadingCtrl.create({
            spinner: 'dots',
            message: this.translateService.instant('LOADING_MESSAGES'),
        }).then(loading => {
            loading.present();
            this.conversationService.getConversationMessages(this.conversation, this.NUM_MESSAGES_PER_REQUEST, this.conversation.lsMessages.length == 1)
                .then(_ => {
                        this.messages = this.conversation.getLsMessages();
                        this.setRealPathsToAnnexs();
                        if (this.NUM_MESSAGES_PER_REQUEST > this.messages.length) {
                            this.allMessagesLoaded = true;
                        }
                        if (this.conversation.isGroup) this.loadParticipantsHistory();
                        loading.dismiss();
                    },
                    (error) => {
                        this.logService.logError(error, this.userProvider.getUser());
                        console.error("ERROR AL BUSCAR MENSAJES");
                        this.messages = this.conversation.getLsMessages();
                        this.setRealPathsToAnnexs();
                        loading.dismiss();
                    }
                );
        });
    }

    loadMoreMessages(event) {
        return new Promise((resolve, reject) => {
            if (!this.allMessagesLoaded && !this.isLoading) {
                this.isLoading = true;
                let lastcdConversationElement = (this.messages.length != 0) ? this.messages[0].cdConversationElement : null;
                this.conversationService.getConversationMessages(this.conversation, this.NUM_MESSAGES_PER_REQUEST, true)
                    .then(_ => {
                            this.messages = this.conversation.getLsMessages();
                            this.setRealPathsToAnnexs();
                            if (lastcdConversationElement != null && this.messages[0].cdConversationElement == lastcdConversationElement) {
                                this.allMessagesLoaded = true;
                            }
                            this.isLoading = false;
                            event.target.complete();
                            if (this.conversation.isGroup) this.loadParticipantsHistory();
                            resolve();
                        },
                        (error) => {
                            this.logService.logError(error, this.userProvider.getUser());
                            console.log("ERROR AL BUSCAR MAS MENSAJES");
                            this.isLoading = false;
                            event.target.complete();
                            this.hideMessagesClass = "show";
                            resolve();
                        }
                    );
            }
        });
    }

    actualizarMensajes() {
        this.messages = this.conversation.getLsMessages();
        this.setRealPathsToAnnexs();
        setTimeout(() => {
            this.scrollContentToBottom(0);
        }, 300);
    }

    ngAfterViewChecked(): void {
        this.markAsReaded();
        if (this.conversation.lsMessages && this.nuMessages != this.conversation.lsMessages.length) {
            this.nuMessages = this.conversation.lsMessages.length;
        }
        else {
            if (this.conversation.lsUsersTyping) {
                if (this.conversation.lsUsersTyping.length != this.nuUsersTyping) {
                    this.nuUsersTyping = this.conversation.lsUsersTyping.length;
                    ;
                }
            }
        }
    }

    ngOnInit(): void {
        this.showBackToBottomButton = true;
        this.subscription = this.conversationService.newMessage$.subscribe();
        this.onKeyboardShowSubscription = this.keyboard.onKeyboardShow().subscribe(() => {
            this.onKeyboardShow();
        });
        this.onKeyboardHideSubscription = this.keyboard.onKeyboardHide().subscribe(() => {
            this.onKeyboardHide();
        });
        this.shouldScrollDown = true;
        this.onScreenOrientationChangeSubscription = this.screenOrientation.onChange().subscribe(() => {
            this.onScreenOrientationChanged();
        });
        this.subscriptionAttach = this.newAttach$.subscribe(origin => {
            this.attach(origin);
        });
        this.init();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.onFinishSyncSubscription.unsubscribe();
        this.onConversationsUpdatedSubscription.unsubscribe();
        this.onKeyboardShowSubscription.unsubscribe();
        this.onKeyboardHideSubscription.unsubscribe();
        this.onScreenOrientationChangeSubscription.unsubscribe();
        this.userProvider.cdCurrentConversation = null;
    }

    init() {
        if (this.conversation.lsMessages && this.conversation.lsMessages.length > 0) {
            this.loadConversation();
        }
        else {
            this.conversation.lsParticipantsHistory = this.conversation.lsParticipants;
            this.conversation.lsMessages = this.messages;
            this.initColors();
            this.allMessagesLoaded = true;
            if (this.command) this.sendCommand();
        }
        this.setParametersFromRoute();
    }

    ionViewDidEnter() {
        this.conversationService.setActualConversation(this.conversation);
        this.notificationProvider.clearNotification(this.conversation.cdConversation);
        this.userProvider.cdCurrentConversation = this.conversation.cdConversation;
        this.zone.runOutsideAngular(() => {
            if (this.message) {
                this.message.nativeElement.addEventListener('keyup', this.keyup.bind(this));
                fromEvent(this.message.nativeElement, 'keyup').pipe(
                    debounceTime(2000)
                ).subscribe(keyboardEvent => {
                    this.stopTyping();
                });
            }
        });
    }

    ionViewWillLeave() {
        if (!this.leaving && this.conversation && this.conversation.cdType == CONVERSATION_TYPE.PRIVATE && this.messages.length == 0) {
            this.conversationService.countChatsPushed = this.conversationService.countChatsPushed - 1;
            this.conversationService.removeConversationInDb(this.conversation);
        }
    }

    markAsReaded() {
        if (this.conversation.nuUnreaded > 0) {
            this.conversationService.markConversationAsReaded(this.conversation);
        }
    }

    scrollToBottom() {
        if (this.firstLoad) {
            setTimeout(() => {
                this.scrollContentToBottom(0);
                this.hideMessagesClass = "show";
                this.firstLoad = false;
                if (this.command) this.sendCommand();
            }, 700);
        }
    }

    public goToMain() {
        this.router.navigate(['/main']);
    }

    /**
     * Returns to the previous page.
     */
    public goBack(): void {
        if (this.backWithPop) {
            this.routerOutlet.pop();
            return;
        }
        this.router.navigate(['/main/chat/list']);
    }

    menu(myEvent) {
        this.popoverCtrl.create({
            component: ChatMenuPage,
            componentProps: {conversation: this.conversation},
            event: myEvent
        }).then(popover => {
            popover.present();
            this.backButtonProvider.registerDismissable(popover);
        });
    }

    /**
     * Determinates if the file it should download or only show it.
     * @param {Attach}  pFile
     * @param {Message} message
     * @param {boolean} showFile
     */
    public openFile(pFile: Attach, message: Message, showFile: boolean): void {
        if (!pFile.downloading) {
            if (pFile.cdLocalPath && pFile.realPath) {
                this.attachService.showFile(pFile);
            }
            if (!this.userProvider.inSync && !pFile.realPath) {
                this.attachService.downloadFile(pFile, message.cdParticipant, showFile);
            }
        }
    }

    sendCommand() {
        let cdCommand = this.command.cdCommand;
        this.sendMessage(cdCommand);
        this.command = null;
    }

    sendMessage(cdCommand?: string) {
        this.logService.logDevelopment("Entro al send", this.userProvider.getUser());
        this.stopTyping();
        let dsContent = this.formatTextContent().trim();
        if (cdCommand) dsContent = cdCommand;
        if (this.uploading) return;
        if ((dsContent != "" && dsContent.length <= 2000) || this.attachedFiles.length > 0) {
            let m = new Message();
            m.setDsContent(dsContent);
            m.setDtLastUpdate(new Date().getTime());
            m.cdParticipant = this.cdUser;
            m.setTpParticipant('USER');
            m.setCdConversation(this.conversation.cdConversation);
            if (this.attachedFiles.length > 0) m.setLsAnnexs(this.attachedFiles);
            m.setCdExecutedCommand(cdCommand);
            this.conversation.lsMessages.push(m);
            setTimeout(() => {
                this.scrollContentToBottom(0);
            }, 200);
            m.msgState = 'SENT';
            this.message.nativeElement.textContent = "";
            let cdParticipant = (this.conversation && this.conversation.cdType == CONVERSATION_TYPE.PRIVATE) ? this.conversation.receptor.cdParticipant : this.cdUser;
            if (!this.userProvider.socketId || this.userProvider.inSync) {
                this.conversationService.insertConversationNewMessageInDb(m, this.conversation, cdParticipant);
            }
            else {
                this.conversationService.sendMessage(m)
                    .then((xResult) => {
                            if (xResult) {
                                m.cdConversationElement = xResult.cdConversationElement;
                                m.msgState = xResult.msgState;
                                if (m.lsAnnexs.length > 0) {
                                    for (let aResult of xResult.lsAnnexs) {
                                        for (let a of m.lsAnnexs) {
                                            if (a.dsName == aResult.dsName) {
                                                a.cdFile = aResult.cdFile;
                                                a.imagePreview = aResult.imagePreview;
                                            }
                                        }
                                    }
                                }
                            }
                            this.conversationService.insertConversationNewMessageInDb(m, this.conversation, cdParticipant);
                        }
                    )
                    .catch((error) => {
                            this.logService.logError(error, this.userProvider.getUser());
                            this.conversationService.insertConversationNewMessageInDb(m, this.conversation, cdParticipant);
                        }
                    );
            }
            this.attachedFiles = [];
        }
        else if (dsContent.length > 2000) {
            this.alertCtrl.create({
                header: this.translateService.instant('MAX_LENGHT_REACHED'),
                message: this.translateService.instant('MAX_LENGHT_REACHED_MESSAGE'),
                buttons: [this.translateService.instant('ACCEPT')]
            }).then(alert => {
                this.backButtonProvider.registerDismissable(alert);
                alert.present();
            });
        }
    }

    buttonClick(messageButton) {
        this.router.navigate(['/iframe-view'], {state: {url: messageButton.dsAction.replace('?','&'), title: messageButton.dsLabel, previousPageData: this.getPageData()}, skipLocationChange: true });
    }

    openMenu() {
        this.menuCtrl.open();
    }

    executeCommand(command) {
        this.alertCtrl.create({
            header: 'Functionality in development.',
            message: 'This functionality is being developed.',
            buttons: ['Ok']
        }).then(alert => {
            this.backButtonProvider.registerDismissable(alert);
            alert.present();
        });
    }

    onFocus() {
        this.hideEmojis();
        setTimeout(() => {
            this.scrollContentToBottom(0);
        }, 600);
    }

    startTyping() {
        if (!this.isTyping) {
            this.isTyping = true;
            if (!this.userProvider.inSync) this.conversationService.startTyping(this.conversation).catch((error) => /* no internet */ this.logService.logError(error, this.userProvider.getUser()));
        }
    }

    stopTyping() {
        if (this.isTyping) {
            this.isTyping = false;
            if (!this.userProvider.inSync) this.conversationService.stopTyping(this.conversation).catch((error) => /* no internet */ this.logService.logError(error, this.userProvider.getUser()));
            ;
        }
    }

    keyup(ev: any) {
        //typing event
        this.startTyping();
        let keynum;
        if (ev.which) {
            keynum = ev.which;
        }
        //end typing event

        //maxlength validator
        this.validateMaxLength();
    }

    validateMaxLength() {
        setTimeout(() => {
            if (this.formatTextContent().trim().length > 2000) {
                this.maxLengthClass = "maxLength"
            }
            else {
                this.maxLengthClass = ""
            }
        }, 50);

    }

    public formatTextContent(): string {
        let textFormatEmojis = '';
        this.message.nativeElement.childNodes.forEach(node => {
            if (node.nodeType === 1) {
                if (node.tagName === 'I') {
                    textFormatEmojis += ':' + node.id + ':';
                    return;
                }
                textFormatEmojis += node.outerHTML.replace(/\u200B/g, '');
                return;
            }
            textFormatEmojis += this.emoticonPipe.revert(node.nodeValue.replace(/\u200B/g, ''));
        });
        return textFormatEmojis;
    }

    goToInfo() {
        if (this.conversation.isGroup) {
            this.router.navigate(['/group-info/' + this.conversation.cdConversation], {state: {conversation: this.conversation}});
        }
        else {
            this.alertCtrl.create({
                header: 'Functionality in development.',
                message: 'This functionality is being developed.',
                buttons: ['Ok']
            }).then(alert => {
                this.backButtonProvider.registerDismissable(alert);
                alert.present();
            });
        }
    }

    getParticipantColor(cdParticipant) {
        return this.participantsColorsMap.get(cdParticipant);
    }

    initColors() {
        if (this.conversation.isGroup) {
            for (let participant of this.conversation.lsParticipantsHistory) {
                let color = "#2E64FE";
                if (this.colors.length > 0) {
                    color = this.colors.shift();
                }
                if (!this.getParticipantColor(participant.cdParticipant)) this.participantsColorsMap.set(participant.cdParticipant, color);
            }
        }
    }

    get blocked(): boolean {
        return this.conversation.blocked || (this.conversation && this.conversation.cdType == CONVERSATION_TYPE.PRIVATE && !this.conversation.receptor.isVisible);
    }

    getParticipantDescription(cdParticipant) {
        for (let p of this.conversation.lsParticipantsHistory) {
            if (p.cdParticipant === cdParticipant) {
                return p.dsParticipant.replace(",", "");
            }
        }
        return cdParticipant;
    }

    backButtonPressed() {
        if (this.openEmojiPicker) {
            this.hideEmojis();
            this.leaving = false;
        }
        else if (this.openDatePicker){
          this.backButtonProvider.unregisterListener(this);
          this.leaving = false;
        }
    }

    public canDeactivate(): boolean {
        const leaving = this.leaving;
        this.leaving = true;
        return leaving;
    }

    loadParticipantsHistory() {
        let subQuery = this.dbProvider.getRepository(Message).createQueryBuilder('Message')
            .select("message.cdParticipant", "cdParticipant")
            .where("message.conversationId = " + this.conversation.id)
            .groupBy("message.cdParticipant");

        this.dbProvider.getRepository(Participant).createQueryBuilder('Participant')
            .where("participant.cdParticipant IN (" + subQuery.getQuery() + ")")
            .getMany().then((participants) => {
            for (let p of participants) {
                this.conversation.lsParticipantsHistory.push(p);
            }
            if (this.conversation.lsParticipantsHistory.length == 0) {
                this.conversation.lsParticipantsHistory = this.conversation.lsParticipants;
            }
            this.initColors();
        });
    }

    onScreenOrientationChanged() {
        if (this.shouldScrollDown) {
            setTimeout(() => {
                this.scrollContentToBottom(100);
            }, 200);
            this.shouldScrollDown = false;
        }
    }

    ////////////////////
    ////// EMOJIS //////
    ////////////////////

    showEmojis() {
        this.backButtonProvider.registerListener(this);
        if (this.keyboardPresent) {
            this.keyboard.hide();
            setTimeout(() => {
                this.openEmojiPicker = true;
                //this.content.resize();
            }, 50);
        }
        else {
            this.openEmojiPicker = true;
            //this.content.resize();
        }
    }

    hideEmojis() {
        this.openEmojiPicker = false;
        //this.content.resize();
        this.backButtonProvider.unregisterListener(this);
        this.initialEmojisSlide.initialSlide = this.userEmojis.length == 0 ? 1 : 0;
        this.currentEmojisSlide = this.userEmojis.length == 0 ? 1 : 0;
    }

    addEmoji(emoji, updateUsed = true) {

        let sel = window.getSelection();
        if (sel.getRangeAt && sel.rangeCount) {
            let range = sel.getRangeAt(0);
            let node = document.createTextNode(emoji[0]);
            range.insertNode(node);
            range.setStartAfter(node);
        }
        else {
            this.message.nativeElement.innerHTML += emoji[0];
        }
        this.validateMaxLength();
        if (updateUsed) this.userProvider.emojiUsed(emoji);
    }

    onKeyboardShow() {
        this.keyboardPresent = true;
        this.hideEmojis();
        this.scrollContentToBottom(0);
    }

    onKeyboardHide() {
        this.keyboardPresent = false;
    }

    hideEmojisAndOpenKeyboard() {
        // this.renderer.invokeElementMethod(this.message.nativeElement, 'focus', []);
        this.hideEmojis();
    }

    emojisSlideChanged() {
        this.emojisSlides.getActiveIndex().then(actualIndex => {
            if (actualIndex < (EMOJIS.length + 1) && actualIndex >= 0) {
                this.currentEmojisSlide = actualIndex;
            }
        });
    }

    emojisSlideSelected(event) {
        this.emojisSlides.slideTo(event.detail.value);
    }

    initEmojisSlides() {
        if (this.userEmojis.length == 0) {
            this.currentEmojisSlide = 1;
            this.emojisSlideSelected(1);
        }
        else {
            this.currentEmojisSlide = 0;
            this.emojisSlideSelected(0);
        }
    }

    /**
     * It open the attach option according to origin.
     * @param {string} origin
     */
    public openAttachOption(origin: string): void {
        if (this.attachedFiles.length + 1 > this.maxLengthAttahs) {
            this.alertCtrl.create({
                header: this.translateService.instant('MAX_LENGTH_ATTACHMENTS_TITLE'),
                message: this.translateService.instant('MAX_SIX_ATTACHMENTS_MESSAGE'),
                buttons: ['Ok']
            }).then(alert => {
                this.backButtonProvider.registerDismissable(alert);
                alert.present();
            });
            return;
        }
        this.newAttach.next(origin);
    }

    attach(origin) {
        this.attachService.captureLocalFile(origin).then((tempPath) => {
            if(!tempPath){
                return;
            }
            let fileName = tempPath.substring(tempPath.lastIndexOf('/') + 1, tempPath.length);
            let folderFile = tempPath.replace(fileName, '');
            let tempNewFileSystemPath = this.attachService.getPlatformBasePath() + APP.PATH_SEND;
            if(this.platform.is("ios")){
                fileName = tempPath.substr(tempPath.lastIndexOf('/') + 1);
                folderFile = tempPath.substr(0,tempPath.lastIndexOf('/') + 1);
            }
            if (this.existAttached(fileName)) {
                return;
            }
            this.logService.logDevelopment('Real Path ' + tempPath, this.userProvider.getUser());
            this.attachService.checkSizeFile(tempPath)
                .then(pass => {
                    if (pass) {
                        this.zone.run(() => {
                            let localCopyFileName = new Date().getTime() + "_" + fileName;
                            this.file.copyFile(folderFile, fileName, tempNewFileSystemPath, localCopyFileName)
                                .then(_ => {
                                    let localCopyFile = tempNewFileSystemPath + localCopyFileName;
                                    let attach = new Attach;
                                    attach.setInitAttributes(localCopyFileName, localCopyFile);
                                    this.attachedFiles.push(attach);
                                    this.hasAttachedFiles = true;
                                    this.uploading = true;
                                    this.attachService.uploadFile(localCopyFile, localCopyFileName)
                                        .then(rta => {
                                            console.log(JSON.stringify(rta));
                                            attach.cdTempPath = rta.tempPath;
                                            attach.cdFile = rta.fileId;
                                            attach.cdState = 1;
                                            this.uploading = false;
                                        })
                                        .catch(err => {
                                            this.logService.logError(err, this.userProvider.getUser());
                                            this.uploading = false;
                                        });
                                }).catch((error) => {
                                this.logService.logError(error, this.userProvider.getUser());
                                this.logService.logDevelopment('Error in Copy File: folderFile-> ' + folderFile, this.userProvider.getUser());
                                this.logService.logDevelopment('Error in Copy File: fileName-> ' + fileName, this.userProvider.getUser());
                                this.logService.logDevelopment('Error in Copy File: PATH_SEND-> ' + APP.PATH_SEND, this.userProvider.getUser());
                                this.logService.logDevelopment('Error in Copy File: localCopyFileName-> ' + localCopyFileName, this.userProvider.getUser());
                                this.logService.logDevelopment('Error in Copy File: --------------------', this.userProvider.getUser());
                            });
                            ;
                        });
                    }
                })
                .catch((error) => {
                    this.logService.logError(error, this.userProvider.getUser());
                });
        })
            .catch((error) => {
                this.logService.logError(error, this.userProvider.getUser());
            });
    }


    existAttached(fileName) {
        let exist = false;
        for (let a of this.attachedFiles) {
            if (a.dsName == fileName) {
                this.alertCtrl.create({
                    header: this.translateService.instant('REPEATED_ATTACHMENT_TITLE'),
                    message: this.translateService.instant('ALREADY_ATTACH_MESSAGE'),
                    buttons: ['Ok']
                }).then(alert => {
                    this.backButtonProvider.registerDismissable(alert);
                    alert.present();
                });
                exist = true;
            }
        }
        return exist;
    }

    removeAttached(file: Attach) {
        this.attachedFiles.splice(this.attachedFiles.indexOf(file, 0), 1);
        if (file.cdState != 1) {
            this.uploading = false;
            this.attachService.abortUploadFile(file);
        }
    }

    executeMessageAction(m: Message, a: MessageAction, ev: any) {
        if (this.networkService.getConnected()) {
            this.processAction = true;
            ev.target.parentNode.parentNode.className = "btn-group btn-group-disabled";
            m.dsValidation = null;
            let messageActionReq = new MessageActionRequest();
            messageActionReq.cdUser = this.cdUser.toLowerCase();
            messageActionReq.cdConversationElement = m.cdConversationElement;
            messageActionReq.cdAction = a.value;
            messageActionReq.cdConversation = m.cdConversation;
            messageActionReq.lsInputs = m.formatInputsForRequest();
            this.conversationService.executeAction(messageActionReq, m.dsExecutionPath)
                .then(xResult => {
                    this.processAction = false;
                    if (xResult["status"] != "ok") {
                        m.dsValidation = xResult.error;
                        if (!xResult.error) this.errorExecutionAction();
                    }
                    ev.target.parentNode.parentNode.className = "btn-group";
                })
                .catch(error => {
                    this.logService.logError(error, this.userProvider.getUser());
                    this.errorExecutionAction();
                });
        }
        else {
            this.errorNotConnected();
        }
    }

    errorExecutionAction() {
        this.alertCtrl.create({
            header: this.translateService.instant('EXECUTE_ACTION_ERROR'),
            message: this.translateService.instant('EXECUTE_ACTION_ERROR_MESSAGE'),
            buttons: [this.translateService.instant('ACCEPT')]
        }).then(alert => {
            this.backButtonProvider.registerDismissable(alert);
            alert.present();
        });
    }

    errorNotConnected() {
        this.alertCtrl.create({
            header: this.translateService.instant('NOT_CONNECTION_ERROR'),
            message: this.translateService.instant('NOT_CONNECTION_ERROR_MESSAGE'),
            buttons: [this.translateService.instant('ACCEPT')]
        }).then(alert => {
            this.backButtonProvider.registerDismissable(alert);
            alert.present();
        });
    }

    onFocusInput() {
        setTimeout(() => {
            this.focusInput = true;
            this.scrollContentToBottom(100);
        }, 100);
    }

    onBlurInput() {
        this.focusInput = false;
        this.keyboard.hide();
        //this.content.resize();
    }

    onFocusDatetime() {
        this.openDatePicker = true;
        this.backButtonProvider.registerListener(this);
    }

    onBlurDatetime() {
        this.openDatePicker = false;
    }

    isNextMessageFromSameParticipant(currentIndex: number): boolean {
        if(this.messages.length > currentIndex + 1){
            return this.messages[currentIndex].cdParticipant == this.messages[currentIndex + 1].cdParticipant;
        }
        return false;
    }

    isPrevMessageFromSameParticipant(currentIndex: number): boolean {
        if(currentIndex > 0){
            return this.messages[currentIndex].cdParticipant == this.messages[currentIndex - 1].cdParticipant;
        }
        return false;
    }

    /**
     * Set the real paths of the annexs list.
     */
    private setRealPathsToAnnexs(): void {
        if(this.messages && this.messages.length > 0){
            this.messages.forEach((message: Message) => {
                if(message.lsAnnexs){
                    message.lsAnnexs.forEach((annex: Attach) => {
                        if (annex.cdLocalPath) {
                            this.attachService.convertFileSrc(annex).then((realPath: string) => {
                                annex.realPath = realPath;
                            });
                        }
                    });
                }
            });
        }
    }

    /**
     * Scrolling the content to bottom position.
     * @param {number} position
     */
    public scrollContentToBottom(position: number): void {
        if (this.content) {
            this.content.scrollToBottom(position);
            this.showBackToBottomButton = false;
        }
    }

    /**
     * Scrolling the content to top position.
     * @param {number} position
     */
    public scrollContentToTop(position: number): void {
        if (this.content) {
            this.content.scrollToTop();
        }
    }

    /**
     * Returns the page data for navigation.
     * @return {any}
     */
    private getPageData(): any {
        return {
            route: '/chat/' + this.conversation.getCdConversation(),
            data: {
                conversation: this.conversation,
                command: this.command
            }
        };
    }

    /**
    * It returns the fab button style.
    * @return {string}
    */
    public getFabButtonStyle(): string {
        return this.appConfigurationService.getFabButtonStyle();
    }

    /**
     * Processes and opens a modal with the received url.
     * @param urlInput 
     */
    async openLinkModal(urlInput: string){
        const modal = await this.modalCtrl.create({
            component: LinkViewModalComponent,
            componentProps: {url: urlInput}
        })
        await modal.present();
    }

    /**
    * Checks if the scroll is at the bottom.
    * @return {Promise<boolean>}
    */
    private async checkScrollInBottom(): Promise<boolean> {
        const scrollElement = await this.content.getScrollElement();
        return Math.round(scrollElement.scrollTop) === (scrollElement.scrollHeight - scrollElement.clientHeight);
    }

    /**
     * Process the scroll event. Hide the back to bottom button when the scroll is at the bottom.
     */
    public async onScroll(event) {
        if (await this.checkScrollInBottom()) {
            this.showBackToBottomButton = false;
            return;
        }
        this.showBackToBottomButton = true;
    }

    /**
    * Set the parameters passed by the route.
    */
    private setParametersFromRoute(): void {
        if (this.router.getCurrentNavigation().extras.state && this.router.getCurrentNavigation().extras.state.backWithPop) {
            this.backWithPop = this.router.getCurrentNavigation().extras.state.backWithPop;
        }
    }

}