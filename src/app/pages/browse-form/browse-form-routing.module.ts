import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BrowseFormPage } from './browse-form';

const routes: Routes = [
  {
    path: 'browse/:cdDocumentName',
    component: BrowseFormPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BrowseFormPageRoutingModule {}
