import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { PipesModule } from '../../pipes/pipes.module';
import { BrowseFormPageRoutingModule } from './browse-form-routing.module';
import { BrowseFormPage } from './browse-form';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PipesModule,
    IonicModule,
    BrowseFormPageRoutingModule
  ],
  declarations: [BrowseFormPage]
})
export class BrowseFormPageModule {}
