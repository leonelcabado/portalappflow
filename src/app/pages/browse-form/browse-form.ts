import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { UserProvider } from '../../services/user.service';
import { AuthProvider } from '../../services/auth.service';
import { ActionService } from '../../services/action.service';
import { PopoverService } from '../../services/popover.service';
import { LogService } from '../../services/log.service';
import { TaskService } from '../../services/task.service';
import { TranslateService } from '../../services/translate.service';
import { AppConfigurationService } from '../../services/app-configuration.service';
import { Action } from '../../model/action.entity';
import { FormBrowseConfiguration } from '../../model/form-browse-configuration.entity';

@Component({
  selector: 'app-browse-form',
  templateUrl: './browse-form.html',
  styleUrls: ['./browse-form.scss'],
})
export class BrowseFormPage implements OnInit {

    private processMessageFn: any;
	public url: SafeResourceUrl;
    public actions: Array<Action>;
    public showInTab: boolean;
    private previousPageData: any;
    private formBrowseConfiguration: FormBrowseConfiguration;

    constructor(private location: Location,
                private router: Router,
                protected domSanitizer: DomSanitizer,
                protected popoverService: PopoverService,
                protected logService: LogService,
                protected userProvider: UserProvider,
                protected authProvider: AuthProvider,
                protected actionService: ActionService,
                protected translateService: TranslateService,
                protected taskService: TaskService,
                private appConfigurationService: AppConfigurationService,
                private changeDetectorRef: ChangeDetectorRef) {}

	public ngOnInit(): void {
		this.setParametersFromRoute();
		this.loadUrl();
	}

    public ionViewDidEnter(): void {
        this.processMessageFn = (event) => this.processMessage(event);
        window.addEventListener('message', this.processMessageFn, false);
    }

    public ionViewWillLeave(): void {
        window.removeEventListener('message', this.processMessageFn, false);
    }

    /**
     * Set the parameters passed by the route.
     */
    private setParametersFromRoute(): void {
        if (this.router.getCurrentNavigation().extras.state) {
            this.setFormBrowseConfiguration();
            if (this.router.getCurrentNavigation().extras.state.previousPageData) {
                this.previousPageData = this.router.getCurrentNavigation().extras.state.previousPageData;
            }
        }
    }

    /**
     * It loads the iframe url to show.
     */
    public loadUrl(): void {
        this.url = this.domSanitizer.bypassSecurityTrustResourceUrl(this.userProvider.getContextUrl() + '/SLResourceShorcut?access_token=' + this.authProvider.getToken() + '&_idCreateSession=true&_idExecuteInMainFrame=false&_idResource=' + this.getBrowseFormUrl() + '&refresh_token=' + this.userProvider.getRefreshToken());
    }

    /**
     * It opens the main menu page.
     * @param {any} event
     */
    public openMainMenu(event: any): void {
        this.popoverService.openMainMenu(event);
    }

    /**
     * Download the log file.
     */
    public downloadLog(): void {
        this.logService.createLogFile(this.userProvider.getUser());
    }

    /**
     * Open the page of the actions lists.
     */
    public openActionsList(): void {
        this.router.navigate(['/action-list'], { state: { previousPageData: this.getPageData() }});
    }

    /**
     * Go back to the previous page.
     */
    public goToBack(): void {
        if (this.previousPageData) {
            this.router.navigate([this.previousPageData.route], this.previousPageData.data);
            return;
        }
        this.location.back();
    }

    /**
     * It returns the fab button style.
     * @return {string}
     */
    public getFabButtonStyle(): string {
        return this.appConfigurationService.getFabButtonStyle();
    }

    /**
     * Returns the url to go to the form browse.
     * @return {string}
     */
    private getBrowseFormUrl(): string {
        return 'SLBrowse&cdDocumentNameHidden=' + this.formBrowseConfiguration.getCdDocumentName() + '&mobile=true&isEmbed=true';
    }

    /**
     * Process the message event that comes from the Deyel Iframe.
     * @param {any} event
     */
    private processMessage(event: any): void {
        if (event.data && event.data.actions && event.data.operation === 'browse-form-actions') {
            this.actions = this.actionService.processActionsFromPlusButton(event.data.actions);
            this.changeDetectorRef.detectChanges();
        }
        if (this.checkMessageOperationToNavigate(event, 'updateForm')) {
            this.navigateToOperation(this.getNavigationConfigurationToIframe('SLGenericDocumentUpdate&' + this.taskService.getParametersToFormsURL(event.data), this.translateService.instant('UPDATE')));
        }
        if (this.checkMessageOperationToNavigate(event, 'showForm')) {
            this.navigateToOperation(this.getNavigationConfigurationToIframe('SLGenericDocumentShow&' + this.taskService.getParametersToFormsURL(event.data), this.translateService.instant('CONSULT')));
        }
        if (this.checkMessageOperationToNavigate(event, 'deleteForm')) {
            this.navigateToOperation(this.getNavigationConfigurationToIframe('SLGenericDocumentDelete&' + this.taskService.getParametersToFormsURL(event.data), this.translateService.instant('DELETE')));
        }
    }

    /**
     * It navigates to the operation.
     * @param {any} operationObject
     */
    private navigateToOperation(operationObject: any): void {
        this.router.navigate(['/iframe-view'], operationObject);
        this.loadUrl();
    }

    /**
     * It checks the message operation to navigate.
     * @param  {any}     event
     * @param  {string}  operationName
     * @return {boolean}
     */
    private checkMessageOperationToNavigate(event: any, operationName: string): boolean {
        return (event.data && event.data.operation === operationName && this.showInTab);
    }

    /**
     * It Returns the navigation configuration to load iframe.
     * @param  {string} url
     * @param  {string} title
     * @return {any}
     */
    private getNavigationConfigurationToIframe(url: string, title: string): any {
        return {
            state: {
                url: url,
                title: title,
                previousPageData: this.getPageData()
            },
            skipLocationChange: true
        };
    }

    /**
     * Open the action selected.
     * @param {Action} action
     */
    public openAction(action: Action): void {
        this.router.navigate(['/iframe-view'], this.getNavigationConfigurationToIframe(action.getUrl(), action.getName()));
    }

    /**
     * Returns the page data for navigation.
     * @return {any}
     */
    private getPageData(): any {
        return {
            route: this.getRouteToBackPage(),
            data: {
                state: {
                    cdDocumentName: this.formBrowseConfiguration.getCdDocumentName(),
                    configuration: this.formBrowseConfiguration
                }
            }
        };
    }

    /**
     * It returns the route to navigate when backs it to the page.
     * @return {string} [description]
     */
    private getRouteToBackPage(): string {
        if (this.showInTab) {
            return '/main/form/browse/' + this.formBrowseConfiguration.getCdDocumentName();
        }
        return '/browse/' + this.formBrowseConfiguration.getCdDocumentName();
    }

    /**
     * Returns the current context name.
     * @return {string}
     */
    public getContextName(): string {
        return this.userProvider.getContextName();
    }

    /**
     * It sets the form browse configuration.
     */
    private setFormBrowseConfiguration(): void {
        const configuration = this.router.getCurrentNavigation().extras.state.configuration;
        if (configuration) {
            this.formBrowseConfiguration = configuration;
            this.showInTab = true;
            return;
        }
        const cdDocumentName = this.router.getCurrentNavigation().extras.state.cdDocumentName;
        if (cdDocumentName) {
            this.formBrowseConfiguration = new FormBrowseConfiguration(cdDocumentName);
        }
    }

    /**
     * It returns from app configuration if the header should be displayed.
     * @returns boolean
     */
    public showHeader(): boolean {
        return this.appConfigurationService.getShowHeader();
    }

    /**
     * It returns the fab button should be displayed from configuration.
     * @return {boolean}
     */
    public showFabButton(): boolean {
        return this.appConfigurationService.showFabButton();
    }

}
