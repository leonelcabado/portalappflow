import {NgModule} from '@angular/core';
import {CommandListPage} from './command-list';
import {RouterModule, Routes} from '@angular/router';
import {IonicModule} from '@ionic/angular';
import {CommonModule} from '@angular/common';

const routes: Routes = [
    {
        path: 'command-list',
        component: CommandListPage,
    },
];

@NgModule({
    declarations: [
        CommandListPage,
    ],
    imports: [
        RouterModule.forChild(routes),
        IonicModule,
        CommonModule
    ],
})
export class CommandListPageModule {
}
