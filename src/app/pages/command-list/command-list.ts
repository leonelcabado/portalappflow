import {Component} from '@angular/core';
import {AlertController} from '@ionic/angular';

//providers
import {BackButtonProvider} from '../../services/back-button.service';


@Component({
    selector: 'page-command-list',
    templateUrl: 'command-list.html',
    styleUrls: ['command-list.scss']
})
export class CommandListPage {

    constructor(private alertCtrl: AlertController,
                private backButtonProvider: BackButtonProvider) {
    }

    ngOnInit() {
    }

    executeCommand() {
        // TODO
        this.alertCtrl.create({
            header: 'Functionality in development.',
            message: 'This functionality is being developed.',
            buttons: ['Ok']
        }).then(alert => {
            this.backButtonProvider.registerDismissable(alert);
            alert.present();
        });
    }

}
