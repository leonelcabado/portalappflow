import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { TitlePage } from './title';

const routes: Routes = [
    {
        path: 'title',
        component: TitlePage,
    },
];

@NgModule({
    declarations: [
        TitlePage,
    ],
    imports: [
        RouterModule.forChild(routes),
        FormsModule,
        ReactiveFormsModule,
        IonicModule,
        CommonModule
    ],
})
export class TitlePageModule {}
