import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PageConfiguration } from '../../model/page-configuration.entity';
import { AppConfigurationService } from '../../services/app-configuration.service';
import { BackButtonProvider } from '../../services/back-button.service';
import { APP } from '../../app.config';

@Component({
  selector: 'app-title-page',
  templateUrl: './title.html',
  styleUrls: ['./title.scss'],
})
export class TitlePage implements OnInit {

	public pageConfiguration: PageConfiguration;

	constructor(private router: Router,
				private backButtonProvider: BackButtonProvider,
				private appConfigurationService: AppConfigurationService) { }

	ngOnInit() {
		this.setPageConfiguration();
	}

	ionViewDidEnter() {
	    this.appConfigurationService.setStatusBar(this.pageConfiguration);
	    this.backButtonProvider.registerView('title', '/title');
	}

	/**
	 * It sets the page configuration.
	 */
	private setPageConfiguration(): void {
	    const configuration = this.router.getCurrentNavigation().extras.state.configuration;
	    if (configuration) {
	        this.pageConfiguration = configuration;
	    }
	}

	/**
	 * It navigates to the page url.
	 * @param {string} pageUrl
	 */
	public navigateTo(pageUrl: string): void {
		this.router.navigate([pageUrl], { state: { configuration: this.appConfigurationService.getConfigurationByUrl(pageUrl) }});
	}

}
