import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { AlertController, ToastController } from '@ionic/angular';
import { PageConfiguration } from '../../model/page-configuration.entity';
import { AppConfigurationService } from '../../services/app-configuration.service';
import { BackButtonProvider } from '../../services/back-button.service';
import { APP } from '../../app.config';

@Component({
  selector: 'app-register-page',
  templateUrl: './register.html',
  styleUrls: ['./register.scss'],
})
export class RegisterPage implements OnInit {

	public pageConfiguration: PageConfiguration;
	public registerForm: FormGroup;
    public openedKeyboard: boolean;

	constructor(public keyboard: Keyboard,
                private router: Router,
				private formBuilder: FormBuilder,
                private changeDetectorRef:ChangeDetectorRef,
				private alertController: AlertController,
				private toastController: ToastController,
				private backButtonProvider: BackButtonProvider,
				private appConfigurationService: AppConfigurationService) { }

	ngOnInit() {
        this.openedKeyboard = false;
		this.initializeForm();
		this.setPageConfiguration();
        this.subscribeKeyboardEvents();
    }

    ionViewDidEnter() {
        this.appConfigurationService.setStatusBar(this.pageConfiguration);
    }

  	/**
	 * It sets the page configuration.
	 */
	private setPageConfiguration(): void {
	    const configuration = this.router.getCurrentNavigation().extras.state.configuration;
	    if (configuration) {
	        this.pageConfiguration = configuration;
	        console.log(this.pageConfiguration);
	    }
	}

	/**
	 * It navigates to the page url.
	 * @param {string} pageUrl
	 */
	public navigateTo(pageUrl: string): void {
		this.router.navigate([pageUrl], { state: { configuration: this.appConfigurationService.getConfigurationByUrl(pageUrl) }});
	}

    /**
     * It navigates to the page url.
     */
    public goToTitle(): void {
        this.router.navigate(['/title'], { state: { configuration: this.appConfigurationService.getConfigurationByUrl('/title') }});
    }

    /**
     * It initializes the login form.
     */
    private initializeForm(): void {
        this.registerForm = this.formBuilder.group({
            name: ['', Validators.required],
            lastName: ['', Validators.required],
            email: new FormControl('',[ Validators.required, Validators.email])
        });
    }

    /**
     * It returns if the form is valid.
     * @return {boolean}
     */
    public isValidForm(): boolean {
        return this.registerForm.valid;
    }

    /**
     * It submits the register form.
     */
    public registerSubmit(): void {
    	if (!this.registerForm.valid) {
            this.alertController.create({
                header: 'Error en Registro',
                subHeader: 'Por favor, compruebe que los datos ingresados sean correctos.',
                buttons: ['OK']
            }).then(alert => {
                this.backButtonProvider.registerDismissable(alert);
                alert.present();
            });
            return;
        }
        this.toastController.create({
			message: 'Se ha enviado un email con su contraseña.',
			duration: 2500,
        }).then(toast => {
            toast.present();
        	this.navigateTo('/login');
        });
    }

    /**
    * It checks if the keyboard is visible.
    * @return {boolean}
    */
    public isVisibleKeyboard(): boolean {
        return this.keyboard.isVisible;
    }

    /**
     * It subscribes to keyboard events.
     */
    public subscribeKeyboardEvents(): void {
        this.keyboard.onKeyboardShow().subscribe(() => {
            this.openedKeyboard = true;
            this.changeDetectorRef.detectChanges();
        });
        this.keyboard.onKeyboardHide().subscribe(() => {
            this.openedKeyboard = false;
            this.changeDetectorRef.detectChanges();
        });
    }

}
