import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { RegisterPage } from './register';

const routes: Routes = [
    {
        path: 'register',
        component: RegisterPage,
    },
];

@NgModule({
    declarations: [
        RegisterPage,
    ],
    imports: [
        RouterModule.forChild(routes),
        FormsModule,
        ReactiveFormsModule,
        IonicModule,
        CommonModule
    ],
})
export class RegisterPageModule {
}
