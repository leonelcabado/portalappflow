import { Component, NgZone, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { ConversationService } from '../../services/conversation.service';
import { RTMService } from '../../services/rtm.service';
import { SynchronizationService } from '../../services/synchronization.service';
import { TranslateService } from '../../services/translate.service';
import { PopoverService } from '../../services/popover.service';
import { LogService } from '../../services/log.service';
import { AppConfigurationService } from '../../services/app-configuration.service';
import { UserProvider } from '../../services/user.service';
import { Conversation } from '../../model/conversation.entity';
import { Message } from '../../model/message.entity';
import { ConfigurationService } from '../../services/configuration.service';

@Component({
    selector: 'page-chat-list',
    templateUrl: 'chat-list.html',
    styleUrls: ['chat-list.scss']
})
export class ChatListPage implements OnInit {

    conversations: Conversation[];
    lastMessage: any;
    loading = false;
    allChatsLoaded: boolean = false;
    cdUser: string = "";
    isSearch: boolean = false;
    isSearching: boolean = false;
    public searchClicked: boolean;
    showSkeleton: Boolean = true;

    constructor(private conversationService: ConversationService,
                public sanitizer: DomSanitizer,
                public userProvider: UserProvider,
                private rtmService: RTMService,
                private translateService: TranslateService,
                private synchronizationService: SynchronizationService,
                private configurationService: ConfigurationService,
                protected popoverService: PopoverService,
                protected logService: LogService,
                private appConfigurationService: AppConfigurationService,
                private ngZone: NgZone,
                private router: Router) {
        this.allChatsLoaded = this.conversationService.allChatsLoaded;
        this.cdUser = this.userProvider.getUser().username.toUpperCase();

        this.conversationService.onSearchChats$.subscribe(text => {
            this.searchChats(text);
        });

        this.conversationService.conversationUpdated$.subscribe(() => {
            this.conversations = this.conversationService.getConversations().filter(conversation => conversation);
            this.loading = false;
            this.showSkeleton = false;
        });
    }

    ngOnInit() {
        this.searchClicked = false;
        this.conversations = this.conversationService.getConversations().filter(conversation => conversation);
        if(this.conversations.length == 0){
            this.allChatsLoaded = false;
            this.loading = true;
        } else {
            this.allChatsLoaded = this.conversations.length != (20 + this.conversationService.countChatsPushed - this.conversationService.countChatsDeleted);
        }
        if(this.conversations) this.showSkeleton = false;
    }

    goToChat(conversation: Conversation) {
        if (this.isSearch) {
            this.searchChats("");
            if (!this.conversationService.getLocalConversationById(conversation.cdConversation)) {
                this.conversationService.conversations.push(conversation);
                this.conversations = this.conversationService.getConversations().filter(conversation => conversation);
                this.conversationService.countChatsPushed++;
                this.openChat(conversation);
            } else {
                this.openChat(conversation);
            }
        }
        else {
            this.openChat(conversation);
        }

    }

    openChat(conversation: Conversation) {
        this.router.navigate(['/chat/' + conversation.cdConversation ], {state: {conversation: conversation}});
    }

    newChat() {
        this.router.navigate(['/user-list']);
    }

    getLastMessageFormatted(message: Message): any {
        if (message.lsAnnexs && message.lsAnnexs.length > 0) {
            if (message.lsAnnexs[message.lsAnnexs.length - 1].imagePreview) {
                this.lastMessage = ["camera", "Image"];
            }
            else {
                this.lastMessage = ["document", message.lsAnnexs[message.lsAnnexs.length - 1].dsName];
            }
        }
        else if (message.lsInputs && message.lsInputs.length > 0) {
            this.lastMessage = ["ios-complete-command", this.translateService.instant('INPUT_MESSAGE')];
        }
        else if (message.lsActions && message.lsActions.length > 0) {
            this.lastMessage = ["ios-complete-command", this.translateService.instant('ACTION_MESSAGE')];
        }
        else if (message.dsContent.indexOf('<ul>') != -1) {
            this.lastMessage = ["ios-ionitron-outline", this.translateService.instant('DATA_MESSAGE')];
        }
        else {
            this.lastMessage = ["", message.dsContent.replace(/<br\s*[\/]?>/gi, " ").replace(/<div>/ig, " ").replace(/<\/div>/ig, " ").replace(/<div>/ig, " ").replace(/<\/div>/ig, " ")];
        }
        return this.lastMessage;
    }

    getLastMessageUsername(message: Message, conversation: Conversation): any {
        if (message.cdParticipant && message.cdParticipant != "") {
            return this.getParticipantDescription(message.cdParticipant, conversation) + ": ";
        }
        return "";
    }

    getParticipantDescription(cdParticipant: string, conversation: Conversation) {
        for (let p of conversation.lsParticipants) {
            if (p.cdParticipant === cdParticipant) {
                let dsParticipant = p.dsParticipant.replace(",", "");
                return dsParticipant.substr(0, dsParticipant.indexOf(" "));
            }
        }
        return "";
    }

    loadMoreConversations(event?: any) {
        if(this.loading){
            if (event && event.target) {
                event.target.complete();
            }
            return;
        }
        let lengthPrevious = this.conversations.length;
        this.conversationService.loadConversationsFromDb().then(
            _ => {
                this.conversationService.countChatsPushed = this.conversationService.countChatsPushed + 20;
                this.conversationService.updateGroupConversationsImage(this.configurationService.tedisConfiguration.systemConfiguration.theme);
                this.conversations = this.conversationService.getConversations().filter(conversation => conversation);
                if (lengthPrevious == this.conversations.length || (lengthPrevious + 20) > this.conversations.length) {
                    this.allChatsLoaded = true;
                }
                if (event && event.target) {
                    event.target.complete();
                    if(this.allChatsLoaded){
                        event.target.disabled = true;
                    }
                }
            }
        );
    }

    searchChats(text: string) {
        this.isSearch = (text != "");
        this.allChatsLoaded = true;
        this.isSearching = true;
        if (text == "") {
            this.conversations = this.conversationService.getConversations().filter(conversation => conversation);
            this.allChatsLoaded = this.conversations.length != (20 + this.conversationService.countChatsPushed - this.conversationService.countChatsDeleted);
            this.isSearching = false;
        } else {
            this.conversationService.searchConversationsFromDb(text).then(
                (conversations) => {
                    this.conversations = conversations.filter(conversation => conversation);
                    this.isSearching = false;
                });
        }
    }

    // ngAfterViewChecked(): void {
    //   console.error("ngAfterviewChecked");
    // }
    //Antes se llamaba a este metodo en el pullToRefresh
    sync(event) {
        if (this.userProvider.inSync) return;
        event.target.complete();
        this.ngZone.run(() => {
            this.rtmService.openConnection(true).catch((error) => {
                this.logService.logDevelopment("Error sync openConnection", this.userProvider.getUser());
                this.logService.logDevelopment(error, this.userProvider.getUser());
                this.userProvider.inProcessSync = false;
                this.synchronizationService.retryOpenConnection();
            })
        });
    }

    /**
     * Open the page of the actions lists.
     */
    public openActionsList(): void {
        this.router.navigate(['/action-list'], { state: { previousPageData: this.getPageData() }});
    }

    /**
     * Returns the current context name.
     * @return {string}
     */
    public getContextName(): string {
        return this.userProvider.getContextName();
    }

    /**
     * Show or hide the search bar.
     */
    public toggleSearchBar(): void {
        this.searchClicked = !this.searchClicked;
    }

    /**
     * Open the main menu page.
     * @param {any} event
     */
    public openMainMenu(event: any): void {
        this.popoverService.openMainMenu(event);
    }

    /**
     * Download the log file.
     */
    public downloadLog(): void {
        this.logService.createLogFile(this.userProvider.getUser());
    }

    /**
     * Filter to the chats list.
     * @param {any} event
     */
    public filterChats(event: any): void {
        if (event && event.detail && event.detail.value && event.detail.value != "") {
            this.conversationService.searchChats(event.detail.value.toLowerCase());
            return;
        }
        this.conversationService.searchChats("");
    }

    /**
     * Returns the page data for navigation.
     * @return {any}
     */
    private getPageData(): any {
        return {
            route: '/main/chat/list',
            data: undefined
        };
    }

    /**
    * It returns the fab button style.
    * @return {string}
    */
    public getFabButtonStyle(): string {
        return this.appConfigurationService.getFabButtonStyle();
    }

    public doFilterChatsWithMessages(): Conversation[] {
        if(this.conversations){
            return this.conversations.filter(function(c){
                if(c.lsMessages){
                    if(c.cdType == 0 && !c.receptor){
                        return false;
                    }
                    return true;
                }
                return false;
            });
        }
        return this.conversations;
    }

    /**
     * It returns from app configuration if the header should be displayed.
     * @returns boolean
     */
    public showHeader(): boolean {
        return this.appConfigurationService.getShowHeader();
    }

    /**
     * It returns the fab button should be displayed from configuration.
     * @return {boolean}
     */
    public showFabButton(): boolean {
        return this.appConfigurationService.showFabButton();
    }

}
