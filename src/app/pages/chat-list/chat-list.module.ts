import { NgModule } from '@angular/core';
import { ChatListPage } from './chat-list';
import { PipesModule } from '../../pipes/pipes.module';
import { RouterModule, Routes } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';

const routes: Routes = [
    {
        path: 'list',
        component: ChatListPage
    }
];

@NgModule({
    declarations: [
        ChatListPage,
    ],
    imports: [
        RouterModule.forChild(routes),
        PipesModule,
        IonicModule,
        CommonModule
    ],
})
export class ChatListPageModule {
}
