import { AlertController, Platform } from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {Router} from '@angular/router';

import { Component, ViewChild, NgZone } from '@angular/core';
// import { Storage } from '@ionic/storage';
import {createConnection, ConnectionOptions, Repository} from 'typeorm';
import {Network} from '@ionic-native/network/ngx';
import {File} from '@ionic-native/file/ngx';
import {FirebaseX} from '@ionic-native/firebase-x/ngx';
import {HttpClient} from '@angular/common/http';

// helpers
import {ApiItemInterface} from './support-classes/api-item-interface';
import {JwtHelper} from './support-classes/jwt-helper';

// services
import {AuthProvider} from './services/auth.service';
import {UserProvider} from './services/user.service';
import {NotificationProvider} from './services/notification.service';
import {LoaderProvider} from './services/loader.service';
import {DbProvider} from './services/db.service';
import {ParticipantService} from './services/participant.service';
import {ConversationService} from './services/conversation.service';
import {TranslateService} from './services/translate.service';
import {RTMService} from './services/rtm.service';
import {SynchronizationService} from './services/synchronization.service';
import {NetworkService} from './services/network.service';
import { VersionUpdateService } from './services/version-update.service';

// models
import {Context} from './model/context.entity';
import {User} from './model/user.entity';
import {Participant} from './model/participant.entity';
import {Command} from './model/command.entity';
import {Conversation} from './model/conversation.entity';
import {Message} from './model/message.entity';
import {Attach} from './model/attach.entity';
import {MessageAction} from './model/message-action.entity';
import {MessageButton} from './model/message-button.entity';
import {MessageInput} from './model/message-input.entity';
import {SelectValue} from './model/select-value.entity';
import {UserUpdate} from './model/user-update.entity';
import {LogEntry} from './model/logEntry.entity';
import {ConversationUnread} from './model/conversation-unread.entity';

import {Autostart} from '@ionic-native/autostart/ngx';

// config
import {APP} from './app.config';
import {ConfigurationService} from './services/configuration.service';
import { ActionService } from './services/action.service';
import { AttachService } from './services/attach.service';
import { LogService } from './services/log.service';
import { CustomLoggerDataBase } from './loggerORM/CustomLoggerDataBase';
import { AppConfigurationService } from './services/app-configuration.service';
import { RuleService } from './services/rule.service';
import { SecurityConfigurationService } from './services/security-configuration.service';
import { TaskService } from './services/task.service';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.scss'],
    providers: [Network]
})
export class AppComponent {

    synchronize = true;
    public theme = 'ts-blue-theme';

    constructor(private platform: Platform,
                private splashScreen: SplashScreen,
                private authProvider: AuthProvider,
                private userProvider: UserProvider,
                private loaderProvider: LoaderProvider,
                private notificationProvider: NotificationProvider,
                private configurationService: ConfigurationService,
                private conversationService: ConversationService,
                private db: DbProvider,
                private networkService: NetworkService,
                private network: Network,
                private file: File,
                private rtmService: RTMService,
                private synchronizationService: SynchronizationService,
                private http: HttpClient,
                private autostart: Autostart,
                private ngZone: NgZone,
                private firebase: FirebaseX,
                private router: Router,
                private versionUpdateService: VersionUpdateService,
                private actionService: ActionService,
                private attachService: AttachService,
                private appConfigurationService: AppConfigurationService,
                private alertCtrl: AlertController,
                private logService: LogService,
                private ruleService: RuleService,
                private securityConfigurationService: SecurityConfigurationService,
                private taskService: TaskService) {
        this.initializeApp();
    }

    initializeApp() {
        console.log("Starting opening processing");
        this.platform.ready().then(() => {
            this.appConfigurationService.setDefaultStatusBar();
            this.configurationService.themeChanged$.subscribe((theme) => {
                this.theme = this.configurationService.getThemeClass(theme);
                this.updateRootHTMLElement(this.theme);
            });

            this.networkService.init(this.network);

            this.attachService.checkFolders("send");
            this.attachService.checkFolders("received");

            this.file.checkFile(APP.PATH_DB_FILE, APP.DB_NAME)
                .then(_ => {
                    this.synchronize = false;
                    this.connectDataBase();
                })
                .catch(error => {
                    if(!error.message && error.message != "NOT_FOUND_ERR"){
                        this.logService.logError(error, this.userProvider.getUser());
                    }
                    this.connectDataBase();
                });
            
            this.platform.resume.subscribe((e) => {
                this.processResume();
            });

            this.platform.pause.subscribe((e) => {
                this.notificationProvider.platformState = "pause";
                this.synchronizationService.closeErrorSyncToast();
                if (this.userProvider.getUser() && this.userProvider.getUser().loggedIn && this.userProvider.getUser().createdAt > 0) {
                    this.networkService.unSubscribeEvents();
                    this.rtmService.closeConnection();
                    this.logService.logDevelopment("pause called", this.userProvider.getUser());
                    this.userProvider.saveUpdate();
                    this.userProvider.saveUsedEmojis();
                    this.securityConfigurationService.getSecurityFunctions();
                }
            });
        });
    }

    connectDataBase() {
        // Init DB
        // Connection options
        let options: ConnectionOptions = {
            type: 'cordova',
            database: APP.DB_NAME,
            location: 'default',
            logging: ["query","error"],
            logger: new CustomLoggerDataBase(this.logService, this.userProvider),
            maxQueryExecutionTime: 1000,
            synchronize: this.synchronize,
            entities: [Context, User, Participant, Command, Conversation, Message, Attach, MessageAction, MessageButton, MessageInput, SelectValue, UserUpdate, LogEntry, ConversationUnread]
        };
        createConnection(options)
            .then((connection) => {
                this.db.setConnection(connection);
                this.userProvider.validatePermissionsForPushNotification().then(result => this.logService.logDevelopment("Result of validation of push notifications permissions: " + result, this.userProvider.getUser()));
                this.userProvider.getTokenFCM()
                    .then(_ => {
                        this.loadData(false)
                            .then((page: string) => {
                                setTimeout(function(){
                                    this.logService.checkDBForErrors(this.userProvider.getUser());
                                }.bind(this), 4000);
                                this.splashScreen.hide();
                                console.log("page", page);
                                if(page){
                                    this.router.navigate([page], { state: { configuration: this.appConfigurationService.getConfigurationByUrl(page) }});
                                    this.notificationProvider.platformState = "active";
                                    this.checkNewAppUpdates(page);
                                    this.actionService.getActions();
                                    console.log("Finished opening processing");
                                } else {
                                    this.onError(null, null);
                                }
                            })
                            .catch((error) => {
                                this.logService.logError(error, this.userProvider.getUser());
                                this.splashScreen.hide();
                                console.log("Finished opening processing with error");
                                this.onError(error, null);
                            });
                    });
            })
            .catch(error => {
                this.logService.logDevelopment("Error creating database connection", this.userProvider.getUser());
                this.logService.logError(error, this.userProvider.getUser())
            });
    }

    loadData(tokenAlreadyFailed: boolean) {
        return new Promise((resolve, reject) => {
            let userRepository = this.db.getRepository(User) as Repository<User>;
            userRepository.findOne({
                where: {loggedIn: 1},
                join: {alias: 'user', innerJoinAndSelect: {'context': 'user.context'}}
            })
                .then((user) => {
                    if (user)  {
                        this.taskService.getVisibleColumns(user)
                            .then(result => {
                                if (user.createdAt > 0) {
                                    this.authProvider.init(user.token);
                                    this.userProvider.init(user);
                                    this.loaderProvider.initFromDB().then(()=>{
                                        //do nothing...
                                    });
                                    this.loaderProvider.init()
                                        .then(
                                            () => {
                                                this.actionService.getActions();
                                                resolve(this.appConfigurationService.getFirstTabUrl(this.securityConfigurationService));
                                                console.log("Resuelvo loadData init()");
                                            },
                                            (error) => {
                                                this.logService.logError(error, this.userProvider.getUser());
                                                resolve('/login');
                                            }
                                        );
                                }else {
                                    resolve(this.resetLoggedUser(user));
                                }
                            })
                            .catch(error => {
                                if (!tokenAlreadyFailed) {
                                    this.userProvider.login(user.username, user.password, user.context).then(_ => {
                                        this.logService.logDevelopment('Reconexion exitosa.', this.userProvider.getUser());
                                        this.securityConfigurationService.getSecurityFunctions();
                                        this.loadData(true).then(page => {
                                            this.logService.logDevelopment('Load Data quiere ir a :' + page, this.userProvider.getUser());
                                            resolve(page);
                                        });
                                    })
                                } else {
                                    resolve(this.resetLoggedUser(user));
                                }
                            });
                    }
                    else {
                        resolve(this.appConfigurationService.getInitialPageUrl(this.securityConfigurationService));
                    }
                })
                .catch((error) => {
                    this.logService.logError(error, this.userProvider.getUser());
                    resolve(this.appConfigurationService.getInitialPageUrl(this.securityConfigurationService));
                });
        });
    }

    private resetLoggedUser(user: User) {
        this.userProvider.setUser(user);
        this.userProvider.signOut()
            .then(_ => {
                this.ruleService.clearTokenDeyel();
                return '/login';
            })
            .catch(error => {
                this.logService.logError(error, this.userProvider.getUser());
                this.onError(error, null);
                return '/login';
            });
    }

    onError(error, loader) {
        this.router.navigate(['/login'], { state: { configuration: this.appConfigurationService.getConfigurationByUrl('/login') }});
    }

    resumeReconectSocket() {
        this.logService.logDevelopment('ESTADO CONEXION RESUME: ' + this.networkService.getConnected(), this.userProvider.getUser());
        this.ngZone.run(() => {
            // this.rtmService.initWsocket();
            this.loaderProvider.init()
                .then()
                .catch(error => this.logService.logError(error, this.userProvider.getUser()));
        });
    }

    updateRootHTMLElement(theme: string) {
        let ionApp = document.getElementsByTagName("ion-app")[0];
        let classes = Array.from(ionApp.classList).filter(c => !c.includes("-theme"));
        ionApp.className = classes.join(" ") + " " + theme;
    }

    /**
     * Checks for new app updates.
     * @param {any} redirectPage
     */
    private checkNewAppUpdates(redirectPage: any): void {
        if (redirectPage !== '/login' && this.userProvider.getUser()) {
            this.versionUpdateService.checkAppUpdate();
        }
    }

    /**
     * It resumes the connection.
     */
    private resumeConnection(): void {
        if (this.network.type != 'none') {
            this.userProvider.inProcessSync = true;
            this.rtmService.onTaskChangeSource.next();
        }
        this.networkService.init(this.network);
        this.logService.logDevelopment('resume called - Network type: ' + this.network.type, this.userProvider.getUser());
        this.notificationProvider.platformState = 'active';
        if (this.rtmService.tailEvents.length == 0) {
            this.resumeReconectSocket();
            return;
        }
        let interval = setInterval(() => {
            if (this.rtmService.tailEvents.length == 0) {
                this.resumeReconectSocket();
                clearInterval(interval);
            }
        }, 500);
    }

    /**
     * It processes the connection resume.
     */
    private processResume(): void {
        let user = undefined;
        if (this.userProvider.getUser()) {
            user = this.userProvider.getUser();
        }
        if (user && user.getLoggedIn() && user.getCreatedAt() > 0) {
            this.taskService.getVisibleColumns(user)
                .then(result => {
                    this.securityConfigurationService.getSecurityFunctions();
                    this.resumeConnection();
                })
                .catch(error => {
                    this.userProvider.login(user.getUsername(), user.getPassword(), user.getContext())
                        .then(_ => {
                            this.securityConfigurationService.getSecurityFunctions();
                            this.resumeConnection();
                        })
                        .catch(error => this.logService.logError(error, user));
                });
        }
    }

}
