import {ErrorHandler, NgModule, LOCALE_ID} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouteReuseStrategy} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { WebIntent } from '@ionic-native/web-intent/ngx';
import {IonicModule, IonicRouteStrategy} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';

import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
// import { ElasticModule } from 'ng-elastic';
// import { IonicStorageModule } from '@ionic/storage';
import {LocalNotifications} from '@ionic-native/local-notifications/ngx';
import {HttpModule} from '@angular/http';
import {HTTP} from '@ionic-native/http/ngx';
import {DirectivesModule} from './directives/directives.module';
import {Keyboard} from '@ionic-native/keyboard/ngx';
import {ScreenOrientation} from '@ionic-native/screen-orientation/ngx';
import {PhotoViewer} from '@ionic-native/photo-viewer/ngx';
import {FileOpener} from '@ionic-native/file-opener/ngx';
import {Autostart} from '@ionic-native/autostart/ngx';
import {File} from '@ionic-native/file/ngx';
import {FileTransfer} from '@ionic-native/file-transfer/ngx';
import {FilePath} from '@ionic-native/file-path/ngx';
import {Camera} from '@ionic-native/camera/ngx';
import {AppMinimize} from '@ionic-native/app-minimize/ngx';
import {AndroidPermissions} from '@ionic-native/android-permissions/ngx';
import {Chooser} from '@awesome-cordova-plugins/chooser/ngx';
import { Device } from '@awesome-cordova-plugins/device/ngx';

// Firebase Cloud Messaging
import {FirebaseX} from '@ionic-native/firebase-x/ngx';

// interceptors
import { JwtInterceptor } from './http/jwt.interceptor';
import { ApiInterceptor } from './http/api.interceptor';

// pages
import {LoginPage} from './pages/login/login';
import {ContextListPage} from './pages/context-list/context-list';
import {ContextListPageModule} from './pages/context-list/context-list.module';
import {ContextFormPage} from './pages/context-form/context-form';
import {ContextFormPageModule} from './pages/context-form/context-form.module';
import {LoginPageModule} from './pages/login/login.module';
import {MainPage} from './pages/main/main';
import {MainPageModule} from './pages/main/main.module';
import {MainMenuPage} from './pages/main-menu/main-menu';
import {MainMenuPageModule} from './pages/main-menu/main-menu.module';
import {ChatListPage} from './pages/chat-list/chat-list';
import {ChatListPageModule} from './pages/chat-list/chat-list.module';
import {ChatPage} from './pages/chat/chat';
import {ChatPageModule} from './pages/chat/chat.module';
import {ChatMenuPage} from './pages/chat-menu/chat-menu';
import {ChatMenuPageModule} from './pages/chat-menu/chat-menu.module';
import {TaskListPage} from './pages/task-list/task-list';
import {TaskListPageModule} from './pages/task-list/task-list.module';
import {TaskMoreActionsPage} from './pages/task-more-actions/task-more-actions';
import {TaskMoreActionsPageModule} from './pages/task-more-actions/task-more-actions.module';
import {TaskFilterMenuPage} from './pages/task-filter-menu/task-filter-menu';
import {TaskFilterMenuPageModule} from './pages/task-filter-menu/task-filter-menu.module';
import {FormListPage} from './pages/form-list/form-list';
import {FormListPageModule} from './pages/form-list/form-list.module';
import {IframeViewPage} from './pages/iframe-view/iframe-view';
import {IframeViewPageModule} from './pages/iframe-view/iframe-view.module';
import {UserListPage} from './pages/user-list/user-list';
import {UserListPageModule} from './pages/user-list/user-list.module';
import {CommandListPage} from './pages/command-list/command-list';
import {CommandListPageModule} from './pages/command-list/command-list.module';
import {BotListPage} from './pages/bot-list/bot-list';
import {BotListPageModule} from './pages/bot-list/bot-list.module';
import {BotCommandsPage} from './pages/bot-commands/bot-commands';
import {BotCommandsPageModule} from './pages/bot-commands/bot-commands.module';
import {GroupNewPage} from './pages/group-new/group-new';
import {GroupNewPageModule} from './pages/group-new/group-new.module';
import {GroupInfoPage} from './pages/group-info/group-info';
import {GroupInfoPageModule} from './pages/group-info/group-info.module';
import {GroupParticipantMenuPage} from './pages/group-participant-menu/group-participant-menu';
import {GroupParticipantMenuPageModule} from './pages/group-participant-menu/group-participant-menu.module';
import {GroupParticipantsPage} from './pages/group-participants/group-participants';
import {GroupParticipantsPageModule} from './pages/group-participants/group-participants.module';
import {DashboardListPageModule} from './pages/dashboard-list/dashboard-list.module';
import {DashboardListPage} from './pages/dashboard-list/dashboard-list';
import {ActionListPageModule} from './pages/action-list/action-list.module';
import {ActionListPage} from './pages/action-list/action-list';
import { BrowseFormPageModule } from './pages/browse-form/browse-form.module';
import { BrowseFormPage } from './pages/browse-form/browse-form';
import { TitlePageModule } from './pages/title/title.module';
import { TitlePage } from './pages/title/title';
import { RegisterPageModule } from './pages/register/register.module';
import { RegisterPage } from './pages/register/register';
import { ZurichCardListPageModule } from './pages/zurich-card-list/zurich-card-list.module';
import { ZurichCardListPage } from './pages/zurich-card-list/zurich-card-list';
import { PageIframePageModule } from './pages/page-iframe/page-iframe.module';
import { PageIframePage } from './pages/page-iframe/page-iframe';
import { NewWindowUrlPageModule } from './pages/new-window-url/new-window-url.module';
import { NewWindowUrlPage } from './pages/new-window-url/new-window-url';

// services
import {ConfigurationService} from './services/configuration.service';
import {ConversationService, ConversationApiService, ConversationDbService} from './services/conversation.service';
import {ParticipantService, ParticipantApiService, ParticipantDbService} from './services/participant.service';
import {TaskService} from './services/task.service';
import {RTMService} from './services/rtm.service';
import {NetworkService} from './services/network.service';
import {TranslateService} from './services/translate.service';
import {DownloadAttachService} from './services/download-attach.service';
import {AttachService} from './services/attach.service';
import {SynchronizationService} from './services/synchronization.service';
import {ActionService} from './services/action.service';
import {RuleService} from './services/rule.service';
import {UserProvider} from './services/user.service';
import {AuthProvider} from './services/auth.service';
import {UpdateProvider} from './services/update.service';
import {NotificationProvider} from './services/notification.service';
import {LoaderProvider} from './services/loader.service';
import {DbProvider} from './services/db.service';
import {BackButtonProvider} from './services/back-button.service';
import {ContextsProvider} from './services/contexts.service';
import { LogService } from './services/log.service';
import { PopoverService } from './services/popover.service';
import { VersionUpdateService } from './services/version-update.service';
import { AppConfigurationService } from './services/app-configuration.service';
import { CaseService } from './services/case.service';
import { SecurityConfigurationService } from './services/security-configuration.service';
import { EventService } from './services/event.service';

import {TRANSLATION_PROVIDERS} from './translate/translation';

// pipes
import {PipesModule} from './pipes/pipes.module';
import {EmoticonPipe} from './pipes/emoticon.pipe';
import {FilterActionPipe} from './pipes/filter-action/filter-action.pipe';

// translations
import {registerLocaleData} from '@angular/common';
import localeEs from '@angular/common/locales/es';
import localePt from '@angular/common/locales/pt';
import { CustomErrorHandler } from './errorHandler/CustomErrorHandler';

// Icons
import { FaIconLibrary, FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';
import { fab } from '@fortawesome/free-brands-svg-icons';

registerLocaleData(localeEs, 'es');
registerLocaleData(localePt, 'pt-BR');

@NgModule({
    declarations: [AppComponent],
    entryComponents: [
        AppComponent,
        LoginPage,
        ContextListPage,
        ContextFormPage,
        MainPage,
        ChatListPage,
        ChatPage,
        ChatMenuPage,
        TaskListPage,
        FormListPage,
        IframeViewPage,
        UserListPage,
        CommandListPage,
        BotListPage,
        BotCommandsPage,
        GroupNewPage,
        GroupInfoPage,
        GroupParticipantMenuPage,
        GroupParticipantsPage,
        DashboardListPage,
        ActionListPage,
        BrowseFormPage,
        TitlePage,
        RegisterPage,
        ZurichCardListPage,
        PageIframePage,
        NewWindowUrlPage
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        IonicModule.forRoot(/*{ tabsHideOnSubPages: true }*/),
        // IonicStorageModule.forRoot(),
        // ElasticModule,
        PipesModule,
        DirectivesModule,
        HttpModule,
        ChatListPageModule,
        ChatPageModule,
        ChatMenuPageModule,
        LoginPageModule,
        ContextListPageModule,
        ContextFormPageModule,
        TaskListPageModule,
        TaskMoreActionsPageModule,
        TaskFilterMenuPageModule,
        MainMenuPageModule,
        FormListPageModule,
        IframeViewPageModule,
        MainPageModule,
        UserListPageModule,
        CommandListPageModule,
        BotListPageModule,
        BotCommandsPageModule,
        GroupNewPageModule,
        GroupInfoPageModule,
        GroupParticipantMenuPageModule,
        GroupParticipantsPageModule,
        DashboardListPageModule,
        ActionListPageModule,
        FormsModule,
        BrowseFormPageModule,
        ReactiveFormsModule,
        FontAwesomeModule,
        TitlePageModule,
        RegisterPageModule,
        ZurichCardListPageModule,
        PageIframePageModule,
        NewWindowUrlPageModule
    ],
    providers: [
        {provide: RouteReuseStrategy, useClass: IonicRouteStrategy},
        {provide: LOCALE_ID, useValue: 'es'},
        {provide: LOCALE_ID, useValue: 'pt-BR'},
        WebIntent,
        AppVersion,
        StatusBar,
        SplashScreen,
        ErrorHandler,
        UserProvider,
        AuthProvider,
        UpdateProvider,
        {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true},
        {provide: HTTP_INTERCEPTORS, useClass: ApiInterceptor, multi: true},
        ConfigurationService,
        ConversationService, ConversationApiService, ConversationDbService,
        ParticipantService, ParticipantApiService, ParticipantDbService,
        TaskService,
        RTMService,
        NetworkService,
        TranslateService,
        SynchronizationService,
        TRANSLATION_PROVIDERS,
        LocalNotifications,
        File,
        NotificationProvider,
        DownloadAttachService,
        AttachService,
        LoaderProvider,
        BackButtonProvider,
        ContextsProvider,
        DbProvider,
        EmoticonPipe,
        Keyboard,
        ScreenOrientation,
        PhotoViewer,
        FileOpener,
        FileTransfer,
        Camera,
        FilePath,
        HTTP,
        AndroidPermissions,
        Autostart,
        AppMinimize,
        ActionService,
        FilterActionPipe,
        RuleService,
        FirebaseX,
        LogService,
        PopoverService,
        VersionUpdateService,
        {provide: ErrorHandler, useClass: CustomErrorHandler},
        AppConfigurationService,
        CaseService,
        SecurityConfigurationService,
        Chooser,
        Device,
        EventService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
    constructor(library: FaIconLibrary) { 
		library.addIconPacks(fas, fab, far);
	}
}
