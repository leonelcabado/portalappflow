import {InjectionToken} from '@angular/core';

export const TRANSLATIONS = new InjectionToken('translations');

export const TRANSLATION_PROVIDERS = [
    {provide: TRANSLATIONS, useValue: {}},
];
