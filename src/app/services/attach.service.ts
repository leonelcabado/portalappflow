import { Injectable, ElementRef } from '@angular/core';
import { AlertController, Platform, ToastController } from '@ionic/angular';
import { FileTransferObject, FileTransfer } from '@ionic-native/file-transfer/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { File, FileEntry } from '@ionic-native/file/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { HTTP } from '@ionic-native/http/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { JwtHelper } from '../support-classes/jwt-helper';
import { UserProvider } from './user.service';
import { BackButtonProvider } from './back-button.service';
import { APP } from '../app.config';
import { Attach } from '../model/attach.entity';
import { DbProvider } from './db.service';
import { TranslateService } from './translate.service';
import { LogService } from './log.service';
import { Chooser } from '@awesome-cordova-plugins/chooser/ngx';

@Injectable()
export class AttachService {

    extensionMap: Map<string, string>;
    toast: any;
    private fileTransfer: FileTransferObject;

    constructor(private alertCtrl: AlertController,
                private transfer: FileTransfer,
                private camera: Camera,
                private file: File,
                private filePath: FilePath,
                private userProvider: UserProvider,
                private backButtonProvider: BackButtonProvider,
                private http: HTTP,
                private androidPermissions: AndroidPermissions,
                private photoViewer: PhotoViewer,
                private dbProvider: DbProvider,
                private fileOpener: FileOpener,
                private toastCtrl: ToastController,
                private translateService: TranslateService,
                private platform: Platform,
                private logService: LogService,
                private chooser: Chooser) {
        this.generateAttachMap();
    }

    async captureLocalFile(origin): Promise<string> {
        const options: CameraOptions = {};
        this.checkFolders("send");
        options.destinationType = this.camera.DestinationType.FILE_URI;
        if (origin == 'camera') {
            options.sourceType = this.camera.PictureSourceType.CAMERA;
            options.mediaType = this.camera.MediaType.PICTURE;
            options.quality = 25;
            options.allowEdit = false;
            options.correctOrientation = true;
            options.encodingType = this.camera.EncodingType.JPEG;
            return this.getPictureFile(options);
        }else{
            return this.getFSFile();
        }
    }

    /**
     * Gets an image taken with the device's camera.
     * @param options 
     * @returns 
     */
    private getPictureFile(options: CameraOptions): Promise<string> {
        return this.camera.getPicture(options)
            .then((imageData) => {
                console.log(imageData);
                if(!imageData){
                    return;
                }
                if(this.platform.is("ios")){
                    return imageData;
                }else{
                    return this.getFileRealPath(imageData).then((filePath: string) => {
                        return filePath;
                    });
                }
            })
            .catch((error: any) => {
                if(error !== "No Image Selected"){
                    this.logService.logError(error, this.userProvider.getUser());
                }
                return undefined;
            });
    }

    /**
     * Gets a selected file from the device's file system.
     * @returns 
     */
    private getFSFile(): Promise<string>{
        return this.chooser.getFile()
            .then(file => {
                if(!file){
                    return;
                }
                return this.getFileRealPath(file.uri).then((filePath: string) => {
                    return filePath;
                });
            })
            .catch((error: any) => {
                this.logService.logError(error, this.userProvider.getUser());
                return undefined;
            });
    }

    /**
     * Returns the real path of the file.
     * @param imageData
     */
    public getFileRealPath(imageData: string): Promise<string> {
            this.logService.logDevelopment('In imageDATA ' + imageData, this.userProvider.getUser());
            if (!imageData.includes('://')) {
                imageData = 'file://' + imageData;
            }
            this.logService.logDevelopment('OUT imageDATA ' + imageData, this.userProvider.getUser());
            return this.filePath.resolveNativePath(imageData)
                .then((filePath: string) => {
                    return filePath;
                })
                .catch((error) => {
                    this.logService.logError(error, this.userProvider.getUser());
                    return undefined;
                });
    }

    checkSizeFile(fullPath): Promise<boolean> {
        return new Promise((resolve, reject) => {
            this.file.resolveLocalFilesystemUrl(fullPath)
                .then((file) => {
                    file.getMetadata((meta) => {
                        if (Math.ceil(meta.size / 1024 / 1024) > 5) {
                            this.alertCtrl.create({
                                header: 'Max size attach.',
                                message: 'The maximum allowed size is 5MB.',
                                buttons: ['Ok']
                            }).then(alert => {
                                this.backButtonProvider.registerDismissable(alert);
                                alert.present();
                            });
                            resolve(false);
                        }
                        resolve(true);
                    });
                })
                .catch(error => {
                        this.logService.logError(error, this.userProvider.getUser());
                        resolve(false);
                    }
                )
        });
    }

    /**
     * Check and create folders DeyelChat, Received and Send
     */
    public checkFolders(folder: string) {
        const platformBasePath = this.getPlatformBasePath();
        this.file.checkDir(platformBasePath, 'DeyelChat')
            .then(_ => {
                this.file.checkDir(platformBasePath + 'DeyelChat/', folder)
                    .then()
                    .catch(_ => this.file.createDir(platformBasePath + 'DeyelChat/', folder, true)
                        .then()
                        .catch(error => {
                            this.logService.logError(error, this.userProvider.getUser());
                            console.error('Error', JSON.stringify(error));})
                    );
            })
            .catch(_ => {
                this.file.createDir(platformBasePath, 'DeyelChat', true)
                    .then(_ => {
                        this.file.createDir(platformBasePath + 'DeyelChat/', folder, true)
                            .then()
                            .catch(error => {
                                this.logService.logError(error, this.userProvider.getUser());
                                console.error('Error', JSON.stringify(error));
                            })
                    })
                    .catch(error => {
                        this.logService.logError(error, this.userProvider.getUser());
                        console.error('Error', JSON.stringify(error));
                    })
            });
    }

    uploadFile(localCopyFile: string, fileName: string, headers?: any): Promise<any> {
        let url = this.userProvider.getContextUrl() + "/SLUploadMessageAttach?currentUser=" + this.userProvider.getUsername();
        let options = {
            fileName: fileName,
            headers: (typeof headers !== "undefined") ? headers : {}
        };
        return new Promise((resolve, reject) => {
            this.fileTransfer = this.fileTransfer ? this.fileTransfer : this.transfer.create();
            this.fileTransfer.upload(localCopyFile, url, options)
                .then(data => {
                    console.log(JSON.parse(data["response"]));
                    resolve(JSON.parse(data["response"]));
                })
                .catch(error => {
                    this.logService.logError(error, this.userProvider.getUser());
                    console.log(JSON.stringify(error));
                    this.showToast("ERROR_UPLOADING_FILE", "toastError");
                });
        });
    }

    abortUploadFile(file) {
        if (this.fileTransfer) this.fileTransfer.abort();
        this.file.removeFile(this.getPlatformBasePath() + APP.PATH_SEND, file.dsName)
            .then()
            .catch(error => this.logService.logError(error, this.userProvider.getUser()));
    }

    /**
     * Get the base path by platform
     */
    public getPlatformBasePath(): string{
        let platformBasePath = this.file.externalRootDirectory;
        if(this.platform.is("ios")){
            platformBasePath = this.file.dataDirectory;
        }
        return platformBasePath;
    }

    /**
     * Call the servlet to download one file.
     * @param {Attach}  file
     * @param {string}  cdParticipant
     * @param {boolean} showFile
     */
    public downloadFile(file: Attach, cdParticipant: string, showFile: boolean): void {
        file.downloading = true;
        let folder = (cdParticipant == (this.userProvider.getUsername()).toUpperCase()) ? this.getPlatformBasePath() + APP.PATH_SEND : this.getPlatformBasePath() + APP.PATH_RECEIVED;
        let servletUrl = this.userProvider.getContextUrl() + "/SLDownloadMessageAttach?currentUser=" + this.userProvider.getUsername() + "&fileId=" + file.cdFile;
        this.download(servletUrl, folder, file, showFile);
    }

    downloadFileWithStoreableString(elementValue: any) {
        if (elementValue) {
            let servletUrl = this.userProvider.getContextUrl() + "/SLDownloadMessageAttach?currentUser=" + this.userProvider.getUsername() + elementValue;
            servletUrl = servletUrl.replace(/%%%/g, "&");
            this.download(servletUrl, this.getPlatformBasePath() + APP.PATH_FORM_DOWNLOAD, null, true);
        }
    }

    /**
     * Checks the permissions and download one file.
     * @param {string}  servletUrl
     * @param {string}  folder
     * @param {Attach}  attach
     * @param {boolean} showFile
     */
    public download(servletUrl: string, folder: string, attach: Attach, showFile: boolean): void {
        if (this.platform.is("android")) {
            this.checkAndroidPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE)
                .then(result => {
                    if (result) {
                        this.processDownloadFile(servletUrl, folder, attach, showFile);
                    }
                })
                .catch(error => {
                    this.logService.logError(error, this.userProvider.getUser());
                    console.error('Error', JSON.stringify(error));
                    this.showToast("ERROR_DOWNLOADING_FILE", "toastError");
                });
        } else {
            this.processDownloadFile(servletUrl, folder, attach, showFile);
        }
    }

    /**
     * Process download one file.
     * @param {string}  servletUrl
     * @param {string}  folder
     * @param {Attach}  attach
     * @param {boolean} showFile
     */
    public processDownloadFile(servletUrl: string, folder: string, attach: Attach, showFile: boolean): void{
        this.http.get(servletUrl, {}, {})
                .then(data => {
                    let tempPath = JSON.parse(data["data"]).tempPath;
                    let localPath = folder + new Date().getTime() + "_" + JSON.parse(data["data"]).fileName;
                    this.fileTransfer = this.fileTransfer ? this.fileTransfer : this.transfer.create();
                    this.fileTransfer.download(tempPath, localPath)
                        .then(data => {
                            let newAtt = false;
                            if (attach === null) {
                                let fileEntry = (<FileEntry>data);
                                attach = new Attach();
                                attach.dsName = fileEntry.name;
                                attach.cdExtension = attach.dsName.substring(attach.dsName.lastIndexOf(".") + 1, attach.dsName.length).toLowerCase();
                                newAtt = true;
                            }
                            attach.cdLocalPath = localPath;
                            if (showFile) {
                                this.showFile(attach, localPath);
                            }
                            this.saveAttach(attach, newAtt);
                            attach.downloading = false;
                            attach.realPath = this.getRealPathFile(attach);
                        })
                        .catch(error => {
                            this.logService.logError(error, this.userProvider.getUser());
                            this.showToast("ERROR_DOWNLOADING_FILE", "toastError");
                            if (attach) {
                                attach.downloading = true;
                            }
                        });
                })
                .catch(error => {
                    this.logService.logError(error, this.userProvider.getUser());
                    console.error('Error', JSON.stringify(error));
                });
    }

    saveAttach(attach, newAtt){
        if (newAtt){
            this.dbProvider.getRepository(Attach).createQueryBuilder()
                .insert()
                .into('attach')
                .values(attach)
                .execute();
        }
        else {
            this.dbProvider.getRepository(Attach).createQueryBuilder()
                .update(Attach)
                .set(attach.getPlainObject())
                .where('id = :id', { id: attach.id })
                .execute();
        }
    }

    showToast($message: string, $class: string) {
        this.toastCtrl.create({
            message: this.translateService.instant($message),
            duration: 3600,
            position: 'bottom',
            cssClass: $class,
        }).then(toast => toast.present());
    }

    checkAndroidPermission(permission: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.androidPermissions.checkPermission(permission)
                .then(result => {
                    if (result.hasPermission) {
                        resolve(true);
                    }
                    else {
                        this.androidPermissions.requestPermission(permission)
                            .then(result => {
                                if (result.hasPermission) {
                                    resolve(true);
                                }
                                else {
                                    resolve(false);
                                }
                            })
                    }
                })
                .catch(error => {
                    this.logService.logError(error, this.userProvider.getUser());
                    resolve(false)
                });
        });
    }

    showFile(pFile: Attach, localPath = null) {
        let path;
        localPath ? path = localPath : path = pFile.cdLocalPath;
        switch (pFile.cdExtension) {
            case 'png':
            case 'jpg':
            case 'jpeg':
            case 'gif': {
                this.photoViewer.show(path, pFile.dsName);
                break;
            }
            case 'pdf': {
                this.openFile(path, 'application/pdf');
                break;
            }
            case 'csv':
            case 'xls':
            case 'xlsx': {
                this.openFile(path, 'application/vnd.ms-excel');
                break;
            }
            case 'doc':
            case 'docx': {
                this.openFile(path, 'application/msword');
                break;
            }
            case 'ppt':
            case 'pptx': {
                this.openFile(path, 'application/vnd.ms-powerpoint');
                break;
            }
            case 'txt': {
                this.openFile(path, 'text/plain');
                break;
            }
            case 'exe': {
                this.openFile(path, 'application/octet-stream');
                break;
            }
            case 'rar':
            case 'zip': {
                this.openFile(path, 'application/zip');
                break;
            }
            case 'avi': {
                this.openFile(path, 'video/x-msvideo');
                break;
            }
            case 'mp4': {
                this.openFile(path, 'video/mp4');
                break;
            }
            default: {
                break;
            }
        }
    }

    errorOpening() {
        this.alertCtrl.create({
            header: this.translateService.instant('ERROR_OPENING_FILE'),
            message: this.translateService.instant('NO_APP_TO_OPEN'),
            buttons: ['OK']
        }).then(alert => {
            this.backButtonProvider.registerDismissable(alert);
            alert.present();
        });
    }

    generateAttachMap() {
        this.extensionMap = new Map();
        this.extensionMap.set("after-effects", "after-effects");
        this.extensionMap.set("ai", "ai");
        this.extensionMap.set("audition", "audition");
        this.extensionMap.set("avi", "avi");
        this.extensionMap.set("bridge", "bridge");
        this.extensionMap.set("css", "css");
        this.extensionMap.set("csv", "csv");//--->ok
        this.extensionMap.set("dbf", "dbf");
        this.extensionMap.set("doc", "doc");//--->ok
        this.extensionMap.set("docx", "doc");//--->ok
        this.extensionMap.set("dreamweaver", "dreamweaver");
        this.extensionMap.set("dwg", "dwg");
        this.extensionMap.set("exe", "exe");//--->ok
        this.extensionMap.set("file", "file");
        this.extensionMap.set("fireworks", "fireworks");
        this.extensionMap.set("fla", "fla");
        this.extensionMap.set("flash", "flash");
        this.extensionMap.set("html", "html");
        this.extensionMap.set("indesign", "indesign");
        this.extensionMap.set("iso", "iso");
        this.extensionMap.set("javascript", "javascript");
        this.extensionMap.set("jpg", "jpg");//--->ok
        this.extensionMap.set("json-file", "json-file");
        this.extensionMap.set("mp3", "mp3");
        this.extensionMap.set("mp4", "mp4");
        this.extensionMap.set("pdf", "pdf");//--->ok
        this.extensionMap.set("photoshop", "photoshop");
        this.extensionMap.set("png", "png");//--->ok
        this.extensionMap.set("ppt", "ppt");//--->ok
        this.extensionMap.set("pptx", "ppt");//--->ok
        this.extensionMap.set("prelude", "prelude");
        this.extensionMap.set("premiere", "premiere");
        this.extensionMap.set("psd", "psd");
        this.extensionMap.set("rtf", "rtf");
        this.extensionMap.set("search", "search");
        this.extensionMap.set("svg", "svg");
        this.extensionMap.set("txt", "txt");//--->ok
        this.extensionMap.set("xls", "xls");//--->ok
        this.extensionMap.set("xlsx", "xls");//--->ok
        this.extensionMap.set("xml", "xml");
        this.extensionMap.set("zip", "zip");
        this.extensionMap.set("zip-1", "zip-1");
        this.extensionMap.set("rar", "zip");
        this.extensionMap.set("default", "file");
    }

    getFile(realPath: string): Promise<any> {
        let fileName = realPath.substring(realPath.lastIndexOf('/') + 1, realPath.length);
        let headers = {
            cdRepositorio: "IDB",
            dsFileUsage: "FORM_FIELD"
        };
        return this.uploadFile(realPath, fileName, headers);
    }

    /**
     * Utility function for converting File URIs.
     * @param  {Attach} attach
     * @return {Promise<string>}
     */
    public convertFileSrc(attach: Attach): Promise<string> {
        let localPath = attach.cdLocalPath;
        let lastIndexSlashLength = localPath.lastIndexOf('/') + 1;
        let filepath = localPath.substring(0, lastIndexSlashLength);
        let filename = localPath.substring(lastIndexSlashLength, localPath.length);
        return this.file.checkFile(filepath, filename)
            .then((file) => {
                return this.getRealPathFile(attach);
            })
            .catch((error) => {
                this.logService.logError(error, this.userProvider.getUser());
                return undefined;
            });
    }

    /**
     * Returns the real path of one file to show.
     * @param  {Attach} attach
     * @return {string}
     */
    public getRealPathFile(attach: Attach): string{
        const windowObject = <any> window;
        if (windowObject.Ionic && windowObject.Ionic.WebView) {
            return windowObject.Ionic.WebView.convertFileSrc(attach.cdLocalPath);
        }
        return undefined;
    }

    /**
     * It open file from a base 64.
     * @param {string} base64String
     * @param {string} contentType
     * @param {string} fileName
     */
    public showBase64(base64String: string, contentType: string, fileName: string): void {
        const writeDirectory = this.platform.is('ios') ? this.file.dataDirectory : this.file.externalDataDirectory;
        this.file.writeFile(writeDirectory, fileName, JwtHelper.convertBase64ToBlob(base64String, `data:${contentType};base64`), {replace: true})
            .then(() => {
                this.fileOpener.open(writeDirectory + fileName, contentType)
                .catch((error) => {
                    this.logService.logError(error, this.userProvider.getUser());
                });
            })
            .catch((error) => {
                this.logService.logError(error, this.userProvider.getUser());
            });
    }

    /**
     * It opens the file.
     * @param {string} filePath
     * @param {string} fileMIMEType
     */
    private openFile(filePath: string, fileMIMEType: string): void {
        this.fileOpener.open(filePath, fileMIMEType)
            .then(() => {
                console.log('File is opened');
            })
            .catch(error => {
                this.logService.logError(error, this.userProvider.getUser());
        });
    }

    /**
     * Process the attach event from iframe.
     * @param {string} origin
     * @param {string} elementId
     * @param {string} containerId
     * @param {number} iterativeIndex
     * @param {ElementRef} frame
     * @return {undefined}
     */
    public attachEventFromIframe(origin: string, elementId: string, containerId: string, iterativeIndex: number, frame: ElementRef): void {
        this.captureLocalFile(origin)
            .then((realPath) => {
                if(realPath) {
                    this.getFile(realPath).then((response) => {
                        frame.nativeElement.contentWindow.postMessage({
                            operation: 'attached',
                            fileId: response.fileId,
                            elementId: elementId,
                            containerId: containerId,
                            iterativeIndex: iterativeIndex
                        }, '*');
                    });
                    return;
                }
                this.logService.logDevelopment('real path is not defined', this.userProvider.getUser());
            })
            .catch( error => {
                this.logService.logError(error, this.userProvider.getUser());
        });
    }

    /**
     * Downloads a file from url.
     * @param {string} url
     * @param {string} fileName
     * @param {string} fileExtension
     */
    public downloadFileFromUrl(url: string, fileName: string, fileExtension: string): void {
        const fileTransfer: FileTransferObject = this.transfer.create();
        const absoluteFileName = this.getPlatformBasePath() + APP.PATH_RECEIVED + fileName + fileExtension;
        fileTransfer.download(encodeURI(url), absoluteFileName).then((fileEntry: FileEntry) => {
            if (fileEntry) {
                this.showToast('File ownloaded successfully in ' + this.getPlatformBasePath() + APP.PATH_RECEIVED, 'toastError');
            }
        })
        .catch((error) => {
            this.logService.logError(error, this.userProvider.getUser());
        });
    }

}
