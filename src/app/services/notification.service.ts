import {Injectable, NgZone} from '@angular/core';
import {LocalNotifications} from '@ionic-native/local-notifications/ngx';
import {Observable} from 'rxjs';
import {Platform, ToastController} from '@ionic/angular';

//models
import {Conversation} from '../model/conversation.entity';
import {Message} from '../model/message.entity';

//providers
import {UserProvider} from './user.service';

//pipes
import {EmoticonPipe} from '../pipes/emoticon.pipe';

//services
import {TranslateService} from './translate.service';
import {TaskService} from './task.service';
import { LogService } from './log.service';

@Injectable()
export class NotificationProvider {

    private observable;
    private observer;
    text: string;
    platformState: string = "active";

    constructor(private localNotifications: LocalNotifications,
                private userProvider: UserProvider,
                private ngZone: NgZone,
                private emoticonPipe: EmoticonPipe,
                private translateService: TranslateService,
                private taskService: TaskService,
                private toastCtrl: ToastController,
                private platform: Platform,
                private logService: LogService) {

        this.platform.ready().then(() => {
            this.localNotifications.on('click').subscribe((notification) => {
                this.userProvider.redirectAction = notification.data.action;
                this.userProvider.redirectTargetId = notification.data.targetId;
                this.userProvider.redirectDsCase = notification.data.dsCase;
                this.userProvider.redirecDsActivity = notification.title;
            });
        });
    }

    newMessage(conversation: Conversation, msg: Message) {
        if (msg.lsAnnexs && msg.lsAnnexs.length > 0) {
            if (msg.lsAnnexs[msg.lsAnnexs.length - 1].imagePreview) {
                this.text = "Image";
            }
            else {
                this.text = "Document";
            }
        }
        else if (msg.lsInputs && msg.lsInputs.length > 0) {
            this.text = "Input message";
        }
        else if (msg.lsActions && msg.lsActions.length > 0) {
            this.text = "Action message";
        }
        else {
            this.text = (msg.dsContent).replace(/<div>/gi, "\n");
            this.text = this.text.replace(/<\/div>/ig, " ");
            this.text = this.text.replace(/<br\s*[\/]?>/gi, "\n");
            this.text = this.emoticonPipe.transform(this.text);
            this.text = msg.dsContent.length > 50 ? this.text.substring(0, 47).concat("...") : this.text;
        }
        console.log('platformState', this.platformState);
        if ((conversation.cdConversation != this.userProvider.cdCurrentConversation) && (msg.cdParticipant != this.userProvider.getUsername().toUpperCase())) {
            this.ngZone.runOutsideAngular(() => {
                this.localNotifications.schedule({
                    id: new Date().getTime(),
                    title: (conversation && conversation.cdType == 0) ? conversation.receptor.dsParticipant : conversation.dsTitle,
                    text: this.text,
                    //icon: 'res://notification_icon', // TODO poner el ícono de la persona
                    smallIcon: 'res://notification_icon',
                    color: '00b6ee',
                    vibrate: true,
                    wakeup: false,
                    data: {
                        action: this.userProvider.OPEN_CONVERSATION_ACTION,
                        targetId: conversation.cdConversation
                    }
                });
            });
        }
    }

    /**
     * It shows the notification to new task.
     * @param {any} task
     */
    public newTask(task: any): void {
        if (task && task.id !== this.userProvider.getConsultedTask()) {
            this.ngZone.runOutsideAngular(() => {
                this.localNotifications.schedule({
                    id: new Date().getTime(),
                    title: task.dsActivity,
                    text: task.dsCase,
                    //icon: 'res://notification_icon', // TODO poner el ícono de la persona
                    smallIcon: 'res://notification_icon',
                    color: '00b6ee',
                    vibrate: true,
                    wakeup: false,
                    data: {
                        action: this.userProvider.OPEN_TASK_ACTION,
                        targetId: task.id,
                        dsCase: task.dsCase
                    }
                });
            });
        }
    }

    clearNotification(targetId) {
        this.localNotifications.getAll().
            then(notifications => {
                notifications.forEach(notification => {
                    let data = JSON.parse(notification.data);
                    if (data.targetId == targetId) {
                        this.localNotifications.clear(notification.id);
                    }
                });
            })
            .catch(error => {
                this.logService.logError(error,this.userProvider.getUser());
            })
    }

    navigation(): Observable<any> {
        if (!this.observable) {
            this.observable = new Observable(observer => {
                this.observer = observer;
            });
        }
        return this.observable;
    }
}
