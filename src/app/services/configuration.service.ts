import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {TedisConfiguration} from '../model/tedis-configuration.entity';
import {UserProvider} from './user.service';

//helpers
import {ApiItemInterface} from '../support-classes/api-item-interface';
import {Subject} from 'rxjs';
import { LogService } from './log.service';

@Injectable()
export class ConfigurationService {

    tedisConfiguration: TedisConfiguration;
    private themeChanged = new Subject<string>();
    themeChanged$ = this.themeChanged.asObservable();

    constructor(private http: HttpClient,
                private userProvider: UserProvider,
                private logService: LogService) {

    }

    public init() {
        this.http.get<ApiItemInterface>(this.userProvider.apiEndpoint() + "/users/" + this.userProvider.getUsername() + "/config")
            .toPromise()
            .then(response => {
                this.tedisConfiguration = response.data as TedisConfiguration;
                this.themeChanged.next(this.tedisConfiguration.systemConfiguration.theme);
                return true;
            })
            .catch(error => {
                return false;
            });
    }

    public notifyThemeChanged(theme) {
        if (this.tedisConfiguration && this.tedisConfiguration.systemConfiguration) {
            this.tedisConfiguration.systemConfiguration.theme = theme;
        }
        this.themeChanged.next(theme);
    }

    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }

    public isGuest(): boolean {
        if (this.tedisConfiguration) {
            return this.tedisConfiguration.isGuest();
        } else {
            return false;
        }
    }

    public getThemeClass(pTheme?: string): string {
        let theme = pTheme ? pTheme : this.tedisConfiguration.systemConfiguration.theme;
        if (theme === 'Rojo') {
            return 'ts-red-theme';
        }
        if (theme === 'Verde') {
            return 'ts-green-theme';
        }
        return '';
    }

}
