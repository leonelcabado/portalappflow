import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class EventService {

    private eventSubject = new Subject<any>();

	/**
	* Emits a event in App.
	* @param {any} data
	* @return {undefined}
	*/
    public emitEvent(data: any): void {
        this.eventSubject.next(data);
    }

	/**
	* Subscribes to any App event.
	* @return {Subject<any>}
	*/
    public subscribeEvents(): Subject<any> {
        return this.eventSubject;
    }
}
