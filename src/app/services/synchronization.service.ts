import {Injectable, NgZone} from '@angular/core';
import {Repository} from 'typeorm';
import {HttpClient} from '@angular/common/http';
import {AlertController, ToastController} from '@ionic/angular';
import {HttpErrorResponse} from '@angular/common/http';
import {Subject} from 'rxjs';
import {timeout} from 'rxjs/internal/operators';

//helpers
import {ApiSyncItemInterface} from '../support-classes/api-sync-item-interface';

//providers
import {UserProvider} from './user.service';
import {UpdateProvider} from './update.service';
import {DbProvider} from './db.service';
import {NotificationProvider} from './notification.service';

//models
import {User} from '../model/user.entity';
import {Participant} from '../model/participant.entity';
import {Conversation} from '../model/conversation.entity';
import {ConversationUnread} from '../model/conversation-unread.entity';
import {Message} from '../model/message.entity';

//services
import {AttachService} from './attach.service';
import {ConversationService} from './conversation.service';
import {ParticipantService} from './participant.service';
import {NetworkService} from './network.service';
import {RTMService} from './rtm.service';
import {TranslateService} from './translate.service';
import { ActionService } from './action.service';
import { LogService } from './log.service';

@Injectable()
export class SynchronizationService {

    private messagesOffline: Message[] = [];
    private messagesAdd: Message[] = [];
    private lastConversationUsed: Conversation = null;
    public onFinishSyncSource = new Subject<boolean>();
    public onFinishSync$ = this.onFinishSyncSource.asObservable();
    notNotification = true;
    toast1: any;
    toast2: any;
    tailSync: number = 0;
    inRtryOC: boolean = false;
    synchronizing: boolean = false;
    auxTime: number;

    constructor(private http: HttpClient,
                private updateProvider: UpdateProvider,
                private userProvider: UserProvider,
                private attachService: AttachService,
                private conversationService: ConversationService,
                private participantService: ParticipantService,
                private dbProvider: DbProvider,
                private networkService: NetworkService,
                private rtmService: RTMService,
                private ngZone: NgZone,
                private alertCtrl: AlertController,
                private toastCtrl: ToastController,
                private translateService: TranslateService,
                private notificationProvider: NotificationProvider,
                private actionService: ActionService,
                private logService: LogService) {
        this.rtmService.onConnect$.subscribe(resumePlatform => {
            if (!this.userProvider.inSync || resumePlatform) {
                this.userProvider.inSync = true;
                this.notNotification = (resumePlatform) ? true : false;
                this.sync()
                    .catch(error => {
                        this.logService.logError(error, this.userProvider.getUser());
                        this.retrySync();
                    })
            }
            else {
                this.tailSync++;
            }
        });
    }

    retrySync() {
        this.errorSyncToast();
        this.notNotification = false;
        let $t = this;
        let interval = setInterval(function () {
            if ($t.userProvider.getUserSocketId()) {
                $t.sync()
                    .then(_ => {
                        $t.closeErrorSyncToast();
                        clearInterval(interval);
                    })
                    .catch(error => {
                        $t.logService.logError(error, $t.userProvider.getUser());
                        console.warn("error");
                    });
            }
        }, 5000);
    }

    retryOpenConnection() {
        console.error("retryOpenConnection");
        this.userProvider.inSync = false;
        let $t = this;
        let numberRetries = 0;
        let interval = setInterval(function () {
            numberRetries++;
            console.error("retryOpenConnection ", numberRetries);
            if (!$t.rtmService.networkConnected || !$t.userProvider.getUser().loggedIn || $t.notificationProvider.platformState != "active") clearInterval(interval);
            if (numberRetries == 5) $t.errorSyncToast();
            $t.rtmService.openConnection(true)
                .then(_ => {
                    if (numberRetries > 4) $t.closeErrorSyncToast();
                    clearInterval(interval);
                })
                .catch(error => {
                    $t.logService.logDevelopment("Error retryOpenConnection", $t.userProvider.getUser());
                    $t.logService.logDevelopment(error, $t.userProvider.getUser());
                });

        }, this.generateRandomTimeInMilliSeconds());
    }

    /**
     * Returns a number that represents a value between 1000 and 20000 milliseconds.
     * @returns 
     */
    public generateRandomTimeInMilliSeconds(): number {
        return (Math.random() * 19000) + 1000;
    };

    sync(loadApp?: boolean) {
        this.userProvider.inProcessSync = true;
        return this.ngZone.runOutsideAngular(() => {
            return new Promise((resolve, error) => {
                if (!this.synchronizing) {
                    this.synchronizing = true;
                    let inicio = new Date().valueOf();
                    let dtLastUpdate = this.updateProvider.dtLastUpdate;
                    this.actionService.getActions();
                    this.logService.logDevelopment("Solicito sync a: " + `${this.userProvider.apiEndpoint()}/users/${this.userProvider.getUsername()}/updates?dtLastUpdate=${dtLastUpdate}`, this.userProvider.getUser());
                    this.http.get<ApiSyncItemInterface>(`${this.userProvider.apiEndpoint()}/users/${this.userProvider.getUsername()}/updates?dtLastUpdate=${dtLastUpdate}`)
                        .pipe(timeout(100000))
                        .toPromise()
                        .then(response => {
                            // console.warn(JSON.stringify(response.updates));
                            console.error('Demoró ' + (new Date().valueOf() - new Date(inicio).valueOf()) + ' milisegundos.');
                            inicio = new Date().valueOf();
                            this.logService.logDevelopment(`Se solicitó sincronización con dtLastUpdate: ${dtLastUpdate}`, this.userProvider.getUser());
                            this.logService.logDevelopment("Comienzo a desmembrar datos que devuelve de la sync", this.userProvider.getUser());
                            let participantsStateUpdates = [];
                            if (response.updates) {
                                let promises: Promise<any>[] = [];
                                let participantsUpdates = [];
                                let conversationsUpdates = [];
                                let messagesUpdates = [];
                                let stateMessagesUpdates = [];

                                for (let element of response.updates) {
                                    switch (element.type) {
                                        case "user":
                                            // console.warn(JSON.stringify(element));
                                            participantsUpdates.push(element);
                                            break;
                                        case "state":
                                            participantsStateUpdates.push(element);
                                            break;
                                        case "conv":
                                            conversationsUpdates.push(element);
                                            break;
                                        case "msg":
                                            console.warn(JSON.stringify(element));
                                            messagesUpdates.push(element);
                                            break;
                                        case "msgState":
                                            console.warn(JSON.stringify(element));
                                            stateMessagesUpdates.push(element);
                                            break;
                                        default: // Shouldn't happend
                                            break;
                                    }
                                }

                                console.error('Demoró ' + (new Date().valueOf() - new Date(inicio).valueOf()) + ' milisegundos.');
                                console.error(participantsUpdates.length + ' elementos de participantes a sync');
                                console.error(conversationsUpdates.length + ' elementos de chats a sync');
                                console.error(messagesUpdates.length + ' elementos de mensajes a sync');
                                console.error(stateMessagesUpdates.length + ' elementos de estado de mensajes a sync');
                                this.logService.logDevelopment("Comienzo a correr cada proceso de sync", this.userProvider.getUser());
                                inicio = new Date().valueOf();
                                this.syncParticipants(participantsUpdates)
                                    .then(() => {
                                        this.logService.logDevelopment('Termina syncParticipants', this.userProvider.getUser());
                                        console.error('Demoró ' + (new Date().valueOf() - new Date(inicio).valueOf()) + ' milisegundos.');
                                        let inicio2 = new Date().valueOf();
                                        this.syncConversations(conversationsUpdates)
                                            .then(() => {
                                                this.logService.logDevelopment('Termina syncConversations', this.userProvider.getUser());
                                                console.error('Demoró ' + (new Date().valueOf() - new Date(inicio2).valueOf()) + ' milisegundos.');
                                                inicio2 = new Date().valueOf();
                                                this.syncMessages(messagesUpdates, loadApp)
                                                    .then(() => {
                                                        this.logService.logDevelopment('Termina syncMessages', this.userProvider.getUser());
                                                        console.error('Demoró ' + (new Date().valueOf() - new Date(inicio2).valueOf()) + ' milisegundos.');
                                                        inicio2 = new Date().valueOf();
                                                        this.dbProvider.getRepository(Conversation).createQueryBuilder("c")
                                                            .innerJoinAndSelect(Message, "m", "m.conversationId = c.id")
                                                            .leftJoinAndSelect(ConversationUnread, "cu", "c.cdConversation = cu.cdConversation and c.userId = cu.userId")
                                                            .where('c.userId = :userId and (c.synchronized = 0 or cu.synchronized = 0 or (m.cdConversationElement is NULL AND m.cdConversation IS NOT NULL AND m.cdParticipant IS NOT NULL))')
                                                            .setParameters({userId: this.userProvider.getUser().id})
                                                            .getRawOne()
                                                            .then((conversations) => {
                                                                this.logService.logDevelopment("Termina sync mensajes de conversaciones no leídas", this.userProvider.getUser());
                                                                if (conversations) {
                                                                    this.sendOfflineConversations()
                                                                        .then(() => {
                                                                            this.logService.logDevelopment("Termina sendOfflineMessages", this.userProvider.getUser());
                                                                            this.sendOfflineMessages()
                                                                                .then(() => {
                                                                                    this.logService.logDevelopment("Termina sendOfflineMessages", this.userProvider.getUser());
                                                                                    this.updateOfflineMessages()
                                                                                        .then(_ => {
                                                                                            this.logService.logDevelopment("Termina updateOfflineMessages", this.userProvider.getUser());
                                                                                            this.sendOfflineConversationsUnreads()
                                                                                                .then(_ => {
                                                                                                    this.logService.logDevelopment("Termina sendOfflineConversationsUnreads", this.userProvider.getUser());
                                                                                                    this.syncStateMessages(stateMessagesUpdates, loadApp)
                                                                                                        .then(_ => {
                                                                                                            this.logService.logDevelopment('Termina syncStateMessages', this.userProvider.getUser())
                                                                                                            if (loadApp) {
                                                                                                                this.syncParticipantStates(participantsStateUpdates).then(_ => {
                                                                                                                    this.logService.logDevelopment('Termina syncParticipantStates', this.userProvider.getUser());
                                                                                                                    this.synchronizing = false;
                                                                                                                    resolve();
                                                                                                                })
                                                                                                                    .catch((err) => {
                                                                                                                        this.logService.logError(err, this.userProvider.getUser());
                                                                                                                        this.synchronizing = false;
                                                                                                                        error(err);
                                                                                                                    });
                                                                                                            } else {
                                                                                                                this.successSync(loadApp, response.dtLastUpdate, participantsStateUpdates);
                                                                                                                this.synchronizing = false;
                                                                                                                resolve();
                                                                                                            }
                                                                                                        })
                                                                                                        .catch((err) => {
                                                                                                            this.logService.logError(err, this.userProvider.getUser());
                                                                                                            this.synchronizing = false;
                                                                                                            error(err);
                                                                                                        });
                                                                                                })
                                                                                                .catch((err) => {
                                                                                                    this.logService.logError(err, this.userProvider.getUser());
                                                                                                    this.synchronizing = false;
                                                                                                    error(err);
                                                                                                });
                                                                                        })
                                                                                        .catch((err) => {
                                                                                            this.logService.logError(err, this.userProvider.getUser());
                                                                                            this.synchronizing = false;
                                                                                            error(err);
                                                                                        });
                                                                                })
                                                                                .catch((err) => {
                                                                                    this.logService.logError(err, this.userProvider.getUser());
                                                                                    this.synchronizing = false;
                                                                                    error(err);
                                                                                });
                                                                        })
                                                                        .catch((err) => {
                                                                            this.logService.logError(err, this.userProvider.getUser());
                                                                            this.synchronizing = false;
                                                                            error(err);
                                                                        });
                                                                }
                                                                else {
                                                                    this.logService.logDevelopment("Sin contenido offLine", this.userProvider.getUser());
                                                                    this.syncStateMessages(stateMessagesUpdates, loadApp)
                                                                        .then(_ => {
                                                                            this.logService.logDevelopment('Termina syncStateMessages', this.userProvider.getUser())
                                                                            if (loadApp) {
                                                                                console.error('Demoró ' + (new Date().valueOf() - new Date(inicio2).valueOf()) + ' milisegundos.');
                                                                                inicio2 = new Date().valueOf();
                                                                                this.syncParticipantStates(participantsStateUpdates).then(_ => {
                                                                                    this.logService.logDevelopment('Termina syncParticipantStates', this.userProvider.getUser());
                                                                                    console.error('Demoró ' + (new Date().valueOf() - new Date(inicio2).valueOf()) + ' milisegundos.');
                                                                                    console.error('Toda la sync desde que llego el JSON, demoró ' + (new Date().valueOf() - new Date(inicio).valueOf()) + ' milisegundos.');
                                                                                    this.synchronizing = false;
                                                                                    resolve();
                                                                                })
                                                                                    .catch((err) => {
                                                                                        this.logService.logError(err, this.userProvider.getUser());
                                                                                        this.synchronizing = false;
                                                                                        error(err);
                                                                                    });
                                                                            } else {
                                                                                console.error('Demoró ' + (new Date().valueOf() - new Date(inicio2).valueOf()) + ' milisegundos.');
                                                                                console.error('Toda la sync desde que llego el JSON, demoró ' + (new Date().valueOf() - new Date(inicio).valueOf()) + ' milisegundos.');
                                                                                this.successSync(loadApp, response.dtLastUpdate, participantsStateUpdates);
                                                                                this.synchronizing = false;
                                                                                resolve();
                                                                            }
                                                                        })
                                                                        .catch((err) => {
                                                                            this.logService.logError(err, this.userProvider.getUser());
                                                                            this.synchronizing = false;
                                                                            error(err);
                                                                        });
                                                                }
                                                            })
                                                            .catch((err) => {
                                                                this.logService.logError(err, this.userProvider.getUser());
                                                                this.synchronizing = false;
                                                                error(err);
                                                            });
                                                    })
                                                    .catch((err) => {
                                                        this.logService.logError(err, this.userProvider.getUser());
                                                        this.synchronizing = false;
                                                        error(err);
                                                    });
                                            })
                                            .catch((err) => {
                                                this.logService.logError(err, this.userProvider.getUser());
                                                this.synchronizing = false;
                                                error(err);
                                            });
                                    })
                                    .catch((err) => {
                                        this.logService.logError(err, this.userProvider.getUser());
                                        this.synchronizing = false;
                                        error(err);
                                    });
                            }
                            else {
                                this.successSync(loadApp, dtLastUpdate, participantsStateUpdates);
                                this.synchronizing = false;
                                resolve();
                            }
                        })
                        .catch((err) => {
                            this.logService.logError(err, this.userProvider.getUser());
                            this.userProvider.inProcessSync = false;
                            this.synchronizing = false;
                            error(err);
                        });
                } else {
                    resolve();
                }
            });
        });
    }

    successSync(loadApp, dtLastUpdate, participantsStateUpdates) {
        this.closeErrorSyncToast();
        this.userProvider.inProcessSync = false;
        if (!loadApp) this.onFinishSyncSource.next();
        if (!loadApp) {
            this.ngZone.run(_ => {
                if (dtLastUpdate) {
                    this.logService.logDevelopment('Seteo nuevo dtLastUpdate successSync ' + dtLastUpdate, this.userProvider.getUser());
                    this.updateProvider.dtLastUpdate = dtLastUpdate;
                }
                if (this.tailSync > 0) {
                    this.tailSync--;
                    this.sync().catch(error => {
                        this.logService.logError(error, this.userProvider.getUser());
                        this.retrySync();
                    });
                }
                else {
                    if (this.rtmService.tailEvents.length > 0) this.rtmService.processEvent(this.rtmService.tailEvents[0]);
                    this.userProvider.inSync = false;
                    this.rtmService.onTaskChangeSource.next();
                }
                this.syncParticipantStates(participantsStateUpdates);
            });
        }
    }

    syncParticipants(elements) {
        let participantAddPromises: Promise<any>[] = [];
        for (let element of elements) {
            if (element.op == "add") {
                if (element.data.flImage)
                    element.data.flImage = (element.data.flImage.substring(0, 5) == 'data:') ? element.data.flImage : this.userProvider.getContextUrl() + '/' + element.data.flImage;
                participantAddPromises.push(this.participantService.newParticipant(element.data));
            }
        }
        return new Promise((resolve, reject) => {
            Promise.all(participantAddPromises)
                .then(() => {
                    this.logService.logDevelopment("Termino participantAddPromises syncParticipants ", this.userProvider.getUser());
                    let participantUpdPromises: Promise<any>[] = [];
                    let updateReceptors = [];
                    for (let element of elements) {
                        if (element.op == "upd") {
                            updateReceptors.push(element.data);
                            if (element.data.flImage) element.data.flImage = (element.data.flImage.substring(0, 5) == 'data:') ? element.data.flImage : this.userProvider.getContextUrl() + '/' + element.data.flImage;
                            participantUpdPromises.push(this.participantService.updateParticipant(element.data));
                        }
                    }
                    Promise.all(participantUpdPromises)
                        .then(() => {
                                this.logService.logDevelopment("Termino participantUpdPromises syncParticipants ", this.userProvider.getUser());
                                if (updateReceptors.length > 0) this.conversationService.updateReceptors()
                                resolve();
                            }
                        )
                        .catch((error) => {
                            this.logService.logError(error, this.userProvider.getUser());
                            reject(error);
                        });
                });
        });
    }

    syncParticipantStates(elements) {
        let $this = this;

        return new Promise(async function (resolve, reject){
            $this.participantService.resetStates();
            $this.conversationService.resetParticipantStates();

            await $this.dbProvider.getConnection()
                .createQueryBuilder()
                .update(Participant)
                .set({cdState: 0})
                .where("userId = :userId", {userId: $this.userProvider.getUser().id})
                .execute()
                .catch((err)=> {
                    this.logService.logError(err, this.userProvider.getUser());
                    reject(err);
                });

            let connectedUsers = [];
            let awayUsers = [];
            let busyUsers = [];

            for (let element of elements) {
                if(element.op == "upd"){
                    if (element.states) {
                        for (let participantState of element.states) {
                            if(participantState.state == 1){
                                //connected
                                connectedUsers.push(participantState.cdUser);
                            } else {
                                if(participantState.state == 2){
                                    //away
                                    awayUsers.push(participantState.cdUser);
                                } else {
                                    //Es busy 3
                                    busyUsers.push(participantState.cdUser);
                                }
                            }
                            $this.participantService.updateState(participantState.cdUser, participantState.cdState);
                            $this.conversationService.reloadStateParticipant(new Participant(participantState.cdUser, "USER", "", participantState.state));
                        }
                    }
                }
            }
            await $this.participantService.updateStateBulk(connectedUsers, 1);
            await $this.participantService.updateStateBulk(awayUsers, 2);
            await $this.participantService.updateStateBulk(busyUsers, 3);
            resolve();
        });
    }

    syncConversations(elements): Promise<any> {
        let syncs = elements.map(element => () => {
            switch (element.op) {
                case "add":
                    return this.conversationService.createConversationSync(element.data, this.notNotification);
                case "upd":
                    return this.conversationService.updateConversation(new Conversation().fromSync(element.data, this.userProvider.getUser().id));
                case "del":
                    return this.conversationService.deleteConversation(element.data);
            }
        });
        return new Promise((resolve, reject) => {
            syncs.reduce((promiseChain, currentTask) => {
                return promiseChain.then(chainResults =>
                    currentTask().then(currentResult => [...chainResults, currentResult])
                );
            }, Promise.resolve([]))
                .then(_ => resolve())
                .catch(error => {
                    this.logService.logError(error, this.userProvider.getUser());
                    reject(error);
                });
        });
    }

    updateMessage(msg, loadApp) {
        return new Promise((resolve, reject) => {
            this.dbProvider.getRepository(Message).createQueryBuilder("message")
                .leftJoinAndSelect("message.lsButtons", "button")
                .leftJoinAndSelect("message.lsInputs", "input")
                .leftJoinAndSelect("message.lsActions", "action")
                .where("message.cdConversationElement = :cdConversationElement and message.cdConversation = :cdConversation")
                .setParameters({
                    cdConversationElement: msg['cdConversationElement'],
                    cdConversation: msg['cdConversation']
                })
                .getOne()
                .then(message => {
                    this.logService.logDevelopment("Termino updateMessage ", this.userProvider.getUser());
                    this.conversationService.db.deleteMsgButtonsActionsInputs(message).then(() => {
                        this.logService.logDevelopment("Termino deleteMsgButtonsActionsInputs ", this.userProvider.getUser());
                        message.lsButtons = msg['lsButtons'];
                        message.lsInputs = msg['lsInputs'];
                        message.lsActions = msg['lsActions'];
                        message.dsContent = msg['dsContent'];
                        this.conversationService.db.insertMsgButtonsActionsInputs([message]);
                    });
                    if (!loadApp) {
                        this.conversationService.updateMessageInMemory(message);
                    }
                    resolve();
                        /*})
                        .catch(e => error(e));*/
                })
                .catch(error => {
                    this.logService.logError(error, this.userProvider.getUser());
                    reject(error);
                });
        });
    }

    deleteMessage(msg, loadApp): Promise<any> {
        return new Promise((resolve, reject) => {
            this.dbProvider.getRepository(Message).createQueryBuilder("message")
                .where("message.cdConversationElement = :cdConversationElement and message.cdConversation = :cdConversation")
                .setParameters({cdConversationElement: msg['cdMessage'], cdConversation: msg['cdConversation']})
                .getOne()
                .then(message => {
                    this.logService.logDevelopment("Termino deleteMessage ", this.userProvider.getUser());
                    if (message) {
                        if (loadApp) {
                            this.conversationService.db.deleteMessage(message)
                                .then(_ => resolve())
                                .catch(error => {
                                    this.logService.logError(error, this.userProvider.getUser());
                                    reject(error);
                                });
                        } else {
                            this.conversationService.removeLocalMessage(message)
                                .then(_ => resolve())
                                .catch(error => {
                                    this.logService.logError(error, this.userProvider.getUser());
                                    reject(error);
                                })
                        }
                    } else {
                        resolve();
                    }
                })
                .catch(error => {
                    this.logService.logError(error, this.userProvider.getUser());
                    reject(error);
                })
        });
    }

    addFirstMessageForConversation(message, notNotification, dtLastChange, newConv = false) {
        this.auxTime = new Date().valueOf();
        return new Promise((resolve, reject) => {
            console.log("Comienzo a acumular mensajes de la misma conversación busco la conversación si existe en la base y sino voy la pido a la API");
            this.conversationService.getOne(message.cdConversation, true)
                .then((conversation) => {
                    this.logService.logDevelopment("Vuelve la conversación cdConversationElement = " + message.cdConversation + ", demoró " + (new Date().valueOf() - new Date(this.auxTime).valueOf()) + ' milisegundos.', this.userProvider.getUser())
                    console.log("Vuelve la conversación cdConversationElement = " + message.cdConversation + ", demoró " + (new Date().valueOf() - new Date(this.auxTime).valueOf()) + ' milisegundos.');
                    message.conversationId = conversation.id;
                    if (message.msgState != 'SENT') notNotification = true;
                    if (!notNotification) this.notificationProvider.newMessage(conversation, message);
                    this.conversationService.removeConversation(conversation.cdConversation);
                    if (message.msgState == 'SENT' && message.cdParticipant != this.userProvider.getUsername().toUpperCase() && !newConv) conversation.nuUnreaded = conversation.nuUnreaded + 1;
                    conversation.dtLastChange = dtLastChange;
                    this.conversationService.addConversation(conversation);
                    this.lastConversationUsed = conversation;
                    this.messagesAdd.push(message);
                    resolve();
                }).catch(error => {
                    this.logService.logError(error, this.userProvider.getUser());
                    reject(error);
                });
        });
    }

    addMessage(msg) {
        return this.conversationService.existMessage(msg['cdConversationElement'], msg['cdConversation'])
            .then((messages) => {
                this.logService.logDevelopment("Termino addMessage ", this.userProvider.getUser());
                if (!messages) {
                    let msgs = this.conversationService.api.serializeMessages([msg]);
                    if (msgs.length > 0) {
                        let message: Message = msgs[0];
                        let dtLastChange = message.dtLastUpdate;
                        let notNotification = this.notNotification;
                        return new Promise((resolve, reject) => {
                            if (!this.lastConversationUsed || this.lastConversationUsed.cdConversation != msg['cdConversation']) {
                                if (this.lastConversationUsed) {
                                    this.logService.logDevelopment("Terminó de acumular mensajes de la misma conversación, demoró " + (new Date().valueOf() - new Date(this.auxTime).valueOf()) + ' milisegundos.', this.userProvider.getUser());
                                    console.error("Terminó de acumular mensajes de la misma conversación, demoró " + (new Date().valueOf() - new Date(this.auxTime).valueOf()) + ' milisegundos.');
                                    this.auxTime = new Date().valueOf();
                                    console.warn("Guardo " + this.messagesAdd.length + " mensajes");
                                    this.logService.logDevelopment("Guardo " + this.messagesAdd.length + " mensajes", this.userProvider.getUser());
                                    this.dbProvider.getRepository(Message).createQueryBuilder()
                                        .insert()
                                        .into('message')
                                        .values(this.messagesAdd)
                                        .execute()
                                        .then(() => {
                                            this.logService.logDevelopment("Termino get menssage to add ", this.userProvider.getUser());
                                            this.conversationService.db.insertMsgButtonsActionsInputs(this.messagesAdd);
                                            let msgs = this.messagesAdd;
                                            this.messagesAdd = [];
                                            if(!this.lastConversationUsed.lsMessages){
                                                this.lastConversationUsed.lsMessages = [];
                                            }
                                            this.lastConversationUsed.lsMessages = this.lastConversationUsed.lsMessages.concat(msgs);
                                            this.dbProvider.getRepository(Conversation).createQueryBuilder()
                                                .update(Conversation)
                                                .set(this.lastConversationUsed.getPlainObject())
                                                .where('id = :id', { id: this.lastConversationUsed.id })
                                                .execute()
                                                .then(_ => {
                                                        this.lastConversationUsed = null;
                                                        for (let m of msgs) {
                                                            this.conversationService.db.insertAttach([m]);
                                                        }
                                                        this.logService.logDevelopment("Demoró " + (new Date().valueOf() - new Date(this.auxTime).valueOf()) + ' milisegundos.', this.userProvider.getUser());
                                                        console.error("Demoró " + (new Date().valueOf() - new Date(this.auxTime).valueOf()) + ' milisegundos.');
                                                        this.addFirstMessageForConversation(message, notNotification, dtLastChange, msg.newConv)
                                                            .then(_ => resolve())
                                                            .catch(error => {
                                                                this.logService.logError(error, this.userProvider.getUser());
                                                                reject(error);
                                                            });
                                                    }
                                                )
                                                .catch(error => {
                                                    this.logService.logError(error, this.userProvider.getUser());
                                                    reject(error);
                                                });
                                        })
                                        .catch(error => {
                                            this.logService.logError(error, this.userProvider.getUser());
                                            reject(error);
                                        });
                                }
                                else {
                                    this.addFirstMessageForConversation(message, notNotification, dtLastChange, msg.newConv)
                                        .then(_ => resolve())
                                        .catch(error => {
                                            this.logService.logError(error, this.userProvider.getUser());
                                            reject(error);
                                        });
                                }
                            }
                            else {
                                let conversation = this.lastConversationUsed;
                                message.conversationId = conversation.id;
                                if (message.msgState != 'SENT') notNotification = true;
                                if (!notNotification) this.notificationProvider.newMessage(conversation, message);
                                if (message.msgState == 'SENT' && message.cdParticipant != this.userProvider.getUsername().toUpperCase() && !msg.newConv) conversation.nuUnreaded = conversation.nuUnreaded + 1;
                                this.messagesAdd.push(message);
                                resolve();
                            }
                        });
                    }
                    return null;
                } else {
                    return null;
                }
            })
            .catch(error => {
                this.logService.logDevelopment("error "+error.message, this.userProvider.getUser());
            });
    }

    syncMessage(element, loadApp): Promise<any> {
        switch (element.op) {
            case "upd":
                return this.updateMessage(element.data, loadApp);
            case "add":
                return this.addMessage(element.data);
            // case "del":
            //   return this.deleteMessage(element.data,loadApp);
            default:
                return null; // Shouldn't happend
        }
    }

    syncMessages(elements, loadApp): Promise<any> {
        let messagesPromises: Promise<any>[] = [];
        let messageElements = [];
        let unreadeds = new Map<number, number>();
        for (let element of elements) {
            if (element.op && element.data) {
                if (!unreadeds.has(element.data.cdConversation)) {
                    unreadeds.set(element.data.cdConversation, 0);
                }
                if (element.op == "add") {
                    if (element.data.cdParticipant != this.userProvider.getUsername().toUpperCase()) {
                        unreadeds.set(element.data.cdConversation, unreadeds.get(element.data.cdConversation) + 1);
                    }
                    else {
                        unreadeds.set(element.data.cdConversation, unreadeds.get(element.data.cdConversation));
                    }
                }
                messageElements.push(element);
            }
        }
        let syncs = messageElements.map(message => () => this.syncMessage(message, loadApp));
        return new Promise((resolve, reject) => {
            syncs.reduce((promiseChain, currentTask) => {
                return promiseChain
                    .then(chainResults =>
                        currentTask()
                            .then(currentResult =>
                                [...chainResults, currentResult]
                            )
                    );
            }, Promise.resolve([]))
                .then(() => {
                    if (this.messagesAdd.length > 0) {
                        this.logService.logDevelopment("Terminó de acumular mensajes de la misma conversación, demoró " + (new Date().valueOf() - new Date(this.auxTime).valueOf()) + ' milisegundos.', this.userProvider.getUser());
                        this.logService.logDevelopment("Guardo " + this.messagesAdd.length + " mensajes", this.userProvider.getUser());
                        console.error("Terminó de acumular mensajes de la misma conversación, demoró " + (new Date().valueOf() - new Date(this.auxTime).valueOf()) + ' milisegundos.');
                        console.warn("Guardo " + this.messagesAdd.length + " mensajes");
                        let inicio = new Date().valueOf();
                        this.dbProvider.getRepository(Message).createQueryBuilder()
                            .insert()
                            .into('message')
                            .values(this.messagesAdd)
                            .execute()
                            .then(() => {
                                this.conversationService.db.insertMsgButtonsActionsInputs(this.messagesAdd);
                                let msgs = this.messagesAdd;
                                this.messagesAdd = [];
                                if(!this.lastConversationUsed.lsMessages){
                                    this.lastConversationUsed.lsMessages = [];
                                }
                                this.lastConversationUsed.lsMessages = this.lastConversationUsed.lsMessages.concat(msgs);
                                this.dbProvider.getRepository(Conversation).createQueryBuilder()
                                    .update(Conversation)
                                    .set(this.lastConversationUsed.getPlainObject())
                                    .where('id = :id', { id: this.lastConversationUsed.id })
                                    .execute()
                                    .then(_ => {
                                            this.logService.logDevelopment("syncMessages Demoró " + (new Date().valueOf() - new Date(inicio).valueOf()) + ' milisegundos.', this.userProvider.getUser());
                                            this.lastConversationUsed = null;
                                            for (let m of msgs) {
                                                this.conversationService.db.insertAttach([m]);
                                            }
                                            console.error("Demoró " + (new Date().valueOf() - new Date(inicio).valueOf()) + ' milisegundos.');
                                            resolve();
                                        }
                                    )
                                    .catch(error => {
                                        this.logService.logError(error, this.userProvider.getUser());
                                        reject(error);
                                    });
                            })
                            .catch(error => {
                                this.logService.logError(error, this.userProvider.getUser());
                                reject(error);
                            });
                    } else {
                        resolve();
                    }
                });
        });
    }

    updateConversationsUnreaded(unreadeds) {
        let unreadedPromises: Promise<any>[] = [];
        unreadeds.forEach((value: number, key: number) => {
            unreadedPromises.push(new Promise((resolve, reject) => {
                if (this.userProvider.cdCurrentConversation != key) {
                    this.conversationService.getOne(key)
                        .then((conversation) => {
                            if (conversation) {
                                conversation.nuUnreaded += unreadeds.get(key);
                                this.conversationService.db.update(conversation)
                                    .then(() => {
                                        resolve();
                                    })
                                    .catch(error => {
                                        this.logService.logDevelopment("error "+error.message, this.userProvider.getUser());
                                    });
                            }
                            else {
                                resolve();
                            }
                        })
                        .catch(error => {
                            this.logService.logDevelopment("error "+error.message, this.userProvider.getUser());
                        });
                }
                else {
                    resolve();
                }
            }));
        });
        return Promise.all(unreadedPromises);
    }

    updateStateMessage(msg: any, loadApp: boolean) {
        return new Promise((resolve, reject) => {
            this.dbProvider.getConnection()
                .createQueryBuilder()
                .update(Message)
                .set({msgState: msg.msgState})
                .where("cdConversation = :cdConversation and cdConversationElement = :cdConversationElement", {
                    cdConversation: msg.cdConversation,
                    cdConversationElement: msg.cdMessage
                })
                .execute()
                .then(() => {
                    this.logService.logDevelopment("Termino updateStateMessage ", this.userProvider.getUser());
                    if (!loadApp) this.conversationService.updateMessageStateInMemory(msg);
                    resolve();
                })
                .catch(error => {
                    this.logService.logError(error, this.userProvider.getUser());
                    reject(error);
                });
        });
    }

    // syncStateMessages(elements) {
    syncStateMessages(elements, loadApp): Promise<any> {
        let stateMessagesPromises: Promise<any>[] = [];
        let conversationsToUnread = [];
        for (let element of elements) {
            let msgState = element.data.msgState;
            if (msgState != 'SENT') {
                if (typeof(conversationsToUnread[element.data.cdConversation]) === "undefined") {
                    conversationsToUnread[element.data.cdConversation] = {
                        cdConversation: element.data.cdConversation,
                        cant: 0
                    };
                }
                if (msgState !== 'SEEN_BY_ALL' && msgState !== 'SEEN') {
                    conversationsToUnread[element.data.cdConversation].cant++;
                }
            }
        }
        let syncs = elements.map(state => () => state.data.msgState == 'DELETED' ? this.deleteMessage(state.data, loadApp) : this.updateStateMessage(state.data, loadApp));
        return new Promise((resolve, reject) => {
            syncs.reduce((promiseChain, currentTask) => {
                return promiseChain
                    .then(chainResults =>
                        currentTask()
                            .then(currentResult =>
                                [...chainResults, currentResult]
                            )
                    );
            }, Promise.resolve([]))
                .then(() => {
                    this.logService.logDevelopment("Dentro nuUnreaded syncStateMessages", this.userProvider.getUser());
                    let promises: Promise<any>[] = [];
                    for (let convToUnread of conversationsToUnread) {
                        if (convToUnread) {
                            promises.push(
                                new Promise((resolve, reject) => {
                                    this.dbProvider.getRepository(Conversation)
                                        .createQueryBuilder("c")
                                        .select("count(c.id)", "countSent")
                                        .innerJoin(Message, "m", "m.conversationId = c.id")
                                        .where("c.userId = :userId")
                                        .andWhere("c.cdConversation = :cdConversation")
                                        .andWhere("m.msgState = 'SENT'")
                                        .andWhere("m.cdParticipant != :userName")
                                        .setParameters({
                                            userId: this.userProvider.getUser().id,
                                            userName: this.userProvider.getUsername().toUpperCase(),
                                            cdConversation: convToUnread.cdConversation
                                        })
                                        .getRawOne()
                                        .then(c => {
                                            this.conversationService.getOne(convToUnread.cdConversation).then(conv => {
                                                if (conv && conv.nuUnreaded != c.countSent) {
                                                    conv.nuUnreaded = c.countSent;
                                                    this.dbProvider.getConnection()
                                                        .createQueryBuilder()
                                                        .update(Conversation)
                                                        .set({nuUnreaded: c.countSent})
                                                        .where("id = :id", {id: conv.id})
                                                        .execute()
                                                        .then(_ => {
                                                            console.warn("actualice nuUnreaded", c.countSent);
                                                            this.logService.logDevelopment("actualice nuUnreaded ", this.userProvider.getUser());
                                                            resolve()
                                                        })
                                                        .catch(error => {
                                                            this.logService.logError(error, this.userProvider.getUser());
                                                            reject(error);
                                                        });
                                                }
                                                resolve();
                                            });
                                        })
                                        .catch(error => {
                                            this.logService.logError(error, this.userProvider.getUser());
                                            reject(error);
                                        });
                                })
                            )
                        }
                    }
                    if (promises.length > 0) {
                        Promise.all(promises)
                            .then(() => {
                                resolve();
                            })
                            .catch(error => {
                                this.logService.logError(error, this.userProvider.getUser());
                                reject(error);
                            });
                    } else {
                        resolve();
                    }
                }).catch(error => {
                    this.logService.logError(error, this.userProvider.getUser());
                    reject(error);
                });
        });
    }


    sendOfflineConversations() {
        return new Promise((resolve, reject) => {
            this.dbProvider.getRepository(Conversation).createQueryBuilder("conversation")
                .innerJoinAndSelect("conversation.lsParticipants", "lsParticipants")
                .leftJoinAndSelect("conversation.receptor", "receptor")
                .where('conversation.userId = :userId')
                .andWhere('conversation.synchronized = :synchronized')
                .orderBy("conversation.id", "ASC")
                .setParameters({userId: this.userProvider.getUser().id, synchronized: 0})
                .getMany()
                .then((conversations) => {
                    if (conversations && conversations.length > 0) {
                        let promises: Promise<any>[] = [];
                        for (let conversation of conversations) {
                            promises.push(this.conversationService.sendConversation(conversation));
                        }
                        Promise.all(promises)
                            .then(() => {
                                resolve();
                            })
                            .catch((error) => {
                                this.logService.logError(error, this.userProvider.getUser());
                                console.log("Error en promise all al crear conversaciones: ", JSON.stringify(error));
                                reject(error);
                            });
                    }
                    else {
                        resolve();
                    }
                })
                .catch((error) => {
                    this.logService.logError(error, this.userProvider.getUser());
                    console.log("ERROR al obtener conversaciones sin sincronizar: ", JSON.stringify(error));
                    reject(error);
                });
        });
    }

    sendOfflineMessage(message) {
        return new Promise((ok, reject) => {
            this.conversationService.api.sendMessage(message)
                .then((msg) => {
                    this.logService.logDevelopment("Send mensajes en offline", this.userProvider.getUser());
                    if (msg.lsButtons) {
                        message.lsButtons = msg.lsButtons;
                    }
                    if (msg.lsInputs) {
                        message.lsInputs = msg.lsInputs;
                    }
                    if (msg.lsAnnexs) {
                        for (let aResult of msg.lsAnnexs) {
                            for (let a of message.lsAnnexs) {
                                if (a.dsName = aResult.dsName) {
                                    a.cdFile = aResult.cdFile;
                                    a.imagePreview = aResult.imagePreview;
                                }
                            }
                        }
                    }
                    message.msgState = msg.msgState; 
                    message.cdConversationElement = msg.cdConversationElement;
                    this.messagesOffline.push(message);
                    this.conversationService.updateMessageInMemory(message);
                    ok();
                })
                .catch((error) => {
                    this.logService.logError(error, this.userProvider.getUser());
                    reject(error);
                });
        })
    }

    sendOfflineMessageWithAttach(message) {
        let uploadAttach: Promise<any>[] = [];
        for (let a of message.lsAnnexs) {
            uploadAttach.push(this.attachService.uploadFile(a.cdLocalPath, a.dsName)
                .then(
                    tempPath => {
                        a.cdTempPath = tempPath;
                        a.cdState = 1;
                    })
                .catch((error) => {
                    this.logService.logError(error, this.userProvider.getUser());
                    console.error('Error en sendOfflineMessageWithAttach', a.dsName);
                }));
        }
        return new Promise((ok, fail) => {
            Promise.all(uploadAttach)
                .then(() => {
                    let stateUpload = message.lsAnnexs.find(a => a.cdState != 1);
                    if (!stateUpload) {
                        this.sendOfflineMessage(message)
                            .then(_ => {
                                this.logService.logDevelopment("Dentro sendOfflineMessage", this.userProvider.getUser());
                                ok()
                            });
                    }
                    else {
                        ok();
                    }
                }).catch(error => {
                    this.logService.logError(error, this.userProvider.getUser());
                    console.error('Error en promise ALLsendOfflineMessageWithAttach');
                });
        });
    }

    updateOfflineMessages(): Promise<any> {
        return new Promise((resolve, reject) => {
            //resolve();
            if (!this.messagesOffline || this.messagesOffline.length === 0) {
                resolve();
            }
            this.messagesOffline.map((msg: Message) => {
                this.dbProvider.getRepository(Message).createQueryBuilder()
                    .update(Message)
                    .set(msg.getPlainObject())
                    //.where('id = (:...messagesOffline.id)', { messagesOffline: this.messagesOffline })
                    .where('id = :id', { id: msg.id })
                    .execute()
                    .then(() => {
                        this.conversationService.db.insertMsgButtonsActionsInputs(this.messagesOffline);
                        this.conversationService.db.insertAttach(this.messagesOffline)
                            .then(() => {
                                resolve();
                            })
                            .catch((error) => {
                                this.logService.logError(error, this.userProvider.getUser());
                                reject(error);
                            });
                    })
                    .catch((error) => {
                        this.logService.logError(error, this.userProvider.getUser());
                        reject(error);
                    });
            });
        });
    }

    sendOfflineMessages(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.dbProvider.getRepository(Message).createQueryBuilder('message')
                .leftJoinAndSelect("message.lsButtons", "button")
                .leftJoinAndSelect("message.lsInputs", "input")
                .leftJoinAndSelect("message.lsAnnexs", "annex")
                .where('message.cdConversationElement IS NULL')
                .andWhere('message.cdConversation IS NOT NULL AND message.cdParticipant IS NOT NULL')
                .orderBy({"message.conversationId": "ASC", "message.id": "ASC"})
                .getMany().then((messages) => {
                let syncs = messages.map(message => () => {
                            if (message.lsAnnexs.length > 0) {
                                return this.sendOfflineMessageWithAttach(message);
                            }
                            else {
                                return this.sendOfflineMessage(message);
                            }
                        }
                );
                syncs.reduce((promiseChain, currentTask) => {
                    return promiseChain.then(chainResults =>
                        currentTask().then(currentResult =>
                            [...chainResults, currentResult]
                        )
                    );
                }, Promise.resolve([]))
                    .then(() => {
                        resolve();
                    })
                    .catch((error) => {
                        this.logService.logError(error, this.userProvider.getUser());
                        reject(error);
                    });
            });
        });
    }

    sendOfflineConversationsUnreads() {
        return new Promise((resolve, reject) => {
            let conversationUnreadRepository = this.dbProvider.getRepository(ConversationUnread) as Repository<ConversationUnread>;
            conversationUnreadRepository
                .createQueryBuilder("conversation_unread")
                .select("conversation_unread.cdConversation", "cdConversation")
                .addSelect("conversation_unread.dtLastMsg", "dtLastMsg")
                .where("conversation_unread.userId = :userId")
                .andWhere("conversation_unread.synchronized = :synchronized")
                .setParameters({userId: this.userProvider.getUser().id, synchronized: 0})
                .getRawMany()
                .then((conversationUnreads) => {
                    if (conversationUnreads && conversationUnreads.length > 0) {
                        let promises: Promise<any>[] = [];
                        for (let conversationUnread of conversationUnreads) {
                            promises.push(
                                new Promise((ok, er) => {
                                    this.conversationService.updateConversationUnreadedsPatch(conversationUnread.cdConversation, conversationUnread.dtLastMsg)
                                        .then(_ => {
                                            this.dbProvider.getConnection()
                                                .createQueryBuilder()
                                                .update(ConversationUnread)
                                                .set({synchronized: true})
                                                .where("userId = :id", {id: this.userProvider.getUser().id})
                                                .andWhere("cdConversation = :cdConversation", {cdConversation: conversationUnread.cdConversation})
                                                .execute();
                                            ok();
                                        })
                                        .catch((error) => {
                                            this.logService.logError(error, this.userProvider.getUser());
                                            ok();
                                        });
                                })
                            );
                        }
                        Promise.all(promises)
                            .then(() => {
                                resolve();
                            })
                            .catch((error) => {
                                this.logService.logError(error, this.userProvider.getUser());
                                reject(error);
                            });
                    }
                    else {
                        resolve();
                    }
                })
                .catch((error) => {
                    this.logService.logError(error, this.userProvider.getUser());
                    reject(error);
                });
        });
    }

    errorSyncToast() {
        this.logService.logDevelopment("Dentro errorSyncToast", this.userProvider.getUser());
        if (this.notificationProvider.platformState == "active") {
            this.toastCtrl.create({
                message: this.translateService.instant('TRYING_TO_RECONNECT'),
                position: 'bottom',
                buttons: [
                    {
                        text: 'Ok',
                        role: 'cancel',
                        handler: () => {
                            //console.log('Cancel clicked');
                        }
                    }
                ]
            }).then(toast => {
                this.toast1 = toast;
                this.toast1.present();
            });
        }
    }

    closeErrorSyncToast() {
        if (this.notificationProvider.platformState == "active" && this.userProvider.getUser().loggedIn && this.toast1) {
            this.toast1.dismiss();
            this.toast1 = null;
            this.toastCtrl.create({
                message: this.translateService.instant('RECONNECT_OK'),
                position: 'bottom',
                duration: 2000,
            }).then(toast => {
                this.toast2 = toast;
                this.toast2.present();
            });
        } else {
            if (this.toast1) {
                this.toast1.dismiss();
                this.toast1 = null;
            }
        }
    }


}
