import {Injectable} from '@angular/core';
import {Http, Headers, RequestOptions} from '@angular/http';
import {Attach} from '../model/attach.entity';
import { LogService } from './log.service';
import { UserProvider } from './user.service';

// import 'rxjs/Rx';

@Injectable()
export class DownloadAttachService {

    private servletUrl: string = "";
    private user: string = "";
    private token: string = "";
    private filePath: string = "";
    private fileId: string = "";

    constructor(private http: Http, private userProvider: UserProvider, private logService: LogService) {
    }

    init(pUser: string, jwt: string, port: string): void {
        this.user = pUser;
        this.token = jwt;
        this.servletUrl = "SLDownloadMessageAttach?nuSecHidden=" + document.getElementById("nuSecHidden").getAttribute("value") + "&currentUser=" + this.user;
    }

    downloadAttach(pFile: Attach) {
        let fd = new FormData();
        // fd.append('downloadFile', new Blob([pFile]));
        this.fileId = pFile.cdFile;
        this.filePath = pFile.cdTempPath;
        return this.http.post(this.servletUrl + "&fileId=" + this.fileId, fd, this.getRequestOptions())
            .toPromise()
            .then(response => {
                let a = document.createElement("a");
                document.body.appendChild(a);
                a.href = response.json().tempPath as string;
                a.download = response.json().fileName as string;
                a.click();
                a.remove();
            })
            .catch(error => {
                this.logService.logError(error, this.userProvider.getUser());
                this.handleError;
            });
    }

    private getRequestOptions(): RequestOptions {
        let params = new URLSearchParams();
        params.append('currentUser', this.user);
        let headers = new Headers();
        return new RequestOptions({headers: headers});
    }

    private handleError(error: any): void {
        console.error('An error occurred', JSON.stringify(error));
    }
}
