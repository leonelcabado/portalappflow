import {Injectable} from '@angular/core';
import {HttpClient, HttpBackend} from '@angular/common/http';
import {UserProvider} from './user.service';
import {AuthProvider} from './auth.service';
import {Action} from '../model/action.entity';
import {RuleService} from './rule.service';
import { LogService } from './log.service';
 
@Injectable()
export class ActionService {

    private actionsObject = null;

    constructor(private userProvider: UserProvider, private http: HttpClient, protected httpBackend: HttpBackend, private auth: AuthProvider, protected ruleService: RuleService, private logService: LogService) {
    }

    /**
     * Get the actions by executing the rule GetIniciar.
     */
    public getActions(): Promise<any> {
        if(this.userProvider.getUser()) {
            return this.ruleService.executeRuleGetIniciar()
                .then((response: any) => {
                    this.actionsObject = {
                        mostUsed: this.processActions(response ? response.pUsados : []),
                        rest: this.processActions(response ? response.pResto : [])
                    };
                    return this.actionsObject;
                })
                .catch(error => {
                    this.logService.logError(error, this.userProvider.getUser());
                    return undefined;
                }) 
        }
        return Promise.resolve(undefined); 
    }

    /**
     * Process the actions list.
     * @param  {Array<string>} actionsList
     * @return {Array<Action>}
     */
    private processActions(actionsList: Array<string>): Array<Action> {
        if (actionsList && actionsList.length > 0) {
            return actionsList.map((action: string) => {
                const splittedAction = action.split('@|@');
                if (splittedAction && splittedAction.length > 0) {
                    if (splittedAction[1] === 'formulario') {
                        return this.createFormAction(splittedAction[0], splittedAction[1], splittedAction[2], null);
                    }
                    return this.createProcessAction(splittedAction[0], splittedAction[1], splittedAction[2], splittedAction[3]);
                }
            });
        }
        return new Array();
    }

    /**
     * Creates the form action to show in view.
     * @param  {string} name
     * @param  {string} type
     * @param  {string} code
     * @param  {string} icon
     * @return {Action}
     */
    private createFormAction(name: string, type: string, code: string, icon: string): Action {
        let formAction = new Action();
        formAction.setName(name);
        formAction.setType(type);
        formAction.setCode(code);
        formAction.setIcon(icon);
        formAction.setUrl('SLGenericDocumentCreate&cdDocumentNameHidden=' + formAction.getCode() + '&cdActionHidden=Add&APP=&mobile=true&isEmbed=true');
        return formAction;
    }

    /**
     * Creates the process action to show in view.
     * @param  {string} name
     * @param  {string} type
     * @param  {string} code
     * @param  {string} version
     * @return {Action}
     */
    private createProcessAction(name: string, type: string, code: string, version: string): Action {
        let processAction = new Action();
        processAction.setName(name);
        processAction.setType(type);
        processAction.setCode(code + '@|@' + version);
        processAction.setUrl('SLProcessDispatcher&cdAsunto=' + code + '&cdProcess=' + processAction.getCode() + '&button=new&APP=&mobile=true&isEmbed=true');
        return processAction;
    }

    /**
     * Process the actions list coming from the plus button.
     * @param  {Array<any>}    actionsList
     * @return {Array<Action>}
     */
    public processActionsFromPlusButton(actionsList: Array<any>): Array<Action> {
        if (actionsList && actionsList.length > 0) {
            return actionsList.map((action: any) => {
                if (action.type === 'form') {
                    return this.createFormAction(action.name, action.type, action.code, action.icon);
                }
                return this.createProcessAction(action.name, action.type, action.code, action.version);
            });
        }
        return new Array();
    }
    
    public getActionsList(): any{
        return this.actionsObject;
    }

    public setActionsList(actionsObject?: any): void {
        this.actionsObject = actionsObject;
    }
}
