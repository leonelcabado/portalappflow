import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { Repository } from 'typeorm';
import { FirebaseX } from '@ionic-native/firebase-x/ngx';
import { AuthProvider } from './auth.service';
import { UpdateProvider } from './update.service';
import { DbProvider } from './db.service';
import { User } from '../model/user.entity';
import { UserUpdate } from '../model/user-update.entity';
import { Context } from '../model/context.entity';
import { APP } from '../app.config';
import { LogService } from './log.service';
import { AppConfigurationService } from './app-configuration.service';

@Injectable()
export class UserProvider {

    user: User;
    username: string;
    contextUrl: string;
    cdCurrentConversation: number;
    dsUser: string = "";
    usedEmojis = null;
    socketId: string = null;
    nuTasks: number = 0;
    inSync: boolean = false;
    inProcessSync: boolean = false;
    tokenFCM: string = "";
    taskConsult = null;
    inLoadTasks = false;

    /* onNotificationOpen params */
    public redirectAction = null;
    public redirectTargetId = null;
    public redirectDsCase = null;
    public redirecDsActivity: string;

    OPEN_CONVERSATION_ACTION = "openConversation";
    OPEN_TASK_ACTION = "openTask";

    constructor(private http: HttpClient,
                private authProvider: AuthProvider,
                private dbProvider: DbProvider,
                private updateProvider: UpdateProvider,
                private firebase: FirebaseX,
                private appConfigurationService: AppConfigurationService,
                private logService: LogService) {
    }

    getTokenFCM() {
        return new Promise((resolve, reject) => {
            this.firebase.getToken()
                .then((token: string) => {
                    this.tokenFCM = token;
                    resolve();
                })
                .catch(error => {
                    this.logService.logError(error, this.getUser());
                    resolve();
                });
        });
    }

    init(user: User) {
        this.setUser(user);
        this.initUsedEmojis();
        this.firebase.onMessageReceived().subscribe((data) => {
            if (data.tap && data['action'] && data['targetId']) {
                this.redirectAction = data['action'];
                this.redirectTargetId = data['targetId'];
                this.redirectDsCase = data['dsCase'];
                this.redirecDsActivity = data['title'];
            }
        });
        this.firebase.onTokenRefresh()
            .subscribe(
                (token: string) => {
                    if (token != this.tokenFCM && token != null) {
                        this.http.patch(this.apiEndpoint() + "/auth/token", {
                                cdUser: this.getUsername(),
                                tokenFCM: this.tokenFCM,
                                newTokenFCM: token,
                                appUUID: this.appConfigurationService.getAppUUID()
                            }
                            , {headers: this.getLoginHeaders(), observe: "response", responseType: 'text'})
                            .subscribe(
                                (response) => this.tokenFCM = token
                                , (err) => console.log("error en patch"));
                    }
                }
            );
    }

    login(username: string, password: string, context: Context) {
        return new Promise((resolve, reject) => {
            username = username.toLowerCase();
            let bodyJson = {cdUser: username, cdPassword: password, tokenFCM: this.tokenFCM, appUUID: this.appConfigurationService.getAppUUID()};
            this.http.post(context.url + APP.API_BASE_NAMESPACE + "/auth/token", bodyJson, {
                observe: "response",
                responseType: 'text',
                headers: this.getLoginHeaders()
            })
                .subscribe(
                    (response) => {
                        let token = response.headers.get('jwt');
                        let refreshToken = (JSON.parse(response.body)).refresh_token;
                        this.processLoggedUser(username, password, context, token, refreshToken).then((userFirstTime: boolean) => {
                            resolve(userFirstTime);
                        }),
                        (error) => {
                            reject(error);
                        }
                    },
                    (err) => {
                        reject(err);
                    }
                );
        });
    }

    saveUser(userRepository, user, userFirstTime){
        return new Promise((resolve, reject) => {
            user.setUsername(user.getUsername().toLowerCase());
            if (userFirstTime){
                userRepository.createQueryBuilder()
                    .insert()
                    .into('user')
                    .values(user)
                    .execute()
                    .then(() => resolve(user));
            }
            else {
                userRepository.createQueryBuilder()
                    .update(User)
                    .set(user.getPlainObject())
                    .where('id = :id', { id: user.id })
                    .execute()
                    .then(() => resolve(user));
            }
        });
    }

    getLoginHeaders(): HttpHeaders {
        let loginHeaders = new HttpHeaders();
        return loginHeaders.append('Content-Type', 'application/json');
    }

    signOut() {
        if(this.tokenFCM != null){
            return new Promise((resolve, error) => {
                this.http.request("delete", this.apiEndpoint() + "/auth/token", {
                    headers: this.getLoginHeaders(),
                    body: {cdUser: this.getUsername(), tokenFCM: this.tokenFCM, appUUID: this.appConfigurationService.getAppUUID()},
                    responseType: 'text'
                }).toPromise()
                    .then((response) => {
                            this.continueToSignOut(resolve);
                        }
                        , (err) => {
                            console.log('salio por el catch', err);
                            console.error(err);
                            error(err);
                        });
            });
        } else {
            return new Promise((resolve, error) => {
                this.continueToSignOut(resolve);
            });
        }
    }

    private continueToSignOut(resolveFunction){
        this.getUser().loggedIn = false;
        this.saveUpdate();
        this.saveUser(this.dbProvider.getRepository(User), this.getUser(), false).then(_ => resolveFunction ? resolveFunction() : null);
    }

    getContext(): Context {
        if (this.getUser()) {
            return this.getUser().context;
        }
    }

    getContextUrl() {
        return this.getContext().url;
    }

    /**
     * Get the context name where the user logged in.
     * @return {string}
     */
    public getContextName(): string {
        if (this.getContext()) {
            return this.getContext().getName();
        }
    }

    getUsername() {
        return this.getUser().username;
    }

    apiEndpoint() {
        return this.getContext().apiEndpoint();
    }

    wssEndpoint(protocol: string = "wss") {
        return this.getContext().wssEndpoint(protocol);
    }

    setUser(user: User): UserProvider {
        this.user = user;
        return this;
    }

    setUserSocketId(id) {
        this.socketId = id;
    }

    getUser(): User {
        return this.user;
    }

    getUserSocketId() {
        return this.socketId;
    }

    setNuTasks(nuTasks) {
        this.nuTasks = nuTasks;
    }

    getNuTasks() {
        return this.nuTasks;
    }

    sync() {
        let userRepository = this.dbProvider.getRepository(User) as Repository<User>;
        this.user.createdAt = new Date().getTime();
        this.getUser().setUsername(this.getUser().getUsername().toLowerCase());
        return userRepository.createQueryBuilder()
            .update(User)
            .set(this.getUser().getPlainObject())
            .where("id = :id", {id: this.getUser().id})
            .execute();
    }

    initUpdate() {
        let userUpdate = new UserUpdate();
        userUpdate.userId = this.getUser().id;
        userUpdate.dtLastUpdate = this.updateProvider.dtLastUpdate ? this.updateProvider.dtLastUpdate : 0;
        let userUpdateRepository = this.dbProvider.getRepository(UserUpdate) as Repository<UserUpdate>;
        return userUpdateRepository.createQueryBuilder()
            .insert()
            .into('user_update')
            .values(userUpdate)
            .execute();
    }

    loadUpdate() {
        let userUpdateRepository = this.dbProvider.getRepository(UserUpdate) as Repository<UserUpdate>;
        return userUpdateRepository.findOne({where: {userId: this.getUser().id}})
            .then((userUpdate) => {
                if (userUpdate) {
                    this.updateProvider.dtLastUpdate = userUpdate.dtLastUpdate;
                }
                return userUpdate;
            });
    }

    saveUpdate() {
        if (this.user && this.user.id && this.updateProvider.dtLastUpdate && !this.inSync) {
            this.dbProvider.getConnection()
                .createQueryBuilder()
                .update(UserUpdate)
                .set({dtLastUpdate: this.updateProvider.dtLastUpdate})
                .where("userId = :id", {id: this.getUser().id})
                .execute()
                .then(_ => this.logService.logDevelopment("Se actualiza dtLastUpdate a " + this.updateProvider.dtLastUpdate, this.getUser()))
                .catch(error => this.logService.logError(error, this.getUser()));
        }
    }

    saveUsedEmojis() {
        if (this.user && this.user.id && this.usedEmojis && (this.usedEmojis.length > 0)) {
            this.dbProvider.getConnection()
                .createQueryBuilder()
                .update(User)
                .set({lastUsedEmojis: JSON.stringify(this.usedEmojis)})
                .where("id = :id", {id: this.getUser().id})
                .execute();
        }
    }

    initUsedEmojis() {
        this.usedEmojis = [];
        if (this.getUser().lastUsedEmojis) {
            this.usedEmojis = JSON.parse(this.getUser().lastUsedEmojis);
        }
    }

    getUsedEmojis() {
        if (!this.usedEmojis) {
            this.initUsedEmojis();
        }
        return this.usedEmojis;
    }

    emojiUsed(emoji) {
        let i = 0;
        let toRemove = -1;
        for (let usedEmoji of this.usedEmojis) {
            if (emoji[1] == usedEmoji[1]) {
                toRemove = i;
            }
            i++;
        }
        if (toRemove > -1) {
            this.usedEmojis.splice(toRemove, 1);
        }
        if (this.usedEmojis.length == APP.MAX_USED_EMOJIS) {
            this.usedEmojis.pop();
        }
        this.usedEmojis.unshift(emoji);
    }

    /**
     * Returns the user translations list.
     * @return {Array<any>}
     */
    public getTranslations(): Array<any> {
        if (this.getUser()) {
            let userTranslations = this.getUser().getTranslations();
            if (userTranslations) {
                return JSON.parse(userTranslations);
            }
        }
        return [];
    }

    /**
     * [resetUserTranslations description]
     */
    public resetUserTranslations(): void {
        if (this.getUser() && this.getUser().getTranslations()) {
            this.getUser().setTranslations(undefined);
        }
    }

    /**
     * The token is requested again.
     * @return {Observable<any>}
     */
    public getNewToken(): Observable<any> {
        return from(this.login(this.getUser().username, this.getUser().password, this.getContext()));
    }

    /**
     * It gets the user refresh token.
     * @return {string}
     */
    public getRefreshToken(): string {
        const user = this.getUser();
        if (user) {
            return user.getRefreshToken();
        }
        return null;
    }

    /**
     * It sets the consulted task.
     * @param {any} consultedTask
     */
    public setConsultedTask(consultedTask: any): void {
        this.taskConsult = consultedTask;
    }

    /**
     * It returns the consulted task.
     * @return {any}
     */
    public getConsultedTask(): any {
        return this.taskConsult;
    }

    /**
     * Validate if there are permissions to receive push notifications.
     * @returns 
     */
    public validatePermissionsForPushNotification(): any {
        return new Promise((resolve, reject) => {
            this.firebase.hasPermission()
                .then(data => {
                    if(!data){
                        this.firebase.grantPermission()
                            .then(success => resolve(true))
                            .catch(error => {
                                this.logService.logDevelopment(error, this.getUser());
                                resolve(false);
                            });
                    }
                    resolve(true);
                })
                .catch(error => {
                    this.logService.logDevelopment(error, this.getUser());
                    resolve(false);
                });
        });
    }

    /**
     * Returns the user security functions.
     * @return {String[]}
     */
     public getSecurityFunctions(): String[] {
        if (this.getUser()) {
            let userSecurityFunctions = this.getUser().getLsSecurityFunctions();
            if (userSecurityFunctions) {
                return userSecurityFunctions;
            }
        }
        return [];
    }

    /**
     * Updates the user's list of security features.
     * @param user 
     * @returns 
     */
    public updateSecurityFunctionsUser(user: User){
        return new Promise((resolve, reject) => {
            this.dbProvider.getRepository(User)
                    .createQueryBuilder()
                    .update(User)
                    .set(user.getPlainObject())
                    .where('id = :id', { id: user.id })
                    .execute()
                    .then((success) => resolve(true))
                    .catch((error) => {
                        this.logService.logDevelopment(error, this.getUser());
                        resolve(false)
                    });
          });
    }

    /**
     * Process the logged user.
     * @param {string} username
     * @param {string} password
     * @param {Context} context
     * @param {string} token
     * @param {string} refreshToken
     * @returns {Promise<boolean>}
     */
    private processLoggedUser(username: string, password: string, context: Context, token: string, refreshToken: string): Promise<boolean> {
        let userRepository = this.dbProvider.getRepository(User) as Repository<User>;
        return this.findUserInRepository(username, context).then((users) => {
            let user = this.processFoundsUserInRepository(users, context, username, password, token, refreshToken);
            let userFirstTime = user.getCreatedAt() === 0;
            return this.saveUser(userRepository, user, userFirstTime).then((result) => {
                this.authProvider.init(user.getToken());
                this.init(user);
                this.logService.logDevelopment("tokenFCM -> " + this.tokenFCM, this.getUser());
                return userFirstTime;
            },
            (error) => {
                return Promise.reject(userFirstTime);
            }
            );
        });
    }

    /**
     * Looks up the user and  thecontext in the user repository.
     * @param {string} username
     * @param {Context} context
     * @returns {Promise<User[]>}
     */
    private findUserInRepository(username: string, context: Context): Promise<User[]> {
        let userRepository = this.dbProvider.getRepository(User) as Repository<User>;
        username = username.toLowerCase();
        return userRepository.find({
            where: {username: username, context: context.getId()},
            join: {
                alias: 'user',
                innerJoinAndSelect: {'context': 'user.context'}
            }
        });
    }

    /**
     * Process the founds user in repository.
     * @param {User[]} foundUsers
     * @param {Context} context
     * @param {string} username
     * @param {string} password
     * @param {string} token
     * @param {string} refreshToken
     * @returns {User}
     */
    private processFoundsUserInRepository(foundUsers: User[], context: Context, username: string, password: string, token: string, refreshToken: string): User {
        let user = this.getUserInstance(foundUsers, context, username, password);
        user.setLoggedIn(true);
        user.setToken(token);
        user.setPassword(password);
        user.setRefreshToken(refreshToken);
        return user;
    }

    /**
     * Creates the user instance.
     * @param {User[]} foundUsers
     * @param {Context} context
     * @param {string} username
     * @param {string} password
     * @returns {User}
     */
    private getUserInstance(foundUsers: User[], context: Context, username: string, password: string): User {
        if (foundUsers.length > 0 && foundUsers['0']['createdAt'] > 0) {
            return foundUsers[0];
        }
        let user = new User();
        user.setContext(context);
        user.setUsername(username);
        user.setPassword(password);
        user.setCreatedAt(0);
        return user;
    }

    /**
     * Execute the login from event.
     * @param {string} username
     * @param {string} token
     * @param {string} refreshToken
     * @param {Context} context
     * @returns {Promise<boolean>}
     */
    public loginFromEvent(username: string, password: string, token: string, refreshToken: string, context: Context): Promise<boolean> {
        return this.processLoggedUser(username.toLowerCase(), password, context, token, refreshToken);
    }

    /**
    * Updates the new token to the logged user.
    * @param {string} username
    * @param {string} password
    * @param {Context} context
    * @param {string} token
    * @param {string} refreshToken
    * @returns {Promise<boolean>}
    */
    public refreshTokenInLoggedUser(username: string, password: string, context: Context, token: string, refreshToken: string): Promise<boolean> {
        let userRepository = this.dbProvider.getRepository(User) as Repository<User>;
        return this.findUserInRepository(username, context).then((users) => {
            let user = this.processFoundsUserInRepository(users, context, username, password, token, refreshToken);
            let userFirstTime = user.getCreatedAt() === 0;
            return this.saveUser(userRepository, user, userFirstTime).then((result) => {
                return true;
            },
            (error) => {
                return Promise.reject(false);
            }
            );
        });
    }

}
