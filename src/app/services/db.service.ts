import {Injectable} from '@angular/core';


@Injectable()
export class DbProvider {

    private connection: any = null;

    constructor() {
    }

    setConnection(con) {
        this.connection = con;
        return this;
    }

    getConnection() {
        return this.connection;
    }

    getRepository(entity) {
        return this.connection.getRepository(entity);
    }

    getEntityManager() {
        return this.connection.entityManager;
    }
}
