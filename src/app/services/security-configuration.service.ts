import { HttpBackend, HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppConfigurationService } from './app-configuration.service';
import { LogService } from './log.service';
import { UserProvider } from './user.service';

@Injectable()
export class SecurityConfigurationService {

  constructor(private appConfiguration: AppConfigurationService, 
    private userProvider: UserProvider, 
    private logService: LogService,
    private httpBackend: HttpBackend) { }

  /**
   * Sets the new security list and updates the user.
   * @param lsSecurityFunctions 
   */
  private updateSecurityFunctions(lsSecurityFunctions: Array<string>): void {
    let user = this.userProvider.getUser();
    user.setLsSecurityFunctions(lsSecurityFunctions);
    this.userProvider.updateSecurityFunctionsUser(user);
    console.log("SECURITY FUNCTIONS LOADED!");
  }

  /**
   * Gets the list of functions from the server and updates the user.
   */
  public async getSecurityFunctions() {
    console.log("getSecurityFunctions called!");
    let user = this.userProvider.getUser();
    const httpClient = new HttpClient(this.httpBackend);
    const httpPutOptions = {
      headers: new HttpHeaders({
        'Authorization': `Bearer ${user.token}`,
        'Content-Type': 'application/json',
      })
    };
    let lsSecurityFunctions = this.appConfiguration.getSecurityFunctionsTabs();
    let appUUID = this.appConfiguration.getAppUUID();
    if (lsSecurityFunctions && appUUID) {

      try{
          let response = await httpClient.post(user.getContext().getUrl() + "/v1.0/apps/security-functions", { data: lsSecurityFunctions, appUUID: appUUID }, httpPutOptions)
              .toPromise();
          if (response) {
              this.updateSecurityFunctions(<Array<string>>response);
          }
      }catch(error){
          this.logService.logDevelopment(error, user);
      }
      return;
    }
  }

  /**
   * Validates if the user has the security function received by parameter.
   * @param cdFunction 
   * @returns 
   */
  public validateSecurity(cdFunction: string): boolean {
    return this.userProvider.getSecurityFunctions().includes(cdFunction);
  }
}
