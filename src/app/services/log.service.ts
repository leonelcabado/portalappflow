import { Injectable } from '@angular/core';
import { File } from '@ionic-native/file/ngx';
import { APP } from '../app.config';
import { DbProvider } from './db.service';
import { LogEntry } from '../model/logEntry.entity';
import { User } from '../model/user.entity';
import { Platform } from '@ionic/angular';
import { HttpBackend, HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AppConfigurationService } from './app-configuration.service';
import { Device } from '@awesome-cordova-plugins/device/ngx';

const LOG_TYPE = {
    0: "CRITIC",
    1: "ERROR",
    2: "SQL_TIME",
    3: "DEVELOPMENT",
};

@Injectable()
export class LogService {

    public dtLastLogSent: number;

    constructor(private file: File,
        private dbProvider: DbProvider,
        private platform: Platform,
        private device: Device,
        private httpBackend: HttpBackend,
        private appConfigurationService: AppConfigurationService) { }

    /**
     * Get the base path by platform
     */
     public getPlatformBasePath(): string{
        let platformBasePath = this.file.externalRootDirectory;
        if(this.platform.is("ios")){
            platformBasePath = this.file.dataDirectory;
        }
        return platformBasePath;
    }

    /**
     * Checks the log directorty and creates one new log file.
     * @param user
     */
    public createLogFile(user: User): void {
        const platformBasePath = this.getPlatformBasePath();
        this.file.checkDir(platformBasePath + 'DeyelChat/', 'log')
            .then(_ => {
                this.writeLog(user);
                //this.cleanDB(user.context.id);
            })
            .catch(_ => this.file.createDir(platformBasePath + 'DeyelChat/', 'log', false)
                .then(_ => {
                    this.writeLog(user);
                    //this.cleanDB(user.context.id);
                })
            );
    }

    /**
     * Creates one new log file in the log directory.
     * @param user
     */
    private writeLog(user: User): void {
        let filename = new Date().getTime();
        this.file.createFile(this.getPlatformBasePath() + 'DeyelChat/log/', filename.toString() + '.txt', false)
            .then((newFile) => {
                this.dbProvider.getRepository(LogEntry)
                    .createQueryBuilder("logEntry")
                    .where('logEntry.userId = :userId')
                    .orderBy("logEntry.createdAt", "ASC")
                    .setParameters({userId: user.id})
                    .getMany().then(
                    (logs) => {
                        let allLogs = "";
                        for (let log of logs) {
                            let dateLog = new Date(log.createdAt);
                            let typeLog = LOG_TYPE[log.type];
                            allLogs += dateLog.toLocaleString() + ' | ' + typeLog + ' | ' + log.message + '\n';
                        }
                        newFile.createWriter(function (fileWriter) {
                            fileWriter.write(allLogs);
                        });
                    });
            });
    }

    /**
     * Insert log in database
     * @param message
     * @param typeLog
     * @param user
     * @param duration
     */
    private log(message: string, typeLog: number, user: User, duration?: number): Promise<any> {
        let logEntry = new LogEntry();
        logEntry.type = typeLog;
        logEntry.environmentId = user.context.id ? user.context.id : null;
        logEntry.userId = user.id ? user.id : null;
        logEntry.duration = duration ? duration : null;
        logEntry.createdAt = new Date().getTime();
        logEntry.message = message;
        logEntry.appVersion = this.appConfigurationService.getAppVersion();
        logEntry.device = this.device.model + " - " + this.device.version;
        logEntry.platform = this.device.platform;
        return this.appConfigurationService.getAppBuildVersion()
            .then((deyelVersion: string) => {
                logEntry.deyelVersion = deyelVersion;
                return this.insertLogEntry(logEntry);
            })
            .catch((error) => {
                this.logDevelopment(error,user);
                logEntry.deyelVersion = "-";
                return this.insertLogEntry(logEntry);
            });
    }

    /**
     * Insert log into database.
     * @param logEntry 
     * @returns 
     */
    private insertLogEntry(logEntry: LogEntry): Promise<any> {
        return this.dbProvider.getConnection().createQueryBuilder()
            .insert()
            .into(LogEntry)
            .values(logEntry)
            .execute();
    }

    private mustLogError(pMsg: string): boolean {
        if(pMsg != null){
            if(pMsg.indexOf("Cannot activate an already activated outlet") > 0){
                return false;
            }
            if(pMsg == ''){
                return false;
            }
        } else{
            return false;
        }
        return true;
    }

    /**
     * It gets the error message from object.
     * @param  {any}    errorMsgObject
     * @return {string}
     */
    private getErrorMsg(errorMsgObject: any): string {
        if (errorMsgObject instanceof Error) {
            return errorMsgObject.stack;
        }
        if (errorMsgObject instanceof HttpErrorResponse) {
            return errorMsgObject.message;
        }
        if (errorMsgObject instanceof Observable) {
            return '';
        }
        return JSON.stringify(errorMsgObject);
    }

    /**
     * Detect critical log
     * @param msgError
     * @param user
     */
     public logCritic(msgError: any, user: User): Promise<any> {
        console.error(msgError);
        let msg = this.getErrorMsg(msgError);
        if(user && msgError != null && this.mustLogError(msg)) return this.log(msg, 0, user);
    }

    /**
     * Detect error log
     * @param msgError
     * @param user
     */
    public logError(msgError: any, user: User) {
        console.error(msgError);
        let msg = this.getErrorMsg(msgError);
        if(user && msgError != null && this.mustLogError(msg)) return this.log(msg, 1, user);
    }

    /**
     * Detect development log
     * @param msgError
     * @param user
     */
    public logDevelopment(msgError: any, user: User) {
        if(user && msgError != null) return this.log(msgError, 3, user);
    }

    /**
     * Detect sql_time log
     * @param msgError
     * @param user
     * @param duration
     */
     public logSqlTime(msgError: any, user: User, duration: number) {
        if(user && msgError != null) return this.log(msgError, 2, user, duration);
    }

    /**
     * Send logs to Deyel endpoint and clean database
     * @param user
     * @param logsError
     */
    private async sendLogs(user: User) {
        let entries = [];
        let logs = await this.dbProvider.getRepository(LogEntry)
            .createQueryBuilder("logEntry")
            .where("logEntry.environmentId = :environmentLogs")
            .orderBy("logEntry.createdAt", "ASC")
            .setParameters({ environmentLogs: user.context.id })
            .getMany();
        if(logs.length > 0){
            for (let log of logs) {
                delete log['id'];
                log.environmentId = user.context.url.replace("https://", "");
                log.userId = user.username.toUpperCase();
                log.type = LOG_TYPE[log.type];
                entries.push(log);
            }
            const httpClient = new HttpClient(this.httpBackend);
            const httpPutOptions = {
                headers: new HttpHeaders({
                    'Authorization': `Bearer ${user.token}`,
                    'Content-Type': 'application/json',
                })
            };
            httpClient.put(user.context.url + "/v1.0/apps/" + 1 + "/log", JSON.stringify(entries), httpPutOptions)
                .toPromise()
                .then((response: any) => {
                    if (response) {
                        this.cleanDB(user.context.id);
                    }
                }, (error) => {
                    this.logError(error, user);
                    this.createLogFile(user);
                });
        }
        
    }

    /**
     * Clean logs in the database by environment
     * @param environmentId
     */
    private cleanDB(environmentId: number) {
        this.dbProvider.getRepository(LogEntry)
            .createQueryBuilder("logEntry")
            .delete()
            .from(LogEntry)
            .where("logEntry.environmentId = :environmentLogs", { environmentLogs: environmentId })
            .execute();
    }

    /**
     * Clean logs last two days in the database by environment
     * @param environmentId
     */
    private async cleanLastTwoDaysDB(environmentId: number) {
        let lastTwoDays = new Date();
        let currentDate = lastTwoDays.getDate();
        let filterDate = currentDate - 2;
        lastTwoDays.setDate(filterDate);
        await this.dbProvider.getRepository(LogEntry)
            .createQueryBuilder("logEntry")
            .delete()
            .from(LogEntry)
            .where("logEntry.environmentId = :environmentLogs")
            .andWhere("logEntry.createdAt < :start_at")
            .setParameters({ environmentLogs: environmentId, start_at: lastTwoDays })
            .execute();
    }

    /**
     * Checks if there are logs of type error and critic in the database
     * @param user
     */
    public async checkDBForErrors(user: User) {
        if(!this.dtLastLogSent || ((new Date().getTime() - this.dtLastLogSent) > 500)){
            this.dtLastLogSent = new Date().getTime();
            if (user && user.token) {
                let userEnvId = user.context.id;
                let logs = await this.dbProvider.getRepository(LogEntry)
                    .createQueryBuilder("logEntry")
                    .where("logEntry.environmentId = :environmentLogs")
                    .andWhere("(logEntry.type = :logError OR logEntry.type = :logCritic)")
                    .setParameters({ environmentLogs: userEnvId, logError: "1", logCritic: "0"})
                    .getCount();
                if (logs > 0) {
                    this.sendLogs(user);
                } else {
                    this.writeLog(user);
                    this.cleanLastTwoDaysDB(userEnvId);
                }
            }
        }
    }

}
