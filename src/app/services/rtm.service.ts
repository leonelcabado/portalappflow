import {Injectable, NgZone} from '@angular/core';
import {Subject} from 'rxjs';

//services
import {ParticipantService} from './participant.service';
import {ConversationService} from './conversation.service';
import {TaskService} from './task.service';

//model
import {EVENT_TYPE, RTMEvent} from '../model/rtm-event.entity';
import {Message} from '../model/message.entity';
import {Participant} from '../model/participant.entity';

//providers
import {UserProvider} from './user.service';
import {NotificationProvider} from './notification.service';
import {DbProvider} from './db.service';
import {UpdateProvider} from './update.service';
import {ConfigurationService} from './configuration.service';
import { LogService } from './log.service';

@Injectable()
export class RTMService {
    private wsocket: WebSocket = null;
    private protocol = "wss";
    subscribeConnect: any = null;
    networkConnected: boolean = false;
    execSync: boolean = false;
    resumePlatform: boolean = false;
    finalCloseCode = 4444;
    public onConnectSource = new Subject<boolean>();
    public onConnect$ = this.onConnectSource.asObservable();
    public onTaskChangeSource = new Subject<boolean>();
    public onTaskChange$ = this.onTaskChangeSource.asObservable();
    url = "";
    tailEvents: MessageEvent [] = [];

    constructor(private participantService: ParticipantService,
                private conversationService: ConversationService,
                private configurationService: ConfigurationService,
                private userProvider: UserProvider,
                private notificationProvider: NotificationProvider,
                private dbProvider: DbProvider,
                private updateProvider: UpdateProvider,
                private ngZone: NgZone,
                private taskService: TaskService,
                private logService: LogService) {

    }

    initWsocket() {
        this.wsocket = null;
    }

    openConnection(sync?: boolean, resumePlatform?: boolean): Promise<any> {
        return new Promise((resolve, reject) => {
            this.userProvider.setUserSocketId(null);
            this.execSync = sync ? sync : false;
            this.resumePlatform = resumePlatform ? resumePlatform : false;
            if (!this.wsocket) {
                this.logService.logDevelopment('Connecting to socket' + ' ' + sync + ' ' + resumePlatform, this.userProvider.getUser());
                this.wsocket = new WebSocket(this.userProvider.wssEndpoint(this.protocol) + "/" + this.userProvider.getUsername().toUpperCase());
                // this.wsocket = new WebSocket(this.url);
                let $this = this;
                this.wsocket.onmessage = function (event) {
                    $this.preProcessEvent(event);
                };

                this.wsocket.onclose = function (event) {
                    let $t = $this;
                    $this.wsocket = null;
                    $this.userProvider.setUserSocketId(null);
                    $this.logService.logDevelopment('Client notified socket has closed ' + event.code, $this.userProvider.getUser());
                    if (event.code != $t.finalCloseCode && $t.networkConnected && $t.userProvider.getUser().loggedIn && $t.notificationProvider.platformState == "active") {
                        $this.userProvider.inProcessSync = false;
                        $this.logService.logDevelopment("reconnectSocket" + ' ' + sync + ' ' + resumePlatform, $this.userProvider.getUser());
                        $t.ngZone.run(() => $t.openConnection(sync, resumePlatform));
                    }
                    ;
                }

                this.wsocket.onerror = function (event) {
                    this.onConnectSource.next(false);
                }.bind(this);
                resolve();
            } else {
                this.closeConnection();
                reject();
            }
        });
    }

    closeConnection() {
        this.logService.logDevelopment("Closing websocket!", this.userProvider.getUser());
        if (this.wsocket) {
            if (this.wsocket.readyState != 0) {
                this.wsocket.close(this.finalCloseCode);
            } else {
                let $t = this;
                setTimeout(function () {
                    if ($t.wsocket) $t.wsocket.close($t.finalCloseCode);
                }, 500);
            }
        }
    }

    reconnectConnection() {
        console.error("Closing websocket!");
        if (this.wsocket) {
            this.wsocket.close(this.finalCloseCode);
            delete this.wsocket;
            this.wsocket = null;
            this.openConnection().then();
        }
    }

    preProcessEvent(event: MessageEvent) {
        let data = JSON.parse(event.data);
        if (data && data.cdType === 12) {
            this.logService.logDevelopment('Llegó socketId ' + data.dsData.wsSessionId, this.userProvider.getUser());
            this.userProvider.setUserSocketId(data.dsData.wsSessionId);
            this.userProvider.setNuTasks(data.dsData.nuTasks);
            this.onConnectSource.next(true);
            return;
        }
        if (!this.userProvider.getUserSocketId()) return;
        if (data && (data.cdType == EVENT_TYPE.USER_TYPING || data.cdType == EVENT_TYPE.USER_STOP_TYPING)) {
            if (!this.userProvider.inSync) this.processEvent(event);
            return;
        }
        if (this.tailEvents.length > 0) {
            this.tailEvents.push(event);
        }
        else {
            if (this.userProvider.inSync) {
                this.tailEvents.push(event);
            }
            else {
                this.tailEvents.push(event);
                this.ngZone.run(() => {
                    this.processEvent(event)
                });
            }
        }
    }

    processEvent(m: MessageEvent) {
        let data = JSON.parse(m.data);
        let cdType = data.cdType;
        let dsData = data.dsData;
        let dtLastUpdate = data.dtLastUpdate;
        console.log('processEvent ', cdType);

        switch (cdType) {

            case EVENT_TYPE.NEW_MESSAGE:
                console.error("RTM NEW_MESSAGE");
                this.conversationService.existMessage(dsData['cdConversationElement'], dsData['cdConversation'])
                    .then((messages) => {
                        if (!messages) {
                            let msgs = this.conversationService.api.serializeMessages([dsData]);
                            if (msgs.length > 0) {
                                let user = (msgs[0]['cdParticipant']) ? msgs[0]['cdParticipant'] : ' null';
                                this.logService.logDevelopment("RTM NEW_MESSAGE - " + "cdParticipant: " + user + ' - cdConversationElement: ' + msgs[0]['cdConversationElement'] + ' - cdConversation: ' + msgs[0]['cdConversation'] + ' - dsContent: ' + msgs[0]['dsContent'], this.userProvider.getUser());
                                this.ngZone.run(() => {
                                    this.conversationService.addMessage(msgs[0], null, false, dsData.dtLastUpdate)
                                        .then(_ => {
                                            this.processNextEvent();
                                            this.updateDtLastUpdate(dsData.dtLastUpdate);
                                        })
                                        .catch((error) => {
                                            this.logService.logError(error, this.userProvider.getUser());
                                            this.processNextEvent();
                                        });
                                });
                            }
                        } else {
                            this.updateDtLastUpdate(dtLastUpdate);
                            this.processNextEvent();
                        }
                    });
                break;

            case EVENT_TYPE.USER_LOGGED_IN:
                console.error("RTM USER_LOGGED_IN");
                if ('cdParticipant' in dsData)
                    this.saveState(dsData);
                this.processNextEvent();
                break;

            case EVENT_TYPE.USER_STATE_CHANGED:
                console.error("RTM USER_STATE_CHANGED");
                if ('cdParticipant' in dsData)
                    this.saveState(dsData);
                this.processNextEvent();
                break;

            case EVENT_TYPE.MESSAGES_STATE_CHANGED:
                console.error("RTM MESSAGES_STATE_CHANGED");
                console.warn(dsData);
                dsData = JSON.parse(dsData);
                this.conversationService.updateMessageStates(dsData)
                    .then(_ => {
                        this.processNextEvent();
                        this.updateDtLastUpdate(dtLastUpdate);
                    });
                break;

            case EVENT_TYPE.NEW_CONV_PARTICIPANT:
                console.error("RTM NEW_CONV_PARTICIPANT");
                let message = new Message().fromJSON(dsData.event);
                if (message) {
                    this.conversationService.newConversationParticipant(dsData.participant, message, dtLastUpdate)
                        .then((conversation) => {
                            if (conversation) {
                                this.updateDtLastUpdate(dtLastUpdate);
                                this.processNextEvent();
                                this.notificationProvider.newMessage(conversation, message);
                            }
                        });
                }
                break;

            case EVENT_TYPE.LEAVE_CONVERSATION:
                console.error("RTM LEAVE_CONVERSATION");
                this.conversationService.removeConversationParticipant(dsData.participant, new Message().fromJSON(dsData.event), dtLastUpdate)
                    .then(_ => {
                        this.processNextEvent();
                        this.updateDtLastUpdate(dtLastUpdate);
                    });
                break;

            case EVENT_TYPE.CONV_TITLE_CHANGED:
                console.error("RTM CONV_TITLE_CHANGED");
                dsData = JSON.parse(dsData);
                this.conversationService.updateGroupTitle(dsData.cdConversation, dsData.dsTitle);
                this.updateDtLastUpdate(dtLastUpdate);
                this.processNextEvent();
                break;

            case EVENT_TYPE.USER_TYPING:
                console.error("RTM USER_TYPING");
                dsData = JSON.parse(dsData);
                if (dsData.cdParticipant != this.userProvider.getUsername().toUpperCase()) {
                    this.conversationService.triggerUserTyping(dsData.cdConversation, dsData.cdParticipant)
                }
                break;

            case EVENT_TYPE.USER_STOP_TYPING:
                console.error("RTM USER_STOP_TYPING");
                dsData = JSON.parse(dsData);
                this.conversationService.triggerUserStopTyping(dsData.cdConversation, dsData.cdParticipant);
                break;

            case EVENT_TYPE.MESSAGE_EXECUTED:
                console.error("RTM MESSAGE_EXECUTED");
                this.conversationService.updateLocalMessage(new Message().fromJSON(dsData))
                    .then(_ => {
                        console.warn(this.tailEvents.length);
                        this.processNextEvent();
                        this.updateDtLastUpdate(dtLastUpdate);
                    });
                break;

            case EVENT_TYPE.MESSAGE_DELETED:
                console.error("RTM MESSAGE_DELETED");
                this.conversationService.removeLocalMessage(new Message().fromJSON(dsData))
                    .then(_ => {
                        this.updateDtLastUpdate(dtLastUpdate);
                        this.processNextEvent();
                    });
                break;

            case EVENT_TYPE.NEW_USER:
                this.participantService.newParticipant(dsData)
                    .then(_ => {
                        this.processNextEvent();
                        this.updateDtLastUpdate(dtLastUpdate);
                    });
                break;

            case EVENT_TYPE.CHANGE_GROUP_OWNER:
                console.error("RTM CHANGE_GROUP_OWNER");
                this.conversationService.updateGroupOwner(dsData.data.newGroupOwner, dsData.data.cdConversation);
                this.updateDtLastUpdate(dtLastUpdate);
                this.processNextEvent();
                break;

            case EVENT_TYPE.USER_TASKS_CHANGED:
                console.error("RTM USER_TASKS_CHANGED");
                this.userProvider.setNuTasks(dsData);
                this.onTaskChangeSource.next();
                this.processNextEvent();
                break;

            case EVENT_TYPE.NEW_TASK:
                console.error("RTM NEW_TASK");
                // console.error("*************************************************");
                // console.error(JSON.stringify(dsData));
                this.notificationProvider.newTask(dsData);
                this.processNextEvent();
                break;
            case EVENT_TYPE.THEME_CHANGED:
                const theme = JSON.parse(dsData).THEME_DIR;
                this.configurationService.notifyThemeChanged(theme);
                this.conversationService.conversationsUpdated();
                this.processNextEvent();
                break;

            default:
                this.processNextEvent();
        }
    }

    updateDtLastUpdate(dtLastUpdate) {
        if (dtLastUpdate && this.updateProvider.dtLastUpdate < dtLastUpdate) {
            this.logService.logDevelopment('Seteo nuevo dtLastUpdate RTM ' + dtLastUpdate, this.userProvider.getUser());
            this.updateProvider.dtLastUpdate = dtLastUpdate;
        }
    }

    processNextEvent() {
        this.tailEvents.splice(0, 1);
        if (this.tailEvents.length > 0) {
            this.ngZone.run(() => {
                this.processEvent(this.tailEvents[0])
            });
        }
    }


    getWebSocket() {
        return this.wsocket;
    }


    sendMessage(message: string) {
        if (this.wsocket) {
            this.wsocket.send(message);
            if (JSON.parse(message).dsData == 0) {
                this.wsocket.close(this.finalCloseCode);
                delete this.wsocket;
                this.wsocket = null;
            }
        }
    }

    saveState(dsData) {
        this.participantService.updateState(dsData.cdParticipant, dsData.cdState);
        this.dbProvider.getRepository(Participant)
            .findOne({
                userId: this.userProvider.getUser().id,
                cdParticipant: dsData.cdParticipant
            })
            .then((participant) => {
                if (participant) {
                    participant.cdState = dsData.cdState;
                    this.dbProvider.getRepository(Participant).createQueryBuilder()
                        .update(Participant)
                        .set(participant.getPlainObject())
                        .where('id = :id', { id: participant.id })
                        .execute();
                    this.conversationService.reloadStateParticipant(participant);
                }
            });
    }

    setState(s: number): void {
        if (this.participantService.changeState(s)) {
            this.trigger(new RTMEvent(EVENT_TYPE.USER_STATE_CHANGED, s));
        }
    }

    trigger(ev: RTMEvent) {
        this.sendMessage(JSON.stringify(ev));
    }

    /**
     * Sends the message with user state changed and closes the websocket connection.
     * @returns {undefined}
     */
    public processLogOut(): void {
        if (this.userProvider.socketId) {
            this.sendMessage(JSON.stringify(new RTMEvent(EVENT_TYPE.USER_STATE_CHANGED, 0)));
        }
        this.closeConnection();
    }

}
