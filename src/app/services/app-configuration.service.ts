import APP_BUILD_CONFIG from '../config/app-build-config.json';
import { APP } from '../app.config';
import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { TabConfiguration } from '../model/tab-configuration.entity';
import { PageConfiguration } from '../model/page-configuration.entity';
import { FormBrowseConfiguration } from '../model/form-browse-configuration.entity';
import { HeaderConfiguration } from '../model/header-configuration.entity';
import { ButtonConfiguration } from '../model/button-configuration.entity';
import { InputConfiguration } from '../model/input-configuration.entity';
import { StatusBarConfiguration } from '../model/status-bar-configuration.entity';
import { DeyelPageConfiguration } from '../model/deyel-page-configuration.entity';
import {SecurityConfigurationService} from "./security-configuration.service";

@Injectable()
export class AppConfigurationService {

    private tabsConfigurations: Array<TabConfiguration>;
    private tabsDefault = {"dashboard-list": "DASH_TAB", "chat-list":"CHAT_TAB", "task-list":"TASK_TAB", "forms/list":"FORMS_TASK_TAB"}

    constructor(private alertController: AlertController,
                private statusBar: StatusBar,
                private appVersion: AppVersion) {}

    /**
     * It checks if the app configuration contains a initial context.
     * @return {boolean}
     */
    public checkInitialContext(): boolean {
        const initialContext = APP_BUILD_CONFIG.appConfiguration.initialContext;
		return initialContext.name != undefined && initialContext.url != undefined && initialContext.name != '' && initialContext.url != '';
    }

    /**
     * It returns the initial context configurated.
     * @return {any}
     */
    public getInitialContext(): any {
        return APP_BUILD_CONFIG.appConfiguration.initialContext;
    }

    /**
     * It returns the app UUID.
     * @return {string}
     */
    public getAppUUID(): string {
        return APP_BUILD_CONFIG.appConfiguration.id;
    }

    /**
     * It returns the app identifier.
     * @return {string}
     */
    public getAppId(): string {
        return APP_BUILD_CONFIG.appConfiguration.appId;
    }

    /**
     * It returns the app name.
     * @return {string}
     */
    public getAppName(): string {
        return APP_BUILD_CONFIG.appConfiguration.appName;
    }

    /**
     * It returns the initial context name.
     * @return {string}
     */
    public getInitialContextName(): string {
    	return this.getInitialContext().name;
    }

    /**
     * It returns the initial context name.
     * @return {string}
     */
    public getInitialContextUrl(): string {
    	return this.getInitialContext().url;
    }

    /**
     * It returns the app version.
     * @return {string}
     */
    public getAppVersion(): string {
        return APP_BUILD_CONFIG.appConfiguration.appVersion;
    }

    /**
     * It returns the app build version.
     * @return {Promise<string>}
     */
    public getAppBuildVersion(): Promise<string> {
        if (this.getAppUUID() === '0000') {
            return this.appVersion.getVersionNumber();
        }
        return Promise.resolve(APP_BUILD_CONFIG.appConfiguration.buildVersion);
    }

    /**
     * It returns the tab configurations object.
     * @return {Array<any>}
     */
    private getTabsConfigurationsObject(): Array<any> {
        return APP_BUILD_CONFIG.appConfiguration.tabs;
    }

    /**
     * It sets the tabs configurations.
     * @param {Array<TabConfiguration>} tabsConfigurations
     */
    private setTabsConfigurations(tabsConfigurations: Array<TabConfiguration>): void {
        this.tabsConfigurations = tabsConfigurations;
    }

    /**
     * It returns the configurations of the tabs.
     * @return {Array<TabConfiguration>}
     */
    public getTabsConfigurations(): Array<TabConfiguration> {
        if (this.tabsConfigurations) {
            return this.tabsConfigurations;
        }
        const tabsConfigurations = this.getTabsConfigurationsObject();
        if (tabsConfigurations && tabsConfigurations.length > 0) {
            this.setTabsConfigurations(this.mapTabsConfigurations());
            return this.tabsConfigurations;
        }
        this.showEmptyTabsMessage();
        return new Array();
    }

    /**
     * It returns the tabs configurations mapped.
     * @return {Array<TabConfiguration>}
     */
    private mapTabsConfigurations(): Array<TabConfiguration> {
        return this.getTabsConfigurationsObject().map((tabConfigurationObject: any) => {
            const pageConfiguration = this.createPageConfiguration(tabConfigurationObject);
            return new TabConfiguration(tabConfigurationObject.type, tabConfigurationObject.title, tabConfigurationObject.iconName, tabConfigurationObject.iconColor, this.getTabUrlToNavigate(tabConfigurationObject.type, pageConfiguration.getUrlId()), tabConfigurationObject.isActived, tabConfigurationObject.isDefault, pageConfiguration);
        });
    }

    /**
     * It returns the tab url for a type.
     * @param  {string} tabType
     * @return {string}
     */
    private getTabUrl(tabType: string): string {
        return tabType.replace('-', '/');
    }

    /**
     * It returns the tab url for a type to use in navigation.
     * @param  {string} tabType
     * @param  {string} urlId
     * @return {string}
     */
    private getTabUrlToNavigate(tabType: string, urlId: string): string {
        if (urlId) {
            return '/main/' + this.getTabUrl(tabType) + '/' + urlId;
        }
        return '/main/' + this.getTabUrl(tabType);
    }

    /**
     * It shows a message when the tabs configurations is empty.
     * @return {Promise<any>}
     */
    private async showEmptyTabsMessage(): Promise<any> {
        const alert = await this.alertController.create({
            header: 'Configuración Errónea',
            message: 'No se han configurado Tabs para mostrar.',
            backdropDismiss: false,
        });
        await alert.present();
    }

    /**
     * It returns the first tab url that the user can see!!
     * @return {string}
     */
    public getFirstTabUrl(securityConfiguration: SecurityConfigurationService): string {
        const tabsConfigurations = this.getTabsConfigurations();
        if (tabsConfigurations && tabsConfigurations.length > 0) {
            console.log("FIRST TAB EVALUATED!");
            let activeTabs = tabsConfigurations.filter((tab) => tab.getIsActived() && securityConfiguration.validateSecurity(this.getTabVisibilitySecurityCode(tab)));
            if(activeTabs[0]){
                return activeTabs[0].getUrl();
            }
        }
        return '';
    }

    /**
     * It checks if exists one tab configuration according to a type.
     * @param  {string}  tabType
     * @return {boolean}
     */
    private existTabConfiguration(tabType: string): boolean {
        const tabsConfigurations = this.getTabsConfigurations();
        if (tabsConfigurations && tabsConfigurations.length > 0) {
            return tabsConfigurations.some((tabConfigurationObject: TabConfiguration) => {
                return tabConfigurationObject.getType() === tabType;
            });
        }
        return false;
    }

    /**
     * If checks if exists the chat tab configuration.
     * @return {boolean}
     */
    public existChatTabConfiguration(): boolean {
        return this.existTabConfiguration('chat-list');
    }

    /**
     * If checks if exists the dashboard tab configuration.
     * @return {boolean}
     */
    public existDashboardTabConfiguration(): boolean {
        return this.existTabConfiguration('dashboard-list');
    }

    /**
     * If checks if exists the tasks tab configuration.
     * @return {boolean}
     */
    public existTasksTabConfiguration(): boolean {
        return this.existTabConfiguration('task-list');
    }

    /**
     * If checks if exists the forms and tasks tab configuration.
     * @return {boolean}
     */
    public existFormAndTaskTabConfiguration(): boolean {
        return this.existTabConfiguration('forms/list');
    }

    /**
     * It creates the page configration instance.
     * @param  {any}               tabConfigurationObject
     * @return {PageConfiguration}
     */
    private createPageConfiguration(tabConfigurationObject: any): PageConfiguration {
        const pageConfigurationObject = tabConfigurationObject.configuration;
        if (pageConfigurationObject && tabConfigurationObject.type === 'form-browse') {
            return new FormBrowseConfiguration(pageConfigurationObject.cdDocumentName);
        }
        if (pageConfigurationObject && tabConfigurationObject.type === 'page-iframe') {
            return new DeyelPageConfiguration(pageConfigurationObject.idPage);
        }
        return new DeyelPageConfiguration(pageConfigurationObject.idPage,
                                    this.createHeaderConfiguration(pageConfigurationObject.header),
                                    pageConfigurationObject.initialPage,
                                    this.createButtonsConfigurations(pageConfigurationObject.buttons),
                                    this.createInputsConfigurations(pageConfigurationObject.inputs),
                                    this.createStatusBarConfiguration(pageConfigurationObject.statusBar),
                                    pageConfigurationObject.topBackgroundColor, 
                                    pageConfigurationObject.bottomBackgroundColor,
                                    pageConfigurationObject.backgroundImage);
    }

    /**
     * It returns the configuration searching by url.
     * @param  {string}            url
     * @return {PageConfiguration}
     */
    public getConfigurationByUrl(url: string): PageConfiguration {
        if (url && url.includes('/main')) {
            return this.getTabConfigurationByUrl(url);
        }
        return this.getPageConfigurationByUrl(url);
    }

    /**
     * It returns the tab configuration searching by url.
     * @param  {string}            tabUrl
     * @return {PageConfiguration}
     */
    private getTabConfigurationByUrl(tabUrl: string): PageConfiguration {
        const tabsConfigurations = this.getTabsConfigurations();
        if (tabsConfigurations && tabsConfigurations.length > 0) {
            const tabConfigurationFound = tabsConfigurations.find((tabConfiguration: TabConfiguration) => {
                return tabConfiguration.getUrl() === tabUrl;
            });
            if (tabConfigurationFound) {
                return tabConfigurationFound.getConfiguration();
            }
        }
        return undefined;
    }

    /**
     * It returns the page configurations.
     * @return {Array<any>}
     */
    private getPagesConfigurations(): Array<any> {
        return APP_BUILD_CONFIG.appConfiguration.pages;
    }

    /**
     * It returns the initial page url.
     * @return {string}
     */
    public getInitialPageUrl(securityConf: SecurityConfigurationService): string {
        if (!this.existDefaultAppTabs()) {
            return this.getFirstTabUrl(securityConf);
        }
        const pagesConfigurations = this.getPagesConfigurations();
        if (pagesConfigurations && pagesConfigurations.length > 0) {
            const initialPageConfiguration = pagesConfigurations.find((pageConfiguration: any) => {
                return pageConfiguration && pageConfiguration.configuration && pageConfiguration.configuration.initialPage;
            });
            if (initialPageConfiguration) {
                return '/' + initialPageConfiguration.type;
            }
        }
        return '/login';
    }

    /**
     * It finds the page configuration by url in the pages list.
     * @param  {string}            url
     * @return {PageConfiguration}
     */
    private getPageConfigurationByUrl(url: string): PageConfiguration {
        const pageConfigurationsObjects = this.getPagesConfigurations();
        if (pageConfigurationsObjects && pageConfigurationsObjects.length > 0) {
            const pageConfigurationObjectFound = pageConfigurationsObjects.find((configurationObject: any) => {
                return ('/' + configurationObject.type) === url;
            });
            if (pageConfigurationObjectFound) {
                return this.createPageConfiguration(pageConfigurationObjectFound);
            }
        }
        return undefined;
    }

    /**
     * It creates the header configuration instance.
     * @param  {any}                 headerConfigurationObject
     * @return {HeaderConfiguration}
     */
    private createHeaderConfiguration(headerConfigurationObject: any): HeaderConfiguration {
        if (headerConfigurationObject) {
            return new HeaderConfiguration(headerConfigurationObject.title, headerConfigurationObject.style);
        }
        return new HeaderConfiguration();
    }

    /**
     * It creates the buttons configurations instances.
     * @param  {Array<any>}                 buttonsConfigurationObject
     * @return {Array<ButtonConfiguration>}
     */
    private createButtonsConfigurations(buttonsConfigurationObject: Array<any>): Array<ButtonConfiguration> {
        if (buttonsConfigurationObject) {
            return buttonsConfigurationObject.map((buttonConfigurationObject: any) => {
                return new ButtonConfiguration(buttonConfigurationObject.name, buttonConfigurationObject.label, buttonConfigurationObject.style);
            });
        }
    }

    /**
     * It creates the inputs configurations instances.
     * @param  {Array<any>}                 inputsConfigurationObject
     * @return {Array<InputConfiguration>}
     */
    private createInputsConfigurations(inputsConfigurationObject: Array<any>): Array<InputConfiguration> {
        if (inputsConfigurationObject) {
            return inputsConfigurationObject.map((inputsConfigurationObject: any) => {
                return new InputConfiguration(inputsConfigurationObject.name, inputsConfigurationObject.label, inputsConfigurationObject.style, inputsConfigurationObject.required);
            });
        }
    }

    /**
     * It returns the fab button style.
     * @return {string}
     */
    public getFabButtonStyle(): string {
        const fabButton= APP_BUILD_CONFIG.appConfiguration.fabButton;
        if (fabButton) {
            const styleFabButton = fabButton.style;
            if (styleFabButton) {
                return styleFabButton;
            }
        }
        return '--background: var(--ion-color-theme-secondary);';
    }

    /**
     * It returns the fab list button style.
     * @return {string}
     */
    public getFabListButtonStyle(): string {
        const fabButton= APP_BUILD_CONFIG.appConfiguration.fabButton;
        if (fabButton) {
            const styleFabListButton = fabButton.listStyle;
            if (styleFabListButton) {
                return styleFabListButton;
            }
        }
        return '--background: var(--ion-color-theme-secondary);';
    }

    /**
     * It creates the status bar configuration.
     * @param  {any}                    statusBarConfigurationObject
     * @return {StatusBarConfiguration}
     */
    private createStatusBarConfiguration(statusBarConfigurationObject: any): StatusBarConfiguration {
        if (statusBarConfigurationObject && statusBarConfigurationObject.backgroundColor && statusBarConfigurationObject.lightText) {
            return new StatusBarConfiguration(statusBarConfigurationObject.backgroundColor, statusBarConfigurationObject.lightText);
        }
    }

    /**
     * It sets the default values of the status bar.
     */
    public setDefaultStatusBar(): void {
        this.statusBar.styleDefault();
        this.statusBar.backgroundColorByHexString(APP.STATUSBAR_COLOR);
    }

    /**
     * It sets the status bar values from a page configuration.
     * @param {PageConfiguration} pageConfiguration
     */
    public setStatusBar(pageConfiguration: PageConfiguration): void {
        if (pageConfiguration && pageConfiguration.getStatusBarConfiguration()) {
            const statusBarConfiguration = pageConfiguration.getStatusBarConfiguration();
            this.statusBar.backgroundColorByHexString(statusBarConfiguration.getBackgroundColor());
            if (statusBarConfiguration.getLightText()) {
                this.statusBar.styleLightContent();
                return;
            }
            this.statusBar.styleDefault();
            return;
        }
        this.setDefaultStatusBar();
    }

    /**
     * Returns a list of safety functions associated with the tabs.
     * @returns 
     */
    public getSecurityFunctionsTabs(): String[]{
        let lsSecurityFunctions = [];
        for(let tab of this.getTabsConfigurationsObject()){
            if(tab.isActived){
                lsSecurityFunctions.push(this.getTabVisibilitySecurityCode(tab));
            }
        }
        return lsSecurityFunctions;
    }

    /**
     * Returns the convention of the function code of the tab.
     * @param tab 
     * @returns 
     */
    public getTabVisibilitySecurityCode(tab: any): string {
        if(tab.type === 'form-browse') {
            return 'D' + tab.configuration.cdDocumentName + 'C001';
        }
        if(tab.type === 'page-iframe') {
            return 'PG_' + tab.configuration.idPage + '_U001';
        }
        if (tab.type === 'forms/list') {
            return 'AF-EXECUT';
        }
        return 'MA' + this.getAppUUID() + '_' + this.tabsDefault[tab.type];
    }

    /**
     * It returns if the header should be displayed.
     * @return {boolean}
     */
    public getShowHeader(): boolean {
        if (APP_BUILD_CONFIG.appConfiguration.hasOwnProperty('showHeader')) {
            return APP_BUILD_CONFIG.appConfiguration.showHeader;
        }
        return true;
    }

    /**
     * It returns the fab button should be displayed.
     * @return {boolean}
     */
    public showFabButton(): boolean {
        const fabButtonConfiguration = APP_BUILD_CONFIG.appConfiguration.fabButton;
        if (fabButtonConfiguration && fabButtonConfiguration.hasOwnProperty('show')) {
            return fabButtonConfiguration.show;
        }
        return true;
    }

    /**
     * It returns the login page should be displayed.
     * @return {boolean}
     */
    public showLoginPage(): boolean {
        const loginConfiguration = this.getPagesConfigurations()[1];
        if (loginConfiguration && this.getAppUUID() !== '0000' && loginConfiguration.hasOwnProperty('showLoginPage')) {
            return loginConfiguration.showLoginPage;
        }
        return false;
    }

    /**
     * It returns if exists the default App Tabs.
     * @return {boolean}
     */
    public existDefaultAppTabs(): boolean {
        const tabsConfigurations = this.getTabsConfigurations();
        if (tabsConfigurations && tabsConfigurations.length > 0) {
            return tabsConfigurations.some((tabConfigurationObject: TabConfiguration) => {
                return tabConfigurationObject.getIsDefault();
            });
        }
        return false;
    }

    /**
     * It returns the login page.
     * @return {string}
     */
    public getLoginPage(): string {
        const loginConfiguration = this.getPagesConfigurations()[1].configuration;
        if (loginConfiguration && loginConfiguration.hasOwnProperty('idPage')) {
            return loginConfiguration.idPage;
        }
        return null;
    }

}
