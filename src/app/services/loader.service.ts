import { LoadingController } from '@ionic/angular';
import { Injectable, NgZone } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { ParticipantService } from './participant.service';
import { ConversationService } from './conversation.service';
import { RTMService } from './rtm.service';
import { SynchronizationService } from './synchronization.service';
import { TranslateService } from './translate.service';
import { NetworkService } from './network.service';
import { AppConfigurationService } from './app-configuration.service';
import { UserProvider } from './user.service';
import { DbProvider } from './db.service';
import { User } from '../model/user.entity';
import { Participant } from '../model/participant.entity';
import { LogService } from './log.service';

@Injectable()
export class LoaderProvider {

    constructor(private participantService: ParticipantService,
                private conversationService: ConversationService,
                private translateService: TranslateService,
                public loadingCtrl: LoadingController,
                private rtmService: RTMService,
                private userProvider: UserProvider,
                private dbProvider: DbProvider,
                private synchronizationService: SynchronizationService,
                private ngZone: NgZone,
                private alertCtrl: AlertController,
                private networkService: NetworkService,
                private appConfigurationService: AppConfigurationService,
                private logService: LogService) {
    }

    init(userFirstTime: boolean = false) {
        return new Promise((resolve, reject) => {
            if (userFirstTime) {
                this.participantService.load()
                    .then(() => {
                            this.participantService.loadBots();
                            this.conversationService.load()
                                .then(() => {
                                        this.translateService.getTranslations()
                                            .then((translations) => {
                                                    this.userProvider.getUser().translations = translations;
                                                    this.userProvider.sync()
                                                        .then(() => {
                                                                this.ngZone.run(() => {
                                                                    this.rtmService.openConnection()
                                                                        .then()
                                                                        .catch(err => reject(err));
                                                                });
                                                                this.setDsUser();
                                                                this.userProvider.initUpdate()
                                                                    .then(() => {
                                                                        resolve();
                                                                    })
                                                                    .catch(err => reject(err));
                                                            },
                                                            (err) => {
                                                                reject(err);
                                                            }
                                                        );
                                                },
                                                (err) => {
                                                    reject(err);
                                                }
                                            );
                                    },
                                    (err) => {
                                        reject(err);
                                    });
                        },
                        (err) => {
                            reject(err);
                        }
                    );
            }
            else {
                this.userProvider.loadUpdate()
                    .then(_ => {
                        this.userProvider.inSync = true;
                        resolve();
                        this.rtmService.openConnection()
                            .then(_ => {
                                this.logService.logDevelopment("Connect Socket esperando socketId", this.userProvider.getUser());
                                let $t = this;
                                let triesNumber = 1;
                                let interval = setInterval(function () {
                                    if ($t.userProvider.getUserSocketId()) {
                                        clearInterval(interval);
                                        $t.syncAndLoad(1)
                                            .then(_ => {
                                                resolve();
                                            })
                                            .catch(error => {
                                                $t.logService.logDevelopment("Error loadUpdate openConnection", $t.userProvider.getUser());
                                                $t.logService.logDevelopment(error, $t.userProvider.getUser());
                                                resolve();
                                            })
                                    }
                                }, this.synchronizationService.generateRandomTimeInMilliSeconds());
                            })
                            .catch(error => {
                                if (error) {
                                    this.logService.logError(error, this.userProvider.getUser());
                                }
                                this.conversationService.loadConversationsFromDb(true)
                                    .then((conversations) => {
                                            this.synchronizationService.retryOpenConnection();
                                            this.setDsUser();
                                            resolve();
                                        },
                                        (err) => {
                                            reject(err);
                                        });
                            });
                    })
                    .catch((error) => {
                        this.logService.logError(error, this.userProvider.getUser());
                        reject(error);
                    });
            }
        });
    }

    initFromDB() {
        return new Promise((resolve, reject) => {
            this.setDsUser();
            resolve();
        });
    }

    syncAndLoad(triesNumber: number) {
        return new Promise((resolve, reject) => {
            if (this.appConfigurationService.existChatTabConfiguration()) {
                this.logService.logDevelopment("syncAndLoad " + triesNumber, this.userProvider.getUser());
                if (triesNumber < 6) {
                    this.synchronizationService.sync(true)
                        .then(() => {
                            this.conversationService.loadConversationsFromDb(true)
                                .then((conversations) => {
                                    console.warn('termina el loadConversationsFromDb');
                                    if (this.rtmService.tailEvents.length > 0) this.rtmService.processEvent(this.rtmService.tailEvents[0]);
                                    this.userProvider.inSync = false;
                                    this.userProvider.inProcessSync = false;
                                    this.setDsUser();
                                    this.conversationService.conversationsUpdated();
                                    resolve();
                                })
                                .catch((error) => {
                                    this.logService.logError(error, this.userProvider.getUser());
                                    reject(error);
                                });
                        })
                        .catch((error) => {
                            this.logService.logError(error, this.userProvider.getUser());
                            this.syncAndLoad(triesNumber + 1)
                                .then(_ => resolve())
                                .catch(error => {
                                    this.logService.logError(error, this.userProvider.getUser());
                                    resolve();
                                });
                        });
                }
                else {
                    this.conversationService.loadConversationsFromDb(true)
                        .then((conversations) => {
                                this.synchronizationService.retrySync();
                                this.setDsUser();
                                resolve();
                            },
                            (err) => {
                                reject(err);
                            });
                }
            }
            resolve();
        });
    }

    setDsUser() {
        this.dbProvider.getRepository(Participant)
            .find({
                cdParticipant: this.userProvider.getUser().username.toUpperCase(),
                userId: this.userProvider.getUser().id
            })
            .then((users) => {
                if(users && users.length > 0){
                    this.userProvider.dsUser = users[0].dsParticipant;
                }
            })
            .catch((error) => this.logService.logDevelopment(error, null));
    }

    errorSync() {
        this.alertCtrl.create({
            header: this.translateService.instant('SYNC_ERROR'),
            subHeader: this.translateService.instant('SYNC_MESSAGE'),
            buttons: ['OK']
        }).then(alert => alert.present());
    }
}
