import {Injectable} from '@angular/core';
import {BackButtonPressedListener} from '../support-classes/back-button-pressed-listener';

//providers
import {UserProvider} from './user.service';

@Injectable()
export class BackButtonProvider {
    dismissableElements: any = [];
    listenerElements: BackButtonPressedListener[] = [];
    listenerHistoryElement: BackButtonPressedListener = null;
    viewsMap: Map<string, string> = new Map<string, string>();

    constructor(private userProvider: UserProvider) {
    }

    registerDismissable(dismissable) {
        this.dismissableElements.push(dismissable);
        dismissable.onDidDismiss().then(param => {
            if (param != "BACKBUTTON") {
                this.unregisterDismissable(dismissable);
            }
        });
    }

    unregisterDismissable(dismissable) {
        var index = this.dismissableElements.indexOf(dismissable);
        if (this.hasDismissableElements() && index > -1) {
            this.dismissableElements.splice(index, 1);
        }
    }

    hasDismissableElements() {
        return this.dismissableElements.length > 0;
    }

    registerListener(listener: BackButtonPressedListener) {
        this.listenerElements.push(listener);
    }

    registerListenerHistory(listener: BackButtonPressedListener) {
        this.listenerHistoryElement = listener;
    }

    unregisterListener(listener: BackButtonPressedListener) {
        var index = this.listenerElements.indexOf(listener);
        if (this.hasListenerElements() && index > -1) {
            this.listenerElements.splice(index, 1);
        }
    }

    hasListenerElements() {
        return this.listenerElements.length > 0;
    }

    back() {
        if (this.userProvider.getConsultedTask()) {
            this.userProvider.setConsultedTask(null);
        }
        if (this.hasDismissableElements()) {
            for (let dismissable of this.dismissableElements) {
                dismissable.dismiss("BACKBUTTON");
            }
            this.dismissableElements = [];
            return true;
        }
        if (this.hasListenerElements()) {
            for (let listener of this.listenerElements) {
                listener.backButtonPressed();
            }
            this.listenerElements = [];
            return true;
        }
        if (this.listenerHistoryElement) {
            this.listenerHistoryElement.backButtonPressed();
            this.listenerHistoryElement = null;
            return true;
        }
        return false;
    }

    registerView(name: string, id: string) {
        this.viewsMap.set(id, name);
    }

    getViewName(id: string) {
        return this.viewsMap.get(id);
    }

    /**
     * Gets the last view name in navigation.
     * @param  {string}  name
     * @return {boolean}
     */
    public checkLastViewName(name: string): boolean {
        if (this.viewsMap.size > 0) {
            const lastPageName = Array.from(this.viewsMap.values()).pop();
            return lastPageName === name;
        }
        return false;
    }

    /**
     * Resets the view map.
     */
    public resetViewMap(): void {
        this.viewsMap = new Map<string, string>();
    }

}
