import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpBackend } from '@angular/common/http';
import { UserProvider } from './user.service';
import { APP } from '../app.config';
import { LogService } from './log.service';

@Injectable()
export class RuleService {

    private deyelApiToken: string = '';

    constructor(private userProvider: UserProvider, protected httpBackend: HttpBackend, private logService: LogService) {
    }

    /**
     * Returns the token to consume the Deyel API.
     * @return {Observable<string>}
     */
    private getToken(): Promise<any> {
        if (this.deyelApiToken) {
            return Promise.resolve(this.deyelApiToken);
        }
        const httpClient = new HttpClient(this.httpBackend);
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
            }),
            observe: 'response'
        };
        let bodyJson = {cdUser: 'DEYELBOT', cdPassword: 'BOTDEYEL312!'};
        return httpClient.post(this.userProvider.getContextUrl() + APP.API_BASE_NAMESPACE +'/auth/token', bodyJson, {observe: 'response', responseType: 'text', headers: this.getTokenHeaders()})
            .toPromise()
            .then(response => {
                this.deyelApiToken = response.headers.get('jwt');
                return this.deyelApiToken;
            })
            .catch(error => {
                this.logService.logDevelopment(error, this.userProvider.getUser());
                return undefined;
            })
    }

    /**
     * Executes the rule called 'getIniciar'.
     * @return {Observable}
     */
    public executeRuleGetIniciar(): Promise<any> {
        return this.getToken()
            .then((token: string) => {
                if (token) {
                    const httpClient = new HttpClient(this.httpBackend);
                    const httpPutOptions = {
                        headers: new HttpHeaders({
                            'Authorization': `Bearer ${token}`,
                        })
                    };
                    const user = this.userProvider.getUsername().toLocaleUpperCase();
                    return httpClient.put(this.userProvider.getContextUrl() + '/v1.0/rules/dygetiniciar-1/execute', {cdUser: user}, httpPutOptions)
                        .toPromise()
                        .then(response => {
                            return response;
                        })
                        .catch(error => {
                            this.logService.logDevelopment(error, this.userProvider.getUser());
                            return undefined;
                        })
                }
                return undefined;
            })
            .catch(error => {
                this.logService.logError(error, this.userProvider.getUser());
                return undefined;
            });
    }

    /**
     * Returns the headers to token call.
     * @return {HttpHeaders}
     */
    private getTokenHeaders(): HttpHeaders {
        let loginHeaders = new HttpHeaders();
        return loginHeaders.append('Content-Type', 'application/json');
    }

    /**
     * Clear user token.
     */
    public clearTokenDeyel(){
        this.deyelApiToken = null;
    }

}
