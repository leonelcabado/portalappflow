import { Injectable } from '@angular/core';
import { HttpBackend, HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, Subject, throwError } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/internal/operators';


//helpers
import {ApiItemInterface} from '../support-classes/api-item-interface'

//providers
import {UserProvider} from './user.service';

//models
import {Task} from '../model/task.entity';

//config
import {APP} from '../app.config';
import { ToastController } from '@ionic/angular';
import { AuthProvider } from './auth.service';
import { CaseButtons } from '../model/case-buttons.entity';
import { TaskListPage } from '../pages/task-list/task-list';
import { LogService } from './log.service';
import { TaskFilter, TaskFilterParameter } from '../model/task-filter.entity';
import { TaskExecute } from '../model/task-execute.entity';
import { User } from '../model/user.entity';


@Injectable()
export class TaskService {

    tasks: Task[];
    processTasks: any[] = [];
    unreadedNotification: string = '';
    msjSuccessExecuteAction: string = '';
    showNuTasks: boolean = false;

    constructor(private userProvider: UserProvider,
                private http: HttpClient,
                protected httpBackend: HttpBackend,
                private auth: AuthProvider,
                private logService: LogService) {

    }

    getTasks(page: number, criteria?: TaskFilterParameter[]): Observable<any> {
        //this.tasks = [];
        var tasks = [];
        this.processTasks = [];
        const httpClient = new HttpClient(this.httpBackend);
        const httpGetOptions = {
            headers: new HttpHeaders({
                'Authorization': `Bearer ${this.auth.getToken()}`,
            })
        };
        const cdUser = this.userProvider.getUsername().toLocaleUpperCase();
        return httpClient.get(this.userProvider.getContextUrl() + '/v1.0/tasks?search=userCode=eq:' + cdUser + this.processParametersInTasks(criteria) +'&page-number=' + page, httpGetOptions)
            .pipe(map((response: any) => {
                for (let task of response.tasks) {
                    var newTask = this.taskMapper(task);
                    tasks.push(newTask);
                    if (this.processTasks.findIndex(process => process.cdProcess === newTask.cdProcess) < 0) {
                        this.processTasks.push({ cdProcess: newTask.cdProcess, dsProcess: newTask.dsProcess });
                    }
                }
                response['tasks'] = tasks;
                return response;
            }), catchError(error => {
                this.logService.logError(error, this.userProvider.getUser());
                return this.processError(page, error);
            }));
    }

    /**
     * It process the parameters to the task request.
     * @param  {number} pageNumber
     * @param  {string} criteria
     * @return {string}
     */
     private processParametersInTasks(criteria: TaskFilterParameter[]): string {
        let urlParameters = [];
        if(criteria){
            for(let obj of criteria){
                let nuOperation = obj.dsOperation.split(",").length;
                if(nuOperation > 1){
                    for(let i = 0 ; i < nuOperation ; i++){
                        urlParameters.push(obj.dsParameter + "=" + obj.dsOperation.split(",")[i] + ":" + obj.dsValue.split(",")[i]); 
                    }
                }else{
                    urlParameters.push(obj.dsParameter + "=" + obj.dsOperation + ":" + obj.dsValue); 
                }
            }
        }
        let dsUrlParameters = urlParameters.join(",");
        dsUrlParameters = (dsUrlParameters != "") ? "," + dsUrlParameters : dsUrlParameters;
        return dsUrlParameters;
    }

    /**
     * Process the HTTP response error.
     * If it is state 401 asks for the token again.
     * @param  {number}            page
     * @param  {HttpErrorResponse} error
     * @return {Observable<any>}
     */
    processError(page: number, error: HttpErrorResponse): Observable<any> {
        if (error.status === 401) {
            return this.userProvider.getNewToken().pipe(switchMap(() => {
                return this.getTasks(page);
            }));
        }
        return throwError(error);
    }

    /**
     * Endpoint value mapper.
     */
    taskMapper(data): Task {
        var task = new Task();
        var lsButtons: CaseButtons[] = [];
        task.id = data.case;
        task.dsCase = data.description;
        task.dsProcess = data.process;
        task.cdProcess = data.cdProcess;
        var fechaInicio = data.executionDate.split(" ")[0].split('/').reverse().join('-');
        var horaInicio = data.executionDate.split(" ")[1];
        task.dtInit = Date.parse(fechaInicio + "T" + horaInicio);
        switch (data.expired.type) {
            case "NONE":
                task.cdAlert = 0;
                break;
            case "TO_EXPIREL":
                task.cdAlert = 1;
                break;
            case "TO_EXPIREH":
                task.cdAlert = 2;
                break;
            case "EXPIRED":
                task.cdAlert = 3;
                break;
            default:
                task.cdAlert = 0;
                break;
        }
        task.cdPriority = data.priority;
        for (let btn of data.buttons) {
            var button = new CaseButtons();
            button.dsColor = btn.cdColor;
            button.dsName = btn.cdButton;
            button.isDefault = btn.isDefault;
            lsButtons.push(button);
        }
        task.lsButtons = lsButtons;
        task.dsActivity = data.activity;
        task.execute = new TaskExecute(data.execute.href, data.execute.rel);
        return task;
    }

    public getProcessTasks() {
        return this.processTasks;
    }

    public getCountTasks() {
        return ((this.tasks && this.tasks.length > 0) ? '(' + this.tasks.length + ')' : '' );
    }

    public executeAction(task, action) {
        const httpClient = new HttpClient(this.httpBackend);
        const httpPutOptions = {
            headers: new HttpHeaders({
                'Authorization': `Bearer ${this.auth.getToken()}`,
                'Content-Type': 'application/json',
            })
        };
        return new Promise((release, reject) => {
            httpClient.put(task.execute.getHref(), {buttonToExecute: action, fieldsActivity: {}}, httpPutOptions)
                .toPromise()
                .then((response: any) => {
                    if(response){
                        release(response.message);
                    }
                })
                .catch(error => {
                    this.logService.logError(error, this.userProvider.getUser());
                    reject(error);
                });
        });
    }

    /**
     * Creates the configuration for navigation to iframe view.
     * @param  {Task}    task
     * @param  {any}     filtersTasks
     * @param  {boolean} sendData
     * @return {any}
     */
    createParamsFor(task: Task, filtersTasks: any, sendData: boolean): any {
        return {
            url: 'SLExpedienteConsulta&nuExpedienteHidden=' + task.id + '&cdActionHidden=CONSULTAR_EXP&mobile=true&isEmbed=true',
            title: task.dsProcess,
            previousPageData: sendData ? this.getPageDataOfTaskList(filtersTasks) : undefined
        };
    }

    /**
     * Returns the page data for navigation.
     * @param  {any} filtersTasks
     * @return {any}
     */
    public getPageDataOfTaskList(filtersTasks: any): any {
        return {
            route: '/main/task/list',
            data: filtersTasks
        };
    }

    /**
     * REturns the parameters to a form url.
     * @param  {any}    dataEvent
     * @return {string}
     */
    public getParametersToFormsURL(dataEvent: any): string {
        if (dataEvent.hasOwnProperty('xJson')) {
            dataEvent = dataEvent.xJson;
        }
        return 'cdDocumentNameHidden=' + dataEvent.cdDocumentNameHidden + '&cdKeyHidden=' + dataEvent.cdKeyHidden + '&cdActionHidden=' + dataEvent.cdActionHidden + '&button=' + dataEvent.button + '&mobile=true&isEmbed=true';
    }

    public getVisibleColumns(user: User): Promise<boolean> {
        const httpClient = new HttpClient(this.httpBackend);
        const httpGetOptions = {
            headers: new HttpHeaders({
                'Authorization': `Bearer ${user.token}`,
            })
        };
        return new Promise((resolve, reject) => {
            httpClient.put(user.getContext().getUrl() + "/v1.0/tasks/get-visible-columns", {}, httpGetOptions)
                .toPromise()
                .then((response: any) => {
                    resolve(true);
                })
                .catch(error => {
                    reject(false);
                });
        });
    }

}
