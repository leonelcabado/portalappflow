import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {TRANSLATIONS} from '../translate/translation';
import {LOCALE_ID} from '@angular/core';

//helpers
import {ApiItemInterface} from '../support-classes/api-item-interface';

//providers
import {UserProvider} from './user.service';
import { LogService } from './log.service';

@Injectable()
export class TranslateService {

    private translations = null;

    constructor(private http: HttpClient,
                private userProvider: UserProvider,
                private logService: LogService) {
    }

    /**
     * Chequea la clave, y retorna el valor de la clave en caso que se encuentre, caso contrario, se retorna la clave.
     * @param  {string} key
     * @return {string}
     */
    private translate(key: string): string {
        this.initTranslations();
        if (this.translations) {
            if (this.translations[key]) {
                return this.translations[key];
            }
        }
        return key;
    }

    /**
     * Initialize the translations list.
     */
    private initTranslations(): void {
        if (!this.translations) {
            this.translations = this.userProvider.getTranslations();
        }
    }

    /**
     * Instancia la traduccion de la clave.
     * @param  {string} key
     * @return {string}
     */
    public instant(key: string): string {
        return this.translate(key);
    }

    /**
     * Se comunica con el servicio para obtener las traducciones del sistema.
     * @return {Promise<any>}
     */
    public getTranslations(): Promise<any> {
        return this.http.get<ApiItemInterface>(this.userProvider.apiEndpoint() + "/users/" + this.userProvider.getUsername() + "/translations")
            .toPromise()
            .then(response => {
                return response.data;
            })
            .catch(error => {
                this.logService.logError(error, this.userProvider.getUser());
                this.handleError;
            });
    }

    /**
     * Retorna el locale del sistema deyel.
     */
    public getLanguaje() {
        this.initTranslations();
        if (this.translations && this.translations['LANGUAGE_ID']) {
            return this.translations['LANGUAGE_ID'] + '-' + this.translations['LOCALE_ID'];
        }
        return "";
    }

    /**
     * Log and rejects one error.
     * @param  {any}          error
     * @return {Promise<any>}
     */
    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }

    /**
     * Set the new translations list.
     * @param {Array<any>} translations
     */
    public setTranslations(translations: Array<any>): void {
      this.translations = translations;
    }

}
