import { Platform } from '@ionic/angular';
import { Injectable } from '@angular/core';
import { WebIntent } from '@ionic-native/web-intent/ngx';
import { FileTransfer } from '@ionic-native/file-transfer/ngx';
import { File } from '@ionic-native/file/ngx';
import { AlertController, LoadingController } from '@ionic/angular';
import { HttpClient, HttpHeaders, HttpBackend, HttpParams } from '@angular/common/http';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { TranslateService } from './translate.service';
import { UserProvider } from './user.service';
import { LogService } from './log.service';
import { AppConfigurationService } from './app-configuration.service';
import { AttachService } from './attach.service';

@Injectable()
export class VersionUpdateService {

    private downloadUrl: string;

    constructor(protected platform: Platform,
                protected fileTransfer: FileTransfer,
    			protected file: File,
    			protected webIntent: WebIntent,
                protected appVersion: AppVersion,
                protected httpBackend: HttpBackend,
    			protected loadingController: LoadingController,
    			protected alertController: AlertController,
                protected userProvider: UserProvider,
                protected translateService: TranslateService,
                protected appConfigurationService: AppConfigurationService,
                private androidPermissions: AndroidPermissions,
                private attachService: AttachService,
                protected logService: LogService) {}

    /**
     * Returns the url of the version update.
     * @return {string}
     */
    private getVersionUpdateUrl(): string {
        return this.userProvider.getContextUrl() + '/v1.0/apps/' + this.appConfigurationService.getAppUUID() + '/getUpdate';
    }

    /**
     * It returns the platform name.
     * @return {string}
     */
    private getPlatformName(): string {
        if (this.platform.is('android')) {
            return 'android';
        }
        return 'ios';
    }

    /**
     * Check if there is a new app version.
     */
    public checkAppUpdate(): void {
        try {
            if (this.platform.is('android')) {
                this.getUpgradeVersion().then((updateVersionObject: any) => {
                    this.processGetUpgradeResponse(updateVersionObject);
                })
                .catch((error) => {
                    this.logService.logError(error, this.userProvider.getUser());
                    console.error(error);
                });
            }
        } catch(error) {
            this.logService.logError(error, this.userProvider.getUser());
            console.error(error);
        }
    }

    /**
     * It process the response of the upgrade request,
     * @param {any} updateVersionObject
     */
    private processGetUpgradeResponse(updateVersionObject: any): void {
        if (updateVersionObject && !updateVersionObject.updated) {
            this.downloadUrl = updateVersionObject.url;
            this.showUpdateAlert();
        }
    }

    /**
     * It returns a promise of the upgrade version.
     * @return {Promise<any>}
     */
    private getUpgradeVersion(): Promise<any> {
        return this.appConfigurationService.getAppBuildVersion().then((appBuildVersion: string) => {
            const httpClient = new HttpClient(this.httpBackend);
            const httpOptions = {
                headers: new HttpHeaders({
                    'Authorization': `Bearer ${this.userProvider.getUser().getToken()}`,
                }),
                params: { appVersion: this.appConfigurationService.getAppVersion(), buildVersion: appBuildVersion, platform: this.getPlatformName()}
            };
            return httpClient.get(this.getVersionUpdateUrl(), httpOptions).toPromise();
        })
        .catch(error => {
            this.logService.logError(error, this.userProvider.getUser());
            console.error('Error', JSON.stringify(error));
        });
    }

    /**
     * Show the update alert.
     */
    private showUpdateAlert(): void {
        this.alertController.create({
            header: this.translateService.instant('NEW_UPDATE'),
            subHeader: this.translateService.instant('APPLICATION_NEEDS_TO_BE_UPDATED'),
            buttons: [ {
                text: this.translateService.instant('ACCEPT'),
                handler: () => {
                    this.showLoadingModal();
                }
            }]
        }).then(alert => {
            alert.present();
        });
    }

    /**
     * Show the download modal.
     */
    private showLoadingModal(): void {
        this.loadingController.create({
            spinner: 'dots',
            message: this.translateService.instant('DOWNLOADING_UPDATE') + '...'
        }).then(loader => {
            loader.present();
            this.downloadAPKBuild(loader);
        });
    }

    /**
     * Returns the intent configuration.
     * @param  {any} entry
     * @return {any}
     */
    private getIntentConfig(entry: any): any {
        return {
            action: this.webIntent.ACTION_VIEW,
            url: entry.toURL(),
            type: 'application/vnd.android.package-archive'
        };
    }

    /**
     * Download the APK file.
     * @param {any} loader
     */
    private downloadAPKBuild(loader: any): void {
        this.attachService.checkAndroidPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE)
            .then(result => {
                if (result) {
                    this.fileTransfer.create().download(this.downloadUrl, this.file.externalCacheDirectory + this.appConfigurationService.getAppName() + '.apk').then((entry) => {
                        loader.dismiss();
                        this.executeWebIntent(entry);
                    })
                    .catch((error) => {
                        loader.dismiss();
                        this.logService.logError(error, this.userProvider.getUser());
                        console.error(error);
                    });
                    return;
                }
                loader.dismiss();
            })
            .catch(error => {
                loader.dismiss();
                this.logService.logError(error, this.userProvider.getUser());
                console.error('Error', JSON.stringify(error));
            });
    }

    /**
     * Execute the WenIntent to install the new update.
     * @param {any} entry
     */
    private executeWebIntent(entry: any): void {
        this.webIntent.startActivity(this.getIntentConfig(entry)).then((response) => {
            if (response === 'OK') {
                navigator['app'].exitApp();
            }
        })
        .catch((error) => {
            this.logService.logError(error, this.userProvider.getUser());
            console.error(error);
        });
    }

}
