import {Injectable, NgZone} from '@angular/core';
import {Observable} from 'rxjs';
import {Subscription} from 'rxjs';
import {Subject} from 'rxjs';
import {getConnection, Repository} from 'typeorm';
import {HttpClient, HttpParams, HttpHeaders} from '@angular/common/http';
import {isUndefined} from 'util';

// models
import {Conversation, CONVERSATION_TYPE} from '../model/conversation.entity';
import {Message} from '../model/message.entity';
import {MessageAction} from '../model/message-action.entity';
import {MessageInput} from '../model/message-input.entity';
import {MessageButton} from '../model/message-button.entity';
import {Participant} from '../model/participant.entity';
import {Command} from '../model/command.entity';
import {Rule} from '../model/rule.entity';
import {EVENT_TYPE} from '../model/rtm-event.entity';
import {MessageActionRequest, MessageActionResponse} from '../model/message-action.entity';
import {Attach} from '../model/attach.entity';
import {ConversationUnread} from '../model/conversation-unread.entity';

// config
import {APP} from '../../app/app.config';

// helpers
import {ApiItemInterface} from '../support-classes/api-item-interface';

// providers
import {UserProvider} from './user.service';
import {AuthProvider} from './auth.service';
import {UpdateProvider} from './update.service';
import {DbProvider} from './db.service';
import {NotificationProvider} from './notification.service';

// services
import {ParticipantService} from './participant.service';
import {resolve} from 'url';
import {ConfigurationService} from './configuration.service';
import { LogService } from './log.service';


@Injectable()
export class ConversationApiService {

    constructor(private http: HttpClient,
                private userProvider: UserProvider,
                private participantService: ParticipantService,
                private logService: LogService) {
    }

    load(count): Promise<Conversation[]> {
        let c: Conversation [];
        return new Promise((resolve, reject) => {
            this.http.get<ApiItemInterface>(this.userProvider.apiEndpoint() + "/users/" + this.userProvider.getUsername() + "/conversations?count=" + count)
                .toPromise()
                .then(response => {
                    if (response.data) {
                        let data = response.data;
                        this.serializeConversations(data.lsConversations)
                            .then((conversations) => {
                                resolve(conversations);
                            });
                    }
                    else {
                        resolve(c);
                    }
                })
                .catch((error) => {
                    this.logService.logError(error, this.userProvider.getUser());
                    reject(error);
                });
        });
    }

    private preloadUsers(conversations: Array<any>): Promise<any> {
        let toSearch = new Set();
        for (let conversation of conversations) {
            for (let participant of conversation.lsParticipants) {
                toSearch.add(participant.cdParticipant);
            }
        }
        return this.participantService.getMany(Array.from(toSearch) as string[]);
    }

    /**
     * Convierte la estructura de la respuesta del backend de un arreglo de conversaciones, a un objeto serializable por la app frontend.
     * @param  {Array<any>}
     * @return {Array<Conversation>}
     */
    async serializeConversations(conversations: Array<any>): Promise<Conversation[]> {
        await this.preloadUsers(conversations);
        let conversationPromises: Promise<any>[] = [];
        for (let conversation of conversations) {
            const messages = this.serializeMessages(conversation.lsMessages);
            let conv = new Conversation().fromJSON(conversation).setMessages(messages);
            let participants: Participant [] = [];
            let promises: Promise<any>[] = [];
            for (let participant of conv.getLsParticipants()) {
                promises.push(this.participantService.getOne(participant.cdParticipant, participant.tpParticipant));
            }
            conversationPromises.push(
                Promise.all(promises)
                    .then((results) => {
                        for (let result of results) {
                            if (result) {
                                participants.push(result);
                            }
                        }
                        conv.setLsParticipants(participants);
                        if (conv && conv.cdType == CONVERSATION_TYPE.PRIVATE) {
                            let participantsExceptMe = conv.getParticipantsExceptMeByCd(this.userProvider.getUsername());
                            if (participantsExceptMe.length > 0) {
                                conv.setReceptor(participantsExceptMe[0]);
                            }
                        }
                        return conv;
                    }));
        }
        return Promise.all(conversationPromises);
    }

    /**
     * Convierte la estructura de la respuesta del backend de un mensaje, a un objeto serializable por la app frontend.
     * @param  {JSOArray<any>}
     * @return {Array<Message>}
     */
    serializeMessages(messages: Array<any>): Array<Message> {
        return messages.map(mensaje => {
            const messageButtons = this.serializeButtons(mensaje);
            const messageAttachs = this.serializeAttachs(mensaje);
            let msg = new Message().fromJSON(mensaje);
            msg.setLsAnnexs(messageAttachs);
            msg.setLsButtons(messageButtons);
            return msg;
        });
    }

    serializeAttachs(mensaje) {
        if (isUndefined(mensaje.lsAnnexs)) {
            return [];
        } else {
            return mensaje.lsAnnexs.map(attach => {
                let xAttach = new Attach().fromJSON(attach);
                xAttach.cdExtension = xAttach.dsName.substring(xAttach.dsName.lastIndexOf(".") + 1, xAttach.dsName.length).toLowerCase();
                return xAttach;
            });
        }
    }

    serializeButtons(mensaje) {
        if (mensaje.lsButtons == null) {
            mensaje.lsButtons = new Array<MessageButton>();
            return mensaje.lsButtons;
        }
        else {
            return mensaje.lsButtons.map(button => {
                let xButton = new MessageButton().fromJSON(button);
                return xButton;
            });
        }
    }

    updateTitle(c: Conversation, dsTitle: string): Promise<any> {
        c.dsTitle = dsTitle;
        return this.http.patch(this.userProvider.apiEndpoint() + "/users/" + this.userProvider.getUsername() + "/conversations/" + c.cdConversation, {dsTitle: c.dsTitle})
            .toPromise();
    }

    preSendMessage(message: Message) {
        if (!message.lsAnnexs) message.lsAnnexs = [];
        if (!message.lsActions) message.lsActions = [];
        if (!message.lsInputs) message.lsInputs = [];
        if (!message.lsButtons) message.lsButtons = [];
    }

    sendMessage(message: Message) {
        this.preSendMessage(message);
        return this.http.post<ApiItemInterface>(this.userProvider.apiEndpoint() + "/users/" + this.userProvider.getUsername() + "/conversations/" + message.cdConversation + "/messages", message)
            .toPromise()
            .then(response => {
                let message = new Message().fromJSON(response.data);
                const messageAttachs = this.serializeAttachs(message);
                return message.setLsAnnexs(messageAttachs);
            })
            .catch((error) => {
                this.logService.logError(error, this.userProvider.getUser());
                return new Message();
            });
    }

    getOne(cdConversation: number): Promise<Conversation> {
        return new Promise((resolve, reject) => {
            this.http.get<ApiItemInterface>(this.userProvider.apiEndpoint() + "/users/" + this.userProvider.getUsername() + "/conversations/" + cdConversation)
                .toPromise()
                .then(response => {
                    this.serializeConversations([response.data])
                        .then((conversations) => {
                            if (conversations && conversations.length > 0) {
                                resolve(conversations[0]);
                            }
                            else {
                                resolve(null);
                            }
                        })
                        .catch((error) => {
                            this.logService.logError(error, this.userProvider.getUser());
                            reject(error);
                        });
                })
                .catch(error => {
                    this.logService.logError(error, this.userProvider.getUser());
                    reject(error);
                });
        });
    }

    /**
     * Calls the api to get the object chat.
     * @param {string} objectId
     * @param {string} objectUrl
     * @param {string} relatedRuleName
     * @param {number} relatedRuleVersion
     * @param {string} objectType
     * @param {boolean} create
     * @param {string} title
     * @return {Promise<Conversation>}
     */
    public getObjectChat(objectId: string, objectUrl: string, relatedRuleName: string, relatedRuleVersion: number, objectType: string, create: boolean, title?: string): Promise<Conversation> {
        return new Promise((resolve, reject) => {
            this.http.get<ApiItemInterface>(this.userProvider.apiEndpoint() + '/users/' + this.userProvider.getUsername() + '/conversations?objectID=' + objectId + '&objectURL=' + objectUrl + '&relatedRuleName=' + relatedRuleName + '&relatedRuleVersion=' + relatedRuleVersion + '&typeObject=' + objectType + '&create=' + create)
                .toPromise()
                .then(response => {
                    this.serializeConversations([response.data])
                        .then((conversations) => {
                            if (conversations && conversations.length > 0) {
                                resolve(conversations[0]);
                            }
                            else {
                                resolve(null);
                            }
                        })
                        .catch((error) => {
                            this.logService.logError(error, this.userProvider.getUser());
                            reject(error);
                        });
                })
                .catch(error => {
                    this.logService.logError(error, this.userProvider.getUser());
                    reject(error);
                });
        });
    }

    getMinCdConversationElement(conversation) {
        let min = -1;
        for (let message of conversation.lsMessages) {
            if (min == -1 || message.cdConversationElement < min) {
                min = message.cdConversationElement;
            }
        }
        return min;
    }

    getConversationMessages(c: Conversation, count?: number, fromLast?: boolean): Promise<Message[]> {
        let query = "";
        let message: Message[] = [];
        if (count && fromLast) {
            let last = this.getMinCdConversationElement(c);
            if (last == 0) return new Promise((resolve) => {
                resolve([]);
            });
            query = "?count=" + count + ((last > -1) ? ("&cdConvElementFrom=" + last) : "");
        }
        else {
            if (count) {
                query = "?count=" + count;
            }
            else {
                if (fromLast) {
                    let last = this.getMinCdConversationElement(c);
                    if (last > -1) {
                        query = "?cdConvElementFrom=" + last;
                    }
                }
            }
        }
        return this.http.get<ApiItemInterface>(this.userProvider.apiEndpoint() + "/users/" + this.userProvider.getUsername() + "/conversations/" + c.cdConversation + "/messages" + query)
            .toPromise()
            .then(response => {
                return this.serializeMessages(response.data);
            })
            .catch(error => {
                this.logService.logError(error, this.userProvider.getUser());
                return Promise.reject(error);
            });
    }

    create(conversation: Conversation): Promise<Conversation> {
        return new Promise((resolve, reject) => {
            this.http.post<ApiItemInterface>(this.userProvider.apiEndpoint() + "/users/" + this.userProvider.getUsername() + "/conversations", conversation)
                .toPromise()
                .then(response => {
                    let conv = new Conversation().fromJSON(response.data);
                    conv.setMessages(this.serializeMessages(response.data.lsMessages));
                    this.postProcessConversation(conv)
                        .then(() => {
                            resolve(conv);
                        })
                        .catch((error) => {
                            this.logService.logError(error, this.userProvider.getUser());
                            reject(error);
                        });
                })
                .catch((error) => {
                    this.logService.logError(error, this.userProvider.getUser());
                    reject(error);
                });
        });
    }

    postProcessConversation(conversation: Conversation): Promise<Conversation> {
        return this.participantService.linkParticipants(conversation);
    }

}

@Injectable()
export class ConversationDbService {

    public messageStatesUpdate = new Subject<Message>();

    constructor(private dbProvider: DbProvider,
                private userProvider: UserProvider,
                private participantService: ParticipantService,
                private logService: LogService) {
    }

    update(conversation: Conversation) {
        return new Promise((resolve, reject) => {
            let conversationRepository = this.dbProvider.getRepository(Conversation) as Repository<Conversation>;
            conversationRepository.createQueryBuilder()
                .update(Conversation)
                .set(conversation.getPlainObject())
                .where('id = :id', { id: conversation.id })
                .execute()
                .then(() => {
                    this.participantService.addParticipantsRelationFor([conversation]).then(() => resolve(conversation));
                });
        });
    }

    insert(conversations: Conversation[], bulk: boolean = false) {
        return new Promise((resolve) => {
            let conversationRepository = this.dbProvider.getRepository(Conversation) as Repository<Conversation>;
            conversationRepository.createQueryBuilder()
                .insert()
                .into('conversation')
                .values(conversations)
                .execute()
                .then(() => {
                    this.participantService.addParticipantsRelationFor(conversations, bulk).then(() => {
                        console.log("Listo la promesa grande");
                        resolve();
                    });
                    console.log("Termino dentro del insert..");
                }, (error) => {
                    resolve();
                });
        });
    }

    insertConversationMessages(lsMessages: Message[], conversationId: number): Promise<Message[]> {
        let messageRepository = this.dbProvider.getRepository(Message) as Repository<Message>;
        for (let message of lsMessages) {
            message.conversationId = conversationId;
        }
        return new Promise((resolve) => {
            messageRepository.createQueryBuilder()
                .insert()
                .into('message')
                .values(lsMessages)
                .execute()
                .then(() => {
                    this.insertMsgButtonsActionsInputs(lsMessages);
                    this.insertAttach(lsMessages)
                        .then(() => {
                            resolve(lsMessages);
                        })
                        .catch((error) => {
                            this.logService.logError(error, this.userProvider.getUser());
                            resolve(lsMessages);
                        });
                })
                .catch((error) => {
                    this.logService.logError(error, this.userProvider.getUser());
                    console.log("ERROR en insertConversationMessages", JSON.stringify(error));
                });
        });
    }

    getConversationMessages(conversation: Conversation, countNeedMessages: number, fromLast?: boolean): Promise<Message[]> {
        let where = "message.conversationId = :conversationId";
        let params: any = {conversationId: conversation.id};
        let repositoryMessages = this.dbProvider.getRepository(Message) as Repository<Message>;
        let last = conversation.lsMessages.length > 0 ? conversation.lsMessages[0] : null;
        if (last) {
            where += " and message.dtLastUpdate < :dtLastUpdate";
            params = {conversationId: conversation.id, dtLastUpdate: last.dtLastUpdate};
        }
        return repositoryMessages.createQueryBuilder("message")
            .take(countNeedMessages)
            .leftJoinAndSelect("message.lsButtons", "button")
            .leftJoinAndSelect("message.lsInputs", "input")
            .leftJoinAndSelect("message.lsActions", "action")
            .leftJoinAndSelect("message.lsAnnexs", "annex")
            .where(where)
            .orderBy("message.dtLastUpdate", "DESC")
            .addOrderBy("action.id")
            .setParameters(params)
            .getMany();
    }

    insertAttach(messages: Message[]): Promise<Attach[]> {
        let attachs: Attach[] = [];
        for (let message of messages) {
            if (message.lsAnnexs){
                for (let attch of message.lsAnnexs) {
                    attch.message = message;
                    attachs.push(attch);
                }
            }
        }
        return new Promise((resolve) => {
            this.dbProvider.getRepository(Attach).createQueryBuilder()
                .insert()
                .into('attach')
                .values(attachs)
                .execute()
                .then(() => {
                    resolve();
                });
        });
    }

    insertMsgButtonsActionsInputs(messages: Message[]): void {
        let buttons: MessageButton[] = [];
        let actions: MessageAction[] = [];
        let inputs: MessageInput[] = [];
        for (let message of messages) {
            if (message.lsButtons){
                for (let button of message.lsButtons) {
                    button.message = message;
                    buttons.push(button);
                }
            }
            if (message.lsActions){
                for (let action of message.lsActions) {
                    action.message = message;
                    actions.push(action);
                }
            }
            if (message.lsInputs){
                for (let input of message.lsInputs) {
                    input.message = message;
                    inputs.push(input);
                }
            }
        }
        this.dbProvider.getRepository(MessageButton).createQueryBuilder()
            .insert()
            .into('message_button')
            .values(buttons)
            .execute();
        this.dbProvider.getRepository(MessageAction).createQueryBuilder()
            .insert()
            .into('message_action')
            .values(actions)
            .execute();
        this.dbProvider.getRepository(MessageInput).createQueryBuilder()
            .insert()
            .into('message_input')
            .values(inputs)
            .execute();
    }

    deleteMsgButtonsActionsInputs(message: Message){
        return new Promise((resolve) => {
            let lsButtonsIds = message.lsButtons ? message.lsButtons.map(button => button.id) : [];
            let lsActionsIds = message.lsActions ? message.lsActions.map(action => action.id) : [];
            let lsInputsIds = message.lsInputs ? message.lsInputs.map(input => input.id) : [];
            let promises: Promise<any>[] = [];
            let promiseDeleteButtons = this.dbProvider.getRepository(MessageButton).createQueryBuilder()
                .delete()
                .from(MessageButton)
                .where('id IN (:...ids)', { ids: lsButtonsIds })
                .execute();
            promises.push(promiseDeleteButtons);
            let promiseDeleteActions = this.dbProvider.getRepository(MessageAction).createQueryBuilder()
                .delete()
                .from(MessageAction)
                .where('id IN (:...ids)', { ids: lsActionsIds })
                .execute();
            promises.push(promiseDeleteActions);
            let promiseDeleteInputs = this.dbProvider.getRepository(MessageInput).createQueryBuilder()
                .delete()
                .from(MessageInput)
                .where('id IN (:...ids)', { ids: lsInputsIds })
                .execute();
            promises.push(promiseDeleteInputs);
            Promise.all(promises)
                .then(_ => {
                    resolve();
                });
        });
    }

    getOne(cdConversation: number) {
        let conversationRepository = this.dbProvider.getRepository(Conversation) as Repository<Conversation>;
        return conversationRepository.findOne({
            where: {userId: this.userProvider.getUser().id, cdConversation: cdConversation},
            join: {
                alias: "conversation",
                innerJoinAndSelect: {"lsParticipants": "conversation.lsParticipants", "lsMessages": "conversation.lsMessages"},
                leftJoinAndSelect: {"receptor": "conversation.receptor"},
            }
        });
    }

    deleteMessage(message: Message): Promise<any> {
        return new Promise((resolve, reject) => {
            this.dbProvider.getConnection().createQueryBuilder()
                .delete().from(MessageInput)
                .where('message.id =' + message.id).execute()
                .then(_ => {
                    this.dbProvider.getConnection().createQueryBuilder()
                        .delete().from(MessageButton)
                        .where('message.id =' + message.id).execute()
                        .then(_ => {
                            this.dbProvider.getConnection().createQueryBuilder()
                                .delete().from(MessageAction)
                                .where('message.id =' + message.id).execute()
                                .then(_ => {
                                    this.dbProvider.getConnection().createQueryBuilder()
                                        .delete().from(Attach)
                                        .where('message.id =' + message.id).execute()
                                        .then(_ => {
                                            this.dbProvider.getConnection().createQueryBuilder()
                                                .delete().from(Message)
                                                .where('id =' + message.id).execute()
                                                .then(_ => resolve())
                                                .catch(error => {
                                                    this.logService.logError(error, this.userProvider.getUser());
                                                    reject();
                                                });
                                        }).catch(error => {
                                            this.logService.logError(error, this.userProvider.getUser());
                                            reject();
                                        });
                                }).catch(error => {
                                    this.logService.logError(error, this.userProvider.getUser());
                                    reject();
                                });
                        }).catch(error => {
                            this.logService.logError(error, this.userProvider.getUser());
                            reject();
                        });
                }).catch(error => {
                    this.logService.logError(error, this.userProvider.getUser());
                    reject();
                });
        });
    }

    delete(conversation: Conversation) {
        return new Promise((resolve, reject) => {
            this.dbProvider.getRepository(Message).createQueryBuilder()
                .where("conversationId = :conversationId", {conversationId: conversation.id}).getMany()
                .then((messages) => {
                    if (messages.length > 0) {
                        let promises: Promise<any>[] = [];
                        for (let m of messages) {
                            promises.push(this.deleteMessage(m));
                        }
                        Promise.all(promises)
                            .then(_ => {
                                this.dbProvider.getRepository(Conversation).remove(conversation)
                                    .then(() => {
                                        this.participantService.removeAllParticipantsRelationFor(conversation).then(() => resolve());
                                    })
                                    .catch((error) => {
                                        this.logService.logError(error, this.userProvider.getUser());
                                        reject(error);
                                    });
                            });
                    } else {
                        this.dbProvider.getRepository(Conversation).remove(conversation)
                            .then(() => {
                                this.participantService.removeAllParticipantsRelationFor(conversation).then(() => resolve());
                            })
                            .catch((error) => {
                                this.logService.logError(error, this.userProvider.getUser());
                                reject(error);
                            });
                    }
                })
                .catch((error) => {
                    this.logService.logError(error, this.userProvider.getUser());
                    reject(error);
                });
        });
    }

    removeConversationsByUser(userId: number): Promise<any> {
        return new Promise((resolve, reject) => {
            this.dbProvider.getRepository(Conversation).createQueryBuilder("Conversation")
                .where(' userId = ' + userId)
                .getMany().then(
                (conversations) => {
                    let promises: Promise<any>[] = [];
                    for (let c of conversations) {
                        promises.push(this.delete(c));
                    }
                    Promise.all(promises)
                        .then(_ => {
                            resolve()
                        });
                });
        });
    }

    getMessage(cdConversation: number, cdMessage: number) {
        let repositoryMessages = this.dbProvider.getRepository(Message) as Repository<Message>;
        return repositoryMessages.createQueryBuilder("message")
            .innerJoin(Conversation, "c", "c.id = message.conversationId")
            .where("c.cdConversation = :cdConversation AND cdConversationElement = :cdConversationElement")
            .setParameters({cdConversation: cdConversation, cdConversationElement: cdMessage})
            .getOne();
    }

}

@Injectable()
export class ConversationService {
    private newMessage = new Subject<boolean>();

    //Used in load more chats
    countChatsPushed: number = 0;
    countChatsDeleted: number = 0;

    NU_REQ_MESSAGES = 10;
    NU_REQ_CONVERSATIONS = 1000;

    newMessage$ = this.newMessage.asObservable();

    conversations: Conversation[] = [];

    allChatsLoaded = false;

    private actualConversation: Conversation = null;

    messageStatesUpdate$: Observable<Message>;
    messageStatesUpdateSubscription: Subscription;

    private conversationUpdated = new Subject<Conversation>();
    conversationUpdated$ = this.conversationUpdated.asObservable();

    private onSearchChats = new Subject<string>();
    onSearchChats$ = this.onSearchChats.asObservable();

    constructor(private participantService: ParticipantService,
                private http: HttpClient,
                private ngZone: NgZone,
                private userProvider: UserProvider,
                private authProvider: AuthProvider,
                private dbProvider: DbProvider,
                private notificationProvider: NotificationProvider,
                public  api: ConversationApiService,
                public  db: ConversationDbService,
                private configurationService: ConfigurationService,
                private updateProvider: UpdateProvider,
                private logService: LogService) {
        this.messageStatesUpdate$ = this.db.messageStatesUpdate.asObservable();
        this.messageStatesUpdateSubscription = this.messageStatesUpdate$.subscribe(message => {
            // this.onMessageStatesUpdate(message);
        });
    }

    load() {
        return new Promise((resolve, reject) => {
            this.api.load(this.NU_REQ_CONVERSATIONS)
                .then(results => {
                    if (results) {
                        for (let conversation of results) {
                            conversation.userId = this.userProvider.getUser().id;
                            conversation.synchronized = true;
                            conversation.dtLastChange = conversation.dtLastUpdate;
                            if (conversation.lsMessages.length > 0) {
                                conversation.dtLastChange = conversation.lsMessages[0].dtLastUpdate;
                            }
                        }
                        console.log("inserting "+results.length+" chats...");
                        return this.db.insert(results, true)
                            .then(() => {
                                console.log("finalizó la inserción de los chats");
                                this.conversations = results;
                                let messages: Message[] = [];
                                for (let conversation of this.conversations) {
                                    for (let message of conversation.lsMessages) {
                                        message.conversationId = conversation.id;
                                        messages.push(message);
                                    }
                                }
                                let messageRepository = this.dbProvider.getRepository(Message) as Repository<Message>;
                                this.allChatsLoaded = true;
                                messageRepository.createQueryBuilder()
                                    .insert()
                                    .into('message')
                                    .values(messages)
                                    .execute()
                                    .then(() => {
                                        this.db.insertMsgButtonsActionsInputs(messages);
                                        this.conversations.splice(20, this.conversations.length);
                                        this.db.insertAttach(messages);
                                        resolve();
                                    });
                            });
                    }
                    else {
                        resolve();
                    }
                });
        });
    }

    getLocalConversationById(cdConversation: number): Conversation {
        for (let c of this.conversations) {
            if (c.cdConversation === cdConversation) {
                return c;
            }
        }
        return null;
    }

    getConversationByParticipant(participant: Participant, addToDbIfNotExists = true): Promise<Conversation> {
        return new Promise<Conversation>((resolve, reject) => {
            this.getLocalConversationByParticipant(participant)
                .then(conversation => {
                    if (conversation != null || !addToDbIfNotExists) {
                        resolve(conversation);
                    }
                    else {
                        this.insertConversationInDb([participant])
                            .then(xResult => {
                                xResult.setReceptor(participant);
                                this.addConversation(xResult);
                                resolve(xResult);
                            })
                            .catch(error => {
                                this.logService.logError(error, this.userProvider.getUser());
                                reject(error);
                            });
                    }
                })
                .catch(error => {
                    this.logService.logError(error, this.userProvider.getUser());
                    reject(error);
                });
        });
    }

    getLocalConversationByParticipant(p: Participant): Promise<Conversation> {
        return new Promise((resolve, reject) => {
            let conversation: Conversation = null;
            for (let c of this.conversations) {
                if (c && c.cdType === CONVERSATION_TYPE.PRIVATE && c.getParticipantsExceptMeByCd(this.userProvider.getUsername())[0].cdParticipant == p.cdParticipant && c.lsMessages && c.lsMessages.length > 0) {
                    conversation = c;
                    resolve(c)
                }
            }
            if (!conversation) {
                this.countChatsPushed++;
                resolve(this.getLocalConversationByCdParticipantInDb(p));
            }
        });

    }

    private getConversationIndex(cdConversation: number): number {
        for (let i in this.conversations) {
            if (this.conversations[i].cdConversation === cdConversation) {
                return Number(i);
            }
        }
        return -1;
    }

    private getConversationIndexById(id: number): number {
        for (let i in this.conversations) {
            if (this.conversations[i].id === id) {
                return Number(i);
            }
        }
        return -1;
    }

    public removeConversation(cdConversation: number): boolean {
        let i = this.getConversationIndex(cdConversation);
        if (i > -1) {
            this.conversations.splice(i, 1);
            return true;
        }
        return false;
    }

    public updateConversationInMemory(conversation): boolean {
        let i = -1;
        if (conversation.id) {
            i = this.getConversationIndexById(conversation.id);
        }
        if (i == -1 && conversation.cdConversation) {
            i = this.getConversationIndex(conversation.cdConversation);
        }
        if (i > -1) {
            this.conversations[i].dtLastUpdate = conversation.dtLastUpdate;
            this.conversations[i].cdConversation = conversation.cdConversation;
            this.conversations[i].synchronized = conversation.synchronized;
            this.conversations[i].dtLastChange = conversation.dtLastChange;
            return true;
        }
        return false;
    }

    public updateMessageInMemory(m: Message) {
        let c = this.getLocalConversationByIdFromDb(m.conversationId);
        if (c) {
            for (let msg of c.lsMessages) {
                if (msg.id === m.id) {
                    msg.cdConversationElement = m.cdConversationElement;
                    msg.lsButtons = m.lsButtons;
                    msg.lsActions = m.lsActions;
                    msg.lsInputs = m.lsInputs;
                    msg.dsContent = m.dsContent;
                    msg.lsAnnexs = m.lsAnnexs;
                }
            }
        }
    }

    addConversation(c: Conversation) {
        if (!c.cdConversation) {
            this.addConversationDisconnected(c);
        }
        if (!this.getLocalConversationById(c.cdConversation)) {
            this.conversations.unshift(c);
            this.dbProvider.getRepository(Conversation).createQueryBuilder()
                .update(Conversation)
                .set(c.getPlainObject())
                .where('id = :id', { id: c.id })
                .execute();
        } else {
            this.markConversationAsReaded(c);
            const index = this.conversations.findIndex(conv => c.cdConversation === c.cdConversation);
            this.conversations.splice(index, 1, c);
        }
    }

    addConversationDisconnected(c: Conversation) {
        if (!this.getLocalConversationByIdFromDb(c.id)) {
            this.conversations.unshift(c);
        } else {
            const index = this.conversations.findIndex(conv => c.id === c.id);
            this.conversations.splice(index, 1, c);
        }
    }

    getOne(cdConversation: number, addMessagSync?: boolean, searchInApi = true): Promise<Conversation> {
        let c = this.getLocalConversationById(cdConversation);
        if (c) {
            return new Promise((resolve) => {
                return resolve(c);
            });
        }
        else {
            return new Promise((resolve, reject) => {
                this.db.getOne(cdConversation)
                    .then((conversation) => {
                        if (!conversation && searchInApi) {
                            if (!this.userProvider.inSync || (addMessagSync)) {
                                this.api.getOne(cdConversation)
                                    .then((conv) => {
                                        conv.synchronized = true;
                                        conv.dtLastChange = conv.dtLastUpdate;
                                        conv.userId = this.userProvider.getUser().id;
                                        this.db.insert([conv])
                                            .then(() => {
                                                conv.lsMessages = [];
                                                conv.nuUnreaded = 0;
                                                resolve(conv);
                                            })
                                            .catch((error) => {
                                                this.logService.logError(error, this.userProvider.getUser());
                                                reject(error);
                                            })
                                    })
                                    .catch((error) => {
                                        this.logService.logError(error, this.userProvider.getUser());
                                        reject(error);
                                    });
                            } else {
                                resolve(null);
                            }
                        }
                        else {
                            resolve(conversation);
                        }
                    });
            });
        }
    }

    addMessage(message: Message, participant: Participant, addParticipant: boolean, dtLastChange?: number, notUpdateNuUnreaded?: boolean, notNotification?: boolean, addMessageSync?: boolean): Promise<Conversation> {
        return new Promise((resolve, reject) => {
            this.getOne(message.cdConversation, addMessageSync)
                .then((conversation) => {
                    if (conversation) {
                        if (participant && addParticipant && conversation.hasParticipant(participant.cdParticipant)) {
                            resolve(conversation);
                        }
                        else {
                            message.conversationId = conversation.id;
                            console.log("addMessage", message.msgState);
                            if (message.msgState != 'SENT') notNotification = true;
                            if (!notNotification) this.notificationProvider.newMessage(conversation, message);
                            this.removeConversation(conversation.cdConversation);
                            let saveConversation = false;
                            console.error(this.userProvider.cdCurrentConversation);
                            if (conversation.cdConversation != this.userProvider.cdCurrentConversation) {
                                console.error(message.cdParticipant);
                                if (!notUpdateNuUnreaded && message.msgState == 'SENT' && message.cdParticipant != this.userProvider.getUsername().toUpperCase()) {
                                    conversation.nuUnreaded++;
                                    saveConversation = true;
                                }
                            }
                            else if (!participant && !addParticipant) {
                                this.updateConversationUnreadeds(conversation)
                                    .then()
                                    .catch((error) => {
                                        this.logService.logError(error, this.userProvider.getUser());
                                        console.error("Update unreaded, error.", JSON.stringify(error));
                                    })
                            }
                            conversation.lsMessages.push(message);
                            if (dtLastChange) conversation.dtLastChange = dtLastChange;
                            this.addConversation(conversation);
                            this.newMessage.next(true);
                            let messageRepository = this.dbProvider.getRepository(Message) as Repository<Message>;
                            messageRepository.createQueryBuilder()
                                .insert()
                                .into('message')
                                .values(message)
                                .execute()
                                .then(() => {
                                    this.db.insertMsgButtonsActionsInputs([message]);
                                    this.db.insertAttach([message]);
                                    if (participant) {
                                        if (addParticipant) {
                                            if (!conversation.hasParticipant(participant.cdParticipant)) {
                                                this.participantService.getOne(participant.cdParticipant, participant.tpParticipant).then((participant) => {
                                                    if (participant) {
                                                        console.warn("agrego participante a la conv")
                                                        conversation.lsParticipants.push(participant);
                                                        if (conversation.blocked && participant.cdParticipant == this.userProvider.getUsername().toUpperCase()) {
                                                            conversation.blocked = false;
                                                        }
                                                        this.dbProvider.getRepository(Conversation).createQueryBuilder()
                                                            .update(Conversation)
                                                            .set(conversation.getPlainObject())
                                                            .where('id = :id', { id: conversation.id })
                                                            .execute()
                                                            .then(() => {
                                                                this.participantService.addParticipantsRelationFor([conversation]).then(() => {
                                                                    resolve(conversation);
                                                                });
                                                            })
                                                            .catch((error) => {
                                                                this.logService.logError(error, this.userProvider.getUser());
                                                                conversation.lsMessages.pop();
                                                                resolve(conversation);
                                                            });
                                                    }
                                                    else {
                                                        resolve(conversation);
                                                    }
                                                });
                                            }
                                            else {
                                                conversation.lsMessages.pop();
                                                resolve(conversation);
                                            }
                                        }
                                        else {
                                            if (conversation.hasParticipant(participant.cdParticipant)) {
                                                conversation.removeParticipant(participant.cdParticipant);
                                                conversation.blocked = participant.cdParticipant == this.userProvider.getUsername().toUpperCase();
                                                console.warn('remove participant');
                                            } else {
                                                console.warn('pop remove participant');
                                                conversation.lsMessages.pop();
                                            }
                                            this.participantService.removeParticipantRelationFor(conversation, participant).then(() => {
                                                resolve(conversation);
                                            }).catch((error) => {
                                                this.logService.logError(error, this.userProvider.getUser());
                                                reject(error);
                                            });
                                        }
                                    }
                                    else {
                                        // this.dbProvider.getRepository(Conversation).save(conversation)
                                        // .then((conversation) =>
                                        resolve(conversation);
                                        // )
                                        //   .catch((err) => error(err));
                                    }
                                })
                                .catch((error) => {
                                    this.logService.logError(error, this.userProvider.getUser());
                                    reject(error);
                                });
                        }
                    }
                    else {
                        resolve(conversation);
                    }
                })
                .catch((error) => {
                    this.logService.logError(error, this.userProvider.getUser());
                    reject(error);
                });
        });
    }

    existMessage(cdConversationElement: number, cdConversation: number) {
        return this.dbProvider.getRepository(Message).createQueryBuilder("m")
            .innerJoinAndSelect(Conversation, "c", "c.id = m.conversationId")
            .where("c.userId = " + this.userProvider.getUser().id + " and m.cdConversationElement = " + cdConversationElement + " and m.cdConversation = " + cdConversation)
            .getRawOne();
    }

    markConversationAsReaded(c: Conversation): void {
        if (c.nuUnreaded > 0) {
            this.updateConversationUnreadeds(c)
                .then()
                .catch(error => {
                    this.logService.logError(error, this.userProvider.getUser());
                    console.log("error al marcar como leido!", JSON.stringify(error));
                });
            ;
        }
    }

    updateMessageStates(dsData: any) {
        return new Promise((resolve, reject) => {
            this.getOne(dsData.cdConversation, null, false)
                .then((conversation) => {
                    if (conversation) {
                        for (let state of dsData.lsMessageStates) {
                            if (state.msgState != 'DELETED') {
                                this.dbProvider.getConnection()
                                    .createQueryBuilder()
                                    .update(Message)
                                    .set({msgState: state.msgState})
                                    .where("conversationId = :conversationId and cdConversationElement = :cdConversationElement", {
                                        conversationId: conversation.id,
                                        cdConversationElement: state.cdConversationElement
                                    })
                                    .execute()
                                    .then(() => {
                                        if (dsData.cdUser == this.userProvider.getUsername().toUpperCase()) {
                                            conversation.nuUnreaded = 0;
                                            conversation.dtLastChange = state.dtLastUpdate;
                                            this.dbProvider.getRepository(Conversation).createQueryBuilder()
                                                .update(Conversation)
                                                .set(conversation.getPlainObject())
                                                .where('id = :id', { id: conversation.id })
                                                .execute();
                                        }
                                        let m = conversation.getMessageById(state.cdConversationElement);
                                        if (m) {
                                            m.setMsgState(state.msgState);
                                        }
                                    })
                            } else {
                                for (let i = 0; i < conversation.lsMessages.length; i++) {
                                    if (conversation.lsMessages[i].cdConversationElement == state.cdConversationElement) {
                                        this.db.deleteMessage(conversation.lsMessages[i])
                                            .then(_ => {
                                                conversation.lsMessages.splice(i, 1);
                                                if (conversation.lsMessages.length == 0) this.removeConversation(conversation.cdConversation);
                                            });
                                    }
                                }
                            }
                        }
                    }
                    resolve();
                });
        })
    }

    newConversationParticipant(p: Participant, m: Message, dtLastUpdate: number) {
        return this.addMessage(m, p, true, dtLastUpdate);
    }

    removeConversationParticipant(p: Participant, m: Message, dtLastUpdate: number) {
        return this.addMessage(m, p, false, dtLastUpdate);
    }

    triggerUserTyping(cdConversation: number, cdParticipant: string) {
        let c = this.getLocalConversationById(cdConversation);
        if (c) {
            if (!c.lsUsersTyping) {
                c.lsUsersTyping = [];
            }
            for (let p of c.lsUsersTyping) {
                if (p.cdParticipant == cdParticipant) {
                    return;
                }
            }
            c.lsUsersTyping.push(c.getParticipantByCd(cdParticipant));
            let $c = c;
            setTimeout(function () {
                $c.lsUsersTyping = [];
            }, 3000);
        }
    }

    triggerUserStopTyping(cdConversation: number, cdParticipant: string) {
        let c = this.getLocalConversationById(cdConversation);
        if (c) {
            if (!c.lsUsersTyping) {
                c.lsUsersTyping = [];
            }
            for (let i in c.lsUsersTyping) {
                if (c.lsUsersTyping[i].cdParticipant == cdParticipant) {
                    c.lsUsersTyping.splice(Number(i), 1);
                    return;
                }
            }
        }
    }

    updateLocalMessage(m: Message) {
        return new Promise((resolve, reject) => {
            let c = this.getLocalConversationById(m.cdConversation);
            if (c != null) {
                for (let i in c.lsMessages) {
                    if (c.lsMessages[i].cdConversationElement == m.cdConversationElement) {
                        let message = c.lsMessages[i];
                        message.dsContent = m.dsContent;
                        this.db.deleteMsgButtonsActionsInputs(message).then(() => {
                            message.lsActions = m.lsActions;
                            message.lsButtons = m.lsButtons;
                            message.lsInputs = m.lsInputs;
                            message.lsAnnexs = m.lsAnnexs;
                            this.db.insertMsgButtonsActionsInputs([message]);
                            this.dbProvider.getRepository(Message).createQueryBuilder()
                                .update(Message)
                                .set(message.getPlainObject())
                                .where('id = :id', { id: message.id })
                                .execute()
                                .then(() => {
                                    c.lsMessages[i] = message;
                                    resolve();
                                });
                        });
                    }
                }
            }
        });
    }

    removeLocalMessage(m: Message): Promise<any> {
        return new Promise((resolve, reject) => {
            let c = this.getLocalConversationById(m.cdConversation);
            if (c != null) {
                for (let i = 0; i < c.lsMessages.length; i++) {
                    if (c.lsMessages[i].cdConversationElement == m.cdConversationElement) {
                        this.db.deleteMessage(c.lsMessages[i])
                            .then(_ => {
                                c.lsMessages.splice(i, 1);
                                if (c.lsMessages.length == 0) this.removeConversation(c.cdConversation);
                                resolve();
                            })
                            .catch((error) => {
                                this.logService.logError(error, this.userProvider.getUser());
                                reject(error);
                            });
                    }
                }
            }
        });
    }

    //metodos para recuperar info del API

    updateTitle(conversation: Conversation, dsTitle: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.api.updateTitle(conversation, dsTitle)
                .then(() => {
                    this.db.update(conversation)
                        .then((conversation) => {
                            resolve();
                        })
                        .catch((error) => {
                            this.logService.logError(error, this.userProvider.getUser());
                            reject(error);
                        });
                });
        });
    }

    updateConversationUnreadedsPatch(cdConversation: number, dtLastMsg?: number): Promise<any> {
        return this.http.patch<ApiItemInterface>(this.userProvider.apiEndpoint() + "/users/" + this.userProvider.getUsername() + "/conversations/" + cdConversation + "/messages" + (dtLastMsg ? ("?dtLastMsg=" + dtLastMsg) : ""), "").toPromise();
    }

    updateConversationUnreadeds(c: Conversation): Promise<any> {
        return new Promise((resolve, reject) => {
            c.nuUnreaded = 0;
            this.dbProvider.getRepository(Conversation).createQueryBuilder()
                .update(Conversation)
                .set(c.getPlainObject())
                .where('id = :id', { id: c.id })
                .execute()
                .then(() => {
                    this.dbProvider.getConnection()
                        .createQueryBuilder()
                        .update(Message)
                        .set({msgState: 'SEEN'})
                        .where("cdConversation = :cdConversation")
                        .andWhere("msgState = 'SENT'")
                        .andWhere("cdParticipant != :userName")
                        .setParameters({
                            cdConversation: c.cdConversation,
                            userName: this.userProvider.getUsername().toUpperCase()
                        })
                        .execute()
                        .then(() => {
                            if (!this.userProvider.inSync) {
                                this.updateConversationUnreadedsPatch(c.cdConversation)
                                    .then(response => {
                                        resolve();
                                    })
                                    .catch((error) => {
                                        this.logService.logError(error, this.userProvider.getUser());
                                        this.saveConversationUnread(c);
                                    });
                            } else {
                                this.saveConversationUnread(c);
                            }
                        })
                        .catch((error) => {
                            this.logService.logError(error, this.userProvider.getUser());
                            reject(error);
                        });
                })
                .catch((error) => {
                    this.logService.logError(error, this.userProvider.getUser());
                    reject(error);
                });
        });
    }

    saveConversationUnread(c) {
        let messageRepository = this.dbProvider.getRepository(Message) as Repository<Message>;
        messageRepository.createQueryBuilder("message")
            .where('message.conversationId = :conversationId')
            .orderBy('message.cdConversationElement', 'DESC')
            .setParameters({conversationId: c.id})
            .getOne()
            .then((message) => {
                let conversationUnread = null;
                if (message && message.cdConversationElement) {
                    conversationUnread = new ConversationUnread();
                    conversationUnread.cdConversation = c.cdConversation;
                    conversationUnread.dtLastMsg = message.dtStore;
                    conversationUnread.userId = this.userProvider.getUser().id;
                    conversationUnread.createdAt = new Date().getTime();
                    let conversationUnreadRepository = this.dbProvider.getRepository(ConversationUnread) as Repository<ConversationUnread>;
                    conversationUnreadRepository.createQueryBuilder()
                        .insert()
                        .into('conversation_unread')
                        .values(conversationUnread)
                        .execute();
                }
            });
    }

    sendMessage(message: Message): Promise<Message> {
        return this.api.sendMessage(message);
    }

    getConversations(): Conversation[] {
        return this.conversations;
    }

    insertConversationInDb(participants: Participant[], title?: string): Promise<Conversation> {
        return new Promise((resolve, reject) => {
            let c = new Conversation();
            this.participantService.getOne(this.userProvider.getUsername().toUpperCase(), "USER")
                .then((creatorUser) => {
                    participants.push(creatorUser);
                    c.setParticipants(participants);
                    c.cdType = participants.length > 2 ? CONVERSATION_TYPE.GROUP : CONVERSATION_TYPE.PRIVATE;
                    c.setDtLastUpdate(new Date().getTime());
                    c.userId = this.userProvider.getUser().id;
                    c.cdUserCreator = this.userProvider.getUsername().toUpperCase();
                    if (title) {
                        c.dsTitle = title;
                    }
                    if (c && c.cdType == CONVERSATION_TYPE.GROUP) {
                        c.dsImage = APP.DEFAULT_GROUP_IMAGE;
                    }
                    c.dtLastUpdate = new Date().getTime();
                    c.dtLastChange = new Date().getTime();
                    let conversationRepository = this.dbProvider.getRepository(Conversation) as Repository<Conversation>;
                    conversationRepository.createQueryBuilder()
                        .insert()
                        .into('conversation')
                        .values(c)
                        .execute()
                        .then(() => {
                            this.participantService.addParticipantsRelationFor([c]);
                            if (!this.userProvider.inSync) {
                                return this.sendConversation(c)
                                    .then((conv) => {
                                        if (c && c.cdType == CONVERSATION_TYPE.GROUP) {
                                            let msgCreate = new Message();
                                            msgCreate.conversationId = c.id;
                                            msgCreate.dsContent = creatorUser.dsParticipant + " creó la conversación " + c.dsTitle;
                                            msgCreate.dtLastUpdate = c.dtLastChange;
                                            msgCreate.cdConversationElement = 0;
                                            this.dbProvider.getRepository(Message).createQueryBuilder()
                                                .insert()
                                                .into('message')
                                                .values(msgCreate)
                                                .execute()
                                                .then(
                                                    () => {
                                                        this.db.insertMsgButtonsActionsInputs([msgCreate]);
                                                        resolve(conv);
                                                        c.lsMessages.push(msgCreate);
                                                        this.logService.logDevelopment('Seteo nuevo dtLastUpdate insertConversationInDB', this.userProvider.getUser());
                                                        this.updateProvider.dtLastUpdate = new Date().getTime();
                                                        this.userProvider.saveUpdate();
                                                    }
                                                );
                                        }
                                        else {
                                            resolve(conv);
                                        }
                                    }).catch((error) => {
                                        this.logService.logError(error, this.userProvider.getUser());
                                        reject(error);
                                    });
                            }
                            else {
                                resolve(c);
                            }
                        })
                        .catch(error => {
                            this.logService.logError(error, this.userProvider.getUser());
                            reject(error);
                        });
                })
                .catch((error) => {
                    this.logService.logError(error, this.userProvider.getUser());
                    reject(error);
                });
        });
    }

    deleteConversationHistory(c: Conversation): Promise<any> {
        return this.http.delete(this.userProvider.apiEndpoint() + "/users/" + this.userProvider.getUsername() + "/conversations/" + c.cdConversation + "/messages")
            .toPromise()
            .then(response => {
                c.lsMessages = [];
                this.countChatsDeleted++;
                this.userProvider.cdCurrentConversation = null;
                this.removeConversationInDb(c);
            })
            .catch(error => {
                this.logService.logError(error, this.userProvider.getUser());
                this.handleError;
            });
    }

    addConversationParticipants(conversation: Conversation, lsParticipants: Participant[]): Promise<any> {
        return new Promise((resolv, reject) => {
            this.http.post(this.userProvider.apiEndpoint() + "/users/" + this.userProvider.getUsername() + "/conversations/" + conversation.cdConversation + "/participants", {lsParticipants: this.participantService.getShortLsParticipants(lsParticipants)})
                .toPromise()
                .then((result) => {
                    resolv(conversation);
                })
                .catch((error) => {
                    this.logService.logError(error, this.userProvider.getUser());
                    reject(error);
                })
        });
    }

    removeConversationParticipants(c: Conversation, lsParticipants: Participant[]): Promise<any> {
        return this.http.request("delete", this.userProvider.apiEndpoint() + "/users/" + this.userProvider.getUsername() + "/conversations/" + c.cdConversation + "/participants", {
            body: {lsParticipants: this.participantService.getShortLsParticipants(lsParticipants)}
        })
            .toPromise();
    }

    startTyping(c: Conversation) {
        return this.http.post(this.userProvider.apiEndpoint() + "/users/" + this.userProvider.getUsername() + "/conversations/" + c.cdConversation + "?event_type=" + EVENT_TYPE.USER_TYPING, JSON.stringify(this.getLightParticipants(c.lsParticipants)))
            .toPromise()
            .then();
    }

    stopTyping(c: Conversation) {
        return this.http.post(this.userProvider.apiEndpoint() + "/users/" + this.userProvider.getUsername() + "/conversations/" + c.cdConversation + "?event_type=" + EVENT_TYPE.USER_STOP_TYPING, JSON.stringify(this.getLightParticipants(c.lsParticipants)))
            .toPromise()
            .then();
    }

    getLightParticipants(pLsParticipant: Participant[]): Participant[]{
        let lsLightParticipant = [];
        if(pLsParticipant){
          for(let p of pLsParticipant){
            const lightParticipant = new Participant(p.cdParticipant, p.tpParticipant);
            lsLightParticipant.push(lightParticipant);
          }
        }
        return lsLightParticipant;
      }

    executeAction(req: MessageActionRequest, path: string): Promise<MessageActionResponse> {
        return this.http.post(path, req)
            .toPromise()
            .then(response => response['data'] as MessageActionResponse)
            .catch(error => {
                this.logService.logError(error, this.userProvider.getUser());
                console.error(JSON.stringify(error));
                return null;
            })
    }

    getCommands(p: Participant) {
        return this.http.get<ApiItemInterface>(this.userProvider.apiEndpoint() + "/users/" + p.cdParticipant + "/commands?cdCurrentUser=" + this.userProvider.getUsername())
            .toPromise()
            .then(response => {
                let result = response.data;
                return result;
            })
            .catch(error => {
                this.logService.logError(error, this.userProvider.getUser());
                return Promise.reject(error);
            });
    }

    initCommandsForUser(p: Participant) {
        console.warn(p.readed);
        // if(!p.readed){
        this.getCommands(p)
            .then((xResult: Command[]) => {
                // p.readed = true;
                p.lsCommands = xResult;
            })
            .catch(error => {
                this.logService.logError(error, this.userProvider.getUser());
                console.error("error al traer comandos!")
            });
        // }
    }

    private handleError(error: any): Promise<any> {
        const errorMessage = error.data as string;
        switch (error.status) {
            case 413: {
                return Promise.reject(errorMessage || error);
            }
            case 500: {
                console.error('An error occurred', JSON.stringify(error));
            }
            default: {
                console.error('Ocurrio un error inesperado', JSON.stringify(error));
            }
        }
    }

    setActualConversation(conversation: Conversation) {
        this.actualConversation = conversation;
        return this;
    }

    reloadStateParticipant(participant: Participant) {
        for (let c of this.conversations) {
            if (c.receptor) {
                if (c.receptor.cdParticipant.toUpperCase() == participant.cdParticipant.toUpperCase()) {
                    c.receptor.cdState = participant.cdState;
                }
            } else {
                for (let p of c.lsParticipants) {
                    if (p.cdParticipant == participant.cdParticipant) p.cdState = participant.cdState;
                }
            }
        }
    }

    /*** METODOS DB LOCAL ***/

    loadConversationsFromDb(init?: boolean) {
        return new Promise((resolve, reject) => {
            if (init) {
                this.participantService.loadBots();
                this.conversations = [];
            }
            let $this = this;
            let conversationRepository = this.dbProvider.getRepository(Conversation) as Repository<Conversation>;
            conversationRepository.createQueryBuilder("conversation")
                .innerJoinAndSelect("conversation.lsParticipants", "lsParticipants")
                .leftJoinAndSelect("conversation.receptor", "receptor")
                .where('conversation.userId = :userId')
                .orderBy({'conversation.dtLastChange': 'DESC', 'conversation.nuUnreaded': 'DESC'})
                .setParameters({userId: this.userProvider.getUser().id})
                .skip(this.conversations.length)
                .take(20)
                .getMany()
                .then(async function (conversations) {
                    try{
                        let repositoryMessages = $this.dbProvider.getRepository(Message) as Repository<Message>;
                        let conversationsIds = [];
                        for (let conversation of conversations) {
                            conversationsIds.push(conversation.id);
                        }

                        let lsMessages = await repositoryMessages.createQueryBuilder("message")
                            .addSelect("max(message.dtLastUpdate)")
                            .distinct(true)
                            .leftJoinAndSelect("message.lsButtons", "button")
                            .leftJoinAndSelect("message.lsInputs", "input")
                            .leftJoinAndSelect("message.lsActions", "action")
                            .leftJoinAndSelect("message.lsAnnexs", "annex")
                            .where("message.conversationId IN (:...conversationIds) and message.msgState <> 'DELETED'")
                            .orderBy("message.dtLastUpdate", "DESC")
                            .addOrderBy("action.id")
                            .groupBy("message.conversationId")
                            .take(20)
                            .setParameters({conversationIds: conversationsIds})
                            .getMany();

                        for(let message of lsMessages){
                            for (let conversation of conversations) {
                                if(conversation.cdConversation == message.cdConversation){
                                    conversation.lsMessages = [message];
                                    break;
                                }
                            }
                        }
                        $this.conversations = $this.conversations.concat(conversations);
                        resolve();
                    } catch(e1) {
                        $this.logService.logError(e1, $this.userProvider.getUser());
                        $this.conversations = $this.conversations.concat(conversations);
                        resolve();
                    }
                });
        });
    }

    searchConversationsFromDb(text: string): Promise<Conversation[]> {
        return new Promise((resolve, reject) => {
            let conversationRepository = this.dbProvider.getRepository(Conversation) as Repository<Conversation>;
            conversationRepository.createQueryBuilder("conversation")
                .innerJoinAndSelect(Message, "message", "message.conversationId = conversation.id")
                .innerJoinAndSelect("conversation.lsParticipants", "lsParticipants")
                .leftJoinAndSelect("conversation.receptor", "receptor")
                .where('conversation.userId = :userId')
                .andWhere("(LOWER(conversation.dsTitle) like '%" + text + "%' or LOWER(receptor.dsParticipant) like '%" + text + "%' or message.dsContent like '%" + text + "%')")
                .orderBy({'conversation.dtLastChange': 'DESC', 'conversation.nuUnreaded': 'DESC'})
                .setParameters({userId: this.userProvider.getUser().id})
                .getMany()
                .then((conversations) => {
                    let promises: Promise<any>[] = [];
                    let repositoryMessages = this.dbProvider.getRepository(Message) as Repository<Message>;
                    for (let conversation of conversations) {
                        promises.push(repositoryMessages.createQueryBuilder("message")
                            .leftJoinAndSelect("message.lsAnnexs", "annex")
                            .where("message.conversationId = :conversationId")
                            .orderBy("message.dtLastUpdate", "DESC")
                            .take(20)
                            .setParameters({conversationId: conversation.id})
                            .getMany().then((lsMessages) => {
                                conversation.lsMessages = lsMessages.reverse();
                            }));
                    }
                    Promise.all(promises)
                        .then(() => {
                            resolve(conversations);
                        })
                        .catch((error) => {
                            this.logService.logError(error, this.userProvider.getUser());
                            resolve(conversations);
                        });
                });
        });
    }

    getConversationMessages(conversation: Conversation, countNeedMessages: number, fromLast?: boolean): Promise<Message[]> {
        return new Promise<Message[]>((resolve, reject) => {
            this.db.getConversationMessages(conversation, countNeedMessages, fromLast)
                .then((lsMessages) => {
                    lsMessages = lsMessages.reverse();
                    if (lsMessages.length < countNeedMessages) {
                        conversation.lsMessages = lsMessages.length > 0 ? lsMessages.concat(conversation.lsMessages) : conversation.lsMessages;
                        if ((this && this.userProvider && !this.userProvider.inSync) || lsMessages.length == 0) {
                            this.api.getConversationMessages(conversation, countNeedMessages, true)
                                .then((messages: Message[]) => {
                                    if (messages && messages.length > 0) {
                                        conversation.lsMessages = messages.concat(conversation.lsMessages);
                                        this.initiateLsMessages(conversation);
                                        this.db.insertConversationMessages(messages, conversation.id)
                                            .then((msgs) => {
                                                resolve();
                                            })
                                            .catch((error) => {
                                                this.logService.logError(error, this.userProvider.getUser());
                                                resolve();
                                            });
                                    }
                                    else {
                                        this.initiateLsMessages(conversation);
                                        resolve();
                                    }
                                })
                                .catch((error) => {
                                    this.initiateLsMessages(conversation);
                                    this.logService.logError(error, this.userProvider.getUser());
                                    reject(error);
                                });
                        } else {
                            this.initiateLsMessages(conversation);
                            reject("inSync");
                        }
                    }
                    else {
                        conversation.lsMessages = lsMessages.concat(conversation.lsMessages);
                        this.initiateLsMessages(conversation);
                        resolve();
                    }
                });
        });
    }

    private initiateLsMessages(c: Conversation){
        if(!c.lsMessages){
            c.lsMessages = [];
        }
    }

    insertConversationNewMessageInDb(message: Message, c: Conversation, cdParticipant: string) {
        message.conversationId = c.id;
        return new Promise((resolve) => {
            let messageRepository = this.dbProvider.getRepository(Message) as Repository<Message>;
            messageRepository.createQueryBuilder()
                .insert()
                .into('message')
                .values(message)
                .execute()
                .then(() => {
                    this.db.insertMsgButtonsActionsInputs([message]);
                    let i = c.lsMessages.length;
                    c.dtLastChange = new Date().getTime();
                    this.dbProvider.getRepository(Conversation).createQueryBuilder()
                        .update(Conversation)
                        .set(c.getPlainObject())
                        .where('id = :id', { id: c.id })
                        .execute();
                    this.removeConversation(c.cdConversation);
                    this.addConversation(c);
                    //if (!message.cdConversationElement) {
                        this.db.insertAttach([message])
                            .then();
                        resolve();
                    //}
                })
                .catch((error) => {
                    this.logService.logError(error, this.userProvider.getUser());
                    console.error("error en Save message ", JSON.stringify(error, Object.getOwnPropertyNames(error)));
                });
        });
    }

    removeConversationInDb(c: Conversation) {
        this.removeConversation(c.cdConversation);
        let conversationRepository = this.dbProvider.getRepository(Conversation) as Repository<Conversation>;
        this.participantService.removeAllParticipantsRelationFor(c);
        this.removeAllMessagesRelationFor(c);
        conversationRepository.remove(c)
            .then()
            .catch(error => this.logService.logError(error, this.userProvider.getUser()));
    }

    removeAllMessagesRelationFor(conversation: Conversation){
        this.dbProvider.getRepository(Message).createQueryBuilder("message")
            .select("message.id")
            .where("conversationId = :conversationId", { conversationId: conversation.id })
            .execute()
            .then(messages => {
                if (messages) {
                    let idsMessage = messages.map(message => message.message_id);
                    this.dbProvider.getRepository(MessageButton).createQueryBuilder()
                        .delete()
                        .from(MessageButton)
                        .where('messageId IN (:...ids)', { ids: idsMessage })
                        .execute()
                            .then()
                            .catch(error => this.logService.logError(error, this.userProvider.getUser()));
                    this.dbProvider.getRepository(MessageAction).createQueryBuilder()
                        .delete()
                        .from(MessageAction)
                        .where('messageId IN (:...ids)', { ids: idsMessage })
                        .execute()
                            .then()
                            .catch(error => this.logService.logError(error, this.userProvider.getUser()));
                    this.dbProvider.getRepository(MessageInput).createQueryBuilder()
                        .delete()
                        .from(MessageInput)
                        .where('messageId IN (:...ids)', { ids: idsMessage })
                        .execute()
                            .then()
                            .catch(error => this.logService.logError(error, this.userProvider.getUser()));
                    this.dbProvider.getRepository(Attach).createQueryBuilder()
                        .delete()
                        .from(Attach)
                        .where('messageId IN (:...ids)', { ids: idsMessage })
                        .execute()
                            .then()
                            .catch(error => this.logService.logError(error, this.userProvider.getUser()));
                    this.dbProvider.getRepository(Message).createQueryBuilder()
                        .delete()
                        .from(Message)
                        .where('id IN (:...ids)', { ids: idsMessage })
                        .execute()
                            .then()
                            .catch(error => this.logService.logError(error, this.userProvider.getUser()));
                }
            })
            .catch(error => this.logService.logError(error, this.userProvider.getUser()));
    }

    getLocalConversationByIdFromDb(id: number): Conversation {
        for (let c of this.conversations) {
            if (c.id === id) {
                return c;
            }
        }
    }

    getLocalConversationByCdParticipantInDb(p: Participant): Conversation {
        return this.dbProvider.getRepository(Conversation).createQueryBuilder("conversation")
            .innerJoinAndSelect("conversation.lsParticipants", "lsParticipants")
            .innerJoinAndSelect("conversation.receptor", "receptor")
            .andWhere("conversation.userId = :userId")
            .andWhere("receptor.id = :id")
            .setParameters({id: p.id, userId: this.userProvider.getUser().id})
            .getOne()
            .then(c => {
                if (c) {
                    this.conversations.push(c);
                    let repositoryMessages = this.dbProvider.getRepository(Message) as Repository<Message>;
                    return repositoryMessages.createQueryBuilder("message")
                        .leftJoinAndSelect("message.lsButtons", "button")
                        .leftJoinAndSelect("message.lsInputs", "input")
                        .leftJoinAndSelect("message.lsActions", "action")
                        .leftJoinAndSelect("message.lsAnnexs", "annex")
                        .where("message.conversationId = :conversationId")
                        .orderBy("message.dtLastUpdate", "DESC")
                        .addOrderBy("action.id")
                        .take(20)
                        .setParameters({conversationId: c.id})
                        .getMany()
                        .then((lsMessages) => {
                            c.lsMessages = lsMessages;
                            return c;
                        });
                }
                else {
                    return null;
                }
            });
    }

    /**
     * Finds the object chat in db.
     * @param  {string}                objectId
     * @return {Promise<Conversation>}
     */
    public getObjectChatInDb(objectId: string): Promise<Conversation> {
        return new Promise((resolve, reject) => {
            this.dbProvider.getRepository(Conversation).createQueryBuilder("conversation")
                .innerJoinAndSelect("conversation.lsParticipants", "lsParticipants")
                .innerJoinAndSelect("conversation.receptor", "receptor")
                .andWhere("conversation.userId = :userId")
                .andWhere("cdObject = :objectId")
                .setParameters({objectId: objectId, userId: this.userProvider.getUser().id})
                .getOne()
                .then(c => {
                    if (c) {
                        console.log("exist inDb relatedObject");
                        this.conversations.push(c);
                        let repositoryMessages = this.dbProvider.getRepository(Message) as Repository<Message>;
                        return repositoryMessages.createQueryBuilder("message")
                            .leftJoinAndSelect("message.lsButtons", "button")
                            .leftJoinAndSelect("message.lsInputs", "input")
                            .leftJoinAndSelect("message.lsActions", "action")
                            .leftJoinAndSelect("message.lsAnnexs", "annex")
                            .where("message.conversationId = :conversationId")
                            .orderBy("message.dtLastUpdate", "DESC")
                            .addOrderBy("action.id")
                            .take(20)
                            .setParameters({conversationId: c.id})
                            .getMany()
                            .then((lsMessages) => {
                                c.lsMessages = lsMessages;
                                resolve(c);
                            });
                    }
                    else {
                        console.log("not relatedObject");
                        resolve(null);
                    }
                });
        });
    }

    updateConversation(conv) {
        return new Promise((resolve, reject) => {
            this.getOne(conv.cdConversation, null, false)
                .then((conversation) => {
                    if (conversation) {
                        this.api.postProcessConversation(conv)
                            .then((convers) => {
                                conversation.dsTitle = convers.dsTitle;
                                conversation.lsParticipants = convers.lsParticipants;
                                conversation.dtLastChange = convers.dtLastUpdate;
                                this.db.update(conversation)
                                    .then(() => {
                                        resolve();
                                    })
                                    .catch((error) => {
                                        this.logService.logError("Error en updateConversation, chat: "+JSON.stringify(conversation), this.userProvider.getUser());
                                        this.logService.logError(error, this.userProvider.getUser());
                                        reject(error);
                                    });
                            })
                            .catch((error) => {
                                this.logService.logError(error, this.userProvider.getUser());
                                reject(error);
                            })
                    }
                    else {
                        resolve();
                    }
                })
                .catch((error) => {
                    this.logService.logError(error, this.userProvider.getUser());
                    reject(error);
                });
        });
    }

    newMenssageByNewConversationSync(c, msg, notNotification) {
        return new Promise((resolve, reject) => {
            this.existMessage(msg['cdConversationElement'], msg['cdConversation'])
                .then((message) => {
                    if (!message) {
                        msg.conversationId = c.id;
                        this.dbProvider.getRepository(Message).createQueryBuilder()
                            .insert()
                            .into('message')
                            .values(msg)
                            .execute()
                            .then(() => {
                                this.db.insertMsgButtonsActionsInputs([msg]);
                                if (!notNotification) this.notificationProvider.newMessage(c, msg);
                                this.db.insertAttach([msg]);
                                resolve();
                            })
                            .catch((error) => {
                                this.logService.logError("Error en newMenssageByNewConversationSync, se quiso guardar el mensaje "+JSON.stringify(msg) + " para el chat.id = "+c.id, this.userProvider.getUser());
                                this.logService.logError(error, this.userProvider.getUser());
                                reject(error);
                            });
                    } else {
                        resolve();
                    }
                })
                .catch((error) => {
                    this.logService.logError(error, this.userProvider.getUser());
                    reject(error);
                });
        });
    }

    createConversationSync(conv, notNotification) {
        return new Promise((resolve, reject) => {
            console.warn("Guardo NUEVO chat");
            let inicio = new Date().valueOf();
            let msg = conv.lsMessages[0];
            this.dbProvider.getRepository(Conversation).createQueryBuilder()
                .where("cdConversation = :cdConversation and userId = :userId")
                .setParameters({cdConversation: conv.cdConversation, userId: this.userProvider.getUser().id})
                .getRawOne()
                .then((c) => {
                    if (!c) {
                        this.api.serializeConversations([conv])
                            .then((conversations) => {
                                    let conversation = conversations[0];
                                    conversation.synchronized = true;
                                    conversation.dtLastChange = conv.dtLastUpdate;
                                    conversation.userId = this.userProvider.getUser().id;
                                    if (!conversation.nuUnreaded) conversation.nuUnreaded = 0;
                                    this.dbProvider.getRepository(Conversation).createQueryBuilder()
                                        .insert()
                                        .into('conversation')
                                        .values(conversation)
                                        .execute()
                                        .then(() => {
                                            this.participantService.addParticipantsRelationFor([conversation]);
                                            console.warn("Guardo mensaje NUEVO chat");
                                            this.newMenssageByNewConversationSync(conversation, msg, notNotification)
                                                .then(_ => {
                                                    this.conversations.unshift(conversation);
                                                    console.error("Demoró " + (new Date().valueOf() - new Date(inicio).valueOf()) + ' milisegundos.');
                                                    resolve();
                                                })
                                                .catch((error) => {
                                                    this.logService.logError(error, this.userProvider.getUser());
                                                    reject(error);
                                                });
                                        })
                                        .catch((error) => {
                                            this.logService.logError(error, this.userProvider.getUser());
                                            reject(error);
                                        });
                                }
                            )
                            .catch((error) => {
                                this.logService.logError(error, this.userProvider.getUser());
                                reject(error);
                            });
                    } else {
                        console.warn("Existe NUEVO chat");
                        console.warn("Guardo mensaje NUEVO chat");
                        this.newMenssageByNewConversationSync(c, msg, notNotification)
                            .then(_ => {
                                console.error("Demoró " + (new Date().valueOf() - new Date(inicio).valueOf()) + ' milisegundos.');
                                resolve();
                            })
                            .catch((error) => {
                                this.logService.logError(error, this.userProvider.getUser());
                                reject(error);
                            });
                    }
                })
                .catch((error) => {
                    this.logService.logError(error, this.userProvider.getUser());
                    reject(error);
                });
        });
    }

    deleteConversation(conv) {
        return new Promise((resolve, reject) => {
            this.getOne(conv.cdConversation, null, false)
                .then((conversation) => {
                    if (conversation) {
                        this.db.delete(conversation)
                            .then(() => {
                                resolve();
                            })
                            .catch((error) => {
                                this.logService.logError(error, this.userProvider.getUser());
                                reject(error);
                            });
                    }
                    else {
                        resolve();
                    }
                });
        });
    }

    updateGroupTitle(cdConversation: number, dsTitle) {
        let c = this.getLocalConversationById(cdConversation);
        c.dsTitle = dsTitle;
        this.dbProvider.getRepository(Conversation).createQueryBuilder()
            .update(Conversation)
            .set(c.getPlainObject())
            .where('id = :id', { id: c.id })
            .execute()
            .then();
    }

    updateGroupOwner(newOwner: string, cdConversation: number) {
        let c = this.getLocalConversationById(cdConversation);
        c.cdUserCreator = newOwner;
        this.dbProvider.getRepository(Conversation).createQueryBuilder()
            .update(Conversation)
            .set(c.getPlainObject())
            .where('id = :id', { id: c.id })
            .execute()
            .then();
    }

    /**
     * Envía una conversación a la API
     */
    sendConversation(c): Promise<Conversation> {
        return new Promise<Conversation>((resolve, reject) => {
            this.api.create(c)
                .then((xC: Conversation) => {
                    xC.copy(c);
                    c.lsMessages = [];
                    c.cdConversation = xC.cdConversation;
                    c.synchronized = true;
                    // TODO revisar asignación de dtLastChange
                    c.dtLastChange = new Date().getTime();
                    this.updateConversationInMemory(c);
                    let conversationRepository = this.dbProvider.getRepository(Conversation) as Repository<Conversation>;
                    conversationRepository.createQueryBuilder()
                        .update(Conversation)
                        .set(c.getPlainObject())
                        .where('id = :id', { id: c.id })
                        .execute()
                        .then(() => {
                            this.dbProvider.getConnection()
                                .createQueryBuilder()
                                .update(Message)
                                .set({cdConversation: c.cdConversation})
                                .where("cdConversation IS NULL AND conversationId = :conversationId", {
                                    conversationId: c.id,
                                    cdConversation: c.cdConversation
                                })
                                .execute()
                                .then(() => {
                                    resolve(c);
                                });
                        })
                        .catch((error) => {
                            this.logService.logError(error, this.userProvider.getUser());
                            reject(error);
                        });
                })
                .catch((error) => {
                    this.logService.logError(error, this.userProvider.getUser());
                    resolve(c);
                });
        });
    }


    searchChats(text) {
        this.onSearchChats.next(text);
    }


    updateMessageStateInMemory(msgStateSync) {
        let i = this.getConversationIndex(msgStateSync.cdConversation);
        if (i > -1) {
            for (let message of this.conversations[i].lsMessages) {
                if (message.cdConversationElement == msgStateSync.cdConversationElement) {
                    message.msgState = msgStateSync.msgState;
                    console.log("updatie state en memoria");
                }
            }
        }
    }

    /**
     * Get the object chat.
     * @param {string} objectId
     * @param {string} objectUrl
     * @param {string} relatedRuleName
     * @param {number} relatedRuleVersion
     * @param {string} objectType
     * @param {boolean} create
     * @param {string} title
     * @return {Promise<Conversation>}
     */
    getObjectChat(objectId: string, objectUrl: string, relatedRuleName: string, relatedRuleVersion: number, objectType: string, create: boolean, title?: string): Promise<Conversation> {
        return new Promise((resolve, reject) => {
            let relatedChat = null;
            for (let c of this.conversations) {
                if (c.cdObject == objectId) relatedChat = c;
            }
            if (!relatedChat) {
                this.getObjectChatInDb(objectId)
                    .then(relatedChatInDb => {
                        if (!relatedChatInDb) {
                            this.api.getObjectChat(objectId, objectUrl, relatedRuleName, relatedRuleVersion, objectType, create, title)
                                .then(conv => {
                                    if (conv.lsParticipants.findIndex(p => p.cdParticipant == this.userProvider.getUsername().toUpperCase()) > -1) {
                                        conv.synchronized = true;
                                        conv.dtLastChange = conv.dtLastUpdate;
                                        conv.userId = this.userProvider.getUser().id;
                                        this.dbProvider.getRepository(Conversation).createQueryBuilder()
                                            .insert()
                                            .into('conversation')
                                            .values(conv)
                                            .execute()
                                            .then(() => {
                                                this.participantService.addParticipantsRelationFor([conv]);
                                                let messages: Message[] = [];
                                                for (let message of conv.lsMessages) {
                                                    message.conversationId = conv.id;
                                                    messages.push(message);
                                                }
                                                let messageRepository = this.dbProvider.getRepository(Message) as Repository<Message>;
                                                messageRepository.createQueryBuilder()
                                                    .insert()
                                                    .into('message')
                                                    .values(messages)
                                                    .execute()
                                                    .then(() => {
                                                        this.db.insertMsgButtonsActionsInputs(messages);
                                                        this.countChatsPushed++;
                                                        this.conversations.unshift(conv);
                                                        this.db.insertAttach(messages);
                                                        resolve(conv);
                                                    })
                                                    .catch((error) => {
                                                        this.logService.logError(error, this.userProvider.getUser());
                                                        reject(error);
                                                    });
                                            })
                                            .catch((error) => {
                                                this.logService.logError(error, this.userProvider.getUser());
                                                reject(error);
                                            });
                                    } else {
                                        resolve(conv);
                                    }
                                })
                                .catch((error) => {
                                    this.logService.logError(error, this.userProvider.getUser());
                                    reject(error);
                                });
                        } else {
                            this.countChatsPushed++;
                            resolve(relatedChatInDb);
                        }
                    });
            } else {
                resolve(relatedChat);
            }

        });


    }

    updateReceptors() {
        for (let c of this.conversations) {
            if (c && c.cdType == CONVERSATION_TYPE.PRIVATE) {
                this.participantService.getOne(c.receptor.cdParticipant, c.receptor.tpParticipant)
                    .then((p) => {
                        if (p) c.setReceptor(p)
                    });
            } else {
                this.participantService.linkParticipants(c);
            }
        }
    }

    conversationsUpdated() {
        if(this.configurationService.tedisConfiguration){
            this.updateGroupConversationsImage(this.configurationService.tedisConfiguration.systemConfiguration.theme);
        }
        this.conversationUpdated.next();
    }

    public updateGroupConversationsImage(theme: string): void {
        for (let c of this.conversations) {
            if (c.isGroup) {
                c.setDsImage(c.image.replace(/newUI\/Azul|newUI\/Rojo|newUI\/Verde/gi, 'newUI/' + theme));
            }
        }
    }

    resetParticipantStates() {
        for (let c of this.getConversations()) {
            if (c.receptor) c.receptor.cdState = 0;
            for (let p of c.lsParticipants) {
                p.cdState = 0;
            }
        }
    }

    /**
    * Resets the conversations list.
    * @return {undefined}
    */
    public resetConversations(): void {
        this.conversations = [];
    }

}
