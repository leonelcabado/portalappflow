import {Injectable} from '@angular/core';
import {ToastController} from '@ionic/angular';
import {Repository} from 'typeorm';

//providers
import {DbProvider} from './db.service';

//models
import {Context} from '../model/context.entity';
import {User} from '../model/user.entity';
import {Participant} from '../model/participant.entity';

//services
import {ConversationDbService} from './conversation.service';
import { LogService } from './log.service';
import { AppConfigurationService } from './app-configuration.service';

@Injectable()
export class ContextsProvider {

    contexts: Context[] = [];

    constructor(private dbProvider: DbProvider,
                private toastCtrl: ToastController,
                private conversationDBService: ConversationDbService,
                private appConfigurationService: AppConfigurationService,
                private logService: LogService) {}

    /**
     * It returns the contexts list.
     * @return {Promise<Context[]>}
     */
    public getContexts(): Promise<Context[]> {
        return new Promise((resolve, reject) => {
            if (this.contexts.length > 0) {
                resolve(this.contexts);
                return;
            }
            this.dbProvider.getRepository(Context).find()
                .then((elements) => {
                    this.contexts = elements
                    resolve(this.contexts);
                })
                .catch((error) => {
                    reject(error);
                });
        });
    }

    /**
     * It adds new context.
     * If the new context exists, returns it.
     * @param  {string}           contextName
     * @param  {string}           contextUrl
     * @return {Promise<Context>}
     */
    public addContext(contextName: string, contextUrl: string): Promise<Context> {
        return new Promise((resolve, reject) => {
            let context = new Context();
            context.name = contextName;
            context.url = contextUrl;
            context.createdAt = new Date().getTime();
            this.getContext(context).then((existingContext: Context) => {
                if (existingContext) {
                    resolve(existingContext);
                    return;
                }
                this.dbProvider.getRepository(Context).createQueryBuilder()
                    .insert()
                    .into('context')
                    .values(context)
                    .execute()
                    .then(() => {
                        this.contexts.push(context);
                        resolve(context);
                    })
                    .catch((error) => {
                        console.error(JSON.stringify(error));
                        reject(error);
                    });
            });
        });
    }

    public updateContext(context) {
        return new Promise((resolve, reject) => {
            this.dbProvider.getRepository(Context).createQueryBuilder()
                .update(Context)
                .set(context.getPlainObject())
                .where('id = :id', { id: context.id })
                .execute()
                .then(() => {
                    resolve();
                });
        });
    }

    public removeContext(context: Context) {
        return new Promise((resolve, reject) => {
            this.dbProvider.getRepository(User).createQueryBuilder('user')
                .where('user.context.id = ' + context.id)
                .getMany().then((users) => {
                let promises: Promise<any>[] = [];
                for (let u of users) {
                    promises.push(
                        this.conversationDBService.removeConversationsByUser(u.id).then(
                            _ => this.dbProvider.getRepository(User).remove(u).then()
                        )
                    );
                }
                Promise.all(promises).then(_ => {
                    this.dbProvider.getRepository(Context).remove(context)
                        .then(() => {
                            this.toastCtrl.create({
                                message: 'Context removed succesfully',
                                duration: 2500,
                                position: 'bottom',
                                cssClass: 'toast-ok'
                            }).then(toast => {
                                toast.present();
                            });
                            resolve();
                        })
                        .catch((error) => {
                            //this.logService.logError(error, null);
                            console.error(JSON.stringify(error))
                        })
                });
            });
        });
    }

    /**
     * It gets one context by namr and url.
     * @param  {Context}          context
     * @return {Promise<Context>}
     */
    public getContext(context: Context): Promise<Context> {
        return this.dbProvider.getRepository(Context).findOne({name: context.name, url: context.url});
    }

    /**
     * It checks if exist a initial context in the app configuration.
     * @return {boolean}
     */
    public checkInitialContextInConfiguration(): boolean {
        return this.appConfigurationService.checkInitialContext();
    }

    /**
     * It initializes the initial context or the contexts list.
     * @return {Promise<Array<Context>>}
     */
    public processContexts(): Promise<Array<Context>> {
        return this.getContexts().then((contexts: Array<Context>) => {
            if (contexts && contexts.length > 0) {
                return contexts;
            }
            return [];
        })
        .catch((error) => {
            this.logService.logError(error, null);
            return [];
        });
    }

    /**
     * It adds the initial context and selects it.
     * @return {Promise<Context>}
     */
    public processInitialContext(): Promise<Context> {
        return this.addContext(this.appConfigurationService.getInitialContextName(), this.appConfigurationService.getInitialContextUrl())
            .then((context: Context) => {
                return context;
            })
            .catch((error) => {
                this.logService.logError(error, null);
                return Promise.reject(undefined);
            });
    }

    /**
     * It returns the initial context instance.
     * @return {Promise<Context>}
     */
    public getInitialContextInstance(): Promise<Context> {
        let context = new Context();
        context.setName(this.appConfigurationService.getInitialContextName());
        context.setUrl(this.appConfigurationService.getInitialContextUrl());
        return this.getContext(context);
    }

}
