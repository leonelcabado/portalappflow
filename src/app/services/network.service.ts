import {Injectable, NgZone} from '@angular/core';
import {ToastController} from '@ionic/angular';
import {Network} from '@ionic-native/network/ngx';
import {Subject} from 'rxjs';
import {Subscription} from 'rxjs';
import {HttpClient} from '@angular/common/http';

//services
import {RTMService} from './rtm.service';
import {TranslateService} from './translate.service';
import {TaskService} from './task.service';

//providers
import {NotificationProvider} from './notification.service';
import {UserProvider} from './user.service';

import {ApiItemInterface} from '../support-classes/api-item-interface'
import {ConfigurationService} from './configuration.service';
import { LogService } from './log.service';

@Injectable()
export class NetworkService {
    toast: any;
    previousStateNetwork: string = "";
    connected: boolean;
    network: Network;
    disableButtons: string = "";
    subscriptionOnConnect: Subscription;
    subscriptionOnDisconnect: Subscription;

    constructor(private toastCtrl: ToastController,
                private rtmService: RTMService,
                private ngZone: NgZone,
                private notificationProvider: NotificationProvider,
                private userProvider: UserProvider,
                private translateService: TranslateService,
                private configurationService: ConfigurationService,
                private taskService: TaskService,
                private http: HttpClient,
                private logService: LogService) {
    }

    init(network: Network) {
        if (this.toast) this.toast.dismiss();
        this.network = network;
        let $t = this;
        setTimeout(function () {
            $t.previousStateNetwork = ($t.network.type != 'none') ? 'c' : 'd';
            $t.setConnected($t.network.type != 'none');
            $t.taskService.showNuTasks = $t.network.type != 'none';
        }, 200);
        this.subscriptionOnDisconnect = network.onDisconnect().subscribe(() => {
            if (this.connected) {
                this.setConnected(false);
                this.disableButtons = "disabled";
                this.taskService.showNuTasks = false;
                let $t = this;
                setTimeout(function () {
                    if ($t.network.type == 'none' && $t.previousStateNetwork == 'c') {
                        $t.showToast($t.translateService.instant('INTERNET_NOT_WORKING'), 'disconnected');
                        $t.previousStateNetwork = 'd';
                    }
                }, 3000);
            }
        });

        this.subscriptionOnConnect = network.onConnect().subscribe(() => {
            if (!this.connected) {
                this.setConnected(true);
                this.taskService.showNuTasks = true;
                let $t = this;
                setTimeout(function () {
                    if ($t.network.type != 'none' && $t.previousStateNetwork == 'd') {
                        $t.showToast($t.translateService.instant('INTERNET_WORKING'), 'connected');
                        $t.previousStateNetwork = 'c';
                    }
                }, 3000);
                this.disableButtons = "";
                if (this.userProvider.getUser() && this.userProvider.getUser().loggedIn) {
                    this.ngZone.run(() => {
                        if (!this.rtmService.getWebSocket() || (this.rtmService.getWebSocket() && this.rtmService.getWebSocket().readyState > 1)) {
                            this.logService.logDevelopment("reconnect socket INTERNET_WORKING", this.userProvider.getUser());
                            this.rtmService.openConnection(true);
                        }
                    });
                }
            }
        });
    }

    unSubscribeEvents() {
        this.subscriptionOnDisconnect.unsubscribe();
        this.subscriptionOnConnect.unsubscribe();
    }

    showToast($message: string, $class: string) {
        if (this.notificationProvider.platformState == 'active') {
            if (this.toast) this.toast.dismiss();
            this.toastCtrl.create({
                message: $message,
                cssClass: $class,
                position: 'top',
                buttons: [
                    {
                        text: 'Ok',
                        role: 'cancel',
                        handler: () => {
                            //console.log('Cancel clicked');
                        }
                    }
                ]
            }).then(toast => {
                this.toast = toast;
                this.toast.present();
            });
        }
    }

    setConnected(state) {
        this.connected = state;
        this.rtmService.networkConnected = state;
    }

    getConnected() {
        if (!this.connected) {
            this.configurationService.init();
        } else {
            return true;
        }
    }

    getNetwork(): Network {
        return this.network;
    }

}
