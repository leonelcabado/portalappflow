import {Injectable} from '@angular/core';

@Injectable()
export class AuthProvider {

    token: string = null;

    constructor() {
    }

    getToken() {
        return this.token;
    }

    init(token: string) {
        this.token = token;
    }

}
