import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpBackend } from '@angular/common/http';
import { Observable, Subscription, of } from 'rxjs';
import { map, mergeMap } from 'rxjs/internal/operators';
import { UserProvider } from './user.service';

@Injectable()
export class CaseService {

    constructor(private userProvider: UserProvider, protected httpBackend: HttpBackend) { }

    /**
     * It gets the cases with the page number and criteria.
     * @param  {number}       pageNumber
     * @param  {string}       criteria
     * @return {Promise<any>}
     */
    public getCases(pageNumber?: number, criteria?: string): Promise<any> {
        const httpClient = new HttpClient(this.httpBackend);
        const httpOptions = {
            headers: new HttpHeaders({
                'Authorization': `Bearer ${this.userProvider.getUser().getToken()}`,
                'Content-Type': 'application/json',
            })
        };
        return httpClient.get(this.userProvider.getContextUrl() + `/v1.0/cases?${this.processParametersInCases(pageNumber, criteria)}`, httpOptions).toPromise();
    }

    /**
     * It process the parameters to the case request.
     * @param  {number} pageNumber
     * @param  {string} criteria
     * @return {string}
     */
    private processParametersInCases(pageNumber?: number, criteria?: string): string {
        let urlParameters = '';
        if (pageNumber) {
            urlParameters = `page-number=${pageNumber}`;
        }
        if (!pageNumber && criteria) {
            urlParameters = `search=${criteria}`;
        }
        if (criteria) {
            urlParameters = `&search=${criteria}`;
        }
        return urlParameters;
    }

} 
