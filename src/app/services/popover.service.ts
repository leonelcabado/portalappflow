import { Injectable } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { BackButtonProvider } from './back-button.service';
import { MainMenuPage } from '../pages/main-menu/main-menu';

@Injectable()
export class PopoverService {

    constructor(private popoverController: PopoverController, private backButtonProvider: BackButtonProvider) {}

    /**
     * Open the main menu page.
     * @param {any} myEvent
     */
    public openMainMenu(myEvent: any): void {
        this.popoverController.create({
            component: MainMenuPage,
            event: myEvent
        }).then((popover: any) => {
            popover.present();
            this.backButtonProvider.registerDismissable(popover);
        });
    }

}
