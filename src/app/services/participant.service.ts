import {Injectable} from '@angular/core';
import {Repository} from 'typeorm';
import {Subject} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {isUndefined} from 'util';

//models
import {Participant} from '../model/participant.entity';
import {Conversation} from '../model/conversation.entity';

//helpers
import {ApiItemInterface} from '../support-classes/api-item-interface'

//providers
import {UserProvider} from './user.service';
import {DbProvider} from './db.service';

//services
import {ConversationService} from './conversation.service';

//config
import {APP} from '../app.config';
import { LogService } from './log.service';

@Injectable()
export class ParticipantApiService {

    constructor(private userProvider: UserProvider,
                private http: HttpClient,
                private logService: LogService) {
    }

    load() {
        return this.http.get<ApiItemInterface>(this.userProvider.apiEndpoint() + '/users' + "?cdCurrentUser=" + this.userProvider.getUsername())
            .toPromise()
            .then(response => {
                let participants: Participant[] = [];
                for (let participant of response.data[0].concat(response.data[1])) {
                    participant.flImage = (participant.flImage.substring(0, 5) == 'data:') ? participant.flImage : this.userProvider.getContextUrl() + '/' + participant.flImage;
                    participants.push(new Participant().fromJSON(participant));
                }
                return participants;
            })
            .catch(error => {
                this.logService.logError(error, this.userProvider.getUser());
                return Promise.reject(error);
            });
    }

}

@Injectable()
export class ParticipantDbService {

    constructor(private userProvider: UserProvider,
                private dbProvider: DbProvider) {
    }

    /**
     * Reads the users from the db.
     * @param  {string}                 lastParticipant
     * @param  {Participant[]}          lsParticipants
     * @param  {number}                 count
     * @param  {boolean}                isVisible
     * @return {Promise<Participant[]>}
     */
    public getUsersInDB(lastParticipant?: string, lsParticipants?: Participant[], count?: number, isVisible?: boolean): Promise<Participant[]> {
        let last = '';
        if (lastParticipant) last = " and dsParticipant > '" + lastParticipant + "'";
        let notIn: number [] = [];
        if (lsParticipants) {
            for (let p of lsParticipants) {
                notIn.push(p.id);
            }
        }
        return this.dbProvider.getRepository(Participant)
            .createQueryBuilder("participant")
            .where("userId = :userId and cdParticipant <> :username and isVisible = :isVisible" + last,
                {
                    userId: this.userProvider.getUser().id,
                    username: this.userProvider.getUsername().toUpperCase(),
                    isVisible: isVisible ? 1 : 0
                })
            .andWhere("id not in (" + notIn.toString() + ")")
            .orderBy("dsParticipant", "ASC")
            .take(count)
            .getMany();
    }


    loadBots() {
        return this.dbProvider.getRepository(Participant)
            .createQueryBuilder("participant")
            .where("userId = :userId and tpParticipant='CHATBOT' and isVisible = 1", {userId: this.userProvider.getUser().id})
            .orderBy("pinned")
            .addOrderBy("dsParticipant")
            .getMany();
    }

    search(text: string) {
        return this.dbProvider.getRepository(Participant)
            .createQueryBuilder("participant")
            .where("userId = :userId and cdParticipant <> :username and isVisible = 1 and LOWER(dsParticipant) like '%" + text + "%'",
                {
                    userId: this.userProvider.getUser().id,
                    username: this.userProvider.getUsername().toUpperCase()
                })
            .orderBy("dsParticipant", "ASC")
            .getMany();
    }

    getMany(cdParticipants: string[]){
        return this.dbProvider.getRepository(Participant)
            .createQueryBuilder("participant")
            .leftJoinAndSelect('participant.lsCommands', 'command')
            .where("participant.cdParticipant in ('" + cdParticipants.join("','") + "')")
            .getMany();
    }

    insert(participants: Participant[]) {
        let participantRepository = this.dbProvider.getRepository(Participant) as Repository<Participant>;
        for (let participant of participants) {
            participant.userId = this.userProvider.getUser().id;
        }
        return new Promise((resolve) => {
            participantRepository.createQueryBuilder()
                .insert()
                .into('participant')
                .values(participants)
                .orIgnore()
                .execute()
                .then(() => resolve());
        });
    }

    update(participant: Participant) {
        let participantRepository = this.dbProvider.getRepository(Participant) as Repository<Participant>;
        participant.userId = this.userProvider.getUser().id;
        return new Promise((resolve) => {
            participantRepository.createQueryBuilder()
                .update(Participant)
                .set(participant.getPlainObject())
                .where('id = :id', { id: participant.id })
                .execute()
                .then(() => resolve());
        });
    }

    getOne(cdParticipant: string, tpParticipant: string): Promise<Participant> {
        let participantRepository = this.dbProvider.getRepository(Participant) as Repository<Participant>;
        return participantRepository.createQueryBuilder("participant")
            .leftJoinAndSelect('participant.lsCommands', 'command')
            .where('participant.cdParticipant = :cdParticipant AND participant.tpParticipant = :tpParticipant AND participant.userId = :userId', { cdParticipant: cdParticipant, tpParticipant: tpParticipant, userId: this.userProvider.getUser().id })
            .getOne();
    }
}

@Injectable()
export class ParticipantService {
    userState = 1;
    participants: Participant[] = [];
    bots: Participant[] = [];

    cache: Map<string, Participant> = new Map<string, Participant>();

    constructor(private userProvider: UserProvider,
                private api: ParticipantApiService,
                private db: ParticipantDbService,
                private dbProvider: DbProvider,
                private logService: LogService) {
        this.cache.set(this.generateCacheKey("SYS_USER", "USER"), Participant.getSysUser());
    }

    load() {
        return new Promise((resolve, reject) => {
            this.api.load()
                .then((participants: Participant[]) => {
                    this.db.insert(participants).then(() => {
                        this.setParticipants(participants);
                        resolve();
                    })
                        .catch((error) => {
                            this.logService.logError(error, this.userProvider.getUser());
                            reject(error);
                        });
                })
                .catch((error) => {
                    this.logService.logError(error, this.userProvider.getUser());
                    reject(error);
                });
        });
    }

    /**
     * Reads the users.
     * @param  {string}                 lastParticipant
     * @param  {Participant[]}          lsParticipants
     * @param  {number}                 count
     * @param  {boolean}                isVisible
     * @return {Promise<Participant[]>}
     */
    public getUsers(lastParticipant?: string, lsParticipants?: Participant[], count?: number, isVisible?: boolean): Promise<Participant[]> {
        return this.db.getUsersInDB(lastParticipant, lsParticipants, count, isVisible).then((participants: Participant[]) => {
            this.setParticipants(participants);
            return participants;
        })
        .catch((error) => {
            this.logService.logError(error, this.userProvider.getUser());
            return Promise.reject(error);
        });
    }

    searchParticipants(text: string) {
        return this.db.search(text);
    }

    private generateCacheKey(cdParticipant: string, tpParticipant: string) {
        return cdParticipant + "_" + tpParticipant;
    }

    getOne(cdParticipant: string, tpParticipant: string): Promise<Participant> {
        return new Promise((resolve, reject) => {
            let cachedParticipant = this.cache.get(this.generateCacheKey(cdParticipant, tpParticipant));
            if (!isUndefined(cachedParticipant) && cachedParticipant) {
                resolve(cachedParticipant);
            } else {
                console.log("ejecutando el getOne: "+cdParticipant+" "+tpParticipant);
                this.db.getOne(cdParticipant, tpParticipant)
                    .then((participant) => {
                        this.cache.set(this.generateCacheKey(cdParticipant, tpParticipant), participant);
                        resolve(participant);
                    })
                    .catch((error) => {
                        this.logService.logError(error, this.userProvider.getUser());
                        reject();
                    })
            }
        });
    }

    getMany(cdParticipants: string[]): Promise<Participant[]>{
        return new Promise((resolve, reject) => {
            this.db.getMany(cdParticipants)
                .then((participants) => {
                    if(participants){
                        for(let participant of participants){
                            this.cache.set(this.generateCacheKey(participant.cdParticipant, participant.tpParticipant), participant);
                        }
                    }
                    resolve(participants);
                })
                .catch((error) => {
                    this.logService.logError(error, this.userProvider.getUser());
                    reject();
                })
        });
    }

    setParticipants(p: Participant[]): void {
        this.participants = p;
    }

    getParticipant(cdParticipant: string, tpParticipant: string): Participant {
        for (let p of this.participants) {
            if (p.cdParticipant === cdParticipant && p.tpParticipant === tpParticipant) {
                return p;
            }
        }
        return null;
    }


    linkParticipants(conversation: Conversation): Promise<Conversation> {
        let promises: Promise<Participant>[] = [];
        for (let p of conversation.lsParticipants) {
            promises.push(this.getOne(p.cdParticipant, p.tpParticipant));
        }
        return Promise.all(promises).then((participants) => {
            let lsParticipants: Participant[] = [];
            for (let participant of participants) {
                if (participant) {
                    lsParticipants.push(participant);
                }
            }
            conversation.lsParticipants = lsParticipants;
            return conversation;
        });
    }

    changeState(s: number): boolean {
        if (this.userState != s) {
            this.userState = s;
            return true;
        }
        return false;
    }

    updateState(cdParticipant: string, cdState: number) {
        if (cdParticipant != this.userProvider.getUsername()) {
            //need to swap participant from list?
            for (let p of this.participants) {
                if (p.cdParticipant === cdParticipant) {
                    p.cdState = cdState;
                }
            }
            for (let b of this.bots) {
                if (b.cdParticipant === cdParticipant) {
                    b.cdState = cdState;
                }
            }
        }
    }

    getParticipants() {
        return this.participants;
    }

    loadBots() {
        this.db.loadBots().then((bots: Participant[]) => this.bots = bots);
    }

    getBots() {
        return this.bots;
    }

    getShortLsParticipants(lsParticipants: Participant[]): Array<any> {
        let participants = [];
        for (let i of lsParticipants) {
            participants.push({cdParticipant: i.cdParticipant, tpParticipant: i.tpParticipant});
        }
        return participants;
    }

    newParticipant(p) {
        let participant = new Participant().fromJSON(p);
        return new Promise((resolve, reject) => {
            this.dbProvider.getRepository(Participant).createQueryBuilder()
                .where("cdParticipant = :cdParticipant and tpParticipant = :tpParticipant and userId = :userId")
                .setParameters({
                    cdParticipant: participant.cdParticipant,
                    tpParticipant: participant.tpParticipant,
                    userId: this.userProvider.getUser().id
                })
                .getRawOne()
                .then((part) => {
                    if (!part) {
                        part = participant;
                        this.db.insert([part])
                            .then(() => {
                                this.cache.set(this.generateCacheKey(part.cdParticipant, part.tpParticipant), part);
                                this.participants.push(part);
                                resolve(part);
                            })
                            .catch((error) => {
                                this.logService.logError(error, this.userProvider.getUser());
                                reject(error);
                            });
                    } else {
                        resolve();
                    }
                })
                .catch((error) => {
                    this.logService.logError(error, this.userProvider.getUser());
                    reject(error);
                });
        });
    }

    updateParticipant(p) {
        let participant = new Participant().fromJSON(p) as Participant;
        return new Promise((resolve, reject) => {
            this.db.getOne(participant.cdParticipant, participant.tpParticipant)
                .then((part) => {
                    if (part) {
                        part.updateFrom(participant);
                        this.db.update(part)
                            .then(() => {
                                this.cache.set(this.generateCacheKey(part.cdParticipant, part.tpParticipant), part);
                                for (let p of this.participants) {
                                    if (p.cdParticipant == participant.cdParticipant && p.tpParticipant == participant.tpParticipant) p = participant;
                                }
                                // this.conversationService.updateReceptor(participants[0]);
                                resolve(participant);
                            })
                            .catch((error) => {
                                this.logService.logError(error, this.userProvider.getUser());
                                reject(error);
                            });
                    }
                    else {
                        resolve();
                    }
                })
                .catch((error) => {
                    this.logService.logError(error, this.userProvider.getUser());
                    reject(error);
                });
        });
    }

    addParticipantsRelationFor(conversations: Conversation[], bulk :boolean = false): Promise<any> {
        let $this = this;
        return new Promise(async function (resolve){
            console.log("addParticipantsRelationFor count: "+conversations.length);
            if(bulk){
                let entries = [];
                for(let conv of conversations){
                    for(let p of conv.lsParticipants){
                        if(typeof p.id !== 'undefined' && p.id != null){
                            entries.push({
                                conversationId: conv.id,
                                participantId: p.id
                            });
                        }
                    }
                }
                await $this.addParticipantRelationsBulk(entries);
                resolve();
            } else {
                for(let conv of conversations){
                    await $this.addParticipantRelations(conv);
                }
                resolve();
            }
        });
    }

    private addParticipantRelationsBulk(entries: any[]): Promise<any> {
        console.log("addParticipantRelations for all");

        return new Promise((resolve) => {
            if(entries && entries.length > 0){
                this.dbProvider.getConnection()
                .query("INSERT INTO conversation_participant(conversationId, participantId) VALUES "+entries.map((entry)=> "("+entry.conversationId+", "+entry.participantId+")").join(","))
                .then(() => {
                    console.log("final de todo el insert de entries..");
                    resolve();
                })
                .catch((err) => {
                    console.log(err);
                    this.logService.logCritic(err, this.userProvider.getUser());
                    resolve();
                })
            }else{
                resolve();
            }
        });
    }

    private addParticipantRelations(con: Conversation): Promise<any> {
        console.log("addParticipantRelations for: "+con.id);
        let participantIds = [];
        for(let p of con.lsParticipants){
            if(typeof p.id !== 'undefined' && p.id != null){
                participantIds.push(p.id);
            }
        }

        return new Promise((resolve) => {
            this.dbProvider.getConnection().createQueryBuilder()
                .relation(Conversation, 'lsParticipants')
                .of(con.id)
                .loadMany()
                .then((actualRelationships) => {
                    console.log("Primera query for: "+con.id);
                    this.dbProvider.getConnection().createQueryBuilder()
                        .relation(Conversation, 'lsParticipants')
                        .of(con.id)
                        .addAndRemove(participantIds, actualRelationships)
                        .then(() => {
                            console.log("Segunda query for: "+con.id);
                            resolve();
                        })
                        .catch((err) => {
                            this.logService.logCritic(err, this.userProvider.getUser());
                            resolve();
                        })
                })
                .catch((error) => {
                    this.logService.logCritic(error, this.userProvider.getUser());
                    resolve();
                });
        });
    }

    removeParticipantRelationFor(conversation: Conversation, participant: Participant){
        return new Promise((resolve, reject) => {
            this.dbProvider.getConnection().createQueryBuilder()
                .relation(Conversation, 'lsParticipants')
                .of(conversation.id)
                .remove(participant.id)
                .then(() => resolve())
                .catch( error => {
                    this.logService.logError(error, this.userProvider.getUser());
                    reject();
                });
        });
    }

    removeAllParticipantsRelationFor(conversation: Conversation){
        return new Promise((resolve, reject) => {
            if(conversation.lsParticipants){
                let participantIds = conversation.lsParticipants.map(participant => participant.id);
                this.dbProvider.getConnection().createQueryBuilder()
                    .relation(Conversation, 'lsParticipants')
                    .of(conversation.id)
                    .remove(participantIds)
                    .then(() => resolve())
                    .catch( error => {
                        this.logService.logError(error, this.userProvider.getUser());
                        reject();
                    });
            }
        });
    }

    resetStates() {
        for (let p of this.participants) {
            p.cdState = 0;
        }
        for (let p of this.bots) {
            p.cdState = 0;
        }
    }

    updateStateBulk(cdUsers: Array<string>, number: number) {
        if(cdUsers.length > 0){
            return this.dbProvider.getConnection()
                .createQueryBuilder()
                .update(Participant)
                .set({cdState: number})
                .where("userId = :userId AND cdParticipant in ('" + cdUsers.join("','") + "')", {
                    userId: this.userProvider.getUser().id
                })
                .execute();
        }
    }

    /**
    * Resets the participants list.
    * @return {undefined}
    */
    public resetParticipants(): void {
        this.participants = [];
    }

}
