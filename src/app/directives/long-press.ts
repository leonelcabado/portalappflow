import {Directive, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output, NgZone} from '@angular/core';
import {Gesture, GestureController} from '@ionic/angular';

@Directive({
    selector: '[long-press]'
})
export class LongPressDirective implements OnInit, OnDestroy {
    @Input() interval: number;

    @Output() onPressStart: EventEmitter<any> = new EventEmitter();
    @Output() onPressing: EventEmitter<any> = new EventEmitter();
    @Output() onPressEnd: EventEmitter<any> = new EventEmitter();

    el: HTMLElement;
    pressGesture: Gesture;
    pressUpGesture: Gesture;

    int: any;

    constructor(public zone: NgZone,
                el: ElementRef,
                private gestureCtrl: GestureController) {
        this.el = el.nativeElement;
    }

    ngOnInit() {
        if (!this.interval) this.interval = 500;
        if (this.interval < 40) {
            throw new Error('A limit of 40ms is imposed so you don\'t destroy device performance. If you need less than a 40ms interval, please file an issue explaining your use case.');
        }

        this.pressGesture = this.gestureCtrl.create({
            el: this.el,
            gestureName: 'press',
            onMove: e => this.onMoveHandlerPress(e)
        }, true);

        this.pressUpGesture = this.gestureCtrl.create({
            el: this.el,
            gestureName: 'pressup',
            onMove: e => this.onMoveHandlerPressUp(e)
        }, true);

        this.pressGesture.enable();
        this.pressUpGesture.enable();
    }

    onMoveHandlerPress(e: any) {
        this.onPressStart.emit(e);
        this.zone.run(() => {
            this.int = setInterval(() => {
                this.onPressing.emit();
            }, this.interval);
        });
    }

    onMoveHandlerPressUp(e: any) {
        this.zone.run(() => {
            clearInterval(this.int);
        });
        this.onPressEnd.emit();
    }

    ngOnDestroy() {
        this.zone.run(() => {
            clearInterval(this.int);
        });
        this.onPressEnd.emit();
        this.pressGesture.destroy();
        this.pressUpGesture.destroy();
    }

}
