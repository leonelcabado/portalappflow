import {NgModule} from '@angular/core';
import {LongPressDirective} from './long-press';

@NgModule({
    declarations: [LongPressDirective],
    imports: [],
    exports: [LongPressDirective]
})
export class DirectivesModule {
}
