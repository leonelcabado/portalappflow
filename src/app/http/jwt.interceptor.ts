import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, from, BehaviorSubject, throwError } from 'rxjs';
import { catchError, switchMap, filter, take } from 'rxjs/internal/operators';
import { JwtHelper } from '../support-classes/jwt-helper';

//providers
import { AuthProvider } from '../services/auth.service';
import { UserProvider } from '../services/user.service';
import { LogService } from '../services/log.service';


@Injectable()
export class JwtInterceptor implements HttpInterceptor {

    private isRefreshing = false;
    private refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null);

    constructor(private auth: AuthProvider,
                private userProvider: UserProvider,
                private logService: LogService) {
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        try {
            if (this.auth.getToken()) {
                request = this.addToken(request, this.auth.getToken());
            }
            return next.handle(request).pipe(catchError(error => {
                return this.processError(request, next, error);
            }));
        } catch (error) {
            this.logService.logError(error, this.userProvider.getUser());
            console.error(error);
        }
    }

    /**
     * Add the token in the request.
     * @param  {HttpRequest<any>} request
     * @param  {string}           token
     * @return {HttpRequest<any>}
     */
    private addToken(request: HttpRequest<any>, token: string): HttpRequest<any> {
        request = request.clone({
            setHeaders: {
                    'Authorization': `Bearer ${this.auth.getToken()}`,
                    'Accept-Encoding': 'gzip,deflate',
                    'X-Ws-Session': `${this.userProvider.getUserSocketId()}`
                }
        });
        this.logService.logDevelopment(JSON.stringify(request), this.userProvider.getUser());
        return request;
    }

    /**
     * Process the HTTP response error.
     * If it is state 401 asks for the token again.
     * @param  {HttpRequest<any>} request
     * @param  {HttpHandler}      next
     * @param  {any}              error
     * @return {Observable}
     */
    private processError(request: HttpRequest<any>, next: HttpHandler, error: any): Observable<HttpEvent<any>> {
        if (error === 401 && this.userProvider.getUser() && this.userProvider.getUser().getLoggedIn()) {
            return this.handle401Error(request, next);
        }
        return throwError(error);
    }

    /**
     * Process the HTTP 401 error.
     * @param  {HttpRequest<any>} request
     * @param  {HttpHandler}      next
     * @return {Observable}
     */
    private handle401Error(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (!this.isRefreshing) {
            this.isRefreshing = true;
            this.refreshTokenSubject.next(null);
            return this.userProvider.getNewToken().pipe(switchMap((token: any) => {
                this.isRefreshing = false;
                this.refreshTokenSubject.next(this.auth.getToken());
                return next.handle(this.addToken(request, this.auth.getToken()));
            }));
        }
        return this.refreshTokenSubject.pipe(
            filter(token => token != null),
            take(1),
            switchMap((jwt: string) => {
                return next.handle(this.addToken(request, jwt));
            })
        );
    }

}
