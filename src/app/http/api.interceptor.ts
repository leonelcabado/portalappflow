import {Injectable} from '@angular/core';
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor,
    HttpErrorResponse,
    HttpResponse
} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {tap, catchError} from 'rxjs/internal/operators';

//providers
import {UpdateProvider} from '../services/update.service';
import {UserProvider} from '../services/user.service';
import {EVENT_TYPE} from '../model/rtm-event.entity';
import { LogService } from '../services/log.service';


@Injectable()
export class ApiInterceptor implements HttpInterceptor {

    constructor(private updateProvider: UpdateProvider,
                private userProvider: UserProvider,
                private logService: LogService) {
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next
            .handle(request)
            .pipe(
                tap((event) => this.handleResponse(event, request)),
                catchError((err, caught) => this.handleError(err, caught))
            );
    }

    handleResponse(event: HttpEvent<any>, request: HttpRequest<any>) {
        if (event instanceof HttpResponse && event.body.dtLastUpdate && !this.userProvider.inSync) {
            const url = new URL(request.urlWithParams);
            const urlParams = new URLSearchParams(url.search);
            const event_type = urlParams.get('event_type');
            if (Number(event_type) !== EVENT_TYPE.USER_TYPING && Number(event_type) !== EVENT_TYPE.USER_STOP_TYPING) {
                this.logService.logDevelopment('Seteo nuevo dtLastUpdate HttpResponse', this.userProvider.getUser());
                this.updateProvider.dtLastUpdate = event.body.dtLastUpdate;
                this.userProvider.saveUpdate();
            }
        }
    }

    handleError(err: any, caught: Observable<HttpEvent<any>>) {
        if (err instanceof HttpErrorResponse) {
            console.error("ERROR en HttpErrorResponse", JSON.stringify(err));
            if (err.status === 401) {
                return throwError(401);
            }
        }
        return throwError(caught);
    }
}
