#!/usr/bin/env bash
SPINNER_BOTTOM_PADDING=200

# Android
ANDROID_FILE=./platforms/android/app/src/main/java/org/apache/cordova/splashscreen/SplashScreen.java
if [ -f $ANDROID_FILE ]; then
  sed -i -- "s/centeredLayout.setGravity(Gravity.CENTER)/centeredLayout.setGravity(Gravity.CENTER_HORIZONTAL)/g" $ANDROID_FILE
  sed -i -- "s/layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE)/layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE); progressBar.setPadding(0,0,0,$SPINNER_BOTTOM_PADDING)/g" $ANDROID_FILE
fi


# IOS
IOS_FILE=./platforms/ios/deyel_chat/Plugins/cordova-plugin-splashscreen/CDVSplashScreen.m
if [ -f $IOS_FILE ]; then
  sed -i -- "s/_activityView.center = CGPointMake(parentView.bounds.size.width \/ 2, parentView.bounds.size.height \/ 2)/_activityView.center = CGPointMake(parentView.bounds.size.width \/ 2, parentView.bounds.size.height - 25)/g" $IOS_FILE
  sed -i -- "s/UIActivityIndicatorViewStyle topActivityIndicatorStyle = UIActivityIndicatorViewStyleGray/UIActivityIndicatorViewStyle topActivityIndicatorStyle = UIActivityIndicatorViewStyleWhiteLarge/g" $IOS_FILE
fi

exit 0
