INSTALACION DEL PROYECTO

  Para la instalacion respetar el documento https://docs.google.com/document/d/1bCfQdAx6HrO1fo31Vei_oqc28HSNVPu4WM4FsQRRMhw/edit#

  Poseer sistema operativo Linux, Ubuntu >= 16.04 (LTS).

    https://www.profesionalreview.com/2016/06/08/como-instalar-ubuntu-16-04-lts-en-tu-pc-paso-a-paso/
    http://releases.ubuntu.com/16.04/?_ga=2.63668527.1267850669.1536754812-1654072616.1536340749

Desde la terminal. (ctr+alt+t)

1- Hacer por las dudas una actualizacion de paquetes.

    sudo apt-get update
    sudo apt-get upgrade

2- Realizar la instalacion de nodejs, npm, git y adb.

    Para nodejs primero installar curl
      sudo apt-get install -y curl
    Luego ejecutar curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -

    Por último
    sudo apt-get install -y build-essential nodejs npm git adb

    Verificar las versiones de node y npm ejecutando, node -v y npm -v

    node -v --> v10.x
    npm -v  --> v6.x

3- Realizar la instalacion de oracle-java.

    Primero hay que agregar el repositorio del paquete y luego actualizar.

      sudo add-apt-repository ppa:webupd8team/java
      sudo apt-get update

    Luego instalar el paquete

      sudo apt-get install oracle-java8-installer

    Por ultimo configurar el path JAVA_HOME.

      export JAVA_HOME=/usr/lib/jvm/java-8-oracle


4- Realizar la instalacion de ionic y cordova con npm.

    sudo npm install -g ionic@latest cordova@latest

5- Descargar android estudio

    Primero hay que agregar el repositorio del paquete y luego actualizar.
      sudo add-apt-repository ppa:maarten-fonville/android-studio
      sudo apt update

    Instalar el paquete
      sudo apt install -y android-studio

    Buscar la aplicacion y abrirla para que descargue los paquetes necesarios.

    Abrir AndroidStudio y utilizar el SDK Manager para bajar el SDKTools y los SDK necesarios API28.

    Por último configurar el path ANDROID_HOME.
      export ANDROID_SDK_ROOT="/home/<USUARIO>/Android/Sdk"


6- Realizar el clone del proyecto utilizando git.

7- Desde la raiz del proyecto, instalar los componenetes utilizando npm.

    npm install

8- Dede la raiz del proyecto, agregar el platform android

    ionic cordova platform add android@8.0.0

9- Modificar plugin Firebase.

    En el archivo platforms/android/app/src/main/java/org/apache/cordova/firebase/FirebasePluginMessagingService.java

    - reemplazar en la linea 100, 'data.get("id")' por 'data.get("idConversation")'.
    - agregar arriba de la linea 137 'bundle.putBoolean("firebaseNotificationOpen", true);'.
    - agregar entre las lineas linea 154 y 155 '.setGroup("com.optaris.deyel")'.

*** NO VA MAS ***
10- Modificar plugin PhotoViewer.

    En el archivo platforms/android/src/com/sarriaroman/PhotoViewer/PhotoActivity.java, comentar las lineas 156 y 157
*****
11- Modificar color para notificaciones push.

    En el archivo platforms/android/app/src/main/res/values/colors.xml modificar la linea 5 por '<color name="accent">#00b6ee</color>'.

12- Modificar MainActivity.

    En el archivo platforms/android/app/src/main/java/com/optaris/deyel/MainActivity.java modificar la linea 34 por 'if (extras != null && extras.getBoolean("cdvStartInBackground", false) && !extras.getBoolean("firebaseNotificationOpen", true)) {'

13- Modificar AndroidManifest

    En el archivo platforms/android/app/src/main/AndroidManifest.xml agregar al elemento application (linea 5) los atributos 'android:allowBackup="false"' y 'android:fullBackupContent="false"'.
platforms/android/AndroidManifest.xml



-----ANTES DE COMPILAR-----:
-----Asegurarse de tener el celular conectado por usb. Tener habilitado el modo desarrolador. En las notificaciones debería verse, además de "Este dispositivo se está cargando mediante USB", la notificación "Depuración por USB conectada".
-----La red a la que está conectada la computadora y el celular debe ser la misma.
-----Los comandos se corren desde la terminal de Ubuntu (consola).

14- Para compilar como debug, ejecutar desde la raiz del proyecto.

    ionic cordova run android -l -c -s --debug
    Nota: el parámetro "-l" indica "live reload". Es decir que al salvar un cambio en el código debería actualizarse instántaneamente en el disposito.

15- Para compilar como prod, ejecutar desde la raiz del proyecto.

    ionic cordova run android --prod

16- Para compilar release, ejecutar desde la raiz del proyecto.

    ionic cordova build android --prod --release -- -- --keystore=consist.keystore --storePassword=c0ns1st --password=c0ns1st ---alias=consist

    El archivo APK se encontrará en la siguiente ruta, dentro del directorio del proyecto: raizDelProyecto/platforms/android/build/outputs/apk/elArchivo.apk (este dato lo informa la consola al generar el archivo)


17- Modificar la linea 66 del archivo node_modules/ionic-angular/components/toast/toast-component.js

    '<div class="toast-message" id="{{hdrId}}" *ngIf="d.message" >{{d.message}}</div> ' -->  '<div class="toast-message" id="{{hdrId}}" *ngIf="d.message" innerHtml="{{d.message}}"></div> ' +

18- Modificar el archivo node_modules/ionic-angular/components/toast/toast-component.metadata.json

      buscar *ngIf=\"d.message\" >{{d.message}}</div>  y reemplazar por *ngIf=\"d.message\" innerHtml=\"{{d.message}}\"></div>


















1- Modificar plugin Firebase.

    En el archivo platforms/android/app/src/main/java/org/apache/cordova/firebase/FirebasePluginMessagingService.java

    - reemplazar en la linea 84, 'data.get("id")' por 'data.get("idConversation")'.
    - agregar arriba de la linea 121 'bundle.putBoolean("firebaseNotificationOpen", true);'.
    - agregar entre las lineas linea 138 y 139 '.setGroup("com.optaris.deyel")'.

2- Modificar color para notificaciones push.

    En el archivo platforms/android/app/src/main/res/values/colors.xml modificar la linea 5 por '<color name="accent">#00b6ee</color>'.







3- Modificar MainActivity.

    En el archivo platforms/android/app/src/main/java/com/optaris/deyel/MainActivity.java modificar la linea 34 por 'if (extras != null && extras.getBoolean("cdvStartInBackground", false) && !extras.getBoolean("firebaseNotificationOpen", true)) {'

4- Modificar AndroidManifest

    En el archivo platforms/android/app/src/main/AndroidManifest.xml agregar al elemento application (linea 5) los atributos 'android:allowBackup="false"' y 'android:fullBackupContent="false"'.
platforms/android/AndroidManifest.xml